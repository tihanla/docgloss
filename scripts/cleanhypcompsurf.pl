use strict;

#this script creates two morphological variaant from hypehneted lines (with and without hphen), and lex for without hyphen
#deletes linesstarting or ending with @-@  as these were created by the break of the hyphented word into three 
#warning there might be sligh overgeneretin since we do not delete segments were the second word of the hyphcomp is the first word of the pahrase
#in
#, service and digital content for	, services and digital content for
#cross @-@ border	cross @-@ border
#@-@ border	@-@ border
#out
#, service and digital content for	, services and digital content for
#cross border	cross-border
#cross border	cross border


my $lex;
my $srf;
my $srf1;
my $srf2;



while(<>)
	{
	s/([^\t]+)\t([^\n]+)/$1\t$2/;
	$lex=$1;
	$srf=$2;
	if($lex =~ s/^\@-\@//g)
		{
		; #do not print this extra line
		}
	elsif($lex =~ s/\@-\@$//g)
		{
		; #do not print this extra line
		}
	elsif ($lex =~ s/\@-\@[ ]?//g)
		{
		#hypenated variant:
		$srf1=$srf;
		$srf1 =~ s/[ ]?\@-\@[ ]?/-/g;
		print "$lex\t$srf1\n";

		#non-hyphen variant
		$srf2=$srf;
		$srf2 =~ s/[ ]?\@-\@[ ]?/ /g;

		print "$lex\t$srf2\n";
		}
	else
		{
		print $_;
		}
	}