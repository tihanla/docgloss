#creates hash from target ids and writes out  source and target lines tht has this id
#created by modification of getidlangline.pl

use strict;
#prints out the columns of table in the specified order

my $order;
my $table;
my $ordersize;
my $insize;
my @inarray;
my @termarray;
my @lookuparray;

my @array;
my $arrsize;

my $argc;
my $in;
my $indb;
my $dbfile;
my $orderi;
#my $srclang;
 #my $lang;	#trglang
my $langcol;
my $idcol;
my %idhash;
my $idcol;
my $langcol;
my $srclang;
my $lookupcol;
my $termcol;
my $primcol;
my $instcol;

my $termsize;
my $lookupsize;
my $commasize;
my $semicolonsize;

my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 9)
	{
	print  "Usage:\n";
	print  "perl getidlangline src_lang idcol langcol termcol lookupcol primcol instcol input output \n";
	#perl %script_path%\getidlangline.pl  %idcol%  %work_path%\found-src.txt  %langcol%  %src% %trg% "%dicinput_path%\%dicinput%" "%work_path%\target.tsv"
	exit ;
	}

$srclang =  $ARGV[0];
$idcol =  $ARGV[1];
$langcol =  $ARGV[2];
$termcol =  $ARGV[3];
$lookupcol =  $ARGV[4];
$primcol =  $ARGV[5];
$instcol =  $ARGV[6];
open(my $dbfile, "<", $ARGV[7] ) || die "can't open $ARGV[7]";
open(my $outfile, ">", $ARGV[8] ) || die "can't open $ARGV[8]";



#skip header line:
$indb = <$dbfile>;

print $outfile "id\tlang\tterm\tlookup\tprimary\tinstitution\ttermsize\tlookupsize\tcommasize\tsemicolonsize\tcomma-semisize\n";

while ($indb = <$dbfile>)
	{
	#DEBUG:
	#print  "count=$count, $indb";

	@inarray = split ('\t', $indb, -1);
	$insize=@inarray;

	if ( ($inarray[$langcol-1] eq $srclang ) && ($inarray[$lookupcol-1] ne "" ))
		{
		@termarray = split (' ', $inarray[$termcol-1], -1);
		$termsize=@termarray;

		@lookuparray = split (' ', $inarray[$lookupcol-1], -1);
		$lookupsize=@lookuparray;

		@array = split (',', $inarray[$lookupcol-1], -1);
		$commasize=@array;

		@array = split (';', $inarray[$lookupcol-1], -1);
		$semicolonsize=@array;

		my $commasemisize=$commasize+$semicolonsize-1;
		
		if ($commasize > 1)
			{
			$commasize = "+";
			}
		else
			{
			$commasize = "";
			}

		

		if ($semicolonsize > 1)
			{
			$semicolonsize = "+";
			}
		else
			{
			$semicolonsize = "";
			}

		
		
		print $outfile "$inarray[$idcol-1]\t$inarray[$langcol-1]\t$inarray[$termcol-1]\t$inarray[$lookupcol-1]\t$inarray[$primcol-1]\t$inarray[$instcol-1]";
		print $outfile "\t$termsize\t$lookupsize\t$commasize\t$semicolonsize\t$commasemisize";
		print $outfile "\n";
		}
	}	
close $dbfile;
close $outfile;
