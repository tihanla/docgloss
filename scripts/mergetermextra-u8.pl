
#mergeterms infile1 N infile2 M outfile
#lines of infile2 (together with their info) is written into outfile if its 1st field occurs in infile1 (plain list)
use strict;

my $in;
my $keyfield;
my $valfield;
my $cnt1;
my $cnt1;
my $i;
my $in1head;
my $in2head;

my $cnt2;
my $cnth1;
my $found;
my $idkey;
my $idkey_lc;
my $idkeyfield;
my $idval;
my $idval_lc;
my $in1field;
my $in2field;
my $index;                                                                                                                                           
my %hash_src;
my %hash_src_lc;


open(my $infile1, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
binmode($infile1);

$in1field =  $ARGV[1];

open(my $infile2, "<", $ARGV[2] ) || die "can't open $ARGV[2]";
binmode($infile2);

$in2field =  $ARGV[3];

#open ($outfile,  ">", $ARGV[1]);
open(my $outfile_found, ">", $ARGV[4] ) || die "can't open $ARGV[4]";
binmode($outfile_found, ":raw");

open(my $outfile_notfound, ">", $ARGV[5] ) || die "can't open $ARGV[5]";
binmode($outfile_notfound, ":raw");


#print "infile1=$infile1\n";
#print "in1field=$in1field\n";
#print "infile1=$infile2\n";
#:print "in2field=$in2field\n";



#open(my $logfile, ">", "logfile.txt" ) || die "can't open logfile.txt";
#binmode($logfile, ":raw:encoding(UTF-16)");

$in2head = <$infile2>;
$in2head =~ s/\r\n//g;
$in2head =~ s/\n//g;
#print "in2hed=$in2head,";

#print "indexing started: in2field=$in2field\n";


#create database from file2
while ($in = <$infile2>)
	{
	#chomp;
	$in =~ s/\r\n//g;
	$in =~ s/\n//g;

	my @inarray = split ('\t', $in, -1);

	$cnt2=1;

	$idval="";
	foreach my $field (@inarray) 
		{
		if($cnt2 == $in2field)
			{
			#$idkeyfield=chomp($field);
			$idkeyfield=$field;

			#print "idkeyfield=$idkeyfield\n";
			#obsolate: $idval=$in;
			}
		else
			{
			if($idval eq "")
				{
				$idval = $field;
				}
			else
				{
				$idval = $idval . "\t" . $field;
				}	
			#print "idval=$idval\n";
			}

		$cnt2++;  
		}

	#print "cnt2=$cnt2\n";

	if ($in2field >= $cnt2 )
		{
		printf ("db contains only cnt2=$cnt2 fields (< in2field=$in2field)\n");
		exit;
		}

	#print "idkey=$idkeyfield\n";

	#$index=0;
	#$idkey = $idkeyfield . "-"  .  $index;
	#find the first non existing 
	for ($index=0;;)
		{
		$idkey = $idkeyfield . "-" .  $index;
		$idkey_lc=lc($idkey);

		#if(exists  $hash_src_lc{$idkey_lc})
		if(exists  $hash_src{$idkey_lc})
			{
			$index++;
			}
		else
			{
			$hash_src{$idkey_lc} = $idval; 
			#$hash_src_lc{$idkey_lc} = $idval_lc; 
			
			#print "added: $lang: hash_src{$idkey} = $hash_src{$idkey} for $idval\n: ";
			#print $hash_src{$idkey};  #!
			last;
			}
		}
        }


$cnt2--;

=pod
#test
my @test_hashsrc;
my $test_hashsrc_size;
my $i;
@test_hashsrc = keys %hash_src;
$test_hashsrc_size=@test_hashsrc;
for($i=0; $i != $test_hashsrc_size; $i++) {print "i=$i: $test_hashsrc[$i]\n";}
=cut

#print "indexing finished\n";

#lookup file1 lines in file2 database:
$in1head = <$infile1>;
$in1head =~ s/\r\n//g;
$in1head =~ s/\n//g;
print $outfile_found "$in1head";
print $outfile_notfound "$in1head";


my @inarray = split ('\t', $in2head, -1);

$cnth1=1;
foreach my $field (@inarray) 
	{
	if($cnth1 != $in2field)
		{
		#print "cnth1=$cnth1, in1field=$in1field, field=$field\n";
		print $outfile_found "\t$field";
		print $outfile_notfound "\t$field";
		}
	$cnth1++;
	}

print $outfile_found "\n";
print $outfile_notfound "\n";

#exit;

while ($in = <$infile1>)
	{
	#chomp($in);
	$in =~ s/\r\n//g;
	$in =~ s/\n//g;
	my @inarray = split ('\t', $in, -1);

	$cnt1=1;

	foreach my $field (@inarray) 
		{
		if($cnt1 == $in1field)
			{
			#$idkeyfield=chomp($field);
			$idkeyfield=$field;
			#print "idkey=$idkey\n";
			#$idval=$in;
			}

		$cnt1++;  
		}

	if ($in1field > $cnt1 )
		{
		printf ("db contains only cnt1=$cnt1 fields (< in1field=$in1field)\n");
		exit;
		}

	$found=0;
	$idkey = $idkeyfield . "-" .  $index;
	$idkey_lc=lc($idkey);
	#print "idkey_lc=$idkey_lc\n";
	if(exists  $hash_src{$idkey_lc})
		{
		print $outfile_found "$in\n";
		$found=1;
		}

		
	if($found==0)
		{
		print $outfile_notfound "$in\n";
		#print $outfile "$idkeyfield\n";
		}


	}
