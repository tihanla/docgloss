#!/usr/bin/perl

use strict;
use warnings;
#use utf8;

use XML::LibXML;

my $filename = "data.xml";
#open (INFILE,  "<", $ARGV[0]) || die "file not found: $ARGV[0]\n", $!;
open (my $outfile, ">:encoding(utf8)", $ARGV[1])|| die "file not found: $ARGV[1]\n", $!;

my $argc=@ARGV;

if ($argc ne 2) 
	{
	print "Usage: input output\n";
	exit;
	}


print $outfile "<?xml version='1.0'?>\n";
print $outfile "<!DOCTYPE martif SYSTEM \"http:\/\/www.ttt.org\/oscarStandards\/tbx\/TBXcoreStructV02.dtd\">\n";
print $outfile "<martif type=\"TBX\" xml:lang=\"en\"><martifHeader><fileDesc></fileDesc></martifHeader>\n";
print $outfile "<text>\n";
print $outfile "<body>\n";



my $parser = XML::LibXML->new();
my $xmldoc = $parser->parse_file($ARGV[0]);

#entry level
for my $entry ( $xmldoc->findnodes('/martif/text/body/termEntry') ) 
	{
	my $type_value   = $entry->findvalue('./@id');
        print $outfile "<termEntry id=\"", $type_value, "\">\n";

	#language  lavel
	my $ei=1;
	my $entrychild =  $entry->getFirstChild();
        while ($entrychild = $entrychild->nextSibling())
		{

		if ($entrychild->nodeName() eq "descrip")
			{
			my $type_value   = $entrychild->findvalue('./@type');
		        print $outfile "\t<descrip type=\"", $type_value, "\">";
		        #print $outfile "\t<descrip>";
			print $outfile $entrychild->textContent();
		        print $outfile "</descrip>\n";
	                }


		if ($entrychild->nodeName() eq "langSet")
			{
			my $type_value   = $entrychild->findvalue('./@xml:lang');
		        print $outfile "\t<langSet xml:lang=\"", $type_value, "\">\n";
		        #print $outfile "\t<langSet>\n";
			my $li=1;
			my $langset =  $entrychild->getFirstChild();
	        	while ($langset = $langset->nextSibling())
				{
			        #print $outfile "term->nodeName=<", $term->nodeName(), ">\n";
				#print $outfile "li=$li\n"; $li++;

				if ($langset->nodeName() eq "descrip")
					{
					my $type_value   = $langset->findvalue('./@type');
				        print $outfile "\t\t<descrip type=\"", $type_value, "\">";
					print $outfile $langset->textContent();
				        print $outfile "</descrip>\n";
	                		}
				elsif ($langset->nodeName() eq "tig")
					{
				        print $outfile "\t\t<tig>\n";
			        	#print $outfile $langset->textContent(), "";
					my $tigset =  $langset->getFirstChild();
	        			while ($tigset = $tigset->nextSibling())  #term, termNote
						{
						if($tigset->nodeName() eq "term")		
							{
							print $outfile "\t\t\t<term>";
							print $outfile $tigset->textContent();
							print $outfile "</term>\n";
							}
						if($tigset->nodeName() eq "termNote")		
							{

							my $type_value   = $tigset->findvalue('./@type');
						        print $outfile "\t\t\t<termNote type=\"", $type_value, "\">";
							#print $outfile "\t\t\t<termNote>";
							print $outfile $tigset->textContent();
							print $outfile "</termNote>\n";
							}
						}

				        print $outfile "\t\t</tig>\n";
				        }
			 	}
		        print $outfile "\t</langSet>\n";
			}
		}
        print $outfile "</termEntry>\n";

	}

print $outfile "</body>\n";
print $outfile "</text>\n";
print $outfile "</martif>\n";



