#creates hash from target ids and writes out  source and target lines tht has this id
#filters out non-phrasal terms (words) 
#created by modification of getidlangline.pl


use strict;
#prints out the columns of table in the specified order

my $order;
my $table;
my $ordersize;
my $insize;
my @inarray;
my $argc;
my $in;
my $indb;
my $dbfile;
my $orderi;
#my $srclang;
 #my $lang;	#trglang
my $langcol;
my $idcol;
my $termcol;
my %idhash;
my %termhash;
my %src_trg_hash;
my %srchash;
my $idcol;
my $langcoldb;
my $srclang;
my $trglang;
my $stcount=0;
my $val;

my @meancnt;
my $lastid;
my $srccnt=0;
my $trgcnt=0;
my $cntmax;
my $i;
my $multitarget=0;
my $duplum=0;
my $key;
my $index;
my $idkey;
my $index;
my %used_src_hash;
my %src_trg_hash;

my $phrsize;
my @phrarray;

my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 5)
	{
	print  "Usage:\n";
	print  "perl getidlangline src trg db_input output log\n";
	#perl %script_path%\getidlangline.pl  %idcol%  %work_path%\found-src.txt  %langcol%  %src% %trg% "%dicinput_path%\%dicinput%" "%work_path%\target.tsv"
	exit ;
	}

$idcol =  0;
$langcol = 1;
$termcol =2;

$srclang =  $ARGV[0];
$trglang =  $ARGV[1];
open(my $dbfile, "<", $ARGV[2] ) || die "can't open $ARGV[2]";
open(my $outfile, ">", $ARGV[3] ) || die "can't open $ARGV[3]";
open(my $logfile, ">", $ARGV[4] ) || die "can't open $ARGV[4]";


#print "source_langdb=$srclang, target_lang=$trglang\n";
#print "idcol=$idcol, lancoldb=$langcol\n";

#exit;

#print "order=$order, ";
#print "ordersize=$ordersize\n";

my $count =1;

$count=1;
$indb = <$dbfile>;    #header
$count++;
#print $outfile  "$indb";

#1. create termhash 
while ($in = <$dbfile>)
	{
	#print  $in;   DEBUG
	#next;
	chomp($in);
	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	#print "$langcol= $langcol, inarray[$langcol]=$inarray[$langcol],  trglang=$trglang\n";

	if($inarray[$langcol] eq $srclang)
		{
		for ($index=0;;)
			{
			$idkey =$inarray[$idcol] . "-" .  $index;
			#print "idkey=$idkey\n";
			if(exists  $termhash{$idkey})
				{
				$index++;
				}
			else
				{
				$termhash{$idkey}=  $inarray[$termcol];
				#$used_src_hash{$inarray[$termcol]}=0;
				last;
				}
			}
		}

	#print src terms
	#OLD print $outfile  "$in\n";
	
	$count++;
	}	

$cntmax=$count;

#reset input, we read it again
close $dbfile;
open(my $dbfile, "<", $ARGV[2] ) || die "can't open $ARGV[2]";

#print "$count added\n";  #DEBUG

#print header line:

$lastid=0;
$indb = <$dbfile>;  #header
$count=1;

#count translation of termhash and store in values
while ($indb = <$dbfile>)
	{
	#DEBUG:
	#print  "count=$count, $indb";
	chomp($indb);

	@inarray = split ('\t', $indb, -1);
	$insize=@inarray;

	#if target line
	if( $inarray[$langcol] eq $trglang )
		{
		#print "inarray[$langcol]=$inarray[$langcol]\n"	;
		#find sourse terms with this target id


		for($index=0; ;$index++)
			{
			$idkey =$inarray[$idcol] . "-" .  $index;
			#print "idkey=$idkey\n";
			if(exists  $termhash{$idkey})
		        	{
				$val=$used_src_hash{ $termhash{$idkey} };
				$val++;
				$used_src_hash{ $termhash{$idkey} }=$val;
				$stcount++;
				$index++;
				}
			else
				{
				last;
				}
			}
		}
	$count++;
	#print "$count\n";
	}	


close $dbfile;
open(my $dbfile, "<", $ARGV[2] ) || die "can't open $ARGV[2]";

#print "$count added\n";  #DEBUG

#print header line:

$indb = <$dbfile>;  #header
$count=1;

# write out results if the source has only one translation

while ($indb = <$dbfile>)
	{
	#DEBUG:
	#print  "count=$count, $indb";
	chomp($indb);

	@inarray = split ('\t', $indb, -1);       
	$insize=@inarray;

	#if target line
	if( $inarray[$langcol] eq $trglang )
		{
		#print "inarray[$langcol]=$inarray[$langcol]\n"	;
		#find sourse terms with this target id


		for($index=0; ;$index++)
			{
			$idkey =$inarray[$idcol] . "-" .  $index;
			#print "idkey=$idkey\n";
			if(exists  $termhash{$idkey})
		        	{
				$val=$used_src_hash{ $termhash{$idkey} };
				if($val==1)
					{
					@phrarray = split (' ', $termhash{$idkey}, -1);
					$phrsize=@phrarray;
					if($phrsize == 1)
						{
						print $logfile "len=$phrsize\t$termhash{$idkey}\t$inarray[$termcol]\n";
						}
					else
						{	
						print $outfile "$termhash{$idkey}\t$inarray[$termcol]\n";
						}
					#$key = $termhash{$idkey} . "\t". $inarray[$termcol];
					}
				else
					{
					print $logfile "trg=$val\t$termhash{$idkey}\t$inarray[$termcol]\n";
					}
				$index++;
				}
			else
				{
				last;
				}
			}
		}
	$count++;
	#print "$count\n";
	}	



close $dbfile;
close $outfile;
