use strict;
use warnings;


my $line;
my $infile;
my $outfile;

if($ARGV[0] eq "")
	{
	print "Usage: input.txt output.txt\n";
	
	exit 1;
	}                  

open($infile, "<:encoding(UTF-16)", $ARGV[0] ) || die "can't open $ARGV[0]";
open($outfile, ">:raw:utf8", $ARGV[1] ) || die "can't open $ARGV[1]";

my $count = 0;
while ($line=<$infile>)
	{
	print $outfile $line;
	$count++ ;
	}
close $infile;  
close $outfile;  

                                                                                       