=pod

<?xml version='1.0'?>
<!DOCTYPE martif SYSTEM "TBXcoreStructV02.dtd">
<martif type="TBX" xml:lang="en">

<martifHeader>
<fileDesc>
<titleStmt>
	<title>IATE extraction</title>
</titleStmt>
<sourceDesc>
	<p>This termbase has been distributed by the Terminology Coordination Unit of the Commission</p>
	<p>DTD source: http://www.ttt.org/oscarStandards/tbx/TBXcoreStructV02.dtd</p>
</sourceDesc>
</fileDesc>
</martifHeader>

<text>
<body>

<termEntry>
<descrip type="IATE-ID">3508767</descrip>
<descrip type="Domains">SCIENCE</descrip>
<descrip type="Primary">Yes</descrip>
<langSet xml:lang="en">
	<tig>
		<term>sequential testing strategy</term>
		<descrip type="Term type">Term</descrip>
		<descrip type="Reliability">Reliable</descrip>
		<descrip type="Term reference">Commission Regulation (EC) No 761/2009 amending, for the purpose of its adaptation to technical progress, Regulation (EC) No 440/2008 laying down test methods pursuant to Regulation (EC) No 1907/2006 of the European Parliament and of the Council on the Registration, Evaluation, Authorisation and Restriction of Chemicals (REACH) CELEX:32009R0761</descrip>
		<descrip type="Institution">COM</descrip>
	</tig>
	<tig>
		<term>sequential testing</term>
		<descrip type="Term type">Term</descrip>
		<descrip type="Reliability">Reliable</descrip>
		<descrip type="Term reference">Commission Regulation (EC) No 440/2008 laying down test methods pursuant to Regulation (EC) No 1907/2006 of the European Parliament and of the Council on the Registration, Evaluation, Authorisation and Restriction of Chemicals (REACH) CELEX:32008R0440/EN</descrip>
		<descrip type="Context">In the preparation of this updated method special attention was given to possible improvements in relation to animal welfare concerns and to the evaluation of all existing information on the test substance in order to avoid unnecessary testing in laboratory animals. This method includes the recommendation that prior to undertaking the described in vivo test for corrosion/irritation of the substance, a weight-of-the-evidence analysis be performed on the existing relevant data. Where insufficient data are available, they can be developed through application of &lt;b&gt;sequential testing&lt;/b&gt; (1).</descrip>
		<descrip type="Context reference">Commission Regulation (EC) No 440/2008 laying down test methods pursuant to Regulation (EC) No 1907/2006 of the European Parliament and of the Council on the Registration, Evaluation, Authorisation and Restriction of Chemicals (REACH) CELEX:32008R0440/EN</descrip>
		<descrip type="Institution">COM</descrip>
	</tig>
</langSet>
<langSet xml:lang="mt">
	<descrip type="Definition">proefiniti</descrip>
	<tig>
		<term>test sekwenzjali</term>
		<descrip type="Term type">Term</descrip>
	</tig>
</langSet>
</termEntry>

=cut

my $id = 0;   #do not print IATE ID its spoils readibilty in OmegaT

#write header:
while (<>)
	{
	print $_;
	if (/<body>/)
		{
		print "\n";
		last;
		}
	}


while (<>)
	{
	if (/^<termEntry>/)
		{
		print $_;
		}
	if (/^<\/termEntry>/)
		{
		print "$_\n";
		}
	elsif (/^<\/?langSet/)
		{
		print $_;
		}
	elsif (s/^\t*(<\/?tig>.*)/$1/)
		{
		print $_;
		}
	elsif (s/^\t*(<term>.*)/$1/)
		{
		print "$1\n";
		}

	if (($id == 1) && (s/^\t*(<descrip type="IATE-ID">.*)/$1/))
		{
		print "$1\n";
		}



	}

#write footer
print "</body>\n";
print "</text>\n";
print "</martif>\n";

            

