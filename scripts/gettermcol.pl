
use strict;
#prints out the columns of table in the specified order

my $order;
my $table;
my $ordersize;
my $insize;
my @inarray;
my $argc;
my $in;
my $orderi;
my $lang;
my $langcol;
my $termcol;

my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 5)
	{
	print  "Usage:\n";
	print  "perl iatepairs.pl lang langcol termcol input output \n";
	print  "columns eg: 5-1-2-3\n";
	exit ;
	}

$lang =  $ARGV[0];
$langcol =  $ARGV[1];
$termcol =  $ARGV[2];

#print $lang;
#exit;

open(my $infile, "<", $ARGV[3] ) || die "can't open $ARGV[4]";
#binmode($infile, ":encoding(UTF-16LE)");


open(my $outfile, ">", $ARGV[4] ) || die "can't open $ARGV[5]";
binmode($outfile, ":raw");

#print "order=$order, ";
#print "ordersize=$ordersize\n";

my $count =1;

while ($in = <$infile>)
	{
	#print $outfile  $in;
	#next;
#	if($count == 1)
#		{
#		$count++;
#		next;  #header
#		}

	chomp($in);
	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	if($inarray[$langcol-1] eq $lang)
		{
		#we clean-up XML tags in the dictionary (whixh is not done by the MT@EC Normalizer, since expects plain text)
		$inarray[$termcol-1] =~ s/<\/?[a-z]*\>//gi;		

		#possible way of development:
		#dictionary normalization, could be refined by generating an alternative  with the content, too 
                #howerer we need only one canonical form now! 
		#if($lang ne "en")  #in English we need to be precice (with incomplete coverage)
		#	{
		#	$inarray[$termcol-1] =~ s/\([^\)]*\)//gi;  
		#	$inarray[$termcol-1] =~ s/[ ]+/ /g;
		#	$inarray[$termcol-1] =~ s/^ //g;
		#	$inarray[$termcol-1] =~ s/ $//g;
		#	}
		

		print $outfile "$inarray[$termcol-1]";
		print $outfile  "\n";
		}
	else
		{
		print $outfile  "\n";
		#DEBUG
		#print $outfile "$inarray[$langcol-1]\n";
		}
		
	$count++;
	}	

close $infile;
close $outfile;
