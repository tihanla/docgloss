use strict;
#!/usr/bin/perl 
#compare two files using perl hash

=pod
in: file1=stemmed text     
in: file2=src keys of stemmed dict:  
in: file2_dic          
out:FOUND_IN_DICT              	            
out:NOT_FOUND_IN_DIC                     
out:FOUND_DIC_TERMS                             
optional part:
in: stem,lookupform keys
in: termcol to for lookupforms 
LOG
=cut
my $argc=@ARGV;

if ( $argc < 7)
{
#                         1                2    3             4                 5                6               7       8
print "Usage: stemmed_txt stemmed_src_dict dict found_in_dict not_found_in_dict found_dict_terms [surf-stem_keys idcol langcol primarycol termcol lookup_dict_terms]\n";
}

my $file1;
my $file2;
my $file2_dic;

my $found;
my $miss;
my $found_dic;
my $found_lookupdic;
my %hash;
my $arrsize;

my $srfstm;
my %stmsrf_hash;
my $index;
my $key;

my @srfstmarr;
my $idcol;
my $langcol;
my $termcol;
my $primarycol;

my @dictarr;
my $i;



open ($file1, "<", $ARGV[0])|| die "can't open $ARGV[0]";
open ($file2, "<", $ARGV[1])|| die "can't open $ARGV[1]";
open ($file2_dic, "<", $ARGV[2])|| die "can't open $ARGV[2]";

open ($found, ">", $ARGV[3])|| die "can't open $ARGV[3]";
open ($miss,  ">", $ARGV[4])|| die "can't open $ARGV[4]";
open ($found_dic, ">", $ARGV[5])|| die "can't open $ARGV[5]";

if ($argc > 6)
	{
	open ($srfstm, "<", $ARGV[6])|| die "can't open $ARGV[6]";
	$idcol  =$ARGV[7]-1;    #array indexed from 0
	$langcol=$ARGV[8]-1;    #array indexed from 0
	$primarycol=$ARGV[9]-1;    #array indexed from 0
	$termcol=$ARGV[10]-1;    #array indexed from 0
	open ($found_lookupdic, ">", $ARGV[11])|| die "can't open $ARGV[11]";
	}

#print "idcol=$idcol, langcol=$langcol, primarycol=$primarycol, termcol=$termcol, lookup_dic=$ARGV[11]\n";
#print "ARGV=$argc\n";

#if we have optional surface forms 
if ($srfstm)
	{
	while (<$srfstm>)
		{
		 chomp();
		#get surf and stem  from  stem<TAB>surf
		@srfstmarr=split('\t', $_, -1);



		for ($index=0;;)
			{
			$key = $srfstmarr[0] . "-" .  $index;
			if(exists  $stmsrf_hash{$key})
				{
				$index++;
				}
			else
				{
				#we do not need identical forms:
				if($srfstmarr[0] ne $srfstmarr[1])  
				#SDL Trados Studio specific solution: we do not need forms differring only in capitalization:
				#if(lc($srfstmarr[0]) ne lc($srfstmarr[1]))  
					{
					$stmsrf_hash{$key} = $srfstmarr[1];
					}
				last;
				}
			}
		}
	}

#test hash:



#build hash from lemmatized corpus phrase liste
my $cnt=0;
while (<$file1>)
{
 chomp();
 $hash{$_} = 0; 
 $cnt++;
# print "txt: $_\n";   #DEBUG

}


print "$cnt text phrase to be checked\n";


#search file 2 in hash
$cnt=0;
my $cntfound=0;
my $cntmiss=0;
my $cntskip=0;
my $cntlookupform=0;

while (<$file2>)
{
my $dicline = <$file2_dic>;

 # Hash search here
 chomp();
 if($_ eq "")
	{
	print($found "\n");
	$cntskip++;
	next;
	}

#if stemmed dictionary word/phrase  can be found in stemmed text phrases (sar)  
#(text surf) REACH -> (txt stem) reach = (dict stem) reach -> REACH (dict term)
#when we delete REACH->reach from text and iterate on stemmed dict we simple not include REACH into the dict, 
#so it won't be able to find by the Studio even search in case insensitive

if (exists($hash{$_})) 
   {
   print($found "$_\n");
	
   #print "found: $_\n";   #DEBUG	

   chomp($dicline);
   print $found_dic $dicline;
   #old:   if ($srfstm) 	{print $found_dic "\t";}    #print an empty field into the found dic if lookupforms will be added
   print $found_dic "\n";    

   $cntfound++;




   for ($index=0;;)    #iterate on surface forms occured in text
	{
	$key = $_ . "-" . $index;

	if($stmsrf_hash{$key})
		{
		#replace term col with $stmsrf_hash{$_}
		@dictarr=split('\t', $dicline, -1);
		$arrsize=@dictarr;
		for($i=0;$i != $arrsize; $i++)
			{                                        
			if($i == $idcol)
				{
				print $found_lookupdic "$dictarr[$i]\t";  
				}
			elsif($i == $langcol)
				{
				print $found_lookupdic "$dictarr[$i]\t";  
				}
			elsif($i == $primarycol)
				{
				print $found_lookupdic "$dictarr[$i]\t";  
				}
			elsif($i == $termcol)
				{
				print $found_lookupdic "$stmsrf_hash{$key}\t"
				}
			else
				{
				print $found_lookupdic "\t";  
				}
			}
		print $found_lookupdic "$dictarr[$termcol]\n";
		$cntlookupform++;
		}
	else
		{
		last;
		}
	$index++;
 	}
   } 
 else 
   {
   print($miss "$_\n");
   print($found "\n");
   #print "=>$_<=\n";
   $cntmiss++;
   }

 $cnt++;
}
print "term on other lang=$cntskip, term with matching src lang=$cnt,added=$cntfound, missing=$cntmiss, lookupforms added=$cntlookupform\n";

