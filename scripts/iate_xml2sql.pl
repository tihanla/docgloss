=pod

#all elements in sdltb template:  (many not used)
#Problem language, Management, Historical, Primary, Origin, +Domain, +Domain note, +IATE-ID, Collection, +English/Hungarian, Related material, +Definition, +Definition reference
#Definition note, +TERM, +Term reference, Term type, Reliability, +Institution ID, Pre-IATE, Evaluation, Term note, Context, Context-reference, Regional usage, Language usage

#header of DGT SQL
#+               +               +                 +             +        +            +                   +              -           +       +               +
#INSTITUTION_ID	LIL_RECORD_ID	TL_RECORD_ID	LANGUAGE_CODE	TERM	TERM_REF	RELIABILITY_VALUE	TERM_EVALUATION	TERM_TYPE	NOTE	SUBJECT_DOMAIN	DEFINITION

#header of DGT SQL
#+               +               +                 +             +        +            +                   +              -           +       +               +
#TL_RECORD_ID SUBJECT_DOMAIN SUBJECT_DOMAIN_NOTE	LIL_RECORD_ID	PRIMARY		LANGUAGE_CODE	DEFINITION	DEFINITION_REFERENCE	RELATED_MATREIAL	TERM	TERM_REF	RELIABILITY_VALUE	CONTEXT	CONTEXT_REFERNCE	TERM_EVALUATION	PRE_IATE	NOTE		INSTITUTION_ID


<conceptGrp>						TL_RECORD_ID (count)

<descrip type="Domain">				SUBJECT_DOMAIN
<descrip type="Domain note">		NEW: SUBJECT_DOMAIN_NOTE
<xref Ulink="https://iate.cdt.europa.eu/iatenew/manipulation/dataentry/EntryDetailview.jsp?lilId=998187&amp;srcLang=en">	LIL_RECORD_ID
<descrip type="Primary">			NEW: PRIMARY

<languageGrp>
<language lang="HU" type="Hungarian" /> LANGUAGE_CODE
<language lang="EN" type="English" />	
<descrip type="Definition">				DEFINITION
<descrip type="Definition reference">	NEW: DEFINITION_REFERENCE
<descrip type="Related material">		NEW: RELATED_MATERIAL

<termGrp>
<term>								TERM
<descrip type="Term reference">		TERM_REF
<descrip type="Reliability">		RELIABILITY_VALUE
<descrip type="Context">			NEW: CONTEXT
<descrip type="Context reference">	NEW: CONTEXT_REFERENCE
<descrip type="Evaluation">			TERM_EVALUATION
<descrip type="Pre-IATE">			NEW: PRE_IATE
<descrip type="Term note">			NOTE
<descrip type="Institution ID">		INSTITUTION_ID
</termGrp>

</languageGrp>
</conceptGrp>

UNUSED ELEMENTS:
-<descrip type="IATE ID">
-<concept>
</descrip>
<descrip type="Domain" />
<descrip type="Definition" />
<descrip type="Evaluation" />
<descrip type="Related material" />
<descrip type="Definition reference" />

=cut

=pod
unknown: <transacGrp>
unknown: <languageGrp>	 
unknown: <descripGrp>	 
unknown: <termGrp>	 
unknown: <descrip type="Definition"/>
unknown: <descrip type="Evaluation"/>
unknown: <descrip type="Term ref">	http://europa.eu/smartapi/cgi/sga_doc?smartapi!celexplus!prod!CELEXnumdoc&amp;lg=hu&amp;numdoc=31981R2143
=cut



if($ARGV[0] eq "")
	{
	print "Usage: input.txt output.txt config\n";
	
	exit 1;
	}                  


open(my $infile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
#binmode($infile, ":encoding(UTF-16)");

open(my $outfile, ">", $ARGV[1] ) || die "can't open $ARGV[1]";
#binmode($outfile, ":raw:encoding(UTF-16LE)");
my $errfilename =  $ARGV[0] . ".err"; 
open(my $errfile, ">", $errfilename ) || die "can't open $errfilename";

my $config=$ARGV[2];  # accepeted values: itaetbx; iateadmin;


my $SUBJECT_DOMAIN;       
my $SUBJECT_DOMAIN_NOTE;  
my $PRIMARY;              
my $DEFINITION;           
my $DEFINITION_REFERENCE; 
my $RELATED_MATERIAL;     
my $TERM;                 
my $TERM_REF;             
my $RELIABILITY_VALUE;      
my $CONTEXT;              
my $CONTEXT_REFERENCE;    
my $TERM_EVALUATION;      
my $PRE_IATE;             
my $NOTE;                 
my $INSTITUTION_ID;       
my $LANGUAGE_CODE;
my $TERM_TYPE;

my $termformat;

#OLD print $outfile "DOMAIN\tIATE-ID\t$srcuc\tDEFINITION\tRELIABILITY\tEVALUATION\tCOMMENT\tINSTITUTION\t$trguc\tDEFINITION\tRELIABILITY\tEVALUATION\tCOMMENT\tINSTITUTION";
if($config eq "iateadmin")
	{
	print $outfile "LIL_RECORD_ID\tSUBJECT_DOMAIN\tSUBJECT_DOMAIN_NOTE\tPRIMARY\tLANGUAGE_CODE\tDEFINITION\tDEFINITION_REFERENCE\tRELATED_MATERIAL\tTERM\tTERM_FORMAT\tTERM_REF\tRELIABILITY_VALUE\tCONTEXT\tCONTEXT_REFERENCE\tTERM_EVALUATION\tPRE_IATE\tNOTE\tINSTITUTION_ID\n";
	}
elsif($config eq "iatepublic")
	{
	print $outfile "LIL_RECORD_ID\tSUBJECT_DOMAIN\tSUBJECT_DOMAIN_NOTE\tLANGUAGE_CODE\tTERM\tTERM_FORMAT\tTERM_TYPE\tRELIABILITY_VALUE\tTERM_EVALUATION\n";
	}

#old print $outfile "TL_RECORD_ID\tSUBJECT_DOMAIN\tSUBJECT_DOMAIN_NOTE\tLIL_RECORD_ID\tPRIMARY\tLANGUAGE_CODE\tDEFINITION\tDEFINITION_REFERENCE\tRELATED_MATERIAL\tTERM\tTERM_REF\tRELIABILITY_VALUE\tCONTEXT\tCONTEXT_REFERENCE\tTERM_EVALUATION\tPRE_IATE\tNOTE\tINSTITUTION_ID\n";




my $TL_RECORD_ID=0;
my $line;
my $count=0;
while($line = <$infile>)
	{
	#chomp($line);
	$line =~ s/[ \t]+\n/\n/ ;
	$line =~ s/[ ]+</</ ;

	#skip lines with empty elements:
	if($line =~ s/<language lang=\"([^\"]+)\"/$1/)
		{
		$LANGUAGE_CODE = lc($1);  
		}
	#cdt:
	elsif($line =~ s/<langSet xml:lang=\"([^\"]+)\"/$1/)
		{
		$LANGUAGE_CODE = lc($1);  
		}

	elsif($line =~ s/<descrip type=\"Evaluation\"\/>/$1/)
		{
		$TERM_EVALUATION="None";  
		}
	elsif( $line =~ s/<[^>]+\/>//)
		{;}
	elsif( $line =~ s/^\n//)
		{;}
	elsif( $line =~ s/<conceptGrp>//)
		{
		$TL_RECORD_ID++;
		#print "TL_RECORD_ID=$TL_RECORD_ID\n";
		}
	#elsif($line =~ s/\<language lang=\"([^\"]+)\" type=\"([^\"]+)\" \/>/$1/)
	elsif($line =~ s/<xref Ulink=[^\n]+lilId=([0-9]+)/$1/)
		{
		$LIL_RECORD_ID=$1;
		#print "lil-id: $1\n";
		}
	#cdt
	elsif($line =~ s/<termEntry id=\"IATE-([0-9]+)\"/$1/)
		{
		$LIL_RECORD_ID=$1;
		#print "lil-id: $1\n";
		}

	#elsif($line =~ s/^(<descrip<[^>]+>)([^<]+)/$1\t$2/)  


	elsif($line =~ s/^(<descrip type=\"[^\"]+\">)([^<\n]+)/$1/)  
		{
		#debug
        	#print $errfile "S1=$1\n";


		if($1 eq "<descrip type=\"IATE ID\">")				{$LIL_RECORD_ID=$2;}

		elsif($1 eq "<descrip type=\"subjectField\">") 
			{
			$SUBJECT_DOMAIN = $2;
			$SUBJECT_DOMAIN =~ s/([0-9]+)/ $1 /g;
			#$SUBJECT_DOMAIN =~ s/ 1011 /European Union law/;

$SUBJECT_DOMAIN =~ s/ 0 /NO SUBJECT DOMAIN/;
$SUBJECT_DOMAIN =~ s/ 4 /POLITICS/;
$SUBJECT_DOMAIN =~ s/ 406 /Political framework/;
$SUBJECT_DOMAIN =~ s/ 406001 /Political ideology/;
$SUBJECT_DOMAIN =~ s/ 406002 /Political institution/;
$SUBJECT_DOMAIN =~ s/ 406003 /Political philosophy/;
$SUBJECT_DOMAIN =~ s/ 406004 /Political power/;
$SUBJECT_DOMAIN =~ s/ 406005 /Political system/;
$SUBJECT_DOMAIN =~ s/ 406006 /State/;
$SUBJECT_DOMAIN =~ s/ 411 /Political Parties/;
$SUBJECT_DOMAIN =~ s/ 411001 /Party organisation/;
$SUBJECT_DOMAIN =~ s/ 411002 /Political party/;
$SUBJECT_DOMAIN =~ s/ 411003 /Political tendency/;
$SUBJECT_DOMAIN =~ s/ 416 /Electoral procedure and voting/;
$SUBJECT_DOMAIN =~ s/ 416001 /Election/;
$SUBJECT_DOMAIN =~ s/ 416002 /Electoral law/;
$SUBJECT_DOMAIN =~ s/ 416003 /Electoral system/;
$SUBJECT_DOMAIN =~ s/ 416004 /Majority voting/;
$SUBJECT_DOMAIN =~ s/ 416005 /Organisation of elections/;
$SUBJECT_DOMAIN =~ s/ 416006 /Parliamentary seat/;
$SUBJECT_DOMAIN =~ s/ 416007 /Vote/;
$SUBJECT_DOMAIN =~ s/ 416008 /Voting method/;
$SUBJECT_DOMAIN =~ s/ 421 /Parliament/;
$SUBJECT_DOMAIN =~ s/ 421001 /Composition of parliament/;
$SUBJECT_DOMAIN =~ s/ 421002 /Interparliamentary relations/;
$SUBJECT_DOMAIN =~ s/ 421003 /Member of Parliament/;
$SUBJECT_DOMAIN =~ s/ 421004 /Parliament/;
$SUBJECT_DOMAIN =~ s/ 421005 /Powers of parliament/;
$SUBJECT_DOMAIN =~ s/ 426 /Parliamentary proceedings/;
$SUBJECT_DOMAIN =~ s/ 426001 /Legislative procedure/;
$SUBJECT_DOMAIN =~ s/ 426002 /Parliamentary procedure/;
$SUBJECT_DOMAIN =~ s/ 431 /Politics and public safety/;
$SUBJECT_DOMAIN =~ s/ 431001 /Institutional activity/;
$SUBJECT_DOMAIN =~ s/ 431002 /Politics/;
$SUBJECT_DOMAIN =~ s/ 431003 /Public opinion/;
$SUBJECT_DOMAIN =~ s/ 431004 /Public safety/;
$SUBJECT_DOMAIN =~ s/ 431005 /Trends of opinion/;
$SUBJECT_DOMAIN =~ s/ 436 /Executive power and public service/;
$SUBJECT_DOMAIN =~ s/ 436001 /Administrative law/;
$SUBJECT_DOMAIN =~ s/ 436002 /Administrative structures/;
$SUBJECT_DOMAIN =~ s/ 436003 /Executive body/;
$SUBJECT_DOMAIN =~ s/ 436004 /Public administration/;
$SUBJECT_DOMAIN =~ s/ 436005 /Regional and local authorities/;
$SUBJECT_DOMAIN =~ s/ 8 /INTERNATIONAL RELATIONS/;
$SUBJECT_DOMAIN =~ s/ 806 /International affairs/;
$SUBJECT_DOMAIN =~ s/ 806001 /International affairs/;
$SUBJECT_DOMAIN =~ s/ 806002 /International agreement/;
$SUBJECT_DOMAIN =~ s/ 806003 /International instrument/;
$SUBJECT_DOMAIN =~ s/ 806004 /International organisation/;
$SUBJECT_DOMAIN =~ s/ 806005 /National independence/;
$SUBJECT_DOMAIN =~ s/ 811 /Cooperation policy/;
$SUBJECT_DOMAIN =~ s/ 811001 /Aid policy/;
$SUBJECT_DOMAIN =~ s/ 811002 /Cooperation policy/;
$SUBJECT_DOMAIN =~ s/ 811003 /Humanitarian aid/;
$SUBJECT_DOMAIN =~ s/ 816 /International balance/;
$SUBJECT_DOMAIN =~ s/ 816001 /Foreign policy/;
$SUBJECT_DOMAIN =~ s/ 816002 /International conflict/;
$SUBJECT_DOMAIN =~ s/ 816003 /International issue/;
$SUBJECT_DOMAIN =~ s/ 816004 /International security/;
$SUBJECT_DOMAIN =~ s/ 816005 /Peace/;
$SUBJECT_DOMAIN =~ s/ 816006 /War victim/;
$SUBJECT_DOMAIN =~ s/ 821 /Defence/;
$SUBJECT_DOMAIN =~ s/ 821001 /Armed forces/;
$SUBJECT_DOMAIN =~ s/ 821002 /Arms policy/;
$SUBJECT_DOMAIN =~ s/ 821003 /Defence policy/;
$SUBJECT_DOMAIN =~ s/ 821004 /Military equipment/;
$SUBJECT_DOMAIN =~ s/ 10 /EUROPEAN COMMUNITIES/;
$SUBJECT_DOMAIN =~ s/ 1006 /Community institutions and European civil service/;
$SUBJECT_DOMAIN =~ s/ 1006001 /Community body/;
$SUBJECT_DOMAIN =~ s/ 1006002 /Community body (established by the Treaties)/;
$SUBJECT_DOMAIN =~ s/ 1006003 /EU institution/;
$SUBJECT_DOMAIN =~ s/ 1006004 /European civil service/;
$SUBJECT_DOMAIN =~ s/ 1006005 /Institutional structure/;
$SUBJECT_DOMAIN =~ s/ 1006006 /Operation of the Institutions/;
$SUBJECT_DOMAIN =~ s/ 1011 /European Union law/;
$SUBJECT_DOMAIN =~ s/ 1011001 /Community act/;
$SUBJECT_DOMAIN =~ s/ 1011002 /Community law/;
$SUBJECT_DOMAIN =~ s/ 1011003 /Community legal system/;
$SUBJECT_DOMAIN =~ s/ 1011004 /EC Treaty/;
$SUBJECT_DOMAIN =~ s/ 1016 /European construction/;
$SUBJECT_DOMAIN =~ s/ 1016001 /Deepening of the European Union/;
$SUBJECT_DOMAIN =~ s/ 1016002 /Enlargement of the Union/;
$SUBJECT_DOMAIN =~ s/ 1016003 /EU relations/;
$SUBJECT_DOMAIN =~ s/ 1016004 /European Union/;
$SUBJECT_DOMAIN =~ s/ 1021 /Community finance/;
$SUBJECT_DOMAIN =~ s/ 1021001 /Community budget/;
$SUBJECT_DOMAIN =~ s/ 1021002 /Community expenditure/;
$SUBJECT_DOMAIN =~ s/ 1021003 /Community financing/;
$SUBJECT_DOMAIN =~ s/ 1021004 /Drawing up of the Community budget/;
$SUBJECT_DOMAIN =~ s/ 1021005 /Financing of the Community budget/;
$SUBJECT_DOMAIN =~ s/ 12 /LAW/;
$SUBJECT_DOMAIN =~ s/ 1206 /Sources and branches of the law/;
$SUBJECT_DOMAIN =~ s/ 1206001 /Legal science/;
$SUBJECT_DOMAIN =~ s/ 1206002 /Source of law/;
$SUBJECT_DOMAIN =~ s/ 1211 /Civil law/;
$SUBJECT_DOMAIN =~ s/ 1211001 /Civil law/;
$SUBJECT_DOMAIN =~ s/ 1211002 /Ownership/;
$SUBJECT_DOMAIN =~ s/ 1216 /Criminal law/;
$SUBJECT_DOMAIN =~ s/ 1216001 /Criminal law/;
$SUBJECT_DOMAIN =~ s/ 1216002 /Criminal liability/;
$SUBJECT_DOMAIN =~ s/ 1216003 /Law relating to prisons/;
$SUBJECT_DOMAIN =~ s/ 1216004 /Offence/;
$SUBJECT_DOMAIN =~ s/ 1216005 /Penalty/;
$SUBJECT_DOMAIN =~ s/ 1221 /Justice/;
$SUBJECT_DOMAIN =~ s/ 1221001 /Access to the courts/;
$SUBJECT_DOMAIN =~ s/ 1221002 /Legal action/;
$SUBJECT_DOMAIN =~ s/ 1221003 /Judicial proceedings/;
$SUBJECT_DOMAIN =~ s/ 1221004 /Ruling/;
$SUBJECT_DOMAIN =~ s/ 1226 /Organisation of the legal system/;
$SUBJECT_DOMAIN =~ s/ 1226001 /Legal profession/;
$SUBJECT_DOMAIN =~ s/ 1226002 /Legal system/;
$SUBJECT_DOMAIN =~ s/ 1231 /International law/;
$SUBJECT_DOMAIN =~ s/ 1231001 /International economic law/;
$SUBJECT_DOMAIN =~ s/ 1231002 /International labour law/;
$SUBJECT_DOMAIN =~ s/ 1231003 /International law/;
$SUBJECT_DOMAIN =~ s/ 1231004 /Private international law/;
$SUBJECT_DOMAIN =~ s/ 1231005 /Public international law/;
$SUBJECT_DOMAIN =~ s/ 1236 /Rights and freedoms/;
$SUBJECT_DOMAIN =~ s/ 1236001 /Anti-discriminatory measure/;
$SUBJECT_DOMAIN =~ s/ 1236002 /Economic rights/;
$SUBJECT_DOMAIN =~ s/ 1236003 /Human rights/;
$SUBJECT_DOMAIN =~ s/ 1236004 /Political rights/;
$SUBJECT_DOMAIN =~ s/ 1236005 /Rights of the individual/;
$SUBJECT_DOMAIN =~ s/ 1236006 /Social rights/;
$SUBJECT_DOMAIN =~ s/ 1236007 /Citizen's duties/;
$SUBJECT_DOMAIN =~ s/ 16 /ECONOMICS/;
$SUBJECT_DOMAIN =~ s/ 1606 /Economic policy/;
$SUBJECT_DOMAIN =~ s/ 1606001 /Economic planning/;
$SUBJECT_DOMAIN =~ s/ 1606002 /Economic policy/;
$SUBJECT_DOMAIN =~ s/ 1606003 /Economic support/;
$SUBJECT_DOMAIN =~ s/ 1611 /Economic growth/;
$SUBJECT_DOMAIN =~ s/ 1611001 /Economic conditions/;
$SUBJECT_DOMAIN =~ s/ 1611002 /Economic cycle/;
$SUBJECT_DOMAIN =~ s/ 1611003 /Economic development/;
$SUBJECT_DOMAIN =~ s/ 1616 /Regions and regional policy/;
$SUBJECT_DOMAIN =~ s/ 1616001 /Economic region/;
$SUBJECT_DOMAIN =~ s/ 1616002 /European Region/;
$SUBJECT_DOMAIN =~ s/ 1616003 /Regional policy/;
$SUBJECT_DOMAIN =~ s/ 1621 /Economic structure/;
$SUBJECT_DOMAIN =~ s/ 1621001 /Economic sector/;
$SUBJECT_DOMAIN =~ s/ 1621002 /Economic system/;
$SUBJECT_DOMAIN =~ s/ 1621003 /Economy/;
$SUBJECT_DOMAIN =~ s/ 1626 /National accounts/;
$SUBJECT_DOMAIN =~ s/ 1626001 /Accounting system/;
$SUBJECT_DOMAIN =~ s/ 1626002 /Income/;
$SUBJECT_DOMAIN =~ s/ 1626003 /National accounts/;
$SUBJECT_DOMAIN =~ s/ 1631 /Economic analysis/;
$SUBJECT_DOMAIN =~ s/ 1631001 /Economic analysis/;
$SUBJECT_DOMAIN =~ s/ 1631002 /Economic forecasting/;
$SUBJECT_DOMAIN =~ s/ 1631003 /Statistics/;
$SUBJECT_DOMAIN =~ s/ 20 /TRADE/;
$SUBJECT_DOMAIN =~ s/ 2006 /Trade policy/;
$SUBJECT_DOMAIN =~ s/ 2006001 /Commercial law/;
$SUBJECT_DOMAIN =~ s/ 2006002 /Common commercial policy/;
$SUBJECT_DOMAIN =~ s/ 2006003 /Export policy/;
$SUBJECT_DOMAIN =~ s/ 2006004 /Import policy/;
$SUBJECT_DOMAIN =~ s/ 2006005 /Market/;
$SUBJECT_DOMAIN =~ s/ 2006006 /Public contract/;
$SUBJECT_DOMAIN =~ s/ 2011 /Tariff policy/;
$SUBJECT_DOMAIN =~ s/ 2011001 /Common tariff policy/;
$SUBJECT_DOMAIN =~ s/ 2011002 /Community customs procedure/;
$SUBJECT_DOMAIN =~ s/ 2011003 /Customs regulations/;
$SUBJECT_DOMAIN =~ s/ 2011004 /Customs tariff/;
$SUBJECT_DOMAIN =~ s/ 2011005 /Tariff policy/;
$SUBJECT_DOMAIN =~ s/ 2016 /Trade/;
$SUBJECT_DOMAIN =~ s/ 2016001 /Supply/;
$SUBJECT_DOMAIN =~ s/ 2016002 /Supply and demand/;
$SUBJECT_DOMAIN =~ s/ 2016003 /Trading operation/;
$SUBJECT_DOMAIN =~ s/ 2021 /International trade/;
$SUBJECT_DOMAIN =~ s/ 2021001 /International trade/;
$SUBJECT_DOMAIN =~ s/ 2021002 /Trade relations/;
$SUBJECT_DOMAIN =~ s/ 2021003 /Trade restriction/;
$SUBJECT_DOMAIN =~ s/ 2026 /Consumption/;
$SUBJECT_DOMAIN =~ s/ 2026001 /Consumer/;
$SUBJECT_DOMAIN =~ s/ 2026002 /Consumption/;
$SUBJECT_DOMAIN =~ s/ 2026003 /Goods and services/;
$SUBJECT_DOMAIN =~ s/ 2031 /Marketing/;
$SUBJECT_DOMAIN =~ s/ 2031001 /Commercial transaction/;
$SUBJECT_DOMAIN =~ s/ 2031002 /Marketing/;
$SUBJECT_DOMAIN =~ s/ 2031003 /Preparation for market/;
$SUBJECT_DOMAIN =~ s/ 2036 /Distributive trades/;
$SUBJECT_DOMAIN =~ s/ 2036001 /Distributive trades/;
$SUBJECT_DOMAIN =~ s/ 2036002 /Storage/;
$SUBJECT_DOMAIN =~ s/ 24 /FINANCE/;
$SUBJECT_DOMAIN =~ s/ 2406 /Monetary relations/;
$SUBJECT_DOMAIN =~ s/ 2406001 /Balance of payments/;
$SUBJECT_DOMAIN =~ s/ 2406002 /European Monetary System/;
$SUBJECT_DOMAIN =~ s/ 2406003 /International finance/;
$SUBJECT_DOMAIN =~ s/ 2406004 /Monetary relations/;
$SUBJECT_DOMAIN =~ s/ 2411 /Monetary economics/;
$SUBJECT_DOMAIN =~ s/ 2411001 /Exchange policy/;
$SUBJECT_DOMAIN =~ s/ 2411002 /Monetary policy/;
$SUBJECT_DOMAIN =~ s/ 2411003 /Money market/;
$SUBJECT_DOMAIN =~ s/ 2416 /Financial institutions and credit/;
$SUBJECT_DOMAIN =~ s/ 2416001 /Banking/;
$SUBJECT_DOMAIN =~ s/ 2416002 /Credit/;
$SUBJECT_DOMAIN =~ s/ 2416003 /Credit policy/;
$SUBJECT_DOMAIN =~ s/ 2416004 /Financial institution/;
$SUBJECT_DOMAIN =~ s/ 2421 /Free movement of capital/;
$SUBJECT_DOMAIN =~ s/ 2421001 /Financial market/;
$SUBJECT_DOMAIN =~ s/ 2421002 /Free movement of capital/;
$SUBJECT_DOMAIN =~ s/ 2426 /Financing and investment/;
$SUBJECT_DOMAIN =~ s/ 2426001 /Financing/;
$SUBJECT_DOMAIN =~ s/ 2426002 /Investment/;
$SUBJECT_DOMAIN =~ s/ 2431 /Insurance/;
$SUBJECT_DOMAIN =~ s/ 2431001 /Insurance/;
$SUBJECT_DOMAIN =~ s/ 2436 /Public finance and budget policy/;
$SUBJECT_DOMAIN =~ s/ 2436001 /Budget policy/;
$SUBJECT_DOMAIN =~ s/ 2436002 /Public debt/;
$SUBJECT_DOMAIN =~ s/ 2436003 /Public finance/;
$SUBJECT_DOMAIN =~ s/ 2441 /Budget/;
$SUBJECT_DOMAIN =~ s/ 2441001 /Budget financing/;
$SUBJECT_DOMAIN =~ s/ 2441002 /Budgetary expenditure/;
$SUBJECT_DOMAIN =~ s/ 2441003 /Budgetary procedure/;
$SUBJECT_DOMAIN =~ s/ 2441004 /Implementation of the budget/;
$SUBJECT_DOMAIN =~ s/ 2446 /Taxation/;
$SUBJECT_DOMAIN =~ s/ 2446001 /Fiscal policy/;
$SUBJECT_DOMAIN =~ s/ 2446002 /Tax/;
$SUBJECT_DOMAIN =~ s/ 2446003 /Tax on capital/;
$SUBJECT_DOMAIN =~ s/ 2446004 /Tax on consumption/;
$SUBJECT_DOMAIN =~ s/ 2446005 /Tax on income/;
$SUBJECT_DOMAIN =~ s/ 2446006 /Tax system/;
$SUBJECT_DOMAIN =~ s/ 2451 /Prices/;
$SUBJECT_DOMAIN =~ s/ 2451001 /Farm prices/;
$SUBJECT_DOMAIN =~ s/ 2451002 /Market prices/;
$SUBJECT_DOMAIN =~ s/ 2451003 /Price fluctuation/;
$SUBJECT_DOMAIN =~ s/ 2451004 /Prices/;
$SUBJECT_DOMAIN =~ s/ 2451005 /Prices policy/;
$SUBJECT_DOMAIN =~ s/ 28 /SOCIAL QUESTIONS/;
$SUBJECT_DOMAIN =~ s/ 2806 /Family/;
$SUBJECT_DOMAIN =~ s/ 2806001 /Artificial reproductive techniques/;
$SUBJECT_DOMAIN =~ s/ 2806002 /Family/;
$SUBJECT_DOMAIN =~ s/ 2806003 /Family law/;
$SUBJECT_DOMAIN =~ s/ 2806004 /Family planning/;
$SUBJECT_DOMAIN =~ s/ 2806005 /Marital status/;
$SUBJECT_DOMAIN =~ s/ 2811 /Migration/;
$SUBJECT_DOMAIN =~ s/ 2811001 /Internal migration/;
$SUBJECT_DOMAIN =~ s/ 2811002 /Migration/;
$SUBJECT_DOMAIN =~ s/ 2816 /Demography and population/;
$SUBJECT_DOMAIN =~ s/ 2816001 /Composition of the population/;
$SUBJECT_DOMAIN =~ s/ 2816002 /Demography/;
$SUBJECT_DOMAIN =~ s/ 2816003 /Geographical distribution of the population/;
$SUBJECT_DOMAIN =~ s/ 2816004 /Population dynamics/;
$SUBJECT_DOMAIN =~ s/ 2821 /Social framework/;
$SUBJECT_DOMAIN =~ s/ 2821001 /Social analysis/;
$SUBJECT_DOMAIN =~ s/ 2821002 /Social situation/;
$SUBJECT_DOMAIN =~ s/ 2821003 /Social structure/;
$SUBJECT_DOMAIN =~ s/ 2821004 /Sociocultural group/;
$SUBJECT_DOMAIN =~ s/ 2826 /Social affairs/;
$SUBJECT_DOMAIN =~ s/ 2826001 /Leisure/;
$SUBJECT_DOMAIN =~ s/ 2826002 /Social life/;
$SUBJECT_DOMAIN =~ s/ 2826003 /Social policy/;
$SUBJECT_DOMAIN =~ s/ 2826004 /Social problem/;
$SUBJECT_DOMAIN =~ s/ 2831 /Culture and religion/;
$SUBJECT_DOMAIN =~ s/ 2831001 /Arts/;
$SUBJECT_DOMAIN =~ s/ 2831002 /Cultural policy/;
$SUBJECT_DOMAIN =~ s/ 2831003 /Culture/;
$SUBJECT_DOMAIN =~ s/ 2831004 /Religion/;
$SUBJECT_DOMAIN =~ s/ 2836 /Social protection/;
$SUBJECT_DOMAIN =~ s/ 2836001 /Leave on social grounds/;
$SUBJECT_DOMAIN =~ s/ 2836002 /Social security/;
$SUBJECT_DOMAIN =~ s/ 2836003 /Welfare/;
$SUBJECT_DOMAIN =~ s/ 2841 /Health/;
$SUBJECT_DOMAIN =~ s/ 2841001 /Health care profession/;
$SUBJECT_DOMAIN =~ s/ 2841002 /Health policy/;
$SUBJECT_DOMAIN =~ s/ 2841003 /Illness/;
$SUBJECT_DOMAIN =~ s/ 2841004 /Medical science/;
$SUBJECT_DOMAIN =~ s/ 2841005 /Nutrition/;
$SUBJECT_DOMAIN =~ s/ 2841006 /Pharmaceutical industry/;
$SUBJECT_DOMAIN =~ s/ 2846 /Construction and town planning/;
$SUBJECT_DOMAIN =~ s/ 2846001 /Built-up area/;
$SUBJECT_DOMAIN =~ s/ 2846002 /Construction policy/;
$SUBJECT_DOMAIN =~ s/ 2846003 /Habitat/;
$SUBJECT_DOMAIN =~ s/ 2846004 /Housing/;
$SUBJECT_DOMAIN =~ s/ 2846005 /Housing policy/;
$SUBJECT_DOMAIN =~ s/ 2846006 /Property leasing/;
$SUBJECT_DOMAIN =~ s/ 2846007 /Town planning/;
$SUBJECT_DOMAIN =~ s/ 32 /EDUCATION AND COMMUNICATIONS/;
$SUBJECT_DOMAIN =~ s/ 3206 /Education/;
$SUBJECT_DOMAIN =~ s/ 3206001 /Education policy/;
$SUBJECT_DOMAIN =~ s/ 3206002 /Teaching method/;
$SUBJECT_DOMAIN =~ s/ 3211 /Teaching/;
$SUBJECT_DOMAIN =~ s/ 3211001 /Educational institution/;
$SUBJECT_DOMAIN =~ s/ 3211002 /General education/;
$SUBJECT_DOMAIN =~ s/ 3211003 /Level of education/;
$SUBJECT_DOMAIN =~ s/ 3211004 /Teaching/;
$SUBJECT_DOMAIN =~ s/ 3211005 /Vocational education/;
$SUBJECT_DOMAIN =~ s/ 3216 /Organisation of teaching/;
$SUBJECT_DOMAIN =~ s/ 3216001 /Organisation of teaching/;
$SUBJECT_DOMAIN =~ s/ 3216002 /School life/;
$SUBJECT_DOMAIN =~ s/ 3216003 /Schoolwork/;
$SUBJECT_DOMAIN =~ s/ 3216004 /Teaching materials/;
$SUBJECT_DOMAIN =~ s/ 3221 /Documentation/;
$SUBJECT_DOMAIN =~ s/ 3221001 /Document/;
$SUBJECT_DOMAIN =~ s/ 3221002 /Documentation/;
$SUBJECT_DOMAIN =~ s/ 3221003 /Information service/;
$SUBJECT_DOMAIN =~ s/ 3226 /Communications/;
$SUBJECT_DOMAIN =~ s/ 3226001 /Audiovisual equipment/;
$SUBJECT_DOMAIN =~ s/ 3226002 /Communications industry/;
$SUBJECT_DOMAIN =~ s/ 3226003 /Communications policy/;
$SUBJECT_DOMAIN =~ s/ 3226004 /Communications systems/;
$SUBJECT_DOMAIN =~ s/ 3226005 /Means of communication/;
$SUBJECT_DOMAIN =~ s/ 3231 /Information and information processing/;
$SUBJECT_DOMAIN =~ s/ 3231001 /Information/;
$SUBJECT_DOMAIN =~ s/ 3231002 /Information policy/;
$SUBJECT_DOMAIN =~ s/ 3231003 /Information processing/;
$SUBJECT_DOMAIN =~ s/ 3236 /Information technology and data processing/;
$SUBJECT_DOMAIN =~ s/ 3236001 /Computer system/;
$SUBJECT_DOMAIN =~ s/ 3236002 /Computer systems/;
$SUBJECT_DOMAIN =~ s/ 3236003 /Data-processing law/;
$SUBJECT_DOMAIN =~ s/ 3236004 /Dataprocessing/;
$SUBJECT_DOMAIN =~ s/ 3236005 /Information technology industry/;
$SUBJECT_DOMAIN =~ s/ 36 /SCIENCE/;
$SUBJECT_DOMAIN =~ s/ 3606 /Natural and applied sciences/;
$SUBJECT_DOMAIN =~ s/ 3606001 /Applied sciences/;
$SUBJECT_DOMAIN =~ s/ 3606002 /Earth sciences/;
$SUBJECT_DOMAIN =~ s/ 3606003 /Life sciences/;
$SUBJECT_DOMAIN =~ s/ 3606004 /Physical sciences/;
$SUBJECT_DOMAIN =~ s/ 3606005 /Space science/;
$SUBJECT_DOMAIN =~ s/ 3611 /Humanities/;
$SUBJECT_DOMAIN =~ s/ 3611001 /Behavioural sciences/;
$SUBJECT_DOMAIN =~ s/ 3611002 /Social sciences/;
$SUBJECT_DOMAIN =~ s/ 40 /BUSINESS AND COMPETITION/;
$SUBJECT_DOMAIN =~ s/ 4006 /Business organisation/;
$SUBJECT_DOMAIN =~ s/ 4006001 /Administrative personnel/;
$SUBJECT_DOMAIN =~ s/ 4006002 /Business activity/;
$SUBJECT_DOMAIN =~ s/ 4006003 /Business policy/;
$SUBJECT_DOMAIN =~ s/ 4006004 /Company law/;
$SUBJECT_DOMAIN =~ s/ 4006005 /Company structure/;
$SUBJECT_DOMAIN =~ s/ 4006006 /Economic concentration/;
$SUBJECT_DOMAIN =~ s/ 4011 /Business classification/;
$SUBJECT_DOMAIN =~ s/ 4011001 /Branch of activity/;
$SUBJECT_DOMAIN =~ s/ 4011002 /Size of business/;
$SUBJECT_DOMAIN =~ s/ 4011003 /Type of business/;
$SUBJECT_DOMAIN =~ s/ 4016 /Legal form of organisations/;
$SUBJECT_DOMAIN =~ s/ 4016001 /Organisation/;
$SUBJECT_DOMAIN =~ s/ 4021 /Management/;
$SUBJECT_DOMAIN =~ s/ 4021001 /Financial management/;
$SUBJECT_DOMAIN =~ s/ 4021002 /Management/;
$SUBJECT_DOMAIN =~ s/ 4021003 /Management techniques/;
$SUBJECT_DOMAIN =~ s/ 4026 /Accounting/;
$SUBJECT_DOMAIN =~ s/ 4026001 /Accounting/;
$SUBJECT_DOMAIN =~ s/ 4026002 /Management accounting/;
$SUBJECT_DOMAIN =~ s/ 4031 /Competition/;
$SUBJECT_DOMAIN =~ s/ 4031001 /Competition law/;
$SUBJECT_DOMAIN =~ s/ 4031002 /Competition policy/;
$SUBJECT_DOMAIN =~ s/ 4031003 /Restriction on competition/;
$SUBJECT_DOMAIN =~ s/ 4031004 /Restrictive trade practice/;
$SUBJECT_DOMAIN =~ s/ 44 /EMPLOYMENT AND WORKING CONDITIONS/;
$SUBJECT_DOMAIN =~ s/ 4406 /Employment/;
$SUBJECT_DOMAIN =~ s/ 4406001 /Employment policy/;
$SUBJECT_DOMAIN =~ s/ 4406002 /Employment structure/;
$SUBJECT_DOMAIN =~ s/ 4406003 /Termination of employment/;
$SUBJECT_DOMAIN =~ s/ 4406004 /Unemployment/;
$SUBJECT_DOMAIN =~ s/ 4406005 /Vocational training/;
$SUBJECT_DOMAIN =~ s/ 4411 /Labour market/;
$SUBJECT_DOMAIN =~ s/ 4411001 /Labour force/;
$SUBJECT_DOMAIN =~ s/ 4411002 /Labour market/;
$SUBJECT_DOMAIN =~ s/ 4411003 /Occupational status/;
$SUBJECT_DOMAIN =~ s/ 4411004 /Socioprofessional category/;
$SUBJECT_DOMAIN =~ s/ 4416 /Organisation of work and working conditions/;
$SUBJECT_DOMAIN =~ s/ 4416001 /Organisation of work/;
$SUBJECT_DOMAIN =~ s/ 4416002 /Work/;
$SUBJECT_DOMAIN =~ s/ 4416003 /Working conditions/;
$SUBJECT_DOMAIN =~ s/ 4421 /Personnel management and staff remuneration/;
$SUBJECT_DOMAIN =~ s/ 4421001 /Pay policy/;
$SUBJECT_DOMAIN =~ s/ 4421002 /Personnel administration/;
$SUBJECT_DOMAIN =~ s/ 4421003 /Remuneration of work/;
$SUBJECT_DOMAIN =~ s/ 4426 /Labour law and labour relations/;
$SUBJECT_DOMAIN =~ s/ 4426001 /Labour law/;
$SUBJECT_DOMAIN =~ s/ 4426002 /Labour relations/;
$SUBJECT_DOMAIN =~ s/ 4426003 /Organisation of professions/;
$SUBJECT_DOMAIN =~ s/ 48 /TRANSPORT/;
$SUBJECT_DOMAIN =~ s/ 4806 /Transport policy/;
$SUBJECT_DOMAIN =~ s/ 4806001 /Traffic regulations/;
$SUBJECT_DOMAIN =~ s/ 4806002 /Transport policy/;
$SUBJECT_DOMAIN =~ s/ 4806003 /Transport price/;
$SUBJECT_DOMAIN =~ s/ 4806004 /Transport regulations/;
$SUBJECT_DOMAIN =~ s/ 4811 /Organisation of transport/;
$SUBJECT_DOMAIN =~ s/ 4811001 /Destination of transport/;
$SUBJECT_DOMAIN =~ s/ 4811002 /Means of transport/;
$SUBJECT_DOMAIN =~ s/ 4811003 /Mode of transport/;
$SUBJECT_DOMAIN =~ s/ 4811004 /Organisation of transport/;
$SUBJECT_DOMAIN =~ s/ 4816 /Land transport/;
$SUBJECT_DOMAIN =~ s/ 4816001 /Land transport/;
$SUBJECT_DOMAIN =~ s/ 4821 /Maritime and inland waterway transport/;
$SUBJECT_DOMAIN =~ s/ 4821001 /Inland waterway transport/;
$SUBJECT_DOMAIN =~ s/ 4821002 /Maritime transport/;
$SUBJECT_DOMAIN =~ s/ 4821003 /Ports policy/;
$SUBJECT_DOMAIN =~ s/ 4826 /Air and space transport/;
$SUBJECT_DOMAIN =~ s/ 4826001 /Air transport/;
$SUBJECT_DOMAIN =~ s/ 4826002 /Space transport/;
$SUBJECT_DOMAIN =~ s/ 52 /ENVIRONMENT/;
$SUBJECT_DOMAIN =~ s/ 5206 /Environmental policy/;
$SUBJECT_DOMAIN =~ s/ 5206001 /Environmental policy/;
$SUBJECT_DOMAIN =~ s/ 5206002 /Environmental protection/;
$SUBJECT_DOMAIN =~ s/ 5206003 /Pollution control measures/;
$SUBJECT_DOMAIN =~ s/ 5206004 /Waste management/;
$SUBJECT_DOMAIN =~ s/ 5206005 /Water management/;
$SUBJECT_DOMAIN =~ s/ 5211 /Natural environment/;
$SUBJECT_DOMAIN =~ s/ 5211001 /Climate/;
$SUBJECT_DOMAIN =~ s/ 5211002 /Geophysical environment/;
$SUBJECT_DOMAIN =~ s/ 5211003 /Natural resources/;
$SUBJECT_DOMAIN =~ s/ 5211004 /Physical environment/;
$SUBJECT_DOMAIN =~ s/ 5211005 /Wildlife/;
$SUBJECT_DOMAIN =~ s/ 5216 /Deterioration of the environment/;
$SUBJECT_DOMAIN =~ s/ 5216001 /Degradation of the environment/;
$SUBJECT_DOMAIN =~ s/ 5216002 /Nuisance/;
$SUBJECT_DOMAIN =~ s/ 5216003 /Pollution/;
$SUBJECT_DOMAIN =~ s/ 5216004 /Waste/;
$SUBJECT_DOMAIN =~ s/ 56 /AGRICULTURE, FORESTRY AND FISHERIES/;
$SUBJECT_DOMAIN =~ s/ 5606 /Agricultural policy/;
$SUBJECT_DOMAIN =~ s/ 5606001 /Common agricultural policy/;
$SUBJECT_DOMAIN =~ s/ 5611 /Agricultural structures and production/;
$SUBJECT_DOMAIN =~ s/ 5611001 /Agricultural production policy/;
$SUBJECT_DOMAIN =~ s/ 5611002 /Policy on agricultural structures/;
$SUBJECT_DOMAIN =~ s/ 5611003 /Regulation of agricultural production/;
$SUBJECT_DOMAIN =~ s/ 5616 /Farming systems/;
$SUBJECT_DOMAIN =~ s/ 5616001 /Agricultural holding/;
$SUBJECT_DOMAIN =~ s/ 5616002 /Agricultural performance/;
$SUBJECT_DOMAIN =~ s/ 5616003 /Agricultural real estate/;
$SUBJECT_DOMAIN =~ s/ 5616004 /Farming system/;
$SUBJECT_DOMAIN =~ s/ 5616005 /Type of tenure/;
$SUBJECT_DOMAIN =~ s/ 5616006 /Working population engaged in agriculture/;
$SUBJECT_DOMAIN =~ s/ 5621 /Cultivation of agricultural land/;
$SUBJECT_DOMAIN =~ s/ 5621001 /Cultivation system/;
$SUBJECT_DOMAIN =~ s/ 5621002 /Cultivation techniques/;
$SUBJECT_DOMAIN =~ s/ 5621003 /Land use/;
$SUBJECT_DOMAIN =~ s/ 5626 /Means of agricultural production/;
$SUBJECT_DOMAIN =~ s/ 5626001 /Livestock/;
$SUBJECT_DOMAIN =~ s/ 5626002 /Means of agricultural production/;
$SUBJECT_DOMAIN =~ s/ 5631 /Agricultural activity/;
$SUBJECT_DOMAIN =~ s/ 5631001 /Agricultural product/;
$SUBJECT_DOMAIN =~ s/ 5631002 /Animal health/;
$SUBJECT_DOMAIN =~ s/ 5631003 /Animal nutrition/;
$SUBJECT_DOMAIN =~ s/ 5631004 /Animal production/;
$SUBJECT_DOMAIN =~ s/ 5631005 /Crop production/;
$SUBJECT_DOMAIN =~ s/ 5631006 /Livestock farming/;
$SUBJECT_DOMAIN =~ s/ 5636 /Forestry/;
$SUBJECT_DOMAIN =~ s/ 5636001 /Forest/;
$SUBJECT_DOMAIN =~ s/ 5636002 /Forestry economics/;
$SUBJECT_DOMAIN =~ s/ 5636003 /Forestry policy/;
$SUBJECT_DOMAIN =~ s/ 5641 /Fisheries/;
$SUBJECT_DOMAIN =~ s/ 5641001 /Aquaculture/;
$SUBJECT_DOMAIN =~ s/ 5641002 /Fisheries policy/;
$SUBJECT_DOMAIN =~ s/ 5641003 /Fisheries structure/;
$SUBJECT_DOMAIN =~ s/ 5641004 /Fishery resources/;
$SUBJECT_DOMAIN =~ s/ 5641005 /Fishing grounds/;
$SUBJECT_DOMAIN =~ s/ 5641006 /Fishing industry/;
$SUBJECT_DOMAIN =~ s/ 60 /AGRI-FOODSTUFFS/;
$SUBJECT_DOMAIN =~ s/ 6006 /Plant product/;
$SUBJECT_DOMAIN =~ s/ 6006001 /Cereals/;
$SUBJECT_DOMAIN =~ s/ 6006002 /Fodder plant/;
$SUBJECT_DOMAIN =~ s/ 6006003 /Fruit/;
$SUBJECT_DOMAIN =~ s/ 6006004 /Industrial plant/;
$SUBJECT_DOMAIN =~ s/ 6006005 /Oleaginous plant/;
$SUBJECT_DOMAIN =~ s/ 6006006 /Root crop/;
$SUBJECT_DOMAIN =~ s/ 6006007 /Textile plant/;
$SUBJECT_DOMAIN =~ s/ 6006008 /Tropical plant/;
$SUBJECT_DOMAIN =~ s/ 6006009 /Vegetable/;
$SUBJECT_DOMAIN =~ s/ 6011 /Animal product/;
$SUBJECT_DOMAIN =~ s/ 6011001 /Animal product/;
$SUBJECT_DOMAIN =~ s/ 6016 /Processed agricultural produce/;
$SUBJECT_DOMAIN =~ s/ 6016001 /Essential oil/;
$SUBJECT_DOMAIN =~ s/ 6016002 /Fats/;
$SUBJECT_DOMAIN =~ s/ 6016003 /Milk product/;
$SUBJECT_DOMAIN =~ s/ 6016004 /Protein products/;
$SUBJECT_DOMAIN =~ s/ 6021 /Beverages and sugar/;
$SUBJECT_DOMAIN =~ s/ 6021001 /Beverage/;
$SUBJECT_DOMAIN =~ s/ 6021002 /Sugar/;
$SUBJECT_DOMAIN =~ s/ 6026 /Foodstuff/;
$SUBJECT_DOMAIN =~ s/ 6026001 /Foodstuff/;
$SUBJECT_DOMAIN =~ s/ 6026002 /Processed food product/;
$SUBJECT_DOMAIN =~ s/ 6026003 /Processed foodstuff/;
$SUBJECT_DOMAIN =~ s/ 6031 /Agri-foodstuffs/;
$SUBJECT_DOMAIN =~ s/ 6031001 /Agri-foodstuffs/;
$SUBJECT_DOMAIN =~ s/ 6031002 /Food industry/;
$SUBJECT_DOMAIN =~ s/ 6036 /Food technology/;
$SUBJECT_DOMAIN =~ s/ 6036001 /Food additive/;
$SUBJECT_DOMAIN =~ s/ 6036002 /Food technology/;
$SUBJECT_DOMAIN =~ s/ 64 /PRODUCTION, TECHNOLOGY AND RESEARCH/;
$SUBJECT_DOMAIN =~ s/ 6406 /Production/;
$SUBJECT_DOMAIN =~ s/ 6406001 /Production policy/;
$SUBJECT_DOMAIN =~ s/ 6411 /Technology and technical regulations/;
$SUBJECT_DOMAIN =~ s/ 6411001 /Advanced materials/;
$SUBJECT_DOMAIN =~ s/ 6411002 /Industrial manufacturing/;
$SUBJECT_DOMAIN =~ s/ 6411003 /Materials technology/;
$SUBJECT_DOMAIN =~ s/ 6411004 /Technical regulations/;
$SUBJECT_DOMAIN =~ s/ 6411005 /Technology/;
$SUBJECT_DOMAIN =~ s/ 6416 /Research and intellectual property/;
$SUBJECT_DOMAIN =~ s/ 6416001 /Intellectual property/;
$SUBJECT_DOMAIN =~ s/ 6416002 /Research/;
$SUBJECT_DOMAIN =~ s/ 6416003 /Research policy/;
$SUBJECT_DOMAIN =~ s/ 6416004 /Space policy/;
$SUBJECT_DOMAIN =~ s/ 66 /ENERGY/;
$SUBJECT_DOMAIN =~ s/ 6606 /Energy policy/;
$SUBJECT_DOMAIN =~ s/ 6606001 /Energy industry/;
$SUBJECT_DOMAIN =~ s/ 6606002 /Energy policy/;
$SUBJECT_DOMAIN =~ s/ 6611 /Coal and mining industries/;
$SUBJECT_DOMAIN =~ s/ 6611001 /Coal industry/;
$SUBJECT_DOMAIN =~ s/ 6611002 /Mining industry/;
$SUBJECT_DOMAIN =~ s/ 6611003 /Mining product/;
$SUBJECT_DOMAIN =~ s/ 6616 /Oil industry/;
$SUBJECT_DOMAIN =~ s/ 6616001 /Hydrocarbon/;
$SUBJECT_DOMAIN =~ s/ 6616002 /Oil industry/;
$SUBJECT_DOMAIN =~ s/ 6616003 /Petrochemicals/;
$SUBJECT_DOMAIN =~ s/ 6621 /Electrical and nuclear industries/;
$SUBJECT_DOMAIN =~ s/ 6621001 /Electrical industry/;
$SUBJECT_DOMAIN =~ s/ 6621002 /Nuclear energy/;
$SUBJECT_DOMAIN =~ s/ 6621003 /Nuclear industry/;
$SUBJECT_DOMAIN =~ s/ 6621004 /Nuclear power station/;
$SUBJECT_DOMAIN =~ s/ 6626 /Soft energy/;
$SUBJECT_DOMAIN =~ s/ 6626001 /Soft energy/;
$SUBJECT_DOMAIN =~ s/ 68 /INDUSTRY/;
$SUBJECT_DOMAIN =~ s/ 6806 /Industrial structures and policy/;
$SUBJECT_DOMAIN =~ s/ 6806001 /Industrial policy/;
$SUBJECT_DOMAIN =~ s/ 6806002 /Industrial production/;
$SUBJECT_DOMAIN =~ s/ 6806003 /Industrial structures/;
$SUBJECT_DOMAIN =~ s/ 6806004 /Technical profession/;
$SUBJECT_DOMAIN =~ s/ 6811 /Chemistry/;
$SUBJECT_DOMAIN =~ s/ 6811001 /Chemical compound/;
$SUBJECT_DOMAIN =~ s/ 6811002 /Chemical element/;
$SUBJECT_DOMAIN =~ s/ 6811003 /Chemical industry/;
$SUBJECT_DOMAIN =~ s/ 6811004 /Special chemicals/;
$SUBJECT_DOMAIN =~ s/ 6816 /Iron, steel and other metal industries/;
$SUBJECT_DOMAIN =~ s/ 6816001 /Iron and steel industry/;
$SUBJECT_DOMAIN =~ s/ 6816002 /Metallurgical industry/;
$SUBJECT_DOMAIN =~ s/ 6816003 /Metals/;
$SUBJECT_DOMAIN =~ s/ 6821 /Mechanical engineering/;
$SUBJECT_DOMAIN =~ s/ 6821001 /Machinery/;
$SUBJECT_DOMAIN =~ s/ 6821002 /Mechanical engineering/;
$SUBJECT_DOMAIN =~ s/ 6821003 /Mechanical equipment/;
$SUBJECT_DOMAIN =~ s/ 6821004 /Precision engineering/;
$SUBJECT_DOMAIN =~ s/ 6821005 /Pressure equipment/;
$SUBJECT_DOMAIN =~ s/ 6821006 /Thermal equipment/;
$SUBJECT_DOMAIN =~ s/ 6826 /Electronics and electrical engineering/;
$SUBJECT_DOMAIN =~ s/ 6826001 /Electrical engineering/;
$SUBJECT_DOMAIN =~ s/ 6826002 /Electronics industry/;
$SUBJECT_DOMAIN =~ s/ 6831 /Building and public works/;
$SUBJECT_DOMAIN =~ s/ 6831001 /Building industry/;
$SUBJECT_DOMAIN =~ s/ 6831002 /Building services/;
$SUBJECT_DOMAIN =~ s/ 6831003 /Public works/;
$SUBJECT_DOMAIN =~ s/ 6836 /Wood industry/;
$SUBJECT_DOMAIN =~ s/ 6836001 /Wood industry/;
$SUBJECT_DOMAIN =~ s/ 6841 /Leather and textile industries/;
$SUBJECT_DOMAIN =~ s/ 6841001 /Leather industry/;
$SUBJECT_DOMAIN =~ s/ 6841002 /Textile industry/;
$SUBJECT_DOMAIN =~ s/ 6846 /Miscellaneous industries/;
$SUBJECT_DOMAIN =~ s/ 6846001 /Ceramics/;
$SUBJECT_DOMAIN =~ s/ 6846002 /Miscellaneous industries/;
$SUBJECT_DOMAIN =~ s/ 72 /GEOGRAPHY/;
$SUBJECT_DOMAIN =~ s/ 7206 /Europe/;
$SUBJECT_DOMAIN =~ s/ 7206001 /Eastern Europe/;
$SUBJECT_DOMAIN =~ s/ 7206002 /Europe/;
$SUBJECT_DOMAIN =~ s/ 7206003 /Former USSR/;
$SUBJECT_DOMAIN =~ s/ 7206004 /Northern Europe/;
$SUBJECT_DOMAIN =~ s/ 7206005 /Southern Europe/;
$SUBJECT_DOMAIN =~ s/ 7206006 /Western Europe/;
$SUBJECT_DOMAIN =~ s/ 7211 /Regions of EU Member States/;
$SUBJECT_DOMAIN =~ s/ 7211001 /Regions and communities of Belgium/;
$SUBJECT_DOMAIN =~ s/ 7211002 /Regions of Austria/;
$SUBJECT_DOMAIN =~ s/ 7211003 /Regions of Denmark/;
$SUBJECT_DOMAIN =~ s/ 7211004 /Regions of Finland/;
$SUBJECT_DOMAIN =~ s/ 7211005 /Regions of France/;
$SUBJECT_DOMAIN =~ s/ 7211006 /Regions of Germany/;
$SUBJECT_DOMAIN =~ s/ 7211007 /Regions of Greece/;
$SUBJECT_DOMAIN =~ s/ 7211008 /Regions of Ireland/;
$SUBJECT_DOMAIN =~ s/ 7211009 /Regions of Italy/;
$SUBJECT_DOMAIN =~ s/ 7211010 /Regions of Portugal/;
$SUBJECT_DOMAIN =~ s/ 7211011 /Regions of Spain/;
$SUBJECT_DOMAIN =~ s/ 7211012 /Regions of Sweden/;
$SUBJECT_DOMAIN =~ s/ 7211013 /Regions of the Netherlands/;
$SUBJECT_DOMAIN =~ s/ 7211014 /Regions of the United Kingdom/;
$SUBJECT_DOMAIN =~ s/ 7211015 /Regions of Estonia/;
$SUBJECT_DOMAIN =~ s/ 7211016 /Regions of Hungary/;
$SUBJECT_DOMAIN =~ s/ 7211017 /Regions of Latvia/;
$SUBJECT_DOMAIN =~ s/ 7211018 /Regions of Lithuania/;
$SUBJECT_DOMAIN =~ s/ 7211019 /Regions of Poland/;
$SUBJECT_DOMAIN =~ s/ 7211020 /Regions of Slovakia/;
$SUBJECT_DOMAIN =~ s/ 7211021 /Regions of Slovenia/;
$SUBJECT_DOMAIN =~ s/ 7211022 /Regions of the Czech Republic/;
$SUBJECT_DOMAIN =~ s/ 7216 /America/;
$SUBJECT_DOMAIN =~ s/ 7216001 /America/;
$SUBJECT_DOMAIN =~ s/ 7216002 /Caribbean Islands/;
$SUBJECT_DOMAIN =~ s/ 7216003 /Latin America/;
$SUBJECT_DOMAIN =~ s/ 7216004 /North America/;
$SUBJECT_DOMAIN =~ s/ 7221 /Africa/;
$SUBJECT_DOMAIN =~ s/ 7221001 /Africa/;
$SUBJECT_DOMAIN =~ s/ 7221002 /Central Africa/;
$SUBJECT_DOMAIN =~ s/ 7221003 /East Africa/;
$SUBJECT_DOMAIN =~ s/ 7221004 /North Africa/;
$SUBJECT_DOMAIN =~ s/ 7221005 /Southern Africa/;
$SUBJECT_DOMAIN =~ s/ 7221006 /West Africa/;
$SUBJECT_DOMAIN =~ s/ 7226 /Asia and Oceania/;
$SUBJECT_DOMAIN =~ s/ 7226001 /Asia/;
$SUBJECT_DOMAIN =~ s/ 7226002 /Central Asia/;
$SUBJECT_DOMAIN =~ s/ 7226003 /Far East/;
$SUBJECT_DOMAIN =~ s/ 7226004 /Middle East/;
$SUBJECT_DOMAIN =~ s/ 7226005 /Oceania/;
$SUBJECT_DOMAIN =~ s/ 7226006 /South Asia/;
$SUBJECT_DOMAIN =~ s/ 7226007 /South-East Asia/;
$SUBJECT_DOMAIN =~ s/ 7231 /Economic geography/;
$SUBJECT_DOMAIN =~ s/ 7231001 /AAMS countries/;
$SUBJECT_DOMAIN =~ s/ 7231002 /ACP countries/;
$SUBJECT_DOMAIN =~ s/ 7231003 /Andean Group countries/;
$SUBJECT_DOMAIN =~ s/ 7231004 /APEC countries/;
$SUBJECT_DOMAIN =~ s/ 7231005 /Arab Common Market countries/;
$SUBJECT_DOMAIN =~ s/ 7231006 /Asean countries/;
$SUBJECT_DOMAIN =~ s/ 7231007 /Benelux countries/;
$SUBJECT_DOMAIN =~ s/ 7231008 /CACM countries/;
$SUBJECT_DOMAIN =~ s/ 7231009 /CAEEU countries/;
$SUBJECT_DOMAIN =~ s/ 7231010 /Caricom countries/;
$SUBJECT_DOMAIN =~ s/ 7231011 /CACM countries/;
$SUBJECT_DOMAIN =~ s/ 7231012 /CEAO countries/;
$SUBJECT_DOMAIN =~ s/ 7231013 /CMEA countries/;
$SUBJECT_DOMAIN =~ s/ 7231014 /EFTA countries/;
$SUBJECT_DOMAIN =~ s/ 7231015 /EU country/;
$SUBJECT_DOMAIN =~ s/ 7231016 /LAES countries/;
$SUBJECT_DOMAIN =~ s/ 7231017 /Lafta countries/;
$SUBJECT_DOMAIN =~ s/ 7231018 /Mercosur countries/;
$SUBJECT_DOMAIN =~ s/ 7231019 /NAFTA countries/;
$SUBJECT_DOMAIN =~ s/ 7231020 /OCAS countries/;
$SUBJECT_DOMAIN =~ s/ 7231021 /OECD countries/;
$SUBJECT_DOMAIN =~ s/ 7231022 /OPEC countries/;
$SUBJECT_DOMAIN =~ s/ 7231023 /Third countries in the Mediterranean/;
$SUBJECT_DOMAIN =~ s/ 7231024 /Visegrad countries/;
$SUBJECT_DOMAIN =~ s/ 7231025 /WAEMU countries/;
$SUBJECT_DOMAIN =~ s/ 7236 /Political geography/;
$SUBJECT_DOMAIN =~ s/ 7236001 /Anzus countries/;
$SUBJECT_DOMAIN =~ s/ 7236002 /Arab League countries/;
$SUBJECT_DOMAIN =~ s/ 7236003 /CIS countries/;
$SUBJECT_DOMAIN =~ s/ 7236004 /Council of Europe countries/;
$SUBJECT_DOMAIN =~ s/ 7236005 /Former socialist countries/;
$SUBJECT_DOMAIN =~ s/ 7236006 /GCC countries/;
$SUBJECT_DOMAIN =~ s/ 7236007 /NATO countries/;
$SUBJECT_DOMAIN =~ s/ 7236008 /Nordic Council countries/;
$SUBJECT_DOMAIN =~ s/ 7236009 /OAS countries/;
$SUBJECT_DOMAIN =~ s/ 7236010 /United Arab Emirates countries/;
$SUBJECT_DOMAIN =~ s/ 7236011 /Warsaw Pact countries/;
$SUBJECT_DOMAIN =~ s/ 7236012 /WEU countries/;
$SUBJECT_DOMAIN =~ s/ 7241 /Overseas countries and territories/;
$SUBJECT_DOMAIN =~ s/ 7241001 /French Overseas Departments/;
$SUBJECT_DOMAIN =~ s/ 7241002 /French Overseas Territories/;
$SUBJECT_DOMAIN =~ s/ 7241003 /Netherlands OCT/;
$SUBJECT_DOMAIN =~ s/ 7241004 /United Kingdom OCT/;
$SUBJECT_DOMAIN =~ s/ 76 /INTERNATIONAL ORGANISATIONS/;
$SUBJECT_DOMAIN =~ s/ 7606 /United Nations/;
$SUBJECT_DOMAIN =~ s/ 7606001 /UN specialist institution/;
$SUBJECT_DOMAIN =~ s/ 7606002 /United Nations system/;
$SUBJECT_DOMAIN =~ s/ 7606003 /UNO/;
$SUBJECT_DOMAIN =~ s/ 7611 /European organisations/;
$SUBJECT_DOMAIN =~ s/ 7611001 /European organisation/;
$SUBJECT_DOMAIN =~ s/ 7616 /Extra-European organisations/;
$SUBJECT_DOMAIN =~ s/ 7616001 /African organisation/;
$SUBJECT_DOMAIN =~ s/ 7616002 /Afro-Asian organisations/;
$SUBJECT_DOMAIN =~ s/ 7616003 /American organisation/;
$SUBJECT_DOMAIN =~ s/ 7616004 /Arab organisation/;
$SUBJECT_DOMAIN =~ s/ 7616005 /Asian organisation/;
$SUBJECT_DOMAIN =~ s/ 7616006 /Latin American organisation/;
$SUBJECT_DOMAIN =~ s/ 7621 /Intergovernmental organisations/;
$SUBJECT_DOMAIN =~ s/ 7621001 /Intergovernmental organisation/;
$SUBJECT_DOMAIN =~ s/ 7626 /Non-governmental organisations/;
$SUBJECT_DOMAIN =~ s/ 7626001 /Non-governmental organisation/;
$SUBJECT_DOMAIN =~ s/ 1006007 /EU office or agency/;
$SUBJECT_DOMAIN =~ s/ 1016005 /European Communities/;
$SUBJECT_DOMAIN =~ s/ 1021006 /EU financing/;
$SUBJECT_DOMAIN =~ s/ 3206003 /Education/;
$SUBJECT_DOMAIN =~ s/ 5206006 /Climate change policy/;
$SUBJECT_DOMAIN =~ s/ 5606002 /Agricultural policy/;
$SUBJECT_DOMAIN =~ s/ 6406002 /Production/;
$SUBJECT_DOMAIN =~ s/ 6411006 /Biotechnology/;
$SUBJECT_DOMAIN =~ s/ 10 /EUROPEAN UNION/;
$SUBJECT_DOMAIN =~ s/ 1006 /EU institutions and European civil service/;
$SUBJECT_DOMAIN =~ s/ 1006002 /EU body/;
$SUBJECT_DOMAIN =~ s/ 1011001 /EU act/;
$SUBJECT_DOMAIN =~ s/ 1011002 /EU law/;
$SUBJECT_DOMAIN =~ s/ 1011003 /EU legal system/;
$SUBJECT_DOMAIN =~ s/ 1011004 /European treaties/;
$SUBJECT_DOMAIN =~ s/ 1021 /EU finance/;
$SUBJECT_DOMAIN =~ s/ 1021002 /EU expenditure/;
$SUBJECT_DOMAIN =~ s/ 1021003 /EU budget/;
$SUBJECT_DOMAIN =~ s/ 1021004 /Drawing up of the EU budget/;
$SUBJECT_DOMAIN =~ s/ 1021005 /Financing of the EU budget/;
$SUBJECT_DOMAIN =~ s/ 2011002 /EU customs procedure/;
$SUBJECT_DOMAIN =~ s/ 7241001 /French overseas department and region/;
$SUBJECT_DOMAIN =~ s/ 7241002 /French overseas collectivity/;
$SUBJECT_DOMAIN =~ s/ 7621 /World organisations/;
$SUBJECT_DOMAIN =~ s/ 7621001 /World organisation/;



			#print $errfile "SUBJECT_DOMAIN=$SUBJECT_DOMAIN\n";



			}

		#cdt		
		elsif($1 eq "<descrip type=\"Domain\">")			
			{
			$SUBJECT_DOMAIN=$2;
			}
		elsif($1 eq "<descrip type=\"subjectField\">")			 {$SUBJECT_DOMAIN=$2;}

		elsif($1 eq "<descrip type=\"Domain note\">")			{$SUBJECT_DOMAIN_NOTE=$2;}
		elsif($1 eq "<descrip type=\"Primary\">")			{$PRIMARY =$2; $PRIMARY =~ s/Primary/Yes/;}
		elsif($1 eq "<descrip type=\"Definition\">")			{$DEFINITION=$2;}
		elsif($1 eq "<descrip type=\"Definition reference\">")		{$DEFINITION_REFERENCE=$2;}
		elsif($1 eq "<descrip type=\"Related material\">")			{$RELATED_MATERIAL=$2;}
		elsif($1 eq "<descrip type=\"Term reference\">")		{$TERM_REF=$2;}
		elsif($1 eq "<descrip type=\"Term ref\">")			{$TERM_REF=$2;}
		#cdt		
		elsif($1 eq "<descrip type=\"Reliability\">")			{$RELIABILITY_VALUE=$2;}
		elsif($1 eq "<descrip type=\"reliabilityCode\">") 
			{
			$RELIABILITY_VALUE=$2;
			$RELIABILITY_VALUE =~ s/0/Downgraded prior to deletion/ig;
			$RELIABILITY_VALUE =~ s/1/Reliability not verified/ig;
			$RELIABILITY_VALUE =~ s/2/Minimum reliability/ig;
			$RELIABILITY_VALUE =~ s/3/Reliable/ig;
			$RELIABILITY_VALUE =~ s/4/Very reliable/ig;



			}

		#<descrip type="Reliability">Reliable</descrip></descripGrp>
		#<descrip type="Reliability">Reliable</descrip> </descripGrp>
		elsif($1 eq "<descrip type=\"Context\">")				{$CONTEXT=$2;}
		elsif($1 eq "<descrip type=\"Context reference\">")		{$CONTEXT_REFERENCE=$2;}
		elsif($1 eq "<descrip type=\"Evaluation\">")			{$TERM_EVALUATION=$2;}
		elsif($1 eq "<descrip type=\"Pre-IATE\">")				{$PRE_IATE=$2;}
		elsif($1 eq "<descrip type=\"Term note\">")			{$NOTE=$2;}
		#v5: elsif($1 eq "<descrip type=\"Institution ID\">")			{$INSTITUTION_ID=$2;}
		elsif($1 eq "<descrip type=\"Institution\">")			{$INSTITUTION_ID=$2;}
		#<descrip type="Institution">COM</descrip> </descripGrp>
		#unknown: <concept>	2
		#unknown: <transac type="modification">	Admin
		#cdt
		else                                                                 #
			{
			#print $errfile "line: $line\n"; 
			print $errfile "unknown!!!: ***S1=$1,S2=$2\n"; 
			}
		#print "$1: $2\n";
		}
	elsif($line =~ s/^(<\/descrip>)/$1/)	
		{;}
	elsif($line =~ s/^(<transac type=\"origination\">)/$1/)
		{;}
	elsif($line =~ s/^(<transac type=\"modification\">)/$1/)
		{;}
	elsif($line =~ s/^(<term>)([^<]+)/$1$2/)
		{$TERM=$2;}
#	elsif($line =~ s/^(<date>)([^<]+)/$1$2/)
#		{;}
	#cdt:


	elsif($line =~ s/<termNote type=\"termType\">([^<]+)/$1/)   
		{
		$TERM_TYPE=$1;
		$TERM_TYPE =~ s/fullForm//ig;
		}
	#cdt:
	elsif($line =~ s/<termNote type=\"administrativeStatus\">([^<]+)/$1/)   
		{$TERM_EVALUATION=$1;}

	elsif($line =~ s/^(<note>)([^<]+)/$1$2/)
		{$SUBJECT_DOMAIN_NOTE=$2;}


	elsif($line =~ s/^(<concept>)([^<]+)/$1$2/)
		{;}	
	#cdt
	elsif( ($line =~ s/<\/languageGrp>/$1/) || $line =~ s/<\/langSet/$1/)
	#elsif($line =~ s/<\/languageGrp>/$1/)
	#elsif($line =~ s/<\/langSet/$1/)
		{
		$LANGUAGE_CODE="";  
		$DEFINITION="";
		$DEFINITION_REFERENCE="";
		$RELATED_MATERIAL="";
		}
	#cdt
	elsif(($line =~ s/<\/tig>/$1/) || ($line =~ s/<\/termGrp>/$1/))
	#elsif($line =~ s/<\/termGrp>/$1/)
	#elsif($line =~ s/<\/tig>/$1/)
		{
		$termformat=$TERM;
		$TERM =~ s/&lt;BR&gt;/ /ig;
		$TERM =~ s/&lt;[a-z\/]+&gt;//ig;
		$TERM =~ s/&amp;/&/ig;
		if($termformat eq $TERM)
			{
			$termformat = "";
			}
		#else   #experiment to make tags more readable: does not work: they will not shown at all
		#	{
		#	$termformat =~ s/&lt;BR&gt;/<BR>/ig;
		#	$termformat =~ s/&lt;([a-z\/]+)&gt;/<$1>/ig;
		#	$termformat =~ s/&amp;/&/ig;
		#	}

		$LANGUAGE_CODE =~ s/SH/HR/;
		if($config eq "iateadmin")
			{
			print $outfile "$LIL_RECORD_ID\t$SUBJECT_DOMAIN\t$SUBJECT_DOMAIN_NOTE\t$PRIMARY\t$LANGUAGE_CODE\t$DEFINITION\t$DEFINITION_REFERENCE\t$RELATED_MATERIAL\t$TERM\t$termformat\t$TERM_REF\t$RELIABILITY_VALUE\t$CONTEXT\t$CONTEXT_REFERENCE\t$TERM_EVALUATION\t$PRE_IATE\t$NOTE\t$INSTITUTION_ID\n";
			}
		elsif($config eq "iatepublic")
			{
			print $outfile "$LIL_RECORD_ID\t$SUBJECT_DOMAIN\t$SUBJECT_DOMAIN_NOTE\t$LANGUAGE_CODE\t$TERM\t$termformat\t$TERM_TYPE\t$RELIABILITY_VALUE\t$TERM_EVALUATION\n";
			}

		#print $outfile "$TL_RECORD_ID\t$SUBJECT_DOMAIN\t$SUBJECT_DOMAIN_NOTE\t$LIL_RECORD_ID\t$PRIMARY\t$LANGUAGE_CODE\t$DEFINITION\t$DEFINITION_REFERENCE\t$RELATED_MATERIAL\t$TERM\t$TERM_REF\t$RELIABILITY_VALUE\t$CONTEXT\t$CONTEXT_REFERENCE\t$TERM_EVALUATION\t$PRE_IATE\t$NOTE\t$INSTITUTION_ID\n";
		$TERM="";
		$TERM_REF="";
		$RELIABILITY_VALUE="";
		$CONTEXT="";
		$CONTEXT_REFERENCE="";
		$TERM_EVALUATION="";
		$PRE_IATE="";
		$NOTE="";
		$INSTITUTION_ID="";
		$TERM_TYPE="";
		}	
	elsif(($line =~ s/<\/termEntry>/$1/) || ($line =~ s/<\/conceptGrp>/$1/))  
#cdt	elsif($line =~ s/<\/conceptGrp>/$1/)  
#	elsif($line =~ s/<\/termEntry>/$1/)  
		{
		$SUBJECT_DOMAIN="";
		$SUBJECT_DOMAIN_NOTE="";
		$LIL_RECORD_ID="";
		$PRIMARY="";
		}
	elsif($line =~ s/^<\/?transacGrp>/$1/)  	{;}
	elsif($line =~ s/^<\/?transac/$1/)  	{;}
	elsif($line =~ s/^<\/?date>/$1/)  	{;}
	elsif($line =~ s/^<\/?descripGrp>/$1/)  	{;}
	elsif($line =~ s/^<\/?languageGrp>/$1/)  	{;}

	elsif($line =~ s/^<\/?termGrp>/$1/)  	{;}
	#cdt:
	elsif($line =~ s/^<\/?tig>/$1/)  	{;}

	elsif($line =~ s/^<\/?concept>/$1/)  	{;}
	elsif($line =~ s/^<\?xml /$1/)  	{;}
	elsif($line =~ s/^<\/?mtf>/$1/)  	{;}

	#cdt
	elsif($line =~ s/^<\/?body>/$1/)  	{;}
	elsif($line =~ s/^<\/?text>/$1/)  	{;}
	elsif($line =~ s/^<\/?martifHeader>/$1/)  	{;}
	elsif($line =~ s/^<\/?fileDesc>/$1/)  	{;}
	elsif($line =~ s/^<\/?sourceDesc>/$1/)  	{;}
	elsif($line =~ s/^<\/?encodingDesc>/$1/)  	{;}
	elsif($line =~ s/^<martif type=\"TBX-Default\" xml:lang=\"en\">/$1/)  	{;}
	elsif($line =~ s/^<\/martif>/$1/)  	{;}
	elsif($line =~ s/^<p>This is a TBX file downloaded from the IATE website. Address any enquiries to iate\@cdt.europa.eu.<\/p>/$1/)  	{;}
	elsif($line =~ s/^<p type=\"XCSURI\">TBXXCS.xcs<\/p>/$1/)  	{;}

	elsif($line =~ s/^<descrip type="Evaluation">//)   {;}
	elsif($line =~ s/^<descrip type="IATE ID">//)   {;}
	elsif($line =~ s/^<descrip type="Definition">//)   {;}
	else
		{
		print $errfile "line=$line\n"; 

		}

	$count++;
	#print "$count: $line\n";
	}
	                	

close $infile;
close $outfile;
close $errfile;
