use strict;
use warnings;


my $line;
my $infile;
my $outfile;

if($ARGV[0] eq "")
	{
	print "Usage: input.txt output.txt\n";
	
	exit 1;
	}                  

open($infile, "<:encoding(UTF-8)", $ARGV[0] ) || die "can't open $ARGV[0]";
#BAD: 
#diff size open($outfile, ">:encoding(UTF-16)", $ARGV[1] ) || die "can't open $ARGV[1]";

#BAD valid file,  same size, only first char is bad in Excel
open($outfile, ">:raw :encoding(UTF-16LE)", $ARGV[1] ) || die "can't open $ARGV[1]";

#BAD, valid file, diff size cr+lf
#open($outfile, ">:raw :encoding(UTF-16):crlf", $ARGV[1] ) || die "can't open $ARGV[1]";



my $count = 0;
print $outfile "\x{FEFF}";
while ($line=<$infile>)
	{
	print $outfile $line;
	$count++ ;
	}
close $infile;  
close $outfile;  

                                                                                       