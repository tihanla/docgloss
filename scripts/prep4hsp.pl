use strict;
my $linecnt=1;

#open(my $infile, "<:encoding(utf8)", $ARGV[0] ) || die "can't open input: $ARGV[0]";
#open(my $outfile, ">:encoding(utf8)", $ARGV[1] ) || die "can't open output: $ARGV[1]";

open(my $infile, "<", $ARGV[0] ) || die "can't open input: $ARGV[0]";
open(my $outfile, ">", $ARGV[1] ) || die "can't open output: $ARGV[1]";

while(<$infile>)
	{
	chomp();
	
	s/^([^\n]*[^\\])[\/](\*?)([^\n]*)/$1$2$3/;

	#print "2=$2\n";
			
	if($2 eq "\*")
		{
		print $outfile "$1\n";	
		}
	else
		{
		print $outfile "$1\t$3\n";	
		}

	$linecnt++;

	}

