#replace nl from end of normal lines with space
#keep empty lines

my $outline="";

while(<>)
	{
	if(/^[^\r\n]/)
		{
		chomp();
		my @arr =split('\t', $_, -1);   #break line to words
		my $arrsize=@arr;
		if (($arrsize == 1) || (($arrsize == 2) && ($arr[1] eq "") ))  #only surf
			{
			#without *
			$outline =$outline . $arr[0] . "/" . $arr[0] . " ";
			#with * : 
			#$outline =$outline . $arr[0] . "/*" . $arr[0] . " ";
			}
		else
			{
			$outline =$outline . $arr[0] . "/" . $arr[1] . " ";
			}
	
		}
	else     
		{
		if($outline ne "")
			{
			$outline =~ s/ $/\n/;   #replace last space  with new line
			print "$outline" ;
			$outline ="";
			}
		else
			{
			print "\n";
			}
		}
	}
if($outline ne "")
	{
	print $outline;
	}

	