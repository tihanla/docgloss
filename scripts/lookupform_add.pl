
#mergeterms infile1 N infile2 M outfile
#lines of infile2 (together with their info) is written into outfile if its 1st field occurs in infile1 (plain list)
use strict;
use Getopt::Long;

my $in;
my $keyfield;
my $valfield;
my $cnt1;
my $cnt1;
my $i;
my $in1head;
my $in2head;

my $cnt2;
my $cnth1;
my $found;
my $idkey;
my $idkey_lc;
my $idkeyfield;
my $idval;
my $idval_lc;
my $in1field;
my $in2field;
my $index;                                                                                                                                           
my %hash_src;
my %hash_src_lc;


my $entrysize=0;
my $langsize=0;
my $termsize=0;
my $keypos=0;  
my $langpos=0;
my $termpos=0;
my $termtypepos=0;
my $relypos=0;
my $evalpos=0;
my $primarypos=0;
my $badcompos=0;
my $lookuppos=0;
my $mode="";
my $udic="";
my $do_hyphen="N";
my $do_udic="N";

my $w1;

my $idlookup;
my %idlookuphash;

my $result = GetOptions (
"mode=s" => \$mode,
"entry=s" =>\$entrysize,
"lang=s" =>\$langsize,
"term=s" =>\$termsize,
"idcol=s" =>\$keypos,
"langcol=s" =>\$langpos,
"termcol=s" =>\$termpos,
"termtypecol=s" =>\$termtypepos,
"relycol=s" =>\$relypos,
"evalcol=s" =>\$evalpos,
"primarycol=s" =>\$primarypos,
"badcomcol=s" =>\$badcompos,
"lookupcol=s" =>\$lookuppos,
"udic=s" =>\$udic,
"do_hyphen=s" =>\$do_hyphen,
"do_udic=s" =>\$do_udic,

#"inxcol=s" =>\$inxpos,
);  # flag


my $argc=@ARGV;

my $outfile;
my $infile;
my $udicfile;

my $entrymax=$entrysize;
my $langmax=$entrymax + $langsize;
my $termmax=$langmax + $termsize;

if ( ($argc ne 4) || ($mode eq "" ) || ($entrysize == 0) || ($langsize == 0) || ($termsize == 0 ) || ($keypos == 0) || ($langpos == 0) || ($termpos == 0)|| ($lookuppos == 0))
	{
	if ($argc ne 4) {print "argc=$argc\n";}
	if($mode eq "" ){print "mode=$mode\n";}
	if($keypos == 0 ){print "missing idcol=\n";}
	if($langpos == 0 ){print "missing langcol=\n";}
	if($termpos == 0 ){print "missing termcol=\n";}
	if($entrysize == 0 ){print "missing entry=\n";}
	if($langsize == 0 ){print "missing lang=\n";}
	if($termsize == 0 ){print "missing term=\n";}

	print  "Usage:\n";
	print  "perl iatepairs.pl input output src trg -udic=path -do_hyphen=Y -do_udic=Y -mode=list|pairs  -entry=N -lang=N  term=N -idcol=N -langcol=N -termcol=N -badcomcol=N -primarycol=N [-relycol=N] [-evalcol=N] [-errfile=filename] [-termtypecol=N] \n";
	print  "input.txt: tab separated table with one monolingual term perl line, IATE export from SQL or XML, UTF-16 text format\n";
	print  "output.txt: tab separated bilingual term entries file, paired by identical IATE ID-s \n";
	print  "src and trg: two letter IATE language identifiers of the source and target language\n";

	print  "-udic=PATH:  path of the user dictionary, tab separated file with two colums: lookupform and term\n";
	print  "-do_udic: add lookup forms from user dictionary\n";
	print  "-do_hyphen: add hyphenated compound lookupforms by deletinf the hyphens\n";
	print  "-mode=list: output synonyms are in listd format, if imported by GlossaryConverter.exe looks similarly to IATE GUI\n";
	print  "-mode=pair: synonyms are paired in every possible combinations, can be used by other programs, needs simple tabular input\n";
	print  "-entry: number of entry level colums in input\n";
	print  "-lang: number of lang level colums in input\n";
	print  "-term: number of term level colums in input\n";
	print  "-idcol: column of the ID field (column count starts with 1)\n";
	print  "-langcol: column of the language field\n";
	print  "-termcol: column of the term field\n";
	print  "-termtypecol: column of the termtype field\n";
	print  "-relycol: optional filter based on Reliability field (Reliability not verified)\n";
	print  "-evalcol: optional filter based on Evaluation field (Depricated)\n";
	print  "-primarycol: column of the Primary field\n";
	print  "-badcomcol: bad commission column field\n";
	print  "-badlookup: lookup column field\n";
	print  "contact info:  laszlo.tihanyi\@ext.ec.europa.eu\n";
	exit 1;
	}                  


open($infile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
binmode($infile, ":encoding(UTF-8)");

open($outfile, ">", $ARGV[1] ) || die "can't open $ARGV[1]";
binmode($outfile, ":raw:encoding(UTF-8)");

my $src = $ARGV[2];
my $trg = $ARGV[3];

if($do_udic eq "Y")
	{
	open($udicfile, "<", $udic ) || die "can't open $udic";
	binmode($udicfile, ":encoding(UTF-8)");
	}


#$do_hyphen=1;
#$do_udic=0;  #turned off until lookup forms revised manually


#binmode($outfile, ":raw");


#print "infile1=$infile1\n";
#print "in1field=$in1field\n";
#print "infile1=$infile2\n";
#:print "in2field=$in2field\n";



#open(my $logfile, ">", "logfile.txt" ) || die "can't open logfile}.txt";

#binmode($logfile, ":raw:encoding(UTF-16)");

#print "indexing started: in2field=$in2field\n";                      

#$keypos=1;
#$termpos=10;
#$termtypepos=12;

#lookup file1 lines in file2 database:
my $inhead = <$infile>;
print $outfile $inhead;

my $term;
my $termlookup;
my %udichash;

my $udicl;

if($do_udic eq "Y")
	{
	#skip header:
	#ID	LANG	TERM	LOOKUP-FORM
	$udicl = <$udicfile>;
	while ($udicl = <$udicfile>)
		{
		$udicl =~ s/\r\n//g;
		$udicl =~ s/\n//g;
		my @udicarray = split ('\t', $udicl, -1);
		$udichash{$udicarray[0]}=$udicarray[3];
		#print "udichash{$udicarray[0]}=$udicarray[3]\n";
		}
	close $udicfile;
	}

while ($in = <$infile>)
	{
	#chomp($in);
	$in =~ s/\r\n//g;
	$in =~ s/\n//g;
	my @inarray = split ('\t', $in, -1);

	#$cnt1=1;
	#only on src side:
	#print "src=$src, langpos=$langpos, inarray[$langpos]=$inarray[$langpos]\n";

	$term = $inarray[$termpos-1];
	$termlookup=$term;

	print $outfile "$in\n";

	#target language  cannot generate lookup forms:
	if( $src ne $inarray[$langpos-1])
		{
		; #nothing to do
		}
	#if it contains a hyphen than replace hyphen with space everywhere except if it connects a single character word to another like "e-mail"
	#if hyphenated:		
	$w1=0;  #marks if there was any replacement 
	if(($do_hyphen eq "Y") && ($termlookup =~ s/-/ /g))  
		{
		#print "$term\n";                               		
		#print "$term\n" ;  #DEBUG
		
		my @termarray = split (' ', $term, -1);
		my $termarrsize = @termarray;
		$termlookup="";
		for (my $ti=0; $ti != $termarrsize; $ti++)
			{
			my @wordarray = split ('-', $termarray[$ti], -1);
			my $wordarrsize = @wordarray;
			#print "termarray[$ti]=$termarray[$ti], wordarrsize=$wordarrsize\n";   #DEBUG
			if($termlookup ne "")
				{
				$termlookup = $termlookup . " ";   
				}
			if($wordarrsize == 1)
				{
				$termlookup = $termlookup . $termarray[$ti];   #does not contaiin -
				}
			else
				{
				for (my $wi=0; $wi != $wordarrsize-1; $wi++)
					{
					$termlookup = $termlookup . $wordarray[$wi];   #does not contaiin -
					my $wil0 = length ($wordarray[$wi]);
					my $wil1 = length ($wordarray[$wi+1]);
					#print "$wordarray[$wi]:wil0=$wil0, $wordarray[$wi+1]:wil1=$wil1\n";  #DEBUG
					if( ($wil0 !=1) && ($wil1 != 1)) 
						{
						$w1=1;
						$termlookup = $termlookup . " ";   
						}
				else
						{
						$termlookup = $termlookup . "-";   
						}
					}
				$termlookup = $termlookup . $wordarray[$wordarrsize-1];   #does not contaiin -
				}
			
			}
		}



	#print lookup into the last column and set term type to Lookup
	if ($w1==1)  #flag for 1 char long word part
		{
		for (my $i=0; $i!=$termmax;$i++)
			{
			if($i == $keypos-1)
				{
				print $outfile $inarray[$i]; 
				}				
			if($i == $langpos-1)
				{
				print $outfile $inarray[$i]; 
				}				
			if($i == $termpos-1)
				{
				#print "$termlookup\n";  #DEBUG
				print $outfile $termlookup; 
				}				
			if($i == $termtypepos-1)
				{
				print $outfile "Lookup"; 
				}      

			if($i == $lookuppos-1)
				{
				print $outfile $term; 
				}
			if($i != $termmax-1)
				{
				print $outfile "\t";
				}				
			}	
		
		print $outfile "\n";
		}

	
	$w1=0;
	#lookup form in a user dictionary:
	#print "lookup udichash{%inarray[$idpos-1]}=$udichash{%inarray[$idpos-1]}\n";
	if( ($do_udic == "Y") && (exists $udichash{$inarray[$keypos-1]}))
		{
		#one entry can get only one lookupform once
		$idlookup= $inarray[$keypos-1] . "-" . $udichash{$inarray[$keypos-1]};
		if(! exists $idlookuphash{$idlookup})
			{
			$termlookup = $udichash{$inarray[$keypos-1]};
	                $idlookuphash{$idlookup} = 1;
			$w1=1;
			}
		}



	#print lookup into the last column and set term type to Lookup
	if ($w1==1)  #flag for 1 char long word part
		{
		for (my $i=0; $i!=$termmax;$i++)
			{
			if($i == $keypos-1)
				{
				print $outfile $inarray[$i]; 
				}				
			if($i == $langpos-1)
				{
				print $outfile $inarray[$i]; 
				}				
			if($i == $termpos-1)
				{
				#print "$termlookup\n";  #DEBUG
				print $outfile $termlookup; 
				}				
			if($i == $termtypepos-1)
				{
				print $outfile "Lookup"; 
				}      

			if($i == $lookuppos-1)
				{
				print $outfile $term; 
				}
			if($i != $termmax-1)
				{
				print $outfile "\t";
				}				
			}	
		
		print $outfile "\n";
		}
}
	                                         
close $infile;
close $outfile;

