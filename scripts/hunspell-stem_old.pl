# reads the HFST analysed text (one segment one line) and the a file which contains unknown words word exported and analysed by Hunspell (one word by line)
# merge the two files: takes words which HFST could not identify but had a succesfull and usabel Hunspell analysis


use strict;
#$|++;
     
my $file;
my $out;
my @sarr;
my $sarrsize;
my $si;
my $hspline;
my $line;
my $lineout;
my $hspfile;
my $outfile;
my $stmcnt;
my $lastst;
my @resarr;
my $resarrsize;
my @marr;
my $marrsize;
my @earr;
my $block;
my $earrsize;
my $i;
my $stem;
my $lang;
my $altmor;
my $mi;

if ( @ARGV ne 4)
	{
	print "Usage:\nhunspell.pl lang input hunspell_input out\n";
	exit;
	}

$lang=$ARGV[0];

open ($file, "<", $ARGV[1])|| die "can't open $ARGV[1]";
open ($hspfile, "<", $ARGV[2])|| die "can't open $ARGV[2]";
open ($outfile, ">", $ARGV[3])|| die "can't open $ARGV[3]";
                                        

my $cnt=1;

while($line = <$file>)
	{
	
	#print "cnt=$cnt ";
	print "line=$line\n";    #DEBUG
	$cnt++;

	#print $outfile "$cnt\n";  #DEBUG


	@sarr =split(' ', $line, -1);   #break line to words
	$sarrsize=@sarr;
	#print "sarrsize=$sarrsize\n";  #DEBUG
	$lineout = "";
	if($sarrsize == 0)  #empty line
		{
		$hspline=<$hspfile>;
		print "hspline=$hspline\n";  #DEBUG
		}
 
	for($si=0;$si!=$sarrsize; $si++)	 #iterate on words
		{
		$hspline=<$hspfile>;
		print "sarr[$si]=$sarr[$si], hspline=$hspline\n";   #DEBUG
		chomp($hspline);
		if($sarr[$si] =~ /[^\\]\/\*/)   #if the word is unknown  use hsp results
			{
			#print "\ninput=$hspline\n";  #DEBUG

			$hspline =~  s/\t+$//;	#chomp last tab, if any
			@resarr =split('\t', $hspline, -1);   #break ana into elements
			$resarrsize=@resarr;
			$stmcnt=0; $block=0;
			$stem=""; $lastst=-1;

			#if $lang=en:
			if($lang eq "en")
				{
				for($i=1; $i < $resarrsize; $i++)	 #use the second element (first is surfface)
					{
					#print "resarr[$i]=$resarr[$i]\n";   #DEBUG
					if( $resarr[$i] =~ /st:/)  #is stem
						{
						$stmcnt++;
						$lastst=$i;				
						}
					if( $resarr[$i] =~ /fl:(F|f|A)/)  #stem parts A:anti F:preF, f:
						{
						$block=1;
						}
					}
					
				if($resarr[0] =~ /[0-9]/ )  #dont use the result if the surface contains number, Hunspell forgerts to return the numbers in stem
					{
					$block = 1;
					}       	

				print "resarrsize=$resarrsize, stmcnt=$stmcnt, block=$block\n";   #DEBUG
				        	
				if( ($resarrsize == 2) && ($stmcnt == 1) && ($block == 0))  #if there is only one result (+ surface =2), and it had no blocking suffix 
					{
					$stem = $resarr[$lastst];
					$stem =~ s/^[^ ]+  st:([^ \t]+)/\1/;
					#$lineout = $lineout . " +++" ;  #DEBUG
					$lineout = $lineout . $resarr[0] . "/" . $stem;  
					}
				else	#we cannot use the hunspell result
					{
					#$lineout = $lineout . " ---" ;  #DEBUG
					$lineout = $lineout . " " . $resarr[0] . "/*"; #. $resarr[0];
			
					}
				print "lineout = $lineout\n";   #DEBUG

				}
			#if lang=hu
			#hyphenated: dont use (hyph compounds, suffixed numbers, foreign words with numbers)  (hu,cs)
			#list with pipes: take the first  (hu, cs:na)
			#list with tabs: take the first  (hu,cs)
			#concat: st:  (hu,cs)
			#concat: sp:  (hu, cs:na)
			if($lang ne "en" )   #hu, cs
				{
				if ($resarr[0] =~ /-/)  #  block everything with hyphen. Numbers with suffix, hyphenated compounds are usually wrong 
					{
					$block = 1;
					}
				
				#print "resarr[0]=$resarr[0]\n";  #DEBUG


				for($i=1; $i!=2; $i++)	 #used the second element (the first is the surface)
					{
					#print "resarr[$i]=$resarr[$i]\n";   #DEBUG

					@marr =split(' ', $resarr[$i], -1);   #res element to morpholigical elements separated by space
					$marrsize=@marr;
					#print "marrsize=$marrsize\n";
					$altmor=0;   #alternative morph elements in the for of (x|y)  we take the first by stopping at the pipe
					for($mi=0; $mi!=$marrsize; $mi++)	 #iterate on morphs separated by space
						{
						#print " marr[$mi]=$marr[$mi]\n";   #DEBUG
						if($altmor == 1)
							{
							next;
							}
						@earr =split(':', $marr[$mi], -1);   #res element to morpholigical elements separated by space
						$earrsize=@earr;         
						#print "  earrsize=$earrsize earr[0]=$earr[0]  earr[1]=$earr[1] \n";    #DEBUG
						if($earrsize < 2)
							{
							if($earr[0] eq "\|")
								{
								$altmor =1;
								}
							next;  
							}

						if( $earr[0] =~ /(st|sp)/)  #st: stem, sp: surface prefix
							{
							if($stem ne "")
								{
								$stem = $stem . "+";
								}
							$stem = $stem . $earr[1];
							#print "stem=$stem\n";
							}

						if( $earr[0] =~ /pa/)  #pa: part of compound, one stem part might missing  (da,de,et,nl,sv), (hu is correct...)
							{
							if ($lang = /(da|de|et|nl|sv)/)
								{
								$block=1;
								#print "stem=$stem\n";
								}
						#else 	{print "non st: $earr[0]\n";}							
							}
						}
					}
	

				if( ($stem ne "") && ($block == 0))  
					{
					$lineout = $lineout . " " . $resarr[0] . "/" . $stem;
					print "stem=$stem\n";  #DEBUG
					}
				else	#we cannot use the hunspell result
					{
					#$lineout = $lineout . " ---" ;  #DEBUG
					$lineout = $lineout . " " . $resarr[0] . "/*"; #. $resarr[0];
					print "lineout=$lineout\n";
					}

				}  #hu

			}  #if we try to use hunspell results 
		else
			{
			$lineout = $lineout . " " . $sarr[$si];
			}
		}
	$lineout =~ s/ $//;
	$lineout =~ s/^ //;
	print $outfile "$lineout\n";
	}
