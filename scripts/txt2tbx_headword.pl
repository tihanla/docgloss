#sorts a doc. spec IATE database where terms are either in term field or in form field

use strict;
#prints out the columns of table in the specified order

my $insize;
my @inarray;
my $argc;
my $in;
my $indb;
#my $dbfile;
#my $srclang;
 #my $lang;	#trglang
#my %hash_headword;
#my %hash_src_term;
my $termcol;
my $formcol;

my %hash_headword_src;
my %hash_headword_srcf;
my %hash_headword_trg;
my %hash_headword_srcf_dup;
my %hash_sort_headword_trg;
my %hash_headword_trg_term;
my %hash_sort_trg;

my %hash_sort_trg_lc;


my $tindex;
my $tkey;
my $trg_term_key;

my $srcf_content;

my $term;
my $form;
my $headword;
my $index;
my $idkey;
my $idval;

my  @trg_term_key_arr;

my $trg_out;

my $argc=@ARGV;
#my $hw;
my $referring;

my $prev_src_term;
my $prev_form;
#print "argc=$argc\n";
my $testformnum;

my $src_entry;
my $trg_entry;

my $src_langx;
my $srcf_langx;
my $trg_langx;


my $order;
my $table;
my $ordersize;
my $insize;
my @orderarray;
my @inarray;
my @headarray;
my @cfgarray;                                                                                               
my @tigarray;
my $argc;
my $in;
my$orderi;



my $entry_size;
my $lang_size;
my $term_size;
#my $badcom=N;
#my $form=N;

my $i;
my $ti;
my $tigsize;

#my $id_col;
#my $lang_col;
#my $term_col;

my $argc=@ARGV;
#print "argc=$argc\n";


my $prevterm="";
my $entry_level;
my $src_lang_level;
my $src_term_level;
my $trg_lang_level;
my $trg_term_level;

my $src_termt;
my $trg_termt;
my $src_term;
my $trg_term;

my $src_lang;
my $trg_lang;
###


my $count =1;

$count=1;


#beg------------
if ( $argc ne 3)
	{
	print  "Usage:\n";
	print  "perl txt2tbx.pl configfile input.txt output.tbx \n";
	exit ;
	}

open(my $cfgfile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
open(my $infile, "<:encoding(utf8)", $ARGV[1] ) || die "can't open $ARGV[1]";
open(my $outfile, ">:encoding(utf8)", $ARGV[2] ) || die "can't open $ARGV[2]";



print $outfile "<?xml version='1.0'?>\n";
print $outfile "<!DOCTYPE martif SYSTEM \"TBXcoreStructV02.dtd\">\n";
print $outfile "<martif type=\"TBX\" xml:lang=\"en\">\n\n";
print $outfile "<martifHeader>\n<fileDesc>\n<titleStmt>\n\t<title>IATE extraction</title>\n</titleStmt>\n";
print $outfile "<sourceDesc>\n\t<p>This termbase has been distributed by the Terminology Coordination Unit of the Commission</p>\n";
print $outfile "\t<p>DTD source: http:\/\/www.ttt.org\/oscarStandards\/tbx\/TBXcoreStructV02.dtd</p>\n</sourceDesc>\n</fileDesc>\n</martifHeader>\n\n";


print $outfile "<text>\n";
 print $outfile "<body>\n\n";



while ($in = <$cfgfile>)
	{
	#if ($in =~ s/^set //)
	$in =~ s/^set //;     #also works for linux config, without 'set' keyword
		#{
		@cfgarray = split ('=', $in, -1);
		if($cfgarray[0] eq "entry")
			{
			$entry_size=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "lang")
			{
			$lang_size=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "term")
			{
			$term_size=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "formc")
			{
			$formcol=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "termc")
			{
			$termcol=$cfgarray[1];
			}
		#}
	}
#end---------

$count=1;



while ($in = <$infile>)
	{
	#print $outfile  $in;
	#next;
	chomp($in);

	@inarray = split ('\t', $in, -1);
	$insize=@inarray;


	#save field names in header:
	if( ($count == 1) )
		{
		@headarray=@inarray;
		$count++;
		next;
		}



###	$in= $hash_headword{$head};
	
	#chomp($in);

	$in =~ s/\&([a-z]+[^a-z;])/&amp;\1/ig;  #quote & if it soes not look like a character entity
	$in =~ s/\&([a-z]+[^a-z;])/&amp;\1/ig;  #needs duplication because of &X& constructions  (X is any single character)
	
	$in =~ s/\&([^a-z])/&amp;/ig;           #as a result both & and &amp;  will be &amp;  on the output! 

#print "in=$in\n";  #DEBUG

	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print $outfile "in=$in\n";  #DEBUG


	# entry_size + 1 (TERM) + lang_size + term_size-1  | 1 (TERM) + lang_size + term_size-1 
	
	#read out: current term and find out if we create a new entry or concat to previous
	#print $outfile "<termEntry>\n";	
	$entry_level="";
	for($i=0 ;$i != $entry_size; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        $entry_level = $entry_level . "\t\t<descrip type=\"" . $headarray[$i] . "\">";
			$entry_level = $entry_level . $inarray[$i];
	        	$entry_level = $entry_level . "</descrip>\n";

			}
		}


	#source side----------------------------
        #print $outfile "<langSet xml:lang=\"", $inarray[$entry_size+1], "\">\n";
	#$src_lang = "<langSet xml:lang=\""; 
	#$src_lang = $src_lang . $inarray[$entry_size+1];
	#$src_lang = $src_lang .  "\">\n";

	$src_lang_level="";
	for($i=$entry_size+2;$i != $entry_size+1+$lang_size; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        $src_lang_level = $src_lang_level .  "\t\t<descrip type=\"" . "S." . $headarray[$i] . "\">";  #mark it with S.for possibly use on trg side
			$src_lang_level = $src_lang_level .  $inarray[$i];
	        	$src_lang_level = $src_lang_level .  "</descrip>\n";

			}
		}


	$src_term =  $inarray[$entry_size];
      	

	$src_term_level="";
	for($i=$entry_size+1+$lang_size;$i != $entry_size+$lang_size+$term_size; $i++)	
		{
		#@tigarray = split ('\|', $inarray[$i], -1);   #input does not contain | (replaced to ,)
		if ($headarray[$i] eq "") {;}
		elsif ($headarray[$i] eq "COM_BAD_MT") {;}
		elsif ($inarray[$i] eq "") {;}
		else
			{
			if($inarray[$i] ne "")
				{
			        $src_term_level = $src_term_level .  "\t\t<descrip type=\"" . $headarray[$i] . "\">";
				$src_term_level = $src_term_level . $inarray[$i];
			        $src_term_level = $src_term_level . "</descrip>\n";
				}
			}
		}
	
	
	#target side-------------------------------
	my $t=$entry_size+$lang_size+$term_size;

        #print $outfile "<langSet xml:lang=\"", $inarray[$t+1], "\">\n";
                                                             #
	$trg_lang_level="";

	#$trg_lang = "<langSet xml:lang=\""; 
	#$trg_lang = $trg_lang . $inarray[$t+1];
	#$trg_lang = $trg_lang .  "\">\n";


	for($i=$t+2;$i != $t+$lang_size+1; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        $trg_lang_level = $trg_lang_level . "\t\t<descrip type=\"" . $headarray[$i] . "\">";
			$trg_lang_level = $trg_lang_level .  $inarray[$i];
	        	$trg_lang_level = $trg_lang_level .  "</descrip>\n";
			}
		}


	
	
      	$trg_term =   $inarray[$t];

	$trg_term_level="";
	for($i=$t+$lang_size+1; $i != $t+$lang_size+$term_size; $i++)	
		{
		if ($headarray[$i] eq "") {;}
		elsif ($headarray[$i] eq "COM_BAD_MT") {;}
		elsif ($inarray[$i] eq "") {;}
		else
			{
	
			if($inarray[$i] ne "")
				{
		        	$trg_term_level = $trg_term_level . "\t\t<descrip type=\"" . $headarray[$i] . "\">";
				$trg_term_level = $trg_term_level . $inarray[$i];
        			$trg_term_level = $trg_term_level . "</descrip>\n";
				}
			}
		}
###########
	

	$form=$inarray[$formcol-1];
	$term=$inarray[$termcol-1];

	$testformnum=$form;

	$src_langx="";
	$srcf_langx="";
	$trg_langx="";


	if( ($testformnum =~ s/^[0-9]+$// ) || ($form eq "") )
	#if( (($form > 0 ) && ($form < 1000 )) || ($inarray[$formcol-1] eq "0"))
		{
		$headword=$term;

		#print "headword-term=$headword\n";  #DEBUG
		$src_langx = "<langSet xml:lang=\""; 
		$src_langx = $src_langx . $inarray[$entry_size+1];
		$src_langx = $src_langx .  "\">\n";

		$src_langx = $src_langx . "\t<tig>\n";	
		$src_langx = $src_langx . "\t\t<term>$src_term</term>\n";
		$src_langx = $src_langx . "\t\t<descrip type=\"Frequency\">$form</descrip>\n";
		$src_langx = $src_langx .  "\t</tig>\n";	
		$src_langx = $src_langx .  "</langSet>\n";	

		#add headword to index:
		$hash_headword_src{$headword}=  $src_langx;  #headword is not index: there can only 1!


		#add target langset:
#		$trg_langx = "<langSet xml:lang=\""; 
#		$trg_langx = $trg_langx . $inarray[$t+1];
		$trg_lang =  $inarray[$t+1];
#		$trg_langx = $trg_langx .  "\">\n";

#		$trg_langx = $trg_langx . "\t<tig>\n";	
#		$trg_langx = $trg_langx . "\t\t<term>$trg_term</term>\n";
		$trg_langx = $trg_langx . "$entry_level";
		if($trg_lang_level eq "")  #missing in many cases, so we can use sours lang level elements
			{
			$trg_lang_level=$src_lang_level;
			}
        	$trg_langx = $trg_langx . "$trg_lang_level";
	
		$trg_langx = $trg_langx . "$trg_term_level";
		

#		$trg_langx = $trg_langx . "\t</tig>\n";	
#		$trg_langx = $trg_langx . "</langSet>\n";	

		for ($index=0;;)
			{
			$idkey = $headword . "\t" .  $index;
			#print "idkey=$idkey\n";
			#old if(exists  $hash_headword{$idkey})
			if(exists  $hash_headword_trg{$idkey})
				{
				#if we have a key, check if the surf is the same (this may happen because of lowercasing)
				#if identical then increase the frequency  otherwise creta new index item
				$index++;
				}
			else    	
				{
				#$hash_headword_src{$idkey}=  $in;
				$hash_headword_trg{$idkey}=  $trg_langx;
				$hash_headword_trg_term{$idkey}=  $trg_term;
#print "hash_headword_trg{$idkey}=$trg_langx\n";  #DEBUG
				$count++;
				last;
				}
			}

	
		}
	
	else
		{
		$headword=$form;

		$srcf_langx = "<langSet xml:lang=\""; 
		$srcf_langx = $srcf_langx . $inarray[$entry_size+1];
		$srcf_langx = $srcf_langx .  "\">\n";

		$srcf_langx = $srcf_langx . "\t<tig>\n";	
		$srcf_langx = $srcf_langx . "\t\t<term>$src_term</term>\n";
		$srcf_langx = $srcf_langx .  "\t\t<descrip type=\"Termtype\">Lookup</descrip>\n";
		$srcf_langx = $srcf_langx .  "\t</tig>\n";	
		$srcf_langx = $srcf_langx .  "</langSet>\n";	
		#print "headword-form=$headword\n";  #DEBUG


		for ($index=0;;)
			{
			$idkey = $headword . "\t" .  $index;
			#print "idkey=$idkey\n";
			#old if(exists  $hash_headword{$idkey})
			if(exists  $hash_headword_srcf{$idkey})
				{
				#if we have a key, check if the surf is the same (this may happen because of lowercasing)
				#if identical then increase the frequency  otherwise creta new index item
				$index++;
				}
			else
				{
				#$hash_headword_src{$idkey}=  $in;
				$hash_headword_srcf{$idkey}=  $srcf_langx;
				#print "hash_headword_srcf{$idkey}=$srcf_langx\n";  #DEBUG
				$count++;
				last;
				}
			}

		}


	$count++;
	}	


	


	
#reset input, we read it again

my $count =1;
foreach my $head (sort  keys %hash_headword_src) 
	{
#my @inxarray;
my $hw;
#@inxarray = split ('\t', $head -1);
#$headword = $inxarray[0];
	
	print $outfile "<termEntry>\n";	
		
	print $outfile "$hash_headword_src{$head}";  #DEBUG
	
	$index =0;
	

	for ($index=0;;$index++)
		{
		$idkey = $head . "\t" .  $index;
		if(! exists $hash_headword_srcf{$idkey})
			{
			last;
		        }
		#allomorph will link to stems for all IATE-ID-s, we want only 1 reference:
		$srcf_content=$hash_headword_srcf{$idkey};
		if(! exists $hash_headword_srcf_dup{$srcf_content})
			{
			print $outfile "$srcf_content";
			}
		$hash_headword_srcf_dup{$srcf_content}=1;
		}

	
	$index =0;
	%hash_sort_trg=();  #empty
	for ($index=0,$tindex=0;;$index++)
		{
		$idkey = $head . "\t" .  $index;
		if (! exists $hash_headword_trg{$idkey})
			{
			last;
		        }
		#old: print all target terms unsorted with duplicates:
		#print  $outfile "$hash_headword_trg{$idkey}";

		#create a sorted target term set from terms belonging to this source headword:
		$trg_term=$hash_headword_trg_term{$idkey};
		$trg_term_key = $trg_term . "\t".  $tindex;
		$hash_sort_trg{$trg_term_key}=$hash_headword_trg{$idkey};

		#2
		#my $trg_term_key_lc= lc $trg_term_key;
		#$hash_sort_trg_lc{$trg_term_key_lc}=$trg_term_key_lc;

		$tindex++;
		#print $outfile "idkey=$idkey, trg_term_key=$trg_term_key\n";
		}


	my $prev_trg="";

	#1:  1: ascii sort, 2: case insensitive sort (to do)
	foreach my $trg_term_key (sort keys %hash_sort_trg)
	#2:
	#foreach my $trg_term_key_lc (sort keys %hash_sort_trg_lc)
		{
		$trg_out="";

		#2:
		#$trg_term_key=$hash_sort_trg_lc{$trg_term_key_lc};
			
		@trg_term_key_arr=split("\t",$trg_term_key,-1);

#		print $outfile "prev_trg=$prev_trg  trg_term_key_arr[0]=$trg_term_key_arr[0]\n";  #DEBUG

		if(($prev_trg ne $trg_term_key_arr[0]) && ($prev_trg ne ""))
			{
			$trg_out = $trg_out . "\t</tig>\n";	
			$trg_out = $trg_out . "</langSet>\n";	
			}

		
		if($prev_trg ne $trg_term_key_arr[0]) 
			{
			$trg_out = $trg_out . "<langSet xml:lang=\""; 
			$trg_out = $trg_out . $trg_lang;
			$trg_out = $trg_out .  "\">\n";

			$trg_out = $trg_out . "\t<tig>\n";	
			$trg_out = $trg_out . "\t\t<term>$trg_term_key_arr[0]</term>\n";
		        }
		#1:
		$trg_out = $trg_out . "$hash_sort_trg{$trg_term_key}";
		#2:
		#$trg_out = $trg_out . "$hash_sort_trg_lc{$trg_term_key_lc}";

		print  $outfile "$trg_out";
		
		#1
		$prev_trg=$trg_term_key_arr[0];
		#2:
		#$prev_trg=lc $trg_term_key_arr[0];

		}
	if($trg_out ne "")
		{
		print  $outfile "\t</tig>\n";	
		print  $outfile "</langSet>\n";	
		}

	#print "hash_headword_trg{$idkey}=$hash_headword_trg{$idkey}\n";	



	print $outfile "</termEntry>\n\n";	
	}	

	

print $outfile "</body>\n";
print $outfile "</text>\n";
print $outfile "</martif>\n";

close $infile;
close $outfile;
