=pod
1	TL_RECORD_ID		
2	Domain			
3	Domain note		
4	IATE-ID			
5	Primary			
6	EN			
7	LANGUAGE_CODE		
8	Definition		
9	Definition reference	
10	Related material	
11	Term reference		
12	Reliability		
13	Context			
14	Context reference	
15	Evaluation		
16	Pre-IATE		
17	Term note		
18	Institution		
19	HU			
20	LANGUAGE_CODE		
21	Definition		
22	Definition reference	
23	Related material	
24	Term reference		
25	Reliability		
26	Context			
27	Context reference	
28	Evaluation		
29	Pre-IATE		
30	Term note		
31	Institution		

6-19-4-2-3-5-8-9-10-11-12-13-14-15-16-17-18-21-21-23-24-25-26-27-28-29-30-31



1	INSTITUTION_ID
2	IATE-ID
3	TL_RECORD_ID
4	EN
5	LANGUAGE_CODE
6	Term reference
7	Reliability
8	Evaluation
9	TERM_TYPE
10	Term note
11	SUBJECT_DOMAIN
12	DEFINITION
13	HU
14	LANGUAGE_CODE
15	Term reference
16	Reliability
17	Evaluation
18	TERM_TYPE
19	Term note
20	SUBJECT_DOMAIN
21	DEFINITION

4-13-2-1-6-7-8-9-10-11-12-14-15-16-17-18-19-20-21


=cut

use strict;
#prints out the columns of table in the specified order

my $order;
my $table;
my $ordersize;
my $insize;
my @orderarray;
my @inarray;
my $argc;
my $in;
my$orderi;

my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 3)
	{
	print  "Usage:\n";
	print  "perl iatepairs.pl input columns output \n";
	print  "columns eg: 5-1-2-3\n";
	exit ;
	}

open(my $infile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
#binmode($infile, ":encoding(UTF-16LE)");

$order =  $ARGV[1];

open(my $outfile, ">", $ARGV[2] ) || die "can't open $ARGV[2]";
binmode($outfile, ":raw");

@orderarray = split ('-', $order, -1);
$ordersize=@orderarray;

#print "order=$order, ";
#print "ordersize=$ordersize\n";

my $count =1;

while ($in = <$infile>)
	{
	#print $outfile  $in;
	#next;

	chomp($in);
	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	if( ($count == 1) &&  ($inarray[0] eq "Term") && ($orderarray[0] != 1) )
		{
		$inarray[0] =~ s/TERM/EN/i;
		}
	
	for(my $i=0 ;$i != $ordersize; $i++)	
		{
  		$orderi=$orderarray[$i]-1;
		#print "orderarray[$i]=$orderarray[$i]\n";
		print $outfile "$inarray[$orderi]";
		if($i!=$ordersize-1)
			{
			print $outfile "\t";
			}
		}
	print $outfile  "\n";
	#print $outfile  $in;

	$count++;
	}	

close $infile;
close $outfile;
