use strict;

my $infile;
my $outfile;
my $patfile;
my $line;

my $key;
my $val;
my %pat;

$infile=$ARGV[1];
$outfile=$ARGV[2];

open PAT, $ARGV[0] or die "error PAT"; binmode PAT, ":utf8";

while(<PAT>) 
	{
	chomp();
	$_ = lc($_);
	next if /^$/;  #empty
	next if /^#/;  #comment
	s/<[^>]*>//g;  #remove <tag>-s form 
	s/\"//g;       #remove " 
	($key, $val) = split (/\t/, $_);
	if($key ne $val)    #skip if identi
		{		
		$pat{$key} = $val;
		}
	#print "pat{$key}=$pat{$key}\n";  #DEBUG
	}
close PAT;

open IN, "<$infile" or die "error IN"; binmode IN, ":utf8";
open OUT, ">$outfile" or die "error OUT"; binmode OUT, ":utf8";

my $count=1;
#print "$count\n";

my $ulkey;
my $ulval;

while (<IN>) 
	{
	chomp();
	$line = " " . $_;
	$line =~ s/_/\\_/g;
	
	while( my ($key, $val) = each %pat)
		{
		 #old $line =~ s/$key/<x translation=\"$pat{$key}\">$key<\/x>/g;
		 $ulkey= $key;
		 $ulkey  =~ s/ /_/g;	
		 $val= $pat{$key};
		 $ulval= $val;
		 $ulval=~ s/ /_/g;
		 $line =~ s/ $key/ <x translation=\"$ulval\">$ulkey<\/x>/g;
		 }

	$line =~ s/([^\\])_/\1 /g;
	$line =~ s/\\_/_/g;
	$line =~ s/^ //;

 	print OUT "$line\n" ;
	#print "$count\n";
	$count++;
	}
close IN;
close OUT;

