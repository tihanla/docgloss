use strict;

my $word;
my %udichash;
my $allcapword;
my $firstcapword;

my $firstchar;
my $restchars;

my $argc=@ARGV;
if ( $argc ne 3)
	{
	print  "Usage:\n";          
	print  "perl lookupiate dict input output \n";
	exit ;
	}

open(my $indbfile, "<:encoding(utf8)", $ARGV[0] ) || die "can't open input: $ARGV[0]";
open(my $infile, "<:encoding(utf8)", $ARGV[1] ) || die "can't open input: $ARGV[1]";
open(my $outfile, ">:encoding(utf8)", $ARGV[2] ) || die "can't open output: $ARGV[2]";

my $lowercase_input=1;

my $ui=1;

while ($word = <$indbfile>)
	{
	chomp($word);
	my @uarr =split('\t', $word, -1);   #break words to ana
	my $uarrsize=@uarr;
	#my $key= lc ($uarr[0]);
	my $key= $uarr[0];
	$udichash{$key} =  $uarr[1];     #build a hash from elements to avoid duplicates
	#print "$ui word=$word key=$key  val=$uarr[1]\n";  #DEBUG
	$ui++;
	}	
print "$ui terms added\n";


my $ui=0;
while($word = <$infile>)
	{
	#lookup dict
	chomp($word);
	if ($word eq "")
		{
		print $outfile "\n";
		next;
		}
	#if not found uppercase first
	 if (exists($udichash{$word})) 
		{
		#print "found: $udichash{$word}\n";
		print $outfile "$word\t$udichash{$word}\n";
		$ui++;
		}
	else
		{
		if($lowercase_input)
			{
			#uppercase all input
			$allcapword = uc($word);
			 if (exists($udichash{$allcapword})) 
				{
				print $outfile "$word\t$udichash{$allcapword}\n";
				$ui++;
				#print "allcap=$udichash{$allcapword}\n";  #DEBUG
			
				}
			else	
				{
				$firstchar = substr  $word, 0, 1;    
				$restchars  = substr $word, 1; 
				$firstchar = uc ($firstchar);
				$firstcapword  = $firstchar . $restchars;

				#print "$firstcapword\n";  #DEBUG

				if (exists($udichash{$firstcapword})) 
					{
					print $outfile "$word\t$udichash{$firstcapword}\n";
					$ui++;
					#print "firstcap=$udichash{$firstcapword}\n";  #DEBUG
					}
				else
					{
					print $outfile "\*$word\n";
					}
				}
			}
		}
	#s/^[^\pL\n]+\n/\n/;
	}
print "$ui terms found\n";
