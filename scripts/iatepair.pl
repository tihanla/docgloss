use strict;

# CdT extraction from XML:
=pod
example input:
e:TL_RECORD_ID
e:SUBJECT_DOMAIN	
e:SUBJECT_DOMAIN_NOTE
e:LIL_RECORD_ID	
e:PRIMARY		
l:LANGUAGE_CODE
l:DEFINITION
l:DEFINITION_REFERENCE
l:RELATED_MATERIAL
t:TERM	
t:TERM_REF
t:RELIABILITY_VALUE
t:CONTEXT	
t:CONTEXT_REFERENCE
t:TERM_EVALUATION
t:PRE_IATE	
t:NOTE
t:INSTITUTION_ID
=cut

my $entrysize=0;
my $langsize=0;
my $termsize=0;
my $keypos=0;  
my $langpos=0;
my $termpos=0;
my $termtypepos=0;
my $relypos=0;
my $evalpos=0;
my $primarypos=0;
my $badcompos=0;
my $mode="";

#DGT extraction from SQL dump TSV format

my $entryData;
my $langData;
my $termData;
my $termTypeData;
my $relyData;
my $evalData;
my $primData;
my $entryHeader;
my $langHeader;
my $termHeader;
my $id;
my $idkey;
my $idlang;
my $ri;
my $counter;
my $in;
my $index;
my $field;
my $header;
my $termsrcHeader;
my $termtrgHeader;
my $ti;
my $pi;
my $preferredterm;
my $term;
my $output;
#CDT: my $unrelytext = "Reliability not verified";   #1
my $unrely_notverified = "Not Verified";   #1
my $unrely_downgraded = "Downgraded prior to deletion"; #0 lowercased on 2016.09.05
my $unrely_minimum = "Minimum reliability";
my $uneval_deprecated = "Deprecated";  # 1
my $uneval_obsolete = "Obsolete";   # 2
my $eval_preferred = "Preferred";
my $badcom_text = "1";  # -> "Y"
my $primary_text = "Yes";   #Primary=No (or N) is marked with empty string, (Primary=Yes (or Y) is marked with Yes)


my $relysrc;
my $relytrg;
my $evalsrc;
my $evaltrg;
my $termarrayti;
my $lowleft_quote = 0;
my $upleft_quote = 0;
my $field_quote = 0;
my $nonpaired_quote = 0 ;
my $right_quote =0;
my $srctermfield;
my $trgtermfield;

my $termformat;

my $src_out;
my $trg_out;

my $abbrev_insrc;
my $shortform_insrc;
my $preferred_insrc;

my $abbrev_intrg;
my $shortform_intrg;
my $preferred_intrg;

my $skipsrc;
my $skiptrg;
my $skipsrc_cnt1=0;
my $skipsrc_cnt2=0;
my $skiptrg_cnt1=0;
my $skiptrg_cnt2=0;

my $idxkey0;

my $srcsyn_entry_cnt=0;
my $trgsyn_entry_cnt=0;

my $entrycnt = 0;
my $src_interm_cnt = 0;
my $trg_interm_cnt = 0;
my $src_outterm_cnt = 0;
my $trg_outterm_cnt = 0;
my $src_skipterm_cnt = 0;
my $trg_skipterm_cnt = 0;
my $skipterm_cnt = 0;

my $src_outterme_cnt = 0;
my $trg_outterme_cnt = 0;

my $src_misterm_cnt = 0;
my $trg_misterm_cnt = 0;

my $entry_cnt=0;
my $want_rely_minimum = "Y";      #this is set to N by the command line switch when used for Commission dictionaries
my $want_rely_notverified = "Y";  #this switch is to be set to N to filter out target terms without verification, this configuration used in the Commission
my $want_primary_no = "Y";        #set to N if we do not want terms with primary=No, eg. the old EU languages 
my $filter_badcom = "N";          #set to 1 (or later Y)  in case of Maltese collections genereted for the Commission

my %hash_id;
my %hash_src_entry;
my %hash_src_lang;
my %hash_src_term;
my %hash_src_termtype;
my %hash_src_rely;
my %hash_src_eval;
my %hash_src_primary;

my %hash_trg_entry;
my %hash_trg_lang;
my %hash_trg_term;
my %hash_trg_termtype;
my %hash_trg_rely;
my %hash_trg_eval;
my %hash_trg_primary;

#for counting list elements if already skipped
my %hash_src_skipped;
my %hash_trg_skipped;


use Getopt::Long;

my $src_idxkey;
my $trg_idxkey;
my $src_index;
my $trg_index;
my $idval;

my $result = GetOptions (
"mode=s" => \$mode,
"entry=s" =>\$entrysize,
"lang=s" =>\$langsize,
"term=s" =>\$termsize,
"idcol=s" =>\$keypos,
"langcol=s" =>\$langpos,
"termcol=s" =>\$termpos,
"termtypecol=s" =>\$termtypepos,
"relycol=s" =>\$relypos,
"evalcol=s" =>\$evalpos,
"primarycol=s" =>\$primarypos,
"badcomcol=s" =>\$badcompos,
"rely_mini=s" =>\$want_rely_minimum,
"rely_notverified=s" =>\$want_rely_notverified,
"primary_no=s" =>\$want_primary_no,
"filter_badcom=s" =>\$filter_badcom,

#"inxcol=s" =>\$inxpos,
);  # flag

my $entrymax=$entrysize;
my $langmax=$entrymax + $langsize;
my $termmax=$langmax + $termsize;

if ($mode eq "l")
	{
	$mode = "list";
	}

if ($mode eq "p")
	{
	$mode = "pair";
	}
if ($mode eq "t")
	{
	$mode = "term";
	}


my $argc=@ARGV;
#print "argc=$argc\n";
#print "mode=$mode\n";
#print "idcol=$keypos\n";
#print "langcol=$langpos\n";
#print "idcol=$inxpos\n";
#print "entry= $entrysize\n";
#print "lang= $langsize\n";
#print "term= $termsize\n";
#print "want_rely_minimum= $want_rely_minimum\n";

my $outfile;
my $infile;

sub print_result
	{
	#clean result:
	$output =~ s/[\x00-\x08\x0B\x0C\x0E-\x1F]//g;
	#delete unpaired quotes:
	#$output =~ s/([\t\|])\"([^\t\|]+)\"([\t\|])/\1\2\3/g;
	#$output =~ s/([\t\|])\"([^\t\|]+)\"([\t\|])/\1\2\3/g;
	#$output =~ s/\"([^\"\|\t]+)\"/#dquote1#\1#dquote2#/g;
	#$output =~ s/\"//g;
	#$output =~ s/#dquote1#/\x{201C}/g;
	#$output =~ s/#dquote2#/\x{201D}/g;
	#ok, but GlossaryConverter crashes...

	#$output =~ s/\"/\x{201C}/g; # � (201C) and  �(201D)  � (201E)
	#GlossaryConverter crashes...

	#$output =~ s/\"//g;

	$output =~ s/[ ]+/ /g;
	$output =~ s/ \t/\t/g;
	$output =~ s/\t /\t/g;
	$output =~ s/ \n/\n/g;
	$output =~ s/^ //g;
	#$output =~ s/\t=/\t/g;

	my @termoutarray =split('\n', $output, -1);
	my $termarraysize=@termoutarray;
	#debug:
	#print "termarraysize=$termarraysize, preferredterm=$preferredterm\n";
	
	if($preferredterm != -1)
		{
		$pi=$preferredterm;
		$termarraysize=$preferredterm+1;
		#print "preferred=$pi\n";
	#	$pi=0;
		}
	else
		{
		$pi=0;
		}

	for(;$pi!=$termarraysize; $pi++)
		{

		#print results but swap term element with language section
		#print $outfile "!$output\n";
		#my @outarray =split('\t', $output, -1);
		my @outarray =split('\t', $termoutarray[$pi], -1);

		#print entry level:
		my $i;
		for($i=0;$i!=$entrymax;$i++)
			{
			print $outfile $outarray[$i];
			print $outfile "\t";
			}
		#print term element
		print $outfile $outarray[$termpos-1];
		print $outfile "\t";
		#print lang level
		for(;$i!=$langmax;$i++)
			{
			print $outfile $outarray[$i];
			print $outfile "\t";
			}
		#print term level without term supoosed to be first in term section
	
		for(;$i!=$termmax;$i++)
			{
			#skip term, already printed:
			if($i != $termpos - 1)
				{
				print $outfile $outarray[$i];
				print $outfile "\t";
				}
			}

		#target side

		#print term element:
		#print "i=$i\n";	
		$i= $termmax + $langsize;
		#print term level without term supoosed to be first in term section
		print $outfile "$outarray[$i]";
		print $outfile "\t";
		#print lang level

		for($i=$termmax ;$i!=$termmax + $langsize;$i++)
			{
			#print "i=$i $outarray[$i]\n";	
			print $outfile $outarray[$i];
			print $outfile "\t";
			}
		
	
		for(;$i!=$termmax + $langsize + $termsize ;$i++)  
			{
			#skip term, already printed:
			if($i !=   $termpos + $langsize + $termsize - 1)  # entrysize - langsize - termsize -langsize -termsize
				{
				#print "i2=$i $outarray[$i]\n";	
				print $outfile $outarray[$i];
				print $outfile "\t";
				}
			}

		$entrycnt++;
		#$trg_outterm_cnt = $trg_outterm_cnt + 1 ;
		#$src_outterm_cnt = $src_outterm_cnt + 1 ;

		print $outfile "\n";
		}  #$termarray
	}
#end sub print_result

if ( ($argc ne 4) || ($mode eq "" ) || ($entrysize == 0) || ($langsize == 0) || ($termsize == 0 ) || ($keypos == 0) || ($langpos == 0) || ($termpos == 0))
	{
	if ($argc ne 4) {print "argc=$argc\n";}
	if($mode eq "" ){print "mode=$mode\n";}
	if($keypos == 0 ){print "missing idcol=\n";}
	if($langpos == 0 ){print "missing langcol=\n";}
	if($termpos == 0 ){print "missing termcol=\n";}
	if($entrysize == 0 ){print "missing entry=\n";}
	if($langsize == 0 ){print "missing lang=\n";}
	if($termsize == 0 ){print "missing term=\n";}

	print  "Usage:\n";
	print  "perl iatepairs.pl input output src trg -mode=list|pairs  -entry=N -lang=N  term=N -idcol=N -langcol=N -termcol=N -badcomcol=N -primarycol=N [-relycol=N] [-evalcol=N] [-errfile=filename] [-termtypecol=N] [-rely_mini=Y/N] [-rely_notverified=Y/N] [-primary_no=Y/N] [-filter_badcom=Y/N]\n";
	print  "input.txt: tab separated table with one monolingual term perl line, IATE export from SQL or XML, UTF-16 text format\n";
	print  "output.txt: tab separated bilingual term entries file, paired by identical IATE ID-s \n";
	print  "src and trg: two letter IATE language identifiers of the source and target language\n";

	print  "-mode=list: output synonyms are in listd format, if imported by GlossaryConverter.exe looks similarly to IATE GUI\n";
	print  "-mode=pair: synonyms are paired in every possible combinations, can be used by other programs, needs simple tabular input\n";
	print  "-entry: number of entry level colums in input\n";
	print  "-lang: number of lang level colums in input\n";
	print  "-term: number of term level colums in input\n";
	print  "-idcol: column of the ID field (column count starts with 1)\n";
	print  "-langcol: column of the language field\n";
	print  "-termcol: column of the term field\n";
	print  "-termtypecol: column of the termtype field\n";
	print  "-relycol: optional filter based on Reliability field (Reliability not verified)\n";
	print  "-evalcol: optional filter based on Evaluation field (Depricated)\n";
	print  "-primarycol: column of the Primary field\n";
	print  "-badcomcol: bad commission column field\n";
	print  "-rely_mini=Y/N: include terms with Reliability=Minimum (default=Y)\n";
	print  "-rely_notverified=Y/N: include terms with Reliability=Not Verified (default=Y)\n";
        print  "-primary_no=Y/N: include terms with Primary=No  (default=Y)\n";
        print  "-filter_badcom=Y/N: filter out terms bad for the Commission (by used Maltese) (default=N)\n";
	print  "contact info:  laszlo.tihanyi\@ext.ec.europa.eu\n";
	exit 1;
	}                  


open($infile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
binmode($infile, ":encoding(UTF-8)");
open($outfile, ">", $ARGV[1] ) || die "can't open $ARGV[1]";
binmode($outfile, ":raw:encoding(UTF-8)");

my $errfilename = $ARGV[1]  . ".log"; 
#print "errorfile=$errfilename\n";

open(my $errfile, ">", $errfilename ) || die "can't open iatepairs.err";
binmode($errfile, ":raw:encoding(UTF-8)");

my $statname = $ARGV[1] . ".stat";
 
open(my $statfile, ">", $statname ) || die "can't open $statname";


my $srcusr = $ARGV[2];
my $trgusr = $ARGV[3];
my $src=uc($srcusr);
my $trg=uc($trgusr);
#my $mode = $ARGV[4];

my $recordsize = $entrysize + $langsize + $termsize;
my $recsize;

$counter = 1;
while ($in = <$infile>)
	{
	$in =~ s/\r\n//g;
	$in =~ s/\n//g;


	my @inarray = split ('\t', $in, -1);

	$recsize=@inarray;
	if  ($recsize != $recordsize)
		{
		print $errfile "recsize: $recsize is not $recordsize in line $counter: $in\n"; 
		next;
		}
	#print "recsize: $recsize, recordsize: $recordsize in line $counter: $in\n"; 

	$ri=0;
	foreach my $field (@inarray) 
		{
#print "ri=$ri\n";

		if($ri == $keypos-1)
			{
			$id=$field;
			#print "idkey=$idkey\n";
			}
		if($ri == $langpos-1)
			{
			$idlang=uc($field);
			#print "idlang=$idlang\n";
			}
		if($ri == $termpos-1)
			{
			$term=uc($field);
			#print "idlang=$idlang\n";
			}
		if($ri == $relypos-1)
			{
			#$field = lc($field);
			$field =~ s/Reliability not verified/Not Verified/;  #if comes from CdT
			$relyData = $field;
			}
		if($ri == $evalpos-1)
			{
			$evalData = $field;
			}
		if($ri == $primarypos-1)
			{
			$primData = $field;
			}
		if($ri == $termtypepos-1)
			{
			$termTypeData = $field;
			#print "termTypeData=$termTypeData\n";
			}
		if($ri >= 0  && $ri < $entrymax )
			{
			if ($entryData eq "") 
				{$entryData = $field;}
			else
				{$entryData = $entryData . "\t" . $field;}
			#print "$ri: entryData=$entryData\n";
			}
		if($ri  >= $entrymax  && $ri < $langmax )
			{
			if ($langData eq "") 
				{$langData = $field;}
			else
				{$langData = $langData . "\t" . $field;}
			#print "$ri: langData=$langData\n";
			}
		if($ri >= $langmax  && $ri < $termmax )
			{
			#if($ri == $termpos-1)
			#	{
				#$termformat=$field;
				#delete formatting from term fields:
				#$field =~ s/&lt;BR&gt;/ /ig;
				#$field =~ s/&lt;[a-z\/]+&gt;//ig;
				#$field =~ s/&amp;/&/ig;
			#	}
			if ($termData eq "") 
				{$termData = $field;}
			else
				{$termData = $termData . "\t" . $field;}
			#print "$ri: termData=$termData\n";
			}
		$ri++;
		}

	#header
	if($counter == 1)
		{


	$entryHeader=$entryData ;
		$langHeader=$langData ;
		$termHeader=$termData ;
		$entryData="";
		$langData="";
		$termData="";
		#$termTypeData="";
		$counter++;
		#print "src: langData=$langData!!!\n";
			
		next;
		}


	$hash_id{$id} =  1;
	#print "added: $idkey: $hash_id{$id}\n";

	#print "src=$src, idlang=$idlang\n";

#	print "src=$src, idlang=$idlang\n";
	if($src eq $idlang)
		{
		for ($index=0;;)
			{
			$idkey = $id . "-" .  $index;
			#print "idkey=$idkey\n";
			if(exists  $hash_src_entry{$idkey})
				{
				$index++;
				}
			else
				{
				$hash_src_entry{$idkey}=  $entryData;
				$hash_src_lang{$idkey} =  $langData;
				#print "src: langData=$langData!!!\n";
				$hash_src_term{$idkey} =  $termData;
				$hash_src_termtype{$idkey} =  $termTypeData;
				$hash_src_rely{$idkey} =  $relyData;
				#print "src added: $idkey: $hash_src_term{$idkey}\n";
				$hash_src_eval{$idkey} =  $evalData;
				$hash_src_primary{$idkey} =  $primData;
				$src_interm_cnt++;
				last;
				}
			}
		}
	if($trg eq $idlang)
		{
		for ($index=0;;)
			{
			$idkey = $id . "-" .  $index;
			if(exists  $hash_trg_entry{$idkey})
				{
				$index++;
				}
			else
				{
				$hash_trg_entry{$idkey}=  $entryData;
				$hash_trg_lang{$idkey} =  $langData;
				#print "trg: langData=$langData!!!\n";
				$hash_trg_term{$idkey} =  $termData;
				$hash_trg_termtype{$idkey} =  $termTypeData;
				$hash_trg_rely{$idkey} =  $relyData;
				$hash_trg_eval{$idkey} =  $evalData;
				$hash_trg_primary{$idkey} =  $primData;
				#print "trg added: $idkey: $hash_trg_rely{$idkey}\n";
				$trg_interm_cnt++;
				last;
				}
			}

		}
	$entryData="";
	$langData="";
	$termData="";
	$counter++;
	}  #while lines



#test:
#foreach my $key ( keys %hash_id )
#	{
#	print  "key: $key, value: $hash_id{$key}\n";
#	}


#DEBUG:
#foreach my $idkey ( keys %hash_trg_term )
#	{
#	print  "idkey: $idkey, value: $hash_trg_term{$idkey}\n";
#	}


#foreach my $key ( keys %hash_src_eval )
#	{
#	print  "key: $key, value: $hash_src_eval{$key}\n";
#	}

#exit;


#Problem language, Management, Historical, Primary, Origin, OK+Domain, +Domain note, OK+IATE-ID, Collection, +English/Hungarian, Related material, OK+Definition, OK+Definition reference
#Definition note, OK+TERM, OK+Term reference, Term type, OKReliability, OK+Institution ID, OKPre-IATE, OKEvaluation, OKTerm note, OKContext, Context-reference, Regional usage, Language usage


$entryHeader = "\t" . $entryHeader . "\t";
$langHeader = "\t" . $langHeader . "\t";
$termHeader = "\t" . $termHeader . "\t";




#CdT:
#$entryHeader =~ s/\tTL_RECORD_ID\t//;	#not displayed
$entryHeader =~ s/\tSUBJECT_DOMAIN\t/\tDomain\t/;
$entryHeader =~ s/\tSUBJECT_DOMAIN_NOTE\t/\tDomain note\t/;
$entryHeader =~ s/\tLIL_RECORD_ID\t/\tIATE-ID\t/;
$entryHeader =~ s/\tPRIMARY\t/\tPrimary\t/;
$entryHeader =~ s/\tIS_PRIMARY\t/\tPrimary\t/;
$entryHeader =~ s/\t\t/\t\t/;
#+DGT:
#/LIL_RECORD_ID/LIL_Record_ID/
#/TL_RECORD_ID/TL_Record_ID/
$entryHeader =~ s/\tDOMAIN\t/\tDomain\t/;
$entryHeader =~ s/\tDOMAINS\t/\tDomains\t/;
$entryHeader =~ s/\tDOMAIN_NOTE\t/\tDomain note\t/;
$entryHeader =~ s/\tPRIM_ET\t/\tPrimary\t/;
$entryHeader =~ s/\tCOLLECTION\t/\tCollection\t/;
$entryHeader =~ s/\tPROBLEM_LANG\t/\tProblem language\t/;
$entryHeader =~ s/\tMANAGEMENT_MESSAGE\t/\tManagement\t/;
$entryHeader =~ s/\tHISTORICAL\t/\tHistorical\t/;
$entryHeader =~ s/\tORIGIN\t/\tOrigin\t/;


#CdT:
#$langHeader =~ s/\tLANGUAGE_CODE\t//;  #not displayed
$langHeader =~ s/\tDEFINITION\t/\tDefinition\t/;
$langHeader =~ s/\tDEFINITION_REFERENCE\t/\tDefinition reference\t/;
$langHeader =~ s/\tDEFINITION_REF\t/\tDefinition reference\t/;
$langHeader =~ s/\tRELATED_MATERIAL\t/\tRelated material\t/;
$langHeader =~ s/\tLL_COMMENT\t/\tDefinition note\t/;



#CdT:
$termsrcHeader = $termHeader;
$termsrcHeader =~ s/\tTERM\t/\t$src\t/;   #name of language displayed instead of code + saves formatted form
$termsrcHeader =~ s/\tTERM_FORMAT\t/\tFormatted term\t/;
$termsrcHeader =~ s/\tTERM_REF\t/\tTerm reference\t/;
$termsrcHeader =~ s/\tRELIABILITY_VALUE\t/\tReliability\t/;
$termsrcHeader =~ s/\tCONTEXT\t/\tContext\t/;
$termsrcHeader =~ s/\tCONTEXT_REFERENCE\t/\tContext reference\t/;
$termsrcHeader =~ s/\tCONTEXT_REF\t/\tContext reference\t/;
$termsrcHeader =~ s/\tTERM_EVALUATION\t/\tEvaluation\t/;
$termsrcHeader =~ s/\tPRE_IATE\t/\tPre-IATE\t/;
$termsrcHeader =~ s/\tNOTE\t/\tTerm note\t/;
$termsrcHeader =~ s/\tINSTITUTION_ID\t/\tInstitution\t/;
$termsrcHeader =~ s/\tINST\t/\tInstitution\t/;
$termsrcHeader =~ s/\tPIFS\t/\tPre-IATE\t/;
#/TERM/Term/
#/TERM_REF/Reference/
#+DGT:
$termsrcHeader =~ s/\tTL_COMMENT\t/\tTerm note\t/;
$termsrcHeader =~ s/\tRELIABILITY\t/\tReliability\t/;
$termsrcHeader =~ s/\tEVALUATION\t/\tEvaluation\t/;
$termsrcHeader =~ s/\tOBSOLETE\t/\tEvaluation\t/;
$termsrcHeader =~ s/\tTERMTYPE\t/\tTerm type\t/;
$termsrcHeader =~ s/\tTERM_TYPE\t/\tTerm type\t/;
$termsrcHeader =~ s/\tINSTITUTION\t/\tInstitution\t/;
$termsrcHeader =~ s/\tTL_INST\t/\tInstitution\t/;
$termsrcHeader =~ s/\tTL_RECORD_ID\t/\tRecord-ID\t/;
$termsrcHeader =~ s/\tLANGUAGE_USAGE\t/\tLanguage usage\t/;
$termsrcHeader =~ s/\tREGIONAL_USAGE\t/\tRegional usage\t/;
$termsrcHeader =~ s/\tTL_FREQ\t/\tFrequency\t/;
$termsrcHeader =~ s/\tTL_FORM\t/\tForm\t/;


$termtrgHeader = $termHeader;
$termtrgHeader =~ s/\tTERM\t/\t$trg\t/;	  #name of language displayed instead of code  +save formatted form
$termtrgHeader =~ s/\tTERM_FORMAT\t/\tFormatted term\t/;
$termtrgHeader =~ s/\tTERM_REF\t/\tTerm reference\t/;
$termtrgHeader =~ s/\tRELIABILITY_VALUE\t/\tReliability\t/;
$termtrgHeader =~ s/\tCONTEXT\t/\tContext\t/;
$termtrgHeader =~ s/\tCONTEXT_REFERENCE\t/\tContext reference\t/;
$termtrgHeader =~ s/\tCONTEXT_REF\t/\tContext reference\t/;
$termtrgHeader =~ s/\tTERM_EVALUATION\t/\tEvaluation\t/;
$termtrgHeader =~ s/\tPRE_IATE\t/\tPre-IATE\t/;
$termtrgHeader =~ s/\tNOTE\t/\tTerm note\t/;
$termtrgHeader =~ s/\tINSTITUTION_ID\t/\tInstitution\t/;
$termtrgHeader =~ s/\tINST\t/\tInstitution\t/;
$termtrgHeader =~ s/\tPIFS\t/\tPre-IATE\t/;
#+DGT:
$termtrgHeader =~ s/\tTL_COMMENT\t/\tTerm note\t/;
$termtrgHeader =~ s/\tRELIABILITY\t/\tReliability\t/;
$termtrgHeader =~ s/\tEVALUATION\t/\tEvaluation\t/;
$termtrgHeader =~ s/\tOBSOLETE\t/\tEvaluation\t/;
$termtrgHeader =~ s/\tTERMTYPE\t/\tTerm type\t/;
$termtrgHeader =~ s/\tTERM_TYPE\t/\tTerm type\t/;
$termtrgHeader =~ s/\tINSTITUTION\t/\tInstitution\t/;
$termtrgHeader =~ s/\tTL_INST\t/\tInstitution\t/;
$termtrgHeader =~ s/\tTL_RECORD_ID\t/\tRecord-ID\t/;
$termtrgHeader =~ s/\tLANGUAGE_USAGE\t/\tLanguage usage\t/;
$termtrgHeader =~ s/\tREGIONAL_USAGE\t/\tRegional usage\t/;
$termtrgHeader =~ s/\tTL_FREQ\t/\tFrequency\t/;
$termtrgHeader =~ s/\tTL_FORM\t/\tForm\t/;


$entryHeader =~ s/\t([^\n]+)\t/$1/;
$langHeader =~ s/\t([^\n]+)\t/$1/;
$termtrgHeader =~ s/\t([^\n]+)\t/$1/;
$termsrcHeader =~ s/\t([^\n]+)\t/$1/;


#can be impoved based on inxcol;
$output =  $entryHeader . "\t" . $langHeader . "\t" . $termsrcHeader . "\t"  . $langHeader . "\t" . $termtrgHeader;
print_result();  

#exit;

#pair mode tries to print out only Abbrev and Short form pairs
#if Abbrev found on source side then it prints out only a pair with the first Abbrev from target side
#source Terms are not paired with target Abbrev-s  if we alredy identified an Abbrev-Abbrev synonym with the same entry ID


#write out results 
if($mode eq "pair")
	{
	
	#TODO: in pair mode we cannot filter Primaries

	while ( ($idkey, $idval) = each (%hash_id))
		{


		$abbrev_insrc=0;
		$abbrev_intrg=0;
		$shortform_insrc=0;
		$shortform_intrg=0;
		$preferred_insrc=0;
		$preferred_intrg=0;
		$src_outterme_cnt=0;
		$trg_outterme_cnt=0;

		#find out if there are  Abbrev-Abbrev, ShortForm-ShortForm or Preferred-Term or Term-Preferrd pairs

		#$src_index="0";
		#$src_idxkey = $idkey . "-" . $src_index;
		#if(exists $hash_src_term{$src_idxkey})
		#	{
		#	#print "src_idxkey=$src_idxkey\n";
		##	if(! exists $hash_trg_term{$src_idxkey})
		#		{
		#		print "not exist: hash_trg_term{$src_idxkey}\n";
		#		}
		#	else
		##		{
		#	;#	print "exist: hash_trg_term{$src_idxkey}\n";
		#		}
		#	}

#		$trg_index="0";
#		$trg_idxkey = $idkey . "-" . $trg_index;
#		print "trg_idxkey=$trg_idxkey\n";
#		if(!exists $hash_trg_term{$trg_idxkey})
##			{
#			print "not exist: hash_trg_term{$trg_idxkey}\n";
#			}
###################


		$src_index=0;
		$src_idxkey = $idkey . "-" . $src_index;
		
		for (;;)
			{

			$src_idxkey = $idkey . "-" . $src_index;
			if(exists $hash_src_term{$src_idxkey})
				{
				if ($hash_src_termtype{$src_idxkey} eq "Abbrev") 
					{
					$abbrev_insrc=1;
					}
				if ($hash_src_termtype{$src_idxkey} eq "Short Form") 
					{
					$shortform_insrc=1;
					}
				if( $hash_src_eval{$src_idxkey} eq "Preferred" )
					{
					$preferred_insrc=1;
					}
				$src_index++;
				#print "!!!hash_src_eval{$src_idxkey}: $hash_src_eval{$src_idxkey}\n"
				#print "src_idxkey: $src_idxkey\n";
				}
			else
				{
				last;
				}
			}



		$trg_index=0;
		$trg_idxkey = $idkey . "-" . $trg_index;

		for (;;)
			{
			$trg_idxkey = $idkey . "-" . $trg_index;


			if(exists $hash_trg_term{$trg_idxkey})
				{
				if ($hash_trg_termtype{$trg_idxkey} eq "Abbrev") 
					{
					$abbrev_intrg=1;
					}
				if ($hash_trg_termtype{$trg_idxkey} eq "Short Form") 
					{
					$shortform_intrg=1;
					}
				if( $hash_trg_eval{$trg_idxkey} eq "Preferred" )
					{
					$preferred_intrg=1;
					}
				$trg_index++;
				}
			else
				{
				last;
				}
        		}

		#print "$src_index $trg_index\n";

		if(($src_index != 0 ) && ($trg_index != 0))
			{
			$entry_cnt=$entry_cnt + $src_index*$trg_index;
			}
		if(($src_index == 0) && ($trg_index != 0))
			{
			$entry_cnt=$entry_cnt + $trg_index;
			}
		if(($src_index != 0 ) && ($trg_index == 0))
			{
			$entry_cnt=$entry_cnt + $src_index;
			}
		#print "src_index=$src_index, trg_index=$trg_index, entrymax=$entry_cnt\n";
		

		$output="";

		$src_idxkey = $idkey . "-" ."0";
		for ($src_index=0;;)
			{
			$src_idxkey = $idkey . "-" . $src_index;
			if(exists $hash_src_term{$src_idxkey})
				{
				$src_out=1;
				my $errormsg_src="";
				if( $hash_src_rely{$src_idxkey} eq $unrely_downgraded)  
					{
					#$src_index++;
					$errormsg_src = $errormsg_src . "src_Reliabilty_Downgraded_to_Deletion ";
					$src_skipterm_cnt++;
					$src_out=0;
					#next;
					}
				elsif ($hash_src_eval{$src_idxkey} eq $uneval_deprecated) 
					{
					#$src_index++;
					$errormsg_src = $errormsg_src . "src_Evaluation_Depricated ";
					$src_skipterm_cnt++;
					$src_out=0;
					#next;
					}
				elsif ($hash_src_eval{$src_idxkey} eq $uneval_obsolete)
					{
					#$src_index++;
					$errormsg_src = $errormsg_src . "src_Evaluation_Obsolete ";
					$src_skipterm_cnt++;
					$src_out=0;
					#next;
					}



				elsif (($hash_src_primary{$src_idxkey} ne $primary_text) && ($want_primary_no eq "N" ))
					{
					#$src_index++;
					$errormsg_src = $errormsg_src . "src_Non_Primary ";
					$src_skipterm_cnt++;
					$src_out=0;
					#print "src_Non_Primary!\n";
					#next;
					}

				#If target Evaluation="Preferred" (and target Institution="COM") then select this and do not select other target terms
				elsif($preferred_insrc == 1) 
					{
					if( $hash_src_eval{$src_idxkey} ne "Preferred" )
						{
						$errormsg_src = $errormsg_src . "src_Preferred_skipped ";
						#$trg_index++;
						$trg_skipterm_cnt++;
						$src_out=0;
						#next;
						}
					}


				#print "hash_src_eval{$src_idxkey}=$hash_src_eval{$src_idxkey}\n";  #DEBUG
				#print "hash_src_primary{$src_idxkey}=$hash_src_primary{$src_idxkey}\n";  #DEBUG


				$trg_index=0;
				$trg_idxkey = $idkey . "-" . $trg_index;

				$output="";
				$preferredterm=-1;
				for (;;)
					{
					$trg_idxkey = $idkey . "-" . $trg_index;
					#print "trg_index==$trg_index\n";
					if(exists $hash_trg_term{$trg_idxkey})
						{
						$trg_out=1;
						my $errormsg_trg="";
						#if both src and trg elements are reliable
						#print "hash_src_eval{$src_idxkey}=$hash_src_eval{$src_idxkey}, unevaltext=$unevaltext\n";
						#print "hash_trg_eval{$trg_idxkey}=$hash_trg_eval{$trg_idxkey}, unevaltext=$unevaltext\n";

						#ori: if(($hash_src_rely{$src_idxkey} ne $unrelytext ) && ($hash_trg_rely{$trg_idxkey} ne $unrelytext)  && ($hash_src_eval{$src_idxkey} ne $unevaltext ) && ($hash_trg_eval{$trg_idxkey} ne $unevaltext))
						#Evaluation, Reliability checked only on target side:



						#do not print if we already had a matched Abbrev=Abbrev and it is a Term-Abbrev
						if( $abbrev_insrc == 1 && $abbrev_intrg == 1)
							{
							if( ($hash_src_termtype{$src_idxkey} eq "Abbrev")  && ($hash_trg_termtype{$trg_idxkey} ne "Abbrev") )
								{
								$errormsg_trg = $errormsg_trg . "src-trg_Abbrev_skip ";
								#$trg_index++;
								$src_skipterm_cnt++;
								$trg_out=0;
								#next;
								}
							elsif( ($hash_trg_termtype{$trg_idxkey} eq "Abbrev")  && ($hash_src_termtype{$src_idxkey} ne "Abbrev") )
								{
								$errormsg_trg = $errormsg_trg . "trg-src_Abbrev_skip ";
								#$trg_index++;
								$src_skipterm_cnt++;
								$trg_out=0;
								#next;
								}
							}

						if( $shortform_insrc == 1 && $shortform_intrg == 1)
							{
							if (($hash_src_termtype{$src_idxkey} eq "Short Form")  && ($hash_trg_termtype{$trg_idxkey} ne "Short Form") )
								{
								$errormsg_trg = $errormsg_trg . "src-trg_ShortForm_skip ";
								#$trg_index++;
								$trg_skipterm_cnt++;
								$trg_out=0;
								#next;
								}
							elsif (($hash_trg_termtype{$trg_idxkey} eq "Short Form")  && ($hash_src_termtype{$src_idxkey} ne "Short Form") )
								{
								$errormsg_trg = $errormsg_trg . "trg-src_ShortForm_skip ";
								#$trg_index++;
								$trg_skipterm_cnt++;
								$trg_out=0;
								#next;
								}
							}
						if($preferred_intrg == 1) 
							{
							if( $hash_trg_eval{$trg_idxkey} ne "Preferred" )
								{
								$errormsg_trg = $errormsg_trg . "trg_Preferred_skipped ";
								#$trg_index++;
								$trg_skipterm_cnt++;
								$trg_out=0;
								#next;
								}
							}
						#print "hash_src_termtype{$src_idxkey}=$hash_src_termtype{$src_idxkey}\n";



						if( $hash_trg_rely{$trg_idxkey} eq $unrely_minimum )  
							{
							$errormsg_trg = $errormsg_trg . "trg_Reliabilty_Minimum ";
							#$trg_index++;
							$trg_skipterm_cnt++;
							$trg_out=0;
							#next;
							}


						if( $hash_trg_rely{$trg_idxkey} eq $unrely_notverified )  
							{
							$errormsg_trg = $errormsg_trg . "trg_Reliabilty_Not_Verified ";
							#$trg_index++;
							$trg_skipterm_cnt++;
							$trg_out=0;
							#next;
							}
						elsif(  $hash_trg_rely{$trg_idxkey} eq  $unrely_downgraded ) 
							{
							$errormsg_trg = $errormsg_trg . "trg_Reliabilty_Downgraded_to_Deletion ";
							#$trg_index++;
							$trg_skipterm_cnt++;
							$trg_out=0;
							#next;
							}
						  elsif ( $hash_trg_eval{$trg_idxkey} eq $uneval_deprecated) 
							{
							$errormsg_trg = $errormsg_trg . "trg_Evaluation_Depricated ";
							#$trg_index++;
							$trg_skipterm_cnt++;
							$trg_out=0;
							#next;
							}
						  elsif ($hash_trg_eval{$trg_idxkey} eq $uneval_obsolete) 
							{
							$errormsg_trg = $errormsg_trg . "trg_Evaluation_Obsolete ";
							#$trg_index++;
							$trg_skipterm_cnt++;
							$trg_out=0;
							#next;
							}

						elsif (($hash_trg_primary{$src_idxkey} ne $primary_text) && ($want_primary_no eq "N" ))
							{
							#$src_index++;
							$errormsg_trg = $errormsg_src . "trg_Non_Primary ";
							$trg_skipterm_cnt++;
							$trg_out=0;
							#next;
							}


					#	print "hash_trg_eval{$trg_idxkey}=$hash_trg_eval{$trg_idxkey}\n";  #DEBUG


						if($src_out && $trg_out)
							{
							$output =$output .  $hash_src_entry{$src_idxkey} . "\t";
							$output = $output . $hash_src_lang{$src_idxkey}  . "\t";
							$output = $output . $hash_src_term{$src_idxkey}  . "\t";
							$output = $output . $hash_trg_lang{$trg_idxkey}  . "\t";
							$output = $output . $hash_trg_term{$trg_idxkey} ;
							$output =~ s/\"//g;
							$src_outterm_cnt++;
							$trg_outterm_cnt++;
							                	
							$src_outterme_cnt++;
							$trg_outterme_cnt++;

							#print $outfile $output; 
							#print_result();
							$output = $output  . "\n";
								
							#$entrycnt++;
							#$trg_outterm_cnt = $trg_outterm_cnt + 1 ;
							#$src_outterm_cnt = $src_outterm_cnt + 1 ;
					

                      					#$output="";

							#print "src_idxkey=$src_idxkey: hash_src_entry{src_idxkey}=$hash_src_entry{$src_idxkey}\n";
							#print "src_idxkey=$src_idxkey: hash_src_lang{src_idxkey}= $hash_src_lang{$src_idxkey}\n";
							#print "src_idxkey=$src_idxkey: hash_src_term{src_idxkey}= $hash_src_term{$src_idxkey}\n";
							#print "trg_idxkey=$trg_idxkey: hash_trg_entry{trg_idxkey}=$hash_trg_entry{$trg_idxkey}\n";
							#print "trg_idxkey=$trg_idxkey: hash_trg_lang{trg_idxkey}= $hash_trg_lang{$trg_idxkey}\n";
							#print "trg_idxkey=$trg_idxkey: hash_trg_term{trg_idxkey}= $hash_trg_term{$trg_idxkey}\n";
							#print "---------------------\n";
						         }
						else
							{
							print $errfile "$errormsg_src";
							if ($errormsg_src ne ""){print $errfile " "};
							print $errfile "$errormsg_trg\t$src_idxkey\t$hash_src_term{$src_idxkey}\t$trg_idxkey\t$hash_trg_term{$trg_idxkey}\n";
							#$errormsg="";
							$skipterm_cnt++;
							}
						$trg_index++;
						}
					else
						{
						#if( ($trg_outterme_cnt == 0)  )
						#	{
						#	print $errfile "$src_idxkey\t$hash_src_term{$src_idxkey}\t$trg_idxkey\t$hash_src_term{$trg_idxkey} Missing trg\n";
						#	$trg_misterm_cnt++;
						#	}

						last;
						}
					}
				chomp($output);  #chomp the last \n 
				print_result();  #prints results for one src term 

				$src_index++;
				}
			else
				{
				last;
				}
			}


		$trg_index=0;
		$trg_idxkey = $idkey . "-" . $trg_index;
		if(!exists $hash_trg_term{$trg_idxkey})
			{
			$src_index=0;
			for (;;)
				{
				$src_idxkey = $idkey . "-" . $src_index;
				if(exists $hash_src_term{$src_idxkey})
					{
					print $errfile "Missing trg\t$src_idxkey\t$hash_src_term{$src_idxkey}\n";
					$trg_misterm_cnt++;
					}
				else
					{
					last;
					}
				$src_index++;
				}
			}
			
		$src_index=0;
		$src_idxkey = $idkey . "-" . $src_index;
		if(!exists $hash_src_term{$src_idxkey})
			{
			$trg_index=0;
			for (;;)
				{
				$trg_idxkey = $idkey . "-" . $trg_index;
				if(exists $hash_trg_term{$trg_idxkey})
					{
					print $errfile "Missing src\t$trg_idxkey\t$hash_trg_term{$trg_idxkey}\n";
					$src_misterm_cnt++;
					}
				else
					{
					last;
					}
				$trg_index++;
				}
			}
		}

	my $allin = $counter-2; #header + lastline
	print  $statfile "INPUT: $ARGV[0]\n";
	print  $statfile "terms: $allin\n";
	print  $statfile "source terms: $src_interm_cnt\n";
	print  $statfile "target terms: $trg_interm_cnt\n";

	print  $statfile "OUTPUT: $ARGV[1]\n";

	$entrycnt--;
	print  $statfile "entries: $entrycnt\n";
	print  $statfile "source terms: $src_outterm_cnt\n";
	print  $statfile "target terms: $trg_outterm_cnt\n";
	
	print $statfile  "entries max:$entry_cnt\n";

	my $filtered_cnt=  $skipterm_cnt + $src_misterm_cnt + $trg_misterm_cnt;   #blocked paired and src/target terms without pairs
	print $statfile  "filtered entries: $filtered_cnt ";
	print $statfile  "(missing src=$src_misterm_cnt, missing trg=$trg_misterm_cnt, skipped=$skipterm_cnt)\n";

	my $remain_cnt=$entry_cnt-$filtered_cnt;
	print $statfile  "remaining entries: $remain_cnt\n";
	

	#print  $statfile "skip source terms: $src_skipterm_cnt\n";
	#print  $statfile "skip target terms: $trg_skipterm_cnt\n";

	#print  $statfile "miss source terms: $src_misterm_cnt\n";
	#print  $statfile "miss target terms: $trg_misterm_cnt\n";
	}


#print "unrelytext=$unrelytext, unrelytext2=$unrelytext2\n";
#print "unevaltext=$unevaltext, unevaltext2=$unevaltext2\n";
#exit;

my $srcsyncnt;
my $trgsyncnt;

my $relytermpos=$relypos-$entrysize-$langsize;
my $evaltermpos=$evalpos-$entrysize-$langsize;
my $badcompos=$badcompos-$entrysize-$langsize;


if($mode eq "list")
	{
	while ( ($idkey, $idval) = each (%hash_id))
		{
		$output="";
		$srcsyncnt=0;
		$trgsyncnt=0;

		$src_idxkey = $idkey . "-" ."0";
		#print $outfile "$hash_src_entry{$src_idxkey}\t";
		#print $outfile "$hash_src_lang{$src_idxkey}\t";
		#OLD: $output  = $output .  $hash_src_entry{$src_idxkey} . "\t";

		my $srcentryti = $hash_src_entry{$src_idxkey};

		my @entryarray=split('\t', $srcentryti, -1);
=pod
		if ( ($entryarray[$primarypos-1]  eq $primary_text) && ($want_primary_no eq "N" ))
			{
			print $errfile  "Primary_No:\t$hash_src_entry{$src_idxkey}\n";
			next;                                                                             
			}

=cut
		#my $debcompos=$entryarray[$badcompos-1];
		#print   "hash_src_entry{$src_idxkey}=$hash_src_entry{$src_idxkey}, $debcompos\n";

		#$srcentryti = FixQuote($srcentryti, $src);
		$output  = $output .  $srcentryti . "\t";

		
		
		$termarrayti = $hash_src_lang{$src_idxkey};
		#$termarrayti = FixQuote($termarrayti, $src);

		$output = $output . $termarrayti;
		$output = $output . "\t";

		#DEBUG
		#print "src_idxkey=$src_idxkey: hash_src_entry{src_idxkey}=$hash_src_entry{$src_idxkey}\n";
		#print "src_idxkey=$src_idxkey: hash_src_lang{src_idxkey}= $hash_src_lang{$src_idxkey}\n";


		#print missing elemnts if no entry because of missing trg/src terms
		$trg_index=0;
		$trg_idxkey = $idkey . "-" . $trg_index;
		if(!exists $hash_trg_term{$trg_idxkey})
			{
			#DEBUG
			#print "Missing trg_idxkey=$trg_idxkey: hash_trg_term{trg_idxkey}=$hash_trg_term{$trg_idxkey}\n";
			$src_index=0;
			for (;;)
				{
				$src_idxkey = $idkey . "-" . $src_index;
				if(exists $hash_src_term{$src_idxkey})
					{
					print $errfile "Missing_trg\t$src_idxkey\t$hash_src_term{$src_idxkey}\n";
					$trg_misterm_cnt++;
					delete $hash_src_term{$src_idxkey};   #not to be found if counted by skiptrg_cnt2 
					}
				else
					{
					last;
					}
				$src_index++;
				}
			}

		$src_index=0;
		$src_idxkey = $idkey . "-" . $src_index;
		if(!exists $hash_src_term{$src_idxkey})
			{
			$trg_index=0;
			for (;;)
				{
				$trg_idxkey = $idkey . "-" . $trg_index;
				if(exists $hash_trg_term{$trg_idxkey})
					{
					print $errfile "Missing_src\t$trg_idxkey\t$hash_trg_term{$trg_idxkey}\n";
					$src_misterm_cnt++;
					delete $hash_trg_term{$trg_idxkey};   #not to be found if counted by skiptrg_cnt2 
					}
				else
					{
					last;
					}
				$trg_index++;
				}
			}

		#now all src/trg terms have trg/src pairs...

		for($ti=0;$ti != $termsize; )
			{
			$srctermfield="";
			for ($src_index=0;;)
				{

				$src_idxkey = $idkey . "-" . $src_index;
				if(exists $hash_src_term{$src_idxkey})
					{
					#non-paired
					#$idxkey0 = $idkey . "-" . "0";
					#if( ($ti == 0) && (!exists $hash_trg_term{$idxkey0}) )
					#	{
	                		#	print $errfile "missing trg: $src_idxkey: $hash_src_term{$src_idxkey}\n"; 
					#	$skipsrc=1;
					#	}


					my $errormsg_src="";


					my @termarray=split('\t', $hash_src_term{$src_idxkey}, -1);

					$skipsrc=0;

					#print "termarray[$relytermpos-1] = $termarray[$relytermpos-1]\n";
					if( $termarray[$relytermpos-1] eq $unrely_downgraded)  
					#if( $hash_src_rely{$src_idxkey} eq $unrely_downgraded)  
						{
						if($ti == 0) 
							{	
							$errormsg_src = $errormsg_src . "src_Reliabilty_Downgraded_to_Deletion ";
							}	
						$skipsrc=1;
						}
				
					if ( $termarray[$evaltermpos-1]  eq $uneval_deprecated) 
						{
						if($ti == 0) 
							{	
							$errormsg_src = $errormsg_src . "src_Evaluation_Depricated ";
							}
						$skipsrc=1;
						}

					if ( $termarray[$evaltermpos-1]  eq $uneval_obsolete)
						{
						if($ti == 0) 
							{	
							$errormsg_src = $errormsg_src . "src_Evaluation_Obsolete ";
							}
						$skipsrc=1;
						}

					if ( ($termarray[$badcompos-1]  eq $badcom_text) && ($filter_badcom eq "Y" ))  #later Y
						{
						if($ti == 0) 
							{	
							$errormsg_src = $errormsg_src . "src_Bad_for_Commission ";
							}
						$skipsrc=1;
						}	
	
	
					if ( ($entryarray[$primarypos-1]  ne $primary_text) && ($want_primary_no eq "N" ))
						{
						if($ti == 0) 
							{
							$errormsg_src = $errormsg_src . "src_Non_Primary ";
							}
						$skipsrc=1;
						}


					if(!$skipsrc)
						{
						if ($src_index !=0 )   #only for non-first
							{
							#$output = $output . "|";
							$srctermfield = $srctermfield . "|";
							}

						#print ("idkey=$idkey skipsrc=$skipsrc\n");

						$termarrayti=$termarray[$ti];
						$srctermfield = $srctermfield . $termarrayti;    #$termarray[$ti]

						if($ti == 0)
							{
							$srcsyncnt++;
							$src_outterm_cnt++;
							}
						}
					else
						{
						if($ti == 0) 
							{
							$skipsrc_cnt1++;
							print $errfile "$errormsg_src\t$src_idxkey\t$hash_src_term{$src_idxkey}\n";
							$hash_src_skipped{$src_idxkey} = 1;

							#delete $hash_src_term{$src_idxkey};   #not to be found if counted by skiptrg_cnt2 
							}
						}

					$src_index++;
					}
				else
					{
					#print "not found: hash_src_entry{$src_idxkey}\n";
					last;
					}
				}
			#$srctermfield = FixQuote($srctermfield, $src);
			#print "termfield2:$termfield\n";
			#NOT: $termfield =~ s/^(\|)+$//;   #delete if only pipes
			$output = $output . $srctermfield . "\t";
			$ti++;
			}

		$trg_idxkey = $idkey . "-" ."0";

		$termarrayti = $hash_trg_lang{$trg_idxkey};
		#$termarrayti = FixQuote($termarrayti, $trg);



		$output = $output . $termarrayti . "\t";
		
		#print "output:$output\n";

		#print "trg_idxkey=$trg_idxkey: hash_trg_entry{trg_idxkey}=$hash_trg_entry{$trg_idxkey}\n";
		#print "trg_idxkey=$trg_idxkey: hash_trg_lang{trg_idxkey}= $hash_trg_lang{$trg_idxkey}\n";

		$relytrg=0;
		$evaltrg=0;


		for($ti=0;$ti != $termsize; )
			{
			$trgtermfield="";
			for ($trg_index=0;;)
				{
				$trg_idxkey = $idkey . "-" . $trg_index;
				#print "trg_idxkey=$trg_idxkey\n";
				if(exists $hash_trg_term{$trg_idxkey})
					{

					#non-paired:
					#$idxkey0 = $idkey . "-" . "0";

					my @termarray=split('\t', $hash_trg_term{$trg_idxkey}, -1);
					$skiptrg=0;

					my $errormsg_trg="";
					#if( !exists $hash_src_term{$idxkey0}) 
					#	{
					#	if($ti == 0)	
					#		{
					#		print $errfile "missing src: $trg_idxkey: $hash_trg_term{$trg_idxkey}\n"; 
					#		}
					#	$skiptrg=1;
					#	}

					


					if( ($termarray[$relytermpos-1]  eq $unrely_minimum ) && ($want_rely_minimum eq "N" ))  
						{
						if($ti == 0)	
							{
							$errormsg_trg = $errormsg_trg . "trg_Reliabilty_Minimum ";
							}
						$skiptrg=1;
						}

					if( ($termarray[$relytermpos-1]  eq $unrely_notverified ) && ($want_rely_notverified eq "N"))  
						{
						if($ti == 0)	
							{
							$errormsg_trg = $errormsg_trg . "trg_Reliabilty_Not_Verified ";
							}
						$skiptrg=1;
						}



					if(  $termarray[$relytermpos-1]  eq  $unrely_downgraded ) 
						{
						if($ti == 0)	
							{
							$errormsg_trg = $errormsg_trg . "trg_Reliabilty_Downgraded_to_Deletion ";
							}
						$skiptrg=1;
						}
					  if ( $termarray[$evaltermpos-1]  eq $uneval_deprecated) 
						{
						if($ti == 0)	
							{
							$errormsg_trg = $errormsg_trg . "trg_Evaluation_Depricated ";
							}
						$skiptrg=1;
						}
  					  if ( $termarray[$evaltermpos-1]  eq $uneval_obsolete)
						{
						if($ti == 0)	
							{
							$errormsg_trg = $errormsg_trg . "trg_Evaluation_Obsolete ";
							}
						$skiptrg=1;
						}
					if ( ($termarray[$badcompos-1]  eq $badcom_text) && ($filter_badcom eq "Y" ))  #later Y
						{
						if($ti == 0) 
							{	
							$errormsg_trg = $errormsg_trg . "trg_Bad_for_Commission ";
							#my $termarray_badcompos_1 = $termarray[$badcompos-1];
							#print "idkey=$idkey, ti=$ti, termarray[$badcompos-1]=$termarray_badcompos_1, badcom_text=$badcom_text, filter_badcom=$filter_badcom\n";
							}
						$skiptrg=1;
						}	



					if ( ($entryarray[$primarypos-1]  ne $primary_text) && ($want_primary_no eq "N" ))
						{
						if($ti == 0) 
							{
							$errormsg_trg = $errormsg_trg . "trg_Non_Primary ";
							}
						$skiptrg=1;
						}


					#print "---- relytrg=$relytrg\n";

					if(!$skiptrg)
						{
						if ($trg_index !=0 )   #only for non-first
						#if( $trgtermfield  ne "" ) 
							{
							#$output = $output . "|";
							$trgtermfield = $trgtermfield . "|";
							#print "termfield=$termfield\n";
        						}
					
						$termarrayti=$termarray[$ti];
						#print "termarrayti=$termarrayti\n";

		                        	
						#$output = $output . $termarrayti;    #$termarray[$ti]
						$trgtermfield = $trgtermfield . $termarrayti;    #$termarray[$ti]

						if($ti == 0)
							{
							$trgsyncnt++;
							$trg_outterm_cnt++;
							}
						}
					else
						{
						if($ti == 0) 
							{
							$skiptrg_cnt1++;
							print $errfile "$errormsg_trg\t$trg_idxkey\t$hash_trg_term{$trg_idxkey}\n";
							$hash_trg_skipped{$trg_idxkey} = 1;
							#delete $hash_trg_term{$trg_idxkey};   #not to be found if counted by skiptrg_cnt2 
							}
						}

					$trg_index++;
					}
				else
					{
					last;
					}
				}
			#$trgtermfield = FixQuote($trgtermfield, $trg);
			#NOT: $termfield =~ s/^(\|)+$//;   #delete if only pipes
			$output = $output . $trgtermfield . "\t";
			$ti++;
			}


		if($srcsyncnt == 0)
			{
			$trg_index=0;
			for (;;)
				{
				$trg_idxkey = $idkey . "-" . $trg_index;
				if(exists $hash_trg_term{$trg_idxkey})
					{
					if( ! exists $hash_trg_skipped{$trg_idxkey})  #do not count twice
						{
						print $errfile "trg_with_filtered_src\t$trg_idxkey\t$hash_trg_term{$trg_idxkey}\n";
						$skiptrg_cnt2++;
						}
#					else	{print "TWICE trg: $trg_idxkey\n"};
					}
				else
					{
					last;
					}
				$trg_index++;
				}
			}

		#print ("trgtermfield2=$trgtermfield\n");


		if($trgsyncnt == 0)
			{
			$src_index=0;
			for (;;)
				{
				$src_idxkey = $idkey . "-" . $src_index;
				if(exists $hash_src_term{$src_idxkey})
					{
					if( ! exists $hash_src_skipped{$src_idxkey})  #do not count twice
						{
						print $errfile "src_with_filtered_trg\t$src_idxkey\t$hash_src_term{$src_idxkey}\n";
						$skipsrc_cnt2++;
						}
					#else 	{print "TWICE: $src_idxkey\n"};
					}
				else
					{
					last;
					}
				$src_index++;
				}
			}




#		if($trgtermfield eq "")
#			{
#			#$hash_src_entry{$src_idxkey0};
#			print $errfile "trg missing\t$idkey\n";  
#			}

		#$output = $output . "\n";
		#print "relysrc=$relysrc, relytrg=$relytrg\n";
		#print ("evalsrc=$evalsrc, evaltrg=$evaltrg\n");
#		if(($relysrc == 1) &&  ($relytrg == 1) && ($evalsrc == 1) &&  ($evaltrg == 1))   #if we have at least one src and trg reliable 
		if($srcsyncnt != 0 && $trgsyncnt != 0)
			{
			#$output =~ s/\"//g;
			#print $outfile $output;
			print_result();
			#$entrycnt++;
			#print $statfile "entrycnt=$entrycnt\n";
			#print $statfile  "src_outterm_cnt=$src_outterm_cnt, src_index=$src_index\n";
			#print $statfile  "trg_outterm_cnt=$trg_outterm_cnt, trg_index=$trg_index\n";
			#print "$srcsyncnt\t$trgsyncnt\n";
			$srcsyn_entry_cnt=$srcsyn_entry_cnt+$srcsyncnt;
			$trgsyn_entry_cnt=$trgsyn_entry_cnt+$trgsyncnt;

			}                            

		#$trg_outterm_cnt=$trg_outterm_cnt + $trgsyncnt;
		}
	

	my $allin = $counter-2; #header + lastline
	print  $statfile "INPUT: $ARGV[0]\n";
	print  $statfile "input terms: $allin\n";
	print  $statfile "input source terms: $src_interm_cnt\n";
	print  $statfile "input target terms: $trg_interm_cnt\n";

	print  $statfile "OUTPUT: $ARGV[1]\n";
	$entrycnt--;
	print  $statfile "entries: $entrycnt\n";
	#old print  $statfile "source terms: $src_outterm_cnt\n";
	#old print  $statfile "target terms: $trg_outterm_cnt\n";

	print  $statfile "source terms: $srcsyn_entry_cnt\n";
	print  $statfile "target terms: $trgsyn_entry_cnt\n";


	#print $statfile  "entries max:$entry_cnt\n";
	my $filtered_cnt=  $skipsrc_cnt1 + $skipsrc_cnt2 + $skiptrg_cnt1 + $skiptrg_cnt2 +$src_misterm_cnt + $trg_misterm_cnt;   #blocked paired and src/target terms without pairs
	print $statfile  "filtered terms: $filtered_cnt ";
	print $statfile  "(missing src=$src_misterm_cnt, missing trg=$trg_misterm_cnt,skip src=$skipsrc_cnt1, skip-unpaired src=$skipsrc_cnt2, skip trg=$skiptrg_cnt1, skip-unpaired trg=$skiptrg_cnt2)\n";

	#this is dispalyed only if there is some calculation problem (assert type of message)
	my $remain_src_cnt=$src_interm_cnt-$skipsrc_cnt1-$skipsrc_cnt2-$src_misterm_cnt;
	my $remain_trg_cnt=$trg_interm_cnt-$skiptrg_cnt1-$skiptrg_cnt2-$trg_misterm_cnt;
	print $statfile  "remaining src: $remain_src_cnt\n";
	print $statfile  "remaining trg: $remain_trg_cnt\n";

#	print $statfile  "remaining entries: $remain_cnt\n";                                           


	}


close $infile;
close $outfile;
close $errfile;



