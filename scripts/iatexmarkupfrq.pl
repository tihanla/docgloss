#IATE Xml-MARKUP for moses input with FReQuency information from euramis corpus
#input files:
#1. IATE terms of the text to be translated with translation (only un-ambigious terms used)
#created for example for the 10k test SMT set:
#call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 

#2. frequency information of morphological variants of the target words occurring in Euramis corpus
#first created on linux machine by C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\scripts\freqmaven.pl 
#C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\test\hhi-euramis-stat\hu.frq
#and then morphologically stemmed by 
#C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\run\call_stmfrqdic.bat
#C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\test\stmfrqdic\hu-stm-num.tsv

#3. text to be translated

#4. output file
#file with mark-ups with morphological variants and probabilities

  
use strict;

my $infile;
my $outfile;
my $line;

my $key;
my $val;
my %pat;
my $count;


#open PAT,      "$ARGV[0] or die "error PAT: $ARGV[0]"; binmode PAT, ":utf8";
#open STEM, $ARGV[1] or die "error MORSTAT: $ARGV[1]"; binmode PAT, ":utf8";

#open IN, "<$infile" or die "error IN"; binmode IN, ":utf8";
#open OUT, ">$outfile" or die "error OUT"; binmode OUT, ":utf8";

my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 4)
	{
	print  "Usage:\n";
	print  "perl iatexmlmarkupfrq.pl iate_pairs target_corpus_word_frequency input output \n";
	exit ;
	}


open (PAT, "<", $ARGV[0]) || die "can't open $ARGV[0]";
open (STEM, "<", $ARGV[1]) || die "can't open $ARGV[1]";
open (IN, "<", $ARGV[2]) || die "can't open $ARGV[2]";
open (OUT, ">", $ARGV[3]) || die "can't open $ARGV[3]";


#$infile =$ARGV[2];
#$outfile=$ARGV[3];
$count=1;
while(<PAT>) 
	{
	chomp();
	$_ = lc($_);
	s/<[^<>]+>//g; #delete tags
	s/\"//g;       #delete quotes

	next if /^$/;  #empty
	next if /^#/;  #comment
	($key, $val) = split (/\t/, $_);
	if($key ne $val)    #skip if identi
		{		
		$pat{$key} = $val;
		}
	#print "pat{$key}=$pat{$key}\n";  #DEBUG
	$count++;
	}
close PAT;
print "$count dictionary items added\n";   #DEBUG

my %hash_stem_surf;
my %hash_stem_freq;
my $index;
my $idkey;
my $surf;
my $stem;
my $freq;

$count=1;

while(<STEM>) 
	{
	chomp();

	s/^([^\t]+)\t([^\t]*)\t([^\t]+)/$1$2$3/;
	if($2 ne "")
		{
		$surf=$1;
		$stem=$2;
		$freq=$3;

		#hash tables for stems to find surf and freq
		#iterate in db as long we do not find an unused index for this stem
		for ($index=0;;)
			{
			$idkey = $stem . "-" .  $index;
			#print "idkey=$idkey\n";
			if(exists  $hash_stem_freq{$idkey})
				{
				#if we have a key, check if the surf is the same (this may happen because of lowercasing)
				#if identical then increase the frequency  otherwise creta new index item
				$index++;
				}
			else
				{
				$hash_stem_freq{$idkey}=  $freq;
				$hash_stem_surf{$idkey}=  $surf;
				#print "$count: hash_stem_freq{$idkey}=$hash_stem_freq{$idkey}\n";  #DEBUG
				$count++;
				last;
				}
			}
		}
	}

print "$count stem_freq items added\n";   #DEBUG

close STEM;


my $count=1;
#print "$count\n";

my $ulkey;
my $ulval;
my @valarr;
my $xmltag;
my $totalfreq;
my $valarrsize;
my $probab;

while (<IN>) 
	{
	chomp();
	$line = " " . $_;
	$line =~ s/_/\\_/g;
	
	#iterarate on all ource-target dictionary replacement patterns, and apply them to the line
	while( my ($key, $val) = each %pat)
		{
		#find last word in key phrase
		@valarr =split(' ', $val, -1);   #res element to morphological elements separated by space
		$valarrsize=@valarr;
		$stem=$valarr[$valarrsize-1];

		#print "stem=$stem, val=$val\n";  #DEBUG

		$totalfreq=0;
		#iterate on surf forms of stem and count total number of occurances
		for ($index=0;;)
			{
			$idkey = $stem . "-" .  $index;
			#print "idkey=$idkey\n";

			#lookup last word in STEM
			if(exists $hash_stem_surf{$idkey} )
				{
				$totalfreq += $hash_stem_freq{$idkey};
				$index++;
				}
			else
				{
				last;
				}
			}

		#print "totalfreq=$totalfreq\n";  #DEBUG	
##########################################x

		if($totalfreq == 0) 	#we do not have alternatives		
			{
			 $ulkey= $key;
			 $ulkey  =~ s/ /_/g;	
			 $val= $pat{$key};
			 $line =~ s/ $key/ <x translation=\"$val\">$ulkey<\/x>/g;
			}
		
		else     #we have alternatives		
			{
			#iterate on surf forms and create XML markup with propabilities
			#<n translation="dwelling||house" prob="0.8||0.2">haus</n>

			my $probability_str="";
			my $translation_str="";
			for ($index=0;;)
				{
				$idkey = $stem . "-" .  $index;
				#print "idkey=$idkey\n";

				#lookup last word in STEM
				if(exists $hash_stem_freq{$idkey} )
					{
					if($index != 0)
						{
						$probability_str = $probability_str . "\|\|";
						}
					$probab=int($hash_stem_freq{$idkey}/$totalfreq*10000);
					$probab = $probab /10000;
					if($probab != 0)
						{
						$probability_str  = $probability_str . $probab;
						$index++;
						}
					else
						{
						last;
						}
					#$xmltag  = $xmltag . $hash_stem_freq{$idkey}/$totalfreq;
					}
				else
					{
					last;
					}
				}
			 
			$probability_str =~ s/\|\|$//;  #chomp last pipes if it was limited  

			my $probcnt=$index;
			for ($index=0; $index !=$probcnt ;)
				{
				$idkey = $stem . "-" .  $index;
				#print "idkey=$idkey\n";

				#lookup last word in STEM
				if(exists $hash_stem_surf{$idkey} )
					{
					if($index != 0)
						{
						$translation_str = $translation_str . "\|\|";
						}
					#weak assumption: only last element of the term phrase changed in the corpus:
					for(my $pi=0; $pi!=$valarrsize-1; $pi++)
						{
						$translation_str = $translation_str . $valarr[$pi] . " ";
						}
					$translation_str = $translation_str . $hash_stem_surf{$idkey};
					$index++;
					}
				else
					{
					last;
					}
				}


			$ulkey= $key;
			$ulkey  =~ s/ /_/g;	
			#$val= $pat{$key}
			$xmltag = "<x translation=\"" . $translation_str . "\" prob=\"" . $probability_str . "\">" .  $ulkey . "<\/x>";
			$line =~ s/ $key/ $xmltag/g;
			#print "xmltag=$xmltag\n";  #DEBUG
			}


######################################x

		 }

	$line =~ s/([^\\])_/\1 /g;
	$line =~ s/\\_/_/g;
	$line =~ s/^ //;

 	print OUT "$line\n" ;
	#print "$count\n";
	$count++;
	}

print "$count lines precessed\n";   #DEBUG


close IN;
close OUT;

