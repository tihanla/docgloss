use strict;

open(my $infile, "<:encoding(utf8)", $ARGV[0] ) || die "can't open input: $ARGV[0]";
open(my $outfile, ">:encoding(utf8)", $ARGV[1] ) || die "can't open output: $ARGV[1]";

while(<$infile>)
	{
	#s/[\pL]/*/g;
	s/^[^\pL\n]+\n/\n/;
	print $outfile $_;
	}