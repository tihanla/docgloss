=pod

set entry=4
set lang=5
set term=11
set idcol=1
set langcol=5
set termcol=10
set termtypecol=12
set relycol=13
set evalcol=17
set primarycol=4
set badcomcol=20
set rely_mini=N
set rely_notverified=N
set primary_no=Y
set filter_badcom=Y

=cut

use strict;
#prints out the columns of table in the specified order

my $order;
my $table;
my $ordersize;
my $insize;
my @orderarray;
my @inarray;
my @headarray;
my @cfgarray;                                                                                               
my @tigarray;
my $argc;
my $in;
my$orderi;

my $entry_size;
my $lang_size;
my $term_size;
#my $badcom=N;
#my $form=N;

my $i;
my $ti;
my $tigsize;

#my $id_col;
#my $lang_col;
#my $term_col;

my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 3)
	{
	print  "Usage:\n";
	print  "perl txt2tbx.pl configfile input.txt output.tbx \n";
	exit ;
	}

open(my $cfgfile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
open(my $infile, "<:encoding(utf8)", $ARGV[1] ) || die "can't open $ARGV[1]";
open(my $outfile, ">:encoding(utf8)", $ARGV[2] ) || die "can't open $ARGV[2]";

print $outfile "<?xml version='1.0'?>\n";
print $outfile "<!DOCTYPE martif SYSTEM \"TBXcoreStructV02.dtd\">\n";
print $outfile "<martif type=\"TBX\" xml:lang=\"en\">\n\n";
print $outfile "<martifHeader>\n<fileDesc>\n<titleStmt>\n\t<title>IATE extraction</title>\n</titleStmt>\n";
print $outfile "<sourceDesc>\n\t<p>This termbase has been distributed by the Terminology Coordination Unit of the Commission</p>\n";
print $outfile "\t<p>DTD source: http:\/\/www.ttt.org\/oscarStandards\/tbx\/TBXcoreStructV02.dtd</p>\n</sourceDesc>\n</fileDesc>\n</martifHeader>\n\n";


print $outfile "<text>\n";
print $outfile "<body>\n\n";

while ($in = <$cfgfile>)
	{
	#if ($in =~ s/^set //)
	$in =~ s/^set //;     #also works for linux config, without 'set' keyword
		#{
		@cfgarray = split ('=', $in, -1);
		if($cfgarray[0] eq "entry")
			{
			$entry_size=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "lang")
			{
			$lang_size=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "term")
			{
			$term_size=$cfgarray[1];
			}
		#}
	}
 
my $count =1;
while ($in = <$infile>)
	{
	chomp($in);
	$in =~ s/\&([a-z]+[^a-z;])/&amp;\1/ig;  #quote & if it soes not look like a character entity
	$in =~ s/\&([a-z]+[^a-z;])/&amp;\1/ig;  #needs duplication because of &X& constructions  (X is any single character)

	$in =~ s/\&([^a-z])/&amp;/ig;           #as a result both & and &amp;  will be &amp;  on the output! 

#print "in=$in\n";  #DEBUG

	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#save field names in header:
	if( ($count == 1) )
		{
		@headarray=@inarray;
		$count++;
		next;
		}

	# entry_size + 1 (TERM) + lang_size + term_size-1  | 1 (TERM) + lang_size + term_size-1 
	print $outfile "<termEntry>\n";	
	for($i=0 ;$i != $entry_size; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        print $outfile "<descrip type=\"", $headarray[$i], "\">";
			print $outfile $inarray[$i];
	        	print $outfile "</descrip>\n";
			}
		}


	#source side----------------------------
        print $outfile "<langSet xml:lang=\"", $inarray[$entry_size+1], "\">\n";
	for($i=$entry_size+2;$i != $entry_size+1+$lang_size; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        print $outfile "\t<descrip type=\"", $headarray[$i], "\">";
			print $outfile $inarray[$i];
	        	print $outfile "</descrip>\n";
			}
		}

	#find out tig size:
	@tigarray = split ('\|', $inarray[$entry_size], -1);   #input does not contain | (replaced to ,)
	$tigsize=@tigarray;
	
	for($ti=0; $ti != $tigsize; $ti++)
		{
		@tigarray = split ('\|', $inarray[$entry_size], -1);   #input does not contain | (replaced to ,)
		if($tigarray[$ti] ne "")
			{
			print $outfile "\t<tig>\n";	
			print $outfile "\t\t<term>";	
                	print $outfile $tigarray[$ti];
			print $outfile "</term>\n";	

			for($i=$entry_size+1+$lang_size;$i != $entry_size+$lang_size+$term_size; $i++)	
				{
				@tigarray = split ('\|', $inarray[$i], -1);   #input does not contain | (replaced to ,)
				if ($headarray[$i] eq "") {;}
				elsif ($headarray[$i] eq "COM_BAD_MT") {;}
				elsif ($tigarray[$ti] eq "") {;}
				else
					{
					if($tigarray[$ti] ne "")
						{
					        print $outfile "\t\t<descrip type=\"", $headarray[$i], "\">";
						print $outfile $tigarray[$ti];
					        print $outfile "</descrip>\n";
						}
					}
				}
			print $outfile "\t</tig>\n";	
			}
	        }

	print $outfile "</langSet>\n";	


	#target side-------------------------------
	my $t=$entry_size+$lang_size+$term_size;
        print $outfile "<langSet xml:lang=\"", $inarray[$t+1], "\">\n";
	for($i=$t+2;$i != $t+$lang_size+1; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        print $outfile "\t<descrip type=\"", $headarray[$i], "\">";
			print $outfile $inarray[$i];
	        	print $outfile "</descrip>\n";
			}
		}


	@tigarray = split ('\|', $inarray[$t], -1);   #input does not contain | (replaced to ,)
	$tigsize=@tigarray;
	
	for($ti=0; $ti != $tigsize; $ti++)
	        {
		@tigarray = split ('\|', $inarray[$t], -1);   #input does not contain | (replaced to ,)
		if($tigarray[$ti] ne "")
			{
			print $outfile "\t<tig>\n";	
			print $outfile "\t\t<term>";	
                	print $outfile $tigarray[$ti];
			print $outfile "</term>\n";	

			for($i=$t+$lang_size+1; $i != $t+$lang_size+$term_size; $i++)	
				{
				@tigarray = split ('\|', $inarray[$i], -1);   #input does not contain | (replaced to ,)
				if ($headarray[$i] eq "") {;}
				elsif ($headarray[$i] eq "COM_BAD_MT") {;}
				elsif ($tigarray[$ti] eq "") {;}
				else
					{
			
					if($tigarray[$ti] ne "")
						{
				        	print $outfile "\t\t<descrip type=\"", $headarray[$i], "\">";
						print $outfile $tigarray[$ti];
		        			print $outfile "</descrip>\n";
						}
					}
				}

			print $outfile "\t</tig>\n";	
			}
		}

	print $outfile "</langSet>\n";	


	print $outfile "</termEntry>\n";	
	print $outfile  "\n";
	#print $outfile  $in;

	$count++;
	}	

print $outfile "</body>\n";
print $outfile "</text>\n";
print $outfile "</martif>\n";

close $infile;
close $outfile;
