#convert a dictionary that has src and trg lang term is the first two column  into an ID-format termbase (like IATE)
#in: 
#en	hu
#apple	alma

#out:
#1	en	apple
#1	hu	alma



use strict;
use warnings;
     
my $file;
my $out;

if ( @ARGV ne 4)
	{
	print "Usage:\ntsv2id.pl textin textout src_lang trg_lang";
	exit;
	}

open ($file, "<", $ARGV[0])|| die "can't open $ARGV[0]";
open ($out, ">", $ARGV[1])|| die "can't open $ARGV[1]";

my $srclang=$ARGV[2];
my $trglang=$ARGV[3];

my $id=1;

#$file = shift or die "Usage: $0 FILE\n";
#open my $fh, '<', $file or die "Could not open '$file' $!";

while (my $line = <$file>) 
	{
	if ($line =~ s/^([^\t]+)\t([^\t]+)/$1$2/)
	        {
		print $out  "$id\t$srclang\t$1\n$id\t$trglang\t$2\n";
		}
	$id++;
	
	}        

