#writes outseg content if lang = input lang
#<tuv xml:lang="EN-GB">
#<seg>laying down the Union Customs Code</seg>

use strict;
use warnings;
     
my $file;
my $out;

if ( @ARGV ne 3)
	{
	print "Usage:\nfreqmaven.pl textin textout lang\n";
	exit;
	}

open ($file, "<", $ARGV[0])|| die "can't open $ARGV[0]";
open ($out, ">", $ARGV[1])|| die "can't open $ARGV[1]";

my $inlang=$ARGV[2];
my $lang;


#$file = shift or die "Usage: $0 FILE\n";
#open my $fh, '<', $file or die "Could not open '$file' $!";

while (my $line = <$file>) 
	{
	if ($line =~ s/^<tuv xml:lang=\"([^\"]+)\"/$1/)
	        {
		$lang = $1;
		#print "lang=$1\n"; #DBG
		}
	if ($line =~ s/^<seg>([^<]+)<\/seg>/$1/)
		{
		if($inlang eq $lang)
			{
			print $out "$1\n";
			}
		}
	
	}        

