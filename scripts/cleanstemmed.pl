#restore parsed phrases: use surface for unanalized and lexical for analyzed
#examples:
#2015\/C/2015\/C  -> 2015/C
#or unparsed quoted strings
# \[  ->  [

while(<>)
	{
	
	s/\\\/\\/\\/g;    		#replace an analyzed single literal backlslash
        s/\\\//_clean_slash_/g;
	s/([^ ]*[^ \\])\/\*([^ \n]+)/\1/g;         #replace   ABC/*ABC or ABC/* -> ABC
	s/([^ ]*[^ \\])\/([^\* \n][^ \n]*)/\2/g;     #replace   ABC/abc -> abc
	s/_clean_slash_/\\\//g;                 #restore \/
	s/\\\/\/\\\//\//g;                     #replace:  \//\/  with /
	s/([^\\])\\/\1/g;                      # \[  -> [
	s/^\\//g;                              # ^\  ->  del
	print $_;
	}