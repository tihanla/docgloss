#IATE Xml-MARKUP for moses input with FReQuency information from euramis corpus
#input files:
#1. IATE terms of the text to be translated with translation (only un-ambigious terms used)
#created for example for the 10k test SMT set:
#call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 

#2. frequency information of morphological variants of the target words occurring in Euramis corpus
#first created on linux machine by C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\scripts\freqmaven.pl 
#C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\test\hhi-euramis-stat\hu.frq
#and then morphologically stemmed by 
#C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\run\call_stmfrqdic.bat
#C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\test\stmfrqdic\hu-stm-num.tsv
#lowercacsed by this file

#3. text to be translated

#4. output file
#file with mark-ups with morphological variants and probabilities

  
use strict;

my $infile;
my $outfile;
my $line;

my $key;
my $val;
my %pat;
my $count;


#open PAT,      "$ARGV[0] or die "error PAT: $ARGV[0]"; binmode PAT, ":utf8";
#open STEM, $ARGV[1] or die "error MORSTAT: $ARGV[1]"; binmode PAT, ":utf8";

#open IN, "<$infile" or die "error IN"; binmode IN, ":utf8";
#open OUT, ">$outfile" or die "error OUT"; binmode OUT, ":utf8";

my $argc=@ARGV;
#print "argc=$argc\n";

#if ( $argc ne 2)
if ( $argc ne 2)
	{
	print  "Usage:\n";
	print  "perl lowercase.pl  input output \n";
	print "input is a 3 column frequency list: surf TAB stem TAB freq\n";
	print "argc=$argc\n";
	exit ;
	}

#$infile=;
#$outfile=$ARGV[1];


open IN, "<$ARGV[0]" or die "error can't open $ARGV[0]"; binmode IN, ":utf8";
open OUT, ">$ARGV[1]" or die "error can't open $ARGV[1]"; binmode OUT, ":utf8";


my %hash_surf_stem;
my %hash_surf_freq;
my $surf;
my $stem;
my $freq;

my $lc=1;
my $lck;

while(<IN>) 
	{
	chomp();
	$_ = lc ($_);
	#print "$_\n";

	s/^([^\t]+)\t([^\t]*)\t([^\t]+)/$1$2$3/;
	if($2 ne "")
		{
		$surf=$1;
		$stem=$2;
		$freq=$3;
		#print "surf=$surf, stem=stem, freq=$freq\n";

		if(exists  $hash_surf_freq{$surf})
			{
			#if we have a key, check if the surf is the same (this may happen because of lowercasing)
			#if identical then increase the frequency  otherwise creta new index item
			 $hash_surf_freq{$surf} =  $hash_surf_freq{$surf} + $freq;
			}
		else
			{
			$hash_surf_freq{$surf}=  $freq;
			$hash_surf_stem{$surf}=  $stem;
			}
		}

	$lc++;
	$lck = int $lc/100000;
	if($lc == $lck*100000) 		
		{print STDERR "$lc\n";}

	}

#print in frequency order



foreach my $surf (reverse sort { $hash_surf_freq{$a} <=> $hash_surf_freq{$b} } keys %hash_surf_freq) 
	{
	print OUT "$surf\t$hash_surf_stem{$surf}\t$hash_surf_freq{$surf}\n";
	}

close IN;
close OUT;

