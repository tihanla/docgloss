#replace nl from end of normal lines with space
#keep empty lines
use strict;

#my $outline="";
my $outsrf="";
my $outstm="";

while(<>)
	{
	if(/^[^\r\n]/)
		{
		chomp();
		my @arr =split('\t', $_, -1);   #break line to words
		my $arrsize=@arr;
		if (($arrsize == 1) || (($arrsize == 2) && ($arr[1] eq "") ))  #only surf
			{
			#without *
			#$outline =$outline . $arr[0] . "/" . $arr[0] . " ";
			$outsrf =$outsrf . $arr[0] . "/" . $arr[0] . " ";

			#with * : 
			#$outline =$outline . $arr[0] . "/*" . $arr[0] . " ";
			}
		else
			{
			#$outline =$outline . $arr[0] . "/" . $arr[1] . " ";
			$outstm =$outsrf . $arr[0] . " ";
			$outsrf =$outstm . $arr[1] . " ";
			}
	
		}
	else     
		{
		if($outsrf ne "")
			{
			$outsrf =~ s/ $//;   #delete last space 
			$outstm =~ s/ $//;   
			print "$outsrf\t$outstm" ;
			$outsrf ="";
			$outstm ="";
			}
		else
			{
			print "\n";
			}
		}
	}
#if($outline ne "")
if($outsrf ne "")
	{
	#print $outline;
	print $outsrf;
	}

	