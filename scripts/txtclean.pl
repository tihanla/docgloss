use strict;
use Getopt::Long;

#1		2		3		4		5	6		7		8		9		10		11	12	
#INSTITUTION_ID	LIL_RECORD_ID	TL_RECORD_ID	LANGUAGE_CODE	TERM	TERM_REF	TL_COMMENT	RELIABILITY	EVALUATION	TERMTYPE	DOMAIN	DEFINITION
#2014-06-19
#1		2	3		4		5		6		7		8		           	9			10	11		12		13		14	15		16		17	18		19			
#TL_RECORD_ID	DOMAIN	DOMAIN_NOTE	LIL_RECORD_ID	PRIM_ENT	LANGUAGE_CODE	DEFINITION	DEFINITION_REF	           	RELATED_MATERIAL	TERM	TERMTYPE	TERM_REF	RELIABILITY	CONTEXT	CONTEXT_REF	EVALUATION	PIFS	TL_COMMENT	INST

#2014-08-12
#1      2       3                                                       4               5               6               7              8                       9       10     		11   		12     		 13     14              15            		16              17
#IATE_ID	DOMAIN	DOMAIN_NOTE	                                LANGUAGE_CODE	DEFINITION	DEFINITION_REF	LL_COMMENT	RELATED_MATERIAL	TERM	TERMTYPE	RELIABILITY	TERM_REF	CONTEXT	CONTEXT_REF	EVALUATION		TL_COMMENT	INST

my $count=1;
my @linearray;
my $linearraysize;
my $inst;
my $TERM;
my $termformat;
my $i;

my $domainpos;
my $instpos;
my $termpos;
my $primarypos;
my $preiatepos;
my $historypos;
my $termtypepos;

my $columns;
my $errorfile;
my $evalpos;
my $relypos;

#DGT_2014-06-19_V4:
#my $domainpos=2;
#my $instpos=19;
#my $termpos=10;
##my $primarypos=5;
#my $preiatepos=17;
                   

if (@ARGV < 1 )  
	{
	print  "usage: perl txtclean.pl [-domainpos=N] [-instpos=N] [-termpos=N], [-primarypos=N] [-preiatepos=N] [-historypos=N] [-evalpos=N] [-relypos=N] [-termtypepos=N] [-columns=N]  input.txt\n";
	exit;                                           
	}
	

my $result;
$result = GetOptions ( 'domainpos=i'   => \$domainpos, 'instpos=i'   => \$instpos, 'termpos=i'   => \$termpos, 'primarypos=i'   => \$primarypos, 'preiatepos=i'   => \$preiatepos, 'historypos=i'   => \$historypos, , 'evalpos=i'   => \$evalpos, 'relypos=i'   => \$relypos, 'termtypepos=i'   => \$termtypepos,'columns=i'   => \$columns );  

#print "historypos=$historypos";


#open(my $errorfile, ">", "error.txt" ) || die "can't open errorfile";


while(<>)
	{
#	if ($count eq 1)
#		{
#		print $_;
#		$count++;
#		next;   #skip header
#		}
	#s/\t//g;
	#s/\n//g;
	s/\r//g;
	s/\|/, /g;
	s/[ ]+/ /g;
        #s/<([^\/])/\n<\1/g;
	#s/></>\n<\1/g;
	s/&/&amp;/g;
	s/</&lt;/g;
	s/>/&gt;/g;

	#invalid unicode sequencies:
	s/\xEF\xBF\xBF//g;
	s/\xEF\x80\xA0//g;
	s/\xEF\xAC\x80/fi/g;  


#=pod
	my @linearray =split('\t', $_, -1);
	my $linearraysize=@linearray;

	#print "line=$_";
	#print "arraysize=$linearraysize\n";

	if($linearraysize != $columns)
		{
		print "$linearraysize!=$columns : $_";
		next;
		}

	for($i=0; $i!=$linearraysize; $i++ )
		{

		#if($i != 5)   #termformat
		#	{
		#	}

		#clear leading ; from DOMAIN
		if($i == $domainpos-1)
			{
			$linearray[$i] =~ s/[0-9]+\-//g;
			$linearray[$i] =~ s/ ;/;/g;
			$linearray[$i] =~ s/^;//g;
			}

		if($i == $primarypos-1)
			{
			$linearray[$i] =~ s/0//ig;
			$linearray[$i] =~ s/1/Yes/ig;
			$linearray[$i] =~ s/^N//ig;
			$linearray[$i] =~ s/^Y/Yes/ig;
			}
		if($i == $historypos-1)
			{
			$linearray[$i] =~ s/0//ig;
			$linearray[$i] =~ s/1/Yes/ig;
			$linearray[$i] =~ s/N//ig;
			$linearray[$i] =~ s/Y/Yes/ig;
			}
		if($i == $evalpos-1)
			{
			$linearray[$i] =~ s/None//ig;
			$linearray[$i] =~ s/0//ig;
			$linearray[$i] =~ s/4/Preferred/ig;
			$linearray[$i] =~ s/3/Admitted/ig;
			$linearray[$i] =~ s/2/Obsolete/ig;
			$linearray[$i] =~ s/1/Deprecated/ig;
			}
		if($i == $relypos-1)
			{
			$linearray[$i] =~ s/0/Downgraded prior to deletion/ig;
			$linearray[$i] =~ s/1/Reliability not verified/ig;
			$linearray[$i] =~ s/2/Minimum reliability/ig;
			$linearray[$i] =~ s/3/Reliable/ig;
			$linearray[$i] =~ s/4/Very reliable/ig;
			}
		if($i == $preiatepos-1)
			{
			$linearray[$i] =~ s/N//ig;
			$linearray[$i] =~ s/Y/1/ig;

			$linearray[$i] =~ s/0//ig;
			$linearray[$i] =~ s/1/Yes/ig;
			}
		#replace id-s with names in INSTITUTION
		if($i == $instpos-1)
			{
			$linearray[$i] =~ s/7/CdT/;
			$linearray[$i] =~ s/36/CEPOL-CdT/;
			$linearray[$i] =~ s/5/CJUE/;
			$linearray[$i] =~ s/2/COM/;
			$linearray[$i] =~ s/31/Com Swiss Data/;
			$linearray[$i] =~ s/8/COR\/EESC JS/;
			$linearray[$i] =~ s/1/Council/;
			$linearray[$i] =~ s/24/Council EUMS/;
			$linearray[$i] =~ s/18/CPVO-CdT/;
			$linearray[$i] =~ s/33/EASA-CdT/;
			$linearray[$i] =~ s/41/EBA-CdT/;
			$linearray[$i] =~ s/6/ECA/;
			$linearray[$i] =~ s/37/ECDC-CdT/;
			$linearray[$i] =~ s/32/ECHA-CdT/;
			$linearray[$i] =~ s/40/EDPS-CdT/;
			$linearray[$i] =~ s/30/EFSA-CdT/;
			$linearray[$i] =~ s/4/EIB/;
			$linearray[$i] =~ s/11/EMA-CdT/;
			$linearray[$i] =~ s/16/EMCDDA-CdT/;
			$linearray[$i] =~ s/3/EP/;
			$linearray[$i] =~ s/34/ERA-CdT/;
			$linearray[$i] =~ s/43/ESMA-CdT/;
			$linearray[$i] =~ s/9/EU-OSHA-CdT/;
			$linearray[$i] =~ s/17/FRA-CdT/;
			$linearray[$i] =~ s/38/FRONTEX-CdT/;
			$linearray[$i] =~ s/20/IATE/;
			$linearray[$i] =~ s/19/OHIM-CdT/;
			}
		if($i==$termpos-1)
			{
			if($count == 1)
				{
				print "TERM";
				print "\t";
				print "TERM_FORMAT";
	        		}
			else
				{
				$TERM=$linearray[$i];
				$termformat=$linearray[$i];
				$TERM =~ s/&lt;BR&gt;/ /ig;
				$TERM =~ s/&lt;[a-z\/]+&gt;//ig;
				$TERM =~ s/&amp;nbsp;/ /ig;  #glossary converter reject &nbsp; in terms
				$TERM =~ s/&amp;/&/ig;
				if($termformat eq $TERM)
					{
					$termformat = "";
					}
				#else   #experiment to make tags more readable: does not work: they will not shown at all
				#	{
				#	$termformat =~ s/&lt;BR&gt;/<BR>/ig;
				#		$termformat =~ s/&lt;([a-z\/]+)&gt;/<$1>/ig;
				#	$termformat =~ s/&amp;/&/ig;
				#	}

		
				print $TERM;
				print "\t";

				#nice outlook in sdldb:
				#$termformat =~ s/&lt;([a-z\/]+)&gt;/<$1>/g;
				print $termformat;
				}
			}
		else
			{
			#Obsolate:   $linearray[$i] =~ s/&lt;([a-z\/]+)&gt;/<$1>/g;  #Studio and Mutiterm display/visualize HTML entities
			print $linearray[$i];
			}

		if($i != $linearraysize-1)
			{
			print "\t";
			}


		}

#=cut
#print $_;

	$count++;
	}
	

#close $errorfile;

