
use strict;
#prints out the columns of table in the specified order

my $order;
my $table;
my $ordersize;
my $insize;
my @inarray;
my $argc;
my $in;
my $orderi;
my $lang;
my $langcol;

my $argc=@ARGV;
#print "argc=$argc\n";

if ( ($argc ne 4 ) && ($argc ne 5 ))
	{
	print  "Usage:\n";
	print  "perl iatepairs.pl lang langcol input output [-nohead]\n";
	exit ;
	}

$lang =  $ARGV[0];
$langcol =  $ARGV[1];

#print $lang;
#exit;

open(my $infile, "<", $ARGV[2] ) || die "can't open $ARGV[2]";
#binmode($infile, ":encoding(UTF-16LE)");


open(my $outfile, ">", $ARGV[3] ) || die "can't open $ARGV[3]";
binmode($outfile, ":raw");

my $head=1;

if($ARGV[4] eq "-nohead")
	{
	$head=0;
	}

#print "order=$order, ";
#print "ordersize=$ordersize\n";

my $count =1;

while ($in = <$infile>)
	{
	#print $outfile  $in;
	#next;
	if($count == 1 && $head == 1)
		{
		$count++;
		print $outfile  "$in";
		next;  #header
		}

	chomp($in);
	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	if($inarray[$langcol-1] eq $lang)
		{
		print $outfile  "$in\n";
		}
		
	$count++;
	}	

close $infile;
close $outfile;
