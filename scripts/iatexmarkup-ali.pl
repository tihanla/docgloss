use strict;

my $infile;
my $outfile;
my $patfile;
my $line;
my $aliline;

my $key;
my $val;
my %pat;

my @aliarr;
my @trgarr;

my $trgline;
my @linearray;
my $linesize;
my @keyarray;
my $keysize;
my $keystart;
my $keyend;
my @aliarr;
my $alisize;
my $trgval;
my $pos;
my $aligpos;
my $ii;
my $si;
my $ai;
my $bi;
my $ti;
my $prevtrgval;

open PAT, $ARGV[0] or die "error PAT"; binmode PAT, ":utf8";
open ALI, $ARGV[1] or die "error ALI"; binmode ALI, ":utf8";
open TRG, $ARGV[2] or die "error TARGET"; binmode TRG, ":utf8";

my $count=1;

while(<PAT>) 
	{
	chomp();
	$_ = lc($_);
	next if /^$/;  #empty
	next if /^#/;  #comment
	s/<[^>]*>//g;  #remove <tag>-s form 
	s/\"//g;       #remove " 
	($key, $val) = split (/\t/, $_);
	if($key ne $val)    #skip if identi
		{		
		$pat{$key} = $val;
		}
	#print "pat{$key}=$pat{$key}\n";  #DEBUG
	$count++;
	}
close PAT;
print "$count pat lines read\n";
$count=1;


$infile=$ARGV[3];
$outfile=$ARGV[4];

open IN, "<$infile" or die "error IN"; binmode IN, ":utf8";
open OUT, ">$outfile" or die "error OUT"; binmode OUT, ":utf8";

#print "$count\n";

my $ulkey;
my $ulval;
my $i;

while (<IN>) 
	{
	chomp();
	$line = " " . $_;
	$line =~ s/_/\\_/g;

	$aliline = <ALI>;
	@aliarr = split(/[- ]/, $aliline);
	$alisize=@aliarr;
	
	$trgline = <TRG>;
	@trgarr = split(/ /, $trgline);

		
	while( my ($key, $val) = each %pat)
		{
		 #old $line =~ s/$key/<x translation=\"$pat{$key}\">$key<\/x>/g;
		 $ulkey= $key;
		 $ulkey  =~ s/ /_/g;	
		 $val= $pat{$key};
		 while($line =~ s/ $key/ <x_translation=\"\">$ulkey<\/x>/)
			{
			#print "$line\n";  #DEBUG	
			#find out position of replacement in src:
                	@linearray = split (' ', $line, -1);
			$linesize=@linearray;
			#print "linesize=$linesize\n";  #DEBUG
			for ($i=0; $i != $linesize; $i++)
				{
				if($linearray[$i] eq "<x_translation=\"\">$ulkey<\/x>")
					{
					$keystart=$i;
					#print "found i=$i  <x_translation=\"\">$ulkey<\/x>\n";  #DEBUG
					last;
					}
				#print "i=$i\n";  #DEBUG
				}
			#find out source length:
                	@keyarray = split ('_', $ulkey, -1);
			$keysize=@keyarray;
			
			#print "$count: keystart=$keystart, keysize=$keysize linearray[$keystart]=$linearray[$keystart]\n";  #DEBUG

			#read out target using the alignment
			$ulval= "";
			$keyend = $keystart + $keysize;
			for($ii=0;$ii < $keysize; $ii++)
			#for($i=$keystart;$i < $keyend; $i++)
				{
				#calculate the position of word in the src sentence array
				$si=$keystart+$ii;            
				#find out that where it is in the alaignment

				for($ai=0; $ai< $alisize ;)
					{
					if($aliarr[$ai] eq $si)
						{
						last;
						}
					$ai++;
					$ai++;   #step by two
					}

				#print "ii=$ii, si=$si, aliarr[$ai]=$aliarr[$ai]\n";   #DEBUG

				#read out the trg alignment pair position
				$bi=$ai+1;  
				$ti=$aliarr[$bi];

				#read out the target word from target array

				$trgval=$trgarr[$ti];
				#print "ai=$ai bi=$bi, ti=$ti, trgval=$trgval\n";  #DEBUG

				if($trgval ne $prevtrgval)  # 1=1 2=1  constaructions
					{
					if($ulval ne "")
						{
						$ulval = $ulval . " ";
						}
					$ulval = $ulval . $trgval;
					}
				$prevtrgval=$trgval;

				}
			#replace 
			$line =~ s/ <x_translation=\"\">$ulkey<\/x>/ <x_translation=\"$ulval\">$ulkey<\/x>/;

			}
		 }

	$line =~ s/([^\\])_/\1 /g;
	$line =~ s/\\_/_/g;
	$line =~ s/^ //;

 	print OUT "$line\n" ;
	#print "line: $count\n";
	$count++;
	#if($count > 10)
	#	{
	#	exit;
	#	}

	}
close IN;
close OUT;
close PAT;
close ALI;
close TRG;

