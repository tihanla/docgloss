if WScript.Arguments.Count < 2 Then
    WScript.Echo "Please specify the source and the destination files. Usage: ExcelToCsv <doc/docx source file> <txt file>"
    Wscript.Quit
End If

Set objFSO = CreateObject("Scripting.FileSystemObject")

src_file = objFSO.GetAbsolutePathName(Wscript.Arguments.Item(0))
dest_file = objFSO.GetAbsolutePathName(WScript.Arguments.Item(1))

Const msoEncodingUTF8 = 65001
Const wdFormatUnicodeText = 7
  
Set objWord = CreateObject("Word.Application")
Set objDoc = objWord.Documents.Open(src_file)
objDoc.SaveAs dest_file, 7, , , , , , , , , , 65001
objDoc.Close
objWord.Quit

Set objDoc = Nothing
Set objWord = Nothing


