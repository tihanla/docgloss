use strict;
use warnings;
     
my $file;
my $out;

if ( @ARGV ne 2)
	{
	print "Usage:\nfreqmaven.pl textin freqout\n";
	exit;
	}

open ($file, "<", $ARGV[0])|| die "can't open $ARGV[0]";
open ($out, ">", $ARGV[1])|| die "can't open $ARGV[1]";

#$file = shift or die "Usage: $0 FILE\n";
#open my $fh, '<', $file or die "Could not open '$file' $!";

while (my $line = <$file>) 
	{
	$line =~ s/xC2\xA0/ /g;            #V3: normalize hard spaces 
	$line =~ s/\xE2\x80\x99/'/g;           #normalize apostrophs from typographic (8217) to ASCII (')
	print "$line";}
	}        

