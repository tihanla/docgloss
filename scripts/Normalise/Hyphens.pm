package Normalise::Hyphens;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_hyphens);

sub normalise_hyphens {
    my ($t, $lang) = @_;
    
    # list of some hyphen characters
    #
    # - U+002D HYPHEN-MINUS
    # ­ U+00AD SOFT HYPHEN
    # ‐ U+2010 HYPHEN
    # ‑ U+2011 NON-BREAKING HYPHEN
    # ‒ U+2012 FIGURE DASH
    # – U+2013 EN DASH
    # — U+2014 EM DASH
    # ― U+2015 HORIZONTAL BAR
    # − U+2212 MINUS SIGN

    # map U+00AD (SOFT HYPHEN) and U+2011 (NON-BREAKING HYPHEN) to U+002D (HYPHEN-MINUS) when followed by space
    $t =~ s/[­‑]+ /-/g;

    # in PT, map U+2011 (NON-BREAKING HYPHEN) to U+002D (HYPHEN-MINUS)
    if ($lang eq "pt") {
	$t =~ s/‑/-/g;
    }

    # map sequence of U+00AD (SOFT HYPHEN) and U+002D (HYPHEN-MINUS) to U+2011 (NON-BREAKING HYPHEN) when between letters
    $t =~ s/([\p{IsAlpha}])(­+-+|-+­+)([\p{IsAlpha}])/$1‑$3/g;

    if ($lang =~ /^pt|it|fr|cs|et|lv|fi$/) {
	# for these languages, map U+00AD (SOFT HYPHEN) to U+2011 (NON-BREAKING HYPHEN)
	$t =~ s/­/‑/g;
    } elsif ($lang eq "sv") {
	# for SV, map U+00AD (SOFT HYPHEN) to # - U+002D HYPHEN-MINUS
	$t =~ s/­/-/g;
	# ...and ¿ between spaces to U+2014 (EM DASH)
	$t =~ s/ ¿ / — /g;
    } else {
	# delete U+00AD (SOFT HYPHEN) in all other languages
	$t =~ s/­//g;
    }

    # occurrences of U+002D, U+2010, U+2011, U+2012, U+2013, U+2014, U+2015 mapped to U+002D (HYPHEN-MINUS) when immediately following a letter but not followed by a letter:
    $t =~ s/([\p{IsAlpha}])[-‐‑‒–—―]+([^\p{IsAlpha}])/$1-$2/g;
    # occurrences of U+002D, U+2010, U+2012, U+2013, U+2014, U+2015 (but not U+2011) mapped to U+002D (HYPHEN-MINUS) when between letters:
    $t =~ s/([\p{IsAlpha}])[-‐‒–—―]+([\p{IsAlpha}])/$1-$2/g;

    # occurrences of U+002D, U+2010, U+2011, U+2012, U+2013, U+2014, U+2015 mapped to U+2014 (EM DASH) when at the beginning of a line:
    $t =~ s/^[-‐‑‒–—―]+/—/g;

    # occurrences of U+002D, U+2010, U+2011, U+2012, U+2013, U+2014, U+2015 mapped to U+2014 (EM DASH) when at the end of a line:
    $t =~ s/[-‐‑‒–—―]+$/—/g;

    # occurrences of U+002D, U+2010, U+2011, U+2012, U+2013, U+2014, U+2015 mapped to U+2014 (EM DASH) when between spaces:
    $t =~ s/ [-‐‑‒–—―]+ / — /g;

    # U+2014 (EM DASH) mapped to U+002D (HYPHEN-MINUS) when between digits; delete spaces
    $t =~ s/(\d)\s*[—-]\s*(\d)/$1-$2/g;
    
    # replace "+/-" by the plus-minus sign
    $t =~ s/\+\/?\s*[-−—]/±/g;

    if ($lang =~ /^pt|sl$/) {
	# remove space between numbers and plus, minus, and plus-minus signs
	$t =~ s/([\+−±])\s*(\d)/$1$2/g;
    } elsif ($lang =~ /^pl|cs$/) {
	# remove space between numbers and plus, minus, and plus-minus signs
	$t =~ s/([\+−±])\s*(\d)/$1$2/g;
	# add non-breaking spaces when between numbers
	$t =~ s/(\d)\s*([\+−])\s*(\d)/$1 $2 $3/g;
	if ($lang eq "cs") {
	    $t =~ s/(\d)\s*±\s*(\d)/$1 ± $2/g;
	}
    } else {
	# add non-breaking space between numbers and plus, minus, and plus-minus signs
	$t =~ s/([\+−±])\s*(\d)/$1 $2/g;
    }
    
    # TODO: Phone numbers starting with "+"

    # dashes in parentheses are probably minusses
    $t =~ s/\([-−—]\)/(−)/g;
    
    return $t;
}

1;
