package Normalise::OfficialJournal;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_oj);

sub normalise_oj {
    my ($t, $lang) = @_;
    
    # JO "space" [CLS] "non-breaking space"
    if ($lang eq "pt") {
	$t =~ s/\bJO[  ](?:(?:n\.º|no\.?|n ?\.o)[  ])?([CLS])[  ](\d+)/JO $1 $2/g;
	$t =~ s/\bJO ([CLS]) (\d+)[  ]*([AE])[  ]+de/JO $1 $2 $3 de/g;
    } elsif ($lang eq "en") {
	$t =~ s/\bOJ[  ](?:No[  ])?([CLS])[  ](\d+)/OJ $1 $2/g;
	$t =~ s/\bOJ ([CLS]) (\d+)[  ]*([AE])[  ]?,/OJ $1 $2 $3,/g;
    } elsif ($lang eq "es") {
	$t =~ s/\bDO[  ](?:(?:n\.?º|no\.?|n ?\.o)[  ])?([CLS])[  ](\d+)/DO $1 $2/g;
	$t =~ s/\bDO ([CLS]) (\d+)[  ]*([AE])[  ]+de/DO $1 $2 $3 de/g;
    } elsif ($lang eq "de") {
	$t =~ s/\bA[bB][lL]\.?[  ](?:nr\.?[  ])?([CLS])[  ](\d+)/ABl. $1 $2/g;
	$t =~ s/\bA[bB][lL]\.? ([CLS]) (\d+)[  ]*([AE])[  ]+vom/ABl. $1 $2 $3 vom/g;
    } elsif ($lang eq "it") {
	$t =~ s/\bGU[  ](?:[nN]\.?[  ])?([CLS])[  ](\d+)/GU $1 $2/g;
	$t =~ s/\bGU ([CLS]) (\d+)[  ]*([AE])[  ]+del/GU $1 $2 $3 del/g;
    } elsif ($lang eq "fr") {
	$t =~ s/\bJO[  ](?:[nN]o[  ])?([CLS])[  ](\d+)/JO $1 $2/g;
	$t =~ s/\bJO ([CLS]) (\d+)[  ]*([AE]),?[  ]+du/JO $1 $2 $3 du/g;
    } elsif ($lang eq "bg"){
	$t =~ s/\bОВ[  ]?([CLS])(\d+)/ОВ $1 $2/g;
	$t =~ s/\bОВ([CLS]) (\d+)[  ]*([AE])[  ]+от/ОВ $1 $2 $3 от/g;
    } elsif ($lang eq "ro") {
	$t =~ s/\bJO[  ](?:n[or]?\.?[  ])?([CLS])[  ](\d+)/JO $1 $2/g;
	$t =~ s/\bJO ([CLS]) (\d+)[  ]*([AE])[  ]+(,|din)/JO $1 $2 $3,/g;
    } elsif ($lang eq "da") {
	$t =~ s/\bE([FU])T[  ](?:nr\.?[  ])?([CLS])[  ](\d+)/E$1T $2 $3/g;
	$t =~ s/\bE([FU])T ([CLS]) (\d+)[  ]*([AE])[  ]+af/E$1T $2 $3 $4 af/g;
	$t =~ s/\bEUT ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.(?:19\d\d|200[012]))/EFT $1/g; # OJ abbreviation changed in 2003
	$t =~ s/\bEFT ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.20(?:0[3-9]|1\d))/EUT $1/g;
    } elsif ($lang eq "el") {
	$t =~ s/\b(?:ΕΕ|EE)[  ](?:[Αα]ριθ\.?[  ])?([CLS])[  ](\d+)/ΕΕ $1 $2/g;
	$t =~ s/\b(?:ΕΕ|EE) ([CLS]) (\d+)[  ]*([AE]),?[  ]+της/ΕΕ $1 $2 $3 της/g;
    } elsif ($lang eq "mt") {
	$t =~ s/\bĠ\.?U\.?[  ](?:[Nn]ru[  ])?([CLS])[  ](\d+)/ĠU $1 $2/g;
	$t =~ s/\b(ĠU [CLS] \d+[  ]*[AE]?)[  ]*,?[  ]?(?:ta(?:[lsdt]|’ l)-[  ]?)?/$1, /g; # the Interinstitutional Style Guide proscribes the use of "ta’" here
	$t =~ s/\b(ĠU [CLS] \d+)[  ]*([AE]),/ĠU $1 $2,/g;
    } elsif ($lang eq "sv") {
	$t =~ s/\bE([GU])T[  ](?:nr[  ])?([CLS])[  ](\d+)/E$1T $2 $3/g;
	$t =~ s/\bE([GU])T ([CLS]) (\d+)[  ]*([AE])[  ]*,/E$1T $2 $3 $4,/g;
	$t =~ s/\bEUT ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.(?:19\d\d|200[012]))/EGT $1/g; # OJ abbreviation changed in 2003
	$t =~ s/\bEGT ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.20(?:0[3-9]|1\d))/EUT $1/g;
    } elsif ($lang eq "nl") {
	$t =~ s/\bPB[  ](?:[nN]r\.?[  ])?([CLS])[  ](\d+)/PB $1 $2/g;
	$t =~ s/\bPB ([CLS]) (\d+)[  ]*([AE])[  ]+van/PB $1 $2 $3 van/g;
    } elsif ($lang eq "pl") {
	$t =~ s/\bD[zZ]\.?U\.?[  ]+([CLS])[  ](\d+)/Dz.U. $1 $2/g;
	$t =~ s/\bDz.U. ([CLS]) (\d+)[  ]*([AE])[  ]+z/Dz.U. $1 $2 $3 z/g;
	$t =~ s/\b(Dz.U. [CLS] \d+(?: [AE]) z[  ]+[\d\. ]+,)[  ]+str\./$1 s./g; # the Interinstitutional Style Guide proscribes the use of "str." instead of "s."
    } elsif ($lang eq "cs") {
	$t =~ s/\b[Úú]ř\.[  ]věst\.[  ](?:[Čč]\.?[  ]*)?([CLS])[  ](\d+)/Úř. věst. $1 $2/g;
	$t =~ s/\bÚř\. věst\. ([CLS]) (\d+)[  ]*([AE])[  ]?,/Úř. věst. $1 $2 $3,/g;
	$t =~ s/\b(Úř\. věst\. [CLS] \d+(?: [AE]),[  ]+[\d\. ]+,)[  ]+str\./$1 s./g; # the Interinstitutional Style Guide proscribes the use of "str." instead of "s."
    } elsif ($lang eq "sk") {
	$t =~ s/\b[Úú]\.[  ]v\.[  ]E([SÚ])[  ]([CLS])[  ](\d+)/Ú. v. E$1 $2 $3/g;
	$t =~ s/\bÚ\. v\. E([SÚ]) ([CLS]) (\d+)[  ]*([AE])[  ]?,/Ú. v. E$1 $2 $3 $4,/g;
	$t =~ s/\b(Ú\. v\. E[SÚ] [CLS] \d+(?: [AE]),[  ]+[\d\. ]+,)[  ]+str\./$1 s./g; # the Interinstitutional Style Guide proscribes the use of "str." instead of "s."
	$t =~ s/\bÚ\. v\. EÚ ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.(?:19\d\d|200[012]))/Ú. v. ES $1/g; # OJ abbreviation changed in 2003
	$t =~ s/\bÚ\. v\. ES ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.20(?:0[3-9]|1\d))/Ú. v. EÚ $1/g;
    } elsif ($lang eq "et") {
	$t =~ s/\bE([LÜ])T[  ]([CLS])[  ](\d+)/E$1T $2 $3/g;
	$t =~ s/\bE([LÜ])T ([CLS]) (\d+)[  ]*([AE])[  ]*,/E$1T $2 $3 $4,/g;
	$t =~ s/\bELT ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.(?:19\d\d|200[012]))/EÜT $1/g; # OJ abbreviation changed in 2003
	$t =~ s/\bEÜT ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.20(?:0[3-9]|1\d))/ELT $1/g;
    } elsif ($lang eq "sl") {
	$t =~ s/\bUL[  ]([CLS])[  ](\d+)/UL $1 $2/g;
	$t =~ s/\bUL ([CLS]) (\d+)[  ]*([AE])[  ]?,/UL $1 $2 $3,/g;
    } elsif ($lang eq "lt") {
	$t =~ s/\bOL[  ]([CLS])[  ](\d+)/OL $1 $2/g;
	$t =~ s/\bOL ([CLS]) (\d+)[  ]*([AE])[  ]?,/OL $1 $2 $3,/g;
    } elsif ($lang eq "lv") {
	$t =~ s/\bOV[  ]([CLS])[  ](\d+)/OV $1 $2/g;
	$t =~ s/\bOV ([CLS]) (\d+)[  ]*([AE])[  ]?,/OV $1 $2 $3,/g;
    } elsif ($lang eq "fi") {
	$t =~ s/\bE([UY])VL[  ]([CLS])[  ](\d+)/E$1VL $2 $3/g;
	$t =~ s/\bE([UY])VL ([CLS]) (\d+)[  ]*([AE])[  ]?,/E$1VL $2 $3 $4,/g;
	$t =~ s/\bEUVL ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.(?:19\d\d|200[012]))/EYVL $1/g; # OJ abbreviation changed in 2003
	$t =~ s/\bEYVL ([CLS] \d+(?: [AE])?,[  ]*(?:[1-9]|[12]\d|3[01])\.1?\d\.20(?:0[3-9]|1\d))/EUVL $1/g;
    } elsif ($lang eq "hu") {
	$t =~ s/\bHL[  ]([CLS])[  ](\d+)/HL $1 $2/g;
	$t =~ s/\bHL ([CLS]) (\d+)\.?[  ]*([AE])[  ]?,/HL $1 $2. $3,/g;
    } elsif ($lang eq "ga") {
	$t =~ s/\bIO[  ]([CLS])[  ](\d+)/IO $1 $2/g;
	$t =~ s/\bIO ([CLS]) (\d+)[  ]*([AE])[  ]?,/IO $1 $2 $3,/g;
    }

    return $t;
}

1;
