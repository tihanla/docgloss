package Normalise::Numbers;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_numbers normalise_articles);

sub normalise_numbers {
    my ($t, $lang) = @_;
    
    # numbers before measurement units
    my $units = "M?EUR|GBP|USD|CHF|HUF|LVL|DEM|GRD|FRF|IEP|ITL|LUF|NLG|ATS|PTE|FIM|SEK|Ah|Å|atm|bar|Bq|k?cal|cd|cv|dam|eV|sr|°[CF]|Gy|ha|yd|kat|kbit|kbyte|[KMGT]B|lb|lm|lx|mi|mmHg|min|mol|Np|\x{03a9}|\x{2126}|oz|h?Pa|ft|ct|kgf|kJ|ra?d|rpm|Sv|[mk]V|[mcdhk]?m|t|[mkK]?g|[mcd]?l|[kM]?Hz|dB|[kM]Wh?";
    # add language-specific abbreviations and long names here:
    if ($lang eq "pt") {
	my $localprefixes = "dec[aâ]|hect[oó]|quil[oó]|mega|giga|tera|peta|exa|zeta|iota|dec[ií]|cent[ií]|mil[ií]|micr[oó]|nan[oó]|pic[oó]|fe[mn]t[oó]";
	my $localunits = "acres?|alqueires?|amp[èe]res?|ångströms?|anos?|ares?|atmosferas?|barn?|becquere(?:l|éis)|be(?:l|éis)|bits?|bytes?|calorias?|candelas?|cavalos?|ciclos?|coulombs?|decibe(?:l|éis)|dias?|electrovolts?|esterradianos?|farads?|gal(?:ão|ões)|gramas?|graus?|grays?|hectares?|henr(?:y|ies)|hertz|horas?|jardas?|joules?|katal|[kK]elvin|léguas?|libras?|litros?|lúmen(?:es)?|lux|metros?|milhas?|minutos?|moles?|neper|newtons?|nós?|ohms?|onças?|pasca(?:l|is)|pés?|polegadas?|quilates?|radianos?|rotações|segundos?|sievert|toneladas?|volts?|watts?";
	my $localother = "milhões|milhares|(?:b|tr)iliões|euros|dólares|coroas|marcos|ecus|unidades|in";
	$units = "(?:de )?(?:$units|(?:$localprefixes)?(?:$localunits)|$localother)";
#    } elsif ($lang eq "es") {
#	my $localprefixes = "dec[aá]|hect[oó]|kil[oó]|mega|giga|tera|peta|exa|zetta|yotta|dec[ií]|cent[ií]|mil[ií]|micr[oó]|nan[oó]|pic[oó]|femt[oó]";
# TODO: units for other languages
    }
    my $nr_re = '(?:[\d  ]+(?:[,\.][\d  ]+)?|(?:[\d\.]+,|[\d,]+\.)[\d  ]+|[  ]*\d{1,3}(?:(?:,\d{3}){2,}|(?:\.\d{3}){2,})[  ]*)';
    my $pre_re = ".*?[ \(\[\{“\«‘\¿\¡\+−±\-]";
    my $out = "";
    # group in blocks of 3 digits
    while ($t =~ /^((?:$pre_re)??)($nr_re)($units)\b/op) {
    	my ($pre, $number, $unit) = ($1, $2, $3);
	$t = ${^POSTMATCH};
	# skip in these three cases:
	# 'numbers' without digits
	# 'numbers' ending in dot/comma
	# Lithuanian years using the abbreviation 'm.' as in '2012 m.'
	if ($number =~ /^\s*$/ || $number =~ /[,\.]\s*$/ || ($lang eq "lt" && $unit eq "m" && $number =~ /^\s*\d{4}\s*$/)) {
	    $out .= "$pre$number$unit";
	    next;
	}
	# remove all spaces except at the beginning
	$number =~ s/(.)[  ]+/$1/g;
	# remove dots or commas when used as a thousands separator
	if ($number =~ /^[  ]*([\d\.]+,\d+|\d{1,3}(\.\d{3}){2,})$/) {
	    $number =~ s/\.//g;
	} elsif ($number =~ /^[  ]*([\d,]+\.\d+|\d{1,3}(,\d{3}){2,})$/) {
	    $number =~ s/,//g;
	}
	$number =~ /^(\s*\d+)([,\.]\d+)?$/;
	my ($precomma, $postcomma) = ($1, $2);
	my $newnr = "";
	while ($precomma =~ /\d{3}$/) {
	    $precomma =~ s/(\d{3})$//;
	    $newnr = " $1$newnr"; # non-breaking space
	}
	$newnr = $precomma.$newnr;
	$newnr =~ s/^ //; # non-breaking space
	if (defined $postcomma) {
	    $newnr .= $postcomma;
	}
	$out .= "$pre$newnr $unit";
    }
    $t = $out.$t;
    # decimal point or comma
    if ($lang =~ /^en|ga|mt$/) {
	$t =~ s/(\d),(\d+)[  ]($units)([^\p{IsAlnum}])/$1.$2 $3$4/g; # non-breaking space
	$t =~ s/(\d),(\d+)[  ]($units)$/$1.$2 $3/g; # non-breaking space
    } else {
	$t =~ s/(\d)\.(\d+)[  ]($units)([^\p{IsAlnum}])/$1,$2 $3$4/g; # non-breaking space
	$t =~ s/(\d)\.(\d+)[  ]($units)$/$1,$2 $3/g; # non-breaking space
    }
    # other numbers with spaces between them are usually codes etc. and thus belong together -> replace spaces by non-breaking spaces
    $t =~ s/(\d) (\d)/$1 $2/g;

    return $t;
}

sub normalise_articles {
    my ($t, $lang) = @_;
    if ($lang eq "pt") {
	my $out;
	while ($t =~ /artigos?[  ](?:,?[  ]*[ea]?[  ]*\d+\.?º?)+/iop) {
	    $out .= ${^PREMATCH};
	    $t = ${^POSTMATCH};
	    my $articles = ${^MATCH};
	    $articles =~ s/(\d+)\.?º?/$1.º/g;
	    $out .= $articles;
	}
	$out .= $t;
	return $out;
    }
    return $t;
}

1;
