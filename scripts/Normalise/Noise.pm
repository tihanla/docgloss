package Normalise::Noise;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(delete_noise repair_noise);

sub delete_noise {
    my ($t, $lang) = @_;

    # delete control characters
    $t =~ s/[\x{0001}-\x{0008}\x{000b}-\x{001f}\x{007f}\x{0081}-\x{0084}\x{0086}-\x{0089}\x{008b}-\x{0092}\x{0095}-\x{009f}]//g;
    unless ($lang eq "lt") {
	# In the Lithuanian corpus, \x{008a} is sometimes used for Š
	$t =~ s/\x{008a}//g;
    }
    unless ($lang eq "mt") {
	# In the Maltese corpus, some of these are used
	$t =~ s/[\x{0085}\x{0093}\x{0094}]//g;
    }

    # delete 'line separator' and 'paragraph separator' characters
    $t =~ s/[\x{2028}\x{2029}]//g;

    # delete 'box drawing' and 'block elements' characters
    $t =~ s/[\x{2500}-\x{259f}]//g;

    # delete characters from Unicode's private use area
    $t =~ s/[\x{e000}-\x{f8ff}]//g;

    # remove tags
    $t =~ s/<\s*\/?\s*(a|b|br|i|keywords|refer|ac|pvtx|jump|hyp|strong|em|u|crossref|docref|date|ul|li|nbsp\/?|title|img|span|cf|p)(\s[^<>]*)?>//gi;
    $t =~ s/<[RC]COC\d+~//g;
    $t =~ s/~[RC]COC\d+>//g;
    $t =~ s/>(PIC FILE|ISO_)[^<>]*>//g;

    # remove "+++++ TIFF +++++"
    $t =~ s/\+{5} TIFF \+{5}//g;

    # delete empty brackets
    $t =~ s/\(\)//g;
    $t =~ s/\[\]//g;

    return $t;
}

sub repair_noise {
    my ($t) = @_;

    # for some reason, the corpus contains words where "REF" has been put into angle brackets, e.g. "<REF>ERENDUM"; undo this
    $t =~ s/<REF>([\p{IsAlpha}])/REF$1/g;
    $t =~ s/([\p{IsAlpha}])<REF>/$1REF/g;

    # replace '__NEWLINE__' by space (actually putting newline characters would obviously mess up the sentence alignment)
    $t =~ s/__NEWLINE__/ /g;

    return $t;
}

1;
