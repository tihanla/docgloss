package Normalise::Predefregexp;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(notouch_patterns vowels);

sub notouch_patterns {

    # patterns for parts of the input string that should never be normalised
    my $pattern_URI = '\b(?:[a-z][\w\-]+:(?:/{1,3}|[a-z0-9\%])|www\d{0,3}[\.]|[a-z0-9\.\-]+\.[a-z]{2,4}/)(?:[^\s\(\)<>]+|\((?:[^\s\(\)<>]+|(?:\([^\s\(\)<>]+\)))*\))+(?:\((?:[^\s\(\)<>]+|(\(?:[^\s\(\)<>]+\)))*\)|[^\s\`!\(\)\[\]\{\}\;:\'\"\.,<>\?«»“”‘’])';
    my $pattern_EMAIL = '[\w\.\-]+\@[\w\.\-]+';
    my $pattern_ENTITY = '\&(?:[a-zA-Z]+|#[0-9]+|#x[0-9a-fA-F]+);';

    # don't forget to add the above pattern names in the expression below
    my $notouch_patterns = " *(?:$pattern_URI|$pattern_EMAIL|$pattern_ENTITY) *";

    return $notouch_patterns;
}

sub vowels {
    my $vowels = "[haeiouäëïöüáéíóúàèìùòâêîôûãẽĩõũăĕĭŏŭőűąęįųāēīōūėœøyåůæăý]";

    return $vowels;
}

1;
