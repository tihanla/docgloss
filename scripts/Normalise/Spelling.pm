package Normalise::Spelling;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_spelling normalise_superscript_o normalise_case normalise_spelling_end);

sub normalise_spelling {
    my ($t, $lang) = @_;
    
    if (eval "use Normalise::Spelling::$lang qw(normalise_sp); 1") {
	$t = &normalise_sp($t);
    }

    # normalise "Leader+"
    $t =~ s/\b(leader)\s*\+/$1+/gi;

    return $t;
}

sub normalise_spelling_end {
    my ($t, $lang) = @_;
    
    if (eval "use Normalise::Spelling::$lang qw(normalise_sp_end); 1") {
	$t = &normalise_sp_end($t);
    }

    return $t;
}

sub normalise_superscript_o {    
    my ($t, $lang) = @_;
    
    # "superscript o"-like symbols standing for degree, number etc.

    if ($lang eq "pt") {
	# o, U+00BA, U+00B0, U+02DA mapped to U+00BA (masculine ordinal indicator) when probably meaning "número(s)":
	$t =~ s/\b([nN])\.[oº°˚](s?[  ])/$1.º$2/g; # space or non-breaking space (U+00A0) at the end
	$t =~ s/\b([nN])[º°˚]/$1.º/g;
	$t =~ s/\bNo\.(s?[  ])/N.º$1/g;
	$t =~ s/\b([nN])\.ºs/$1.os/g;

	# ordinal numbers
	$t =~ s/(\d)\.?[  ]*[º°˚]/$1.º/g;
	$t =~ s/(\d)\.?o/$1.º/g;
    } elsif ($lang eq "es") {
	# o, U+00BA, U+00B0, U+02DA mapped to U+00BA (masculine ordinal indicator) when probably meaning "número(s)":
	$t =~ s/\b([nN])\.?[º°˚](s?[  ])/$1º$2/g;
	$t =~ s/\b([nN])\.o(s?[  ])/$1º$2/g;
	$t =~ s/\b([nN])os?\.[  ](\d)/$1º $2/g;
	
	# ordinal numbers
	$t =~ s/(\d)\.?[  ]*[º°˚]/$1º/g;
	$t =~ s/(\d)\.?o/$1º/g;
    } elsif ($lang eq "fr") {
	# according to two French translators at the EP, the degree sign is generally used in the "numéro" abbreviation. The style guide, however, proscribes the letter "o"
	$t =~ s/\b([nN])[oº°˚]\.?([  ])/$1o$2/g;
	$t =~ s/\b([nN])[oº°˚]s\.?([  ])/$1os$2/g;
    } elsif ($lang eq "mt") {
	# nº isn't used in Maltese
	$t =~ s/\bn[º°]/Nru/gi;
	# geographic coordinates
	$t =~ s/(\d)º[  ]*([NWSE]\b|\d)/$1° $2/g;
	# all other occurrences of º seem to be ill-formated non-breaking spaces
	$t =~ s/([^  ])º([^  ])/$1 $2/g;
    } elsif ($lang eq "sv") {
	# nº isn't used in Swedish
	$t =~ s/\bn[º°]/nr/gi;
    } elsif ($lang eq "nl") {
	# nº isn't used in Dutch
	$t =~ s/\bn[º°]/nr./gi;
    }

    if ($lang =~ /^bg|el|sv|nl|pl|cs|sk|et|sl|lt|lv|fi|hu|ga$/) {
	# in these languages, convert all remaining symbols to the degree sign
	$t =~ s/[º°˚]/°/g;
    }

    if ($lang eq "bg") {
	# Bulgarian uses its own number symbol
	$t =~ s/\b[nN][o°]\.?[  ]*(\d)/№ $1/g;
    }

    # U+00BA, U+00B0, U+02DA mapped to U+00B0 (degree sign) when probably meaning "degree"
    if ($lang eq "pt") {
	$t =~ s/[º°˚] ?([CF]\b)/<DEGREEE>$1/g;
	$t =~ s/([\d ])o([CF]\b)/$1<DEGREEE>$2/g;
	# undo conversion for Portuguese ordinals and números (note: this is not perfect yet; there are a number of cases where it is hard to decide which format is correct)
	$t =~ s/(\b[nN]|\d)\.<DEGREEE>([CF])/$1.º $2/g;
	$t =~ s/<DEGREEE>/°/g;
    } else {
	$t =~ s/[º°˚] ?([CF]\b)/°$1/g;
	$t =~ s/([\d ])o([CF]\b)/$1°$2/g;
    }
    if ($lang eq "it") {
	# In Italian, all other degree signs following a digit are assumed to be masculine ordinal indicators.
	# Note that this assumption corrupts all (correct) uses of the degree sign where it is not followed by "C" or "F"!
	$t =~ s/(\d)[  ]*°/$1º/g;
	# repair at least for geographic coordinates
	$t =~ s/(\d)[º°˚][  ]*([NOSE]\b)/$1° $2/g;
	# replace nº (unused in Italian) by "n."
	$t =~ s/\bn\.?[º°˚]/n./gi;
    }
    
    return $t;
}

sub normalise_case {
    my ($t, $lang) = @_;
    unless ($lang =~ /^de|nl$/) {
	$t =~ s/([:;] )([\p{IsAlpha}][^\p{IsUpper}])/$1.lcfirst($2)/ge;
    }
    return $t;
}

1;
