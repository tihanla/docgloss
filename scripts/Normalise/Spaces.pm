package Normalise::Spaces;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_dots_spaces_start normalise_spaces_end normalise_nbsp finalise_line);

sub normalise_dots_spaces_start {
    my ($t, $lang) = @_;
    
    # replace some more exotic space characters by normal spaces
    $t =~ s/[\x{1680}\x{180E}\x{2000}-\x{200a}\x{2028}\x{2029}\x{202f}\x{205f}\x{3000}]/ /g;
    
    # compact multiple spaces
    $t =~ s/ +/ /g;

    # replace U+2026 (HORIZONTAL ELLIPSIS) with three dots
    $t =~ s/…/.../g;
    if ($lang eq "mt") {
	$t =~ s/\x{0085}/.../g;
    }

    # standardise 'fill-in' lines to '___'
    $t =~ s/___+/___/g;

    return $t;
}

sub normalise_spaces_end {
    my ($t, $lang) = @_;

    # compact multiple spaces
    $t =~ s/ +/ /g;
    $t =~ s/ * + */ /g;

    # have spaces before and after "&"
    $t =~ s/ ?\& ?/ \& /g;

    # eliminate space after...
    $t =~ s/([\(\[\{\¿\¡\/„‚]) /$1/g;
    # eliminate space before...
    $t =~ s/ ([\.,\!\?;:\)\]\}\/·])/$1/g;

    # whether the following are starting or ending quotes depends on the language; eliminate space accordingly
    if ($lang  =~ /^de|bg|cs|sk|sl|lt$/) {
	$t =~ s/ ([“‘])/$1/g;
    } else {
	$t =~ s/([“‘]) /$1/g;
    }
    if ($lang =~ /^da|pl|hu$/) {
	$t =~ s/ «/«/g;
	$t =~ s/» /»/g;
    } else {
	$t =~ s/« /«/g;
	$t =~ s/ »/»/g;
    }
    unless ($lang =~ /^sv|fi$/) {
	# Swedish and Finnish use the same character for both opening and closing quotes -> no changes
	$t =~ s/ ”/”/g;
	unless ($lang eq "nl") {
	    # Dutch uses ’ only as apostrophe, and words can start with it -> no changes
	    if ($lang eq "mt") {
		# Maltese ’l and ’il start with ’ -> protect them from losing the space in front
		$t =~ s/ (’i?l\b)/üüü$1/gi;
	    }
	    $t =~ s/ ’/’/g;
	    if ($lang eq "mt") {
		# undo Maltese hack
		$t =~ s/üüü(’i?l\b)/$1/gi;
	    }
	}
    }
    if ($lang eq "el") {
	# Greek apostrophes are always followed by a non-breaking space
	$t =~ s/([\p{Greek}])’\s*([\p{Greek}])/$1’ $2/g;
    }

    # compact multiple commas
    $t =~ s/,+/,/g;

    return $t;
}

sub finalise_line {
    my ($t) = @_;

    # remove spaces at beginning and end of the line
    $t =~ s/^ *//;
    $t =~ s/ *$//;

    # remove some punctuation items at beginning of sentence that never start a sentence
    $t =~ s/^[,\!\?;: ]+//;

    # remove comma before sentence-final punctuation
    $t =~ s/,+([\.!\?:;])$/$1/;
    
    return $t;
}    

sub normalise_nbsp {
    my ($t, $lang) = @_;
    
    # non-breaking spaces

    if ($lang eq "pt") {
	# add non-breaking space between número etc. and the following digit or Roman numeral
	$t =~ s/\b(n\.ºs?|artigos?|anexos?|quadros?|capítulos?)\s*(\d|[IVX]+\b)/$1 $2/gi;
    } elsif ($lang eq "es") {
	# add non-breaking space between número etc. and the following digit or Roman numeral
	$t =~ s/\b(nºs?|artículos?|anexos?|apartados?|partidas?|capítulos?|sección|secciones|notas?|puntos?|párrafos?)\s*(\d|[IVX]+\b)/$1 $2/gi;
    } elsif ($lang eq "bg") {
	$t =~ s/\b(№|член|регламент|книга|ред|район|позици[яи]|дял|приложени[ея]|раздел)\s*(\d|[IVX]+\b)/$1 $2/gi; # dealing with articles, reglaments, etc followed directly by a Roman numeral
    }

    # add non-breaking space between page abbreviation and the following digit or Roman numeral
    if ($lang =~ /^pt|en|es|fr|ro|mt|lt$/) {
	$t =~ s/\b(pp?\.)\s*(\d|[IVX]+\b)/$1 $2/g;
    } elsif ($lang eq "de") {
	$t =~ s/\bS\.\s*(\d|[IVX]+\b)/S. $1/g;
    } elsif ($lang =~ /^da|sv|fi$/) {
	$t =~ s/\bs\.\s*(\d|[IVX]+\b)/s. $1/g;
    } elsif ($lang eq "it") {
	$t =~ s/\b(pagg?\.)\s*(\d|[IVX]+\b)/$1 $2/g;
    } elsif ($lang eq "bg") {
	$t =~ s/\b(стр\.)\s*(\d|[IVX]+\b)/$1 $2/g;
    } elsif ($lang eq "el") {
	$t =~ s/\bσ\.\s*(\d|[IVX]+\b)/σ. $1/g;
    } elsif ($lang eq "nl") {
	$t =~ s/\bblz\.\s*(\d|[IVX]+\b)/blz. $1/g;
    } elsif ($lang =~ /^pl|cs|sk$/) {
	$t =~ s/\b(s(?:tr)?\.)\s*(\d|[IVX]+\b)/$1 $2/g;
    } elsif ($lang eq "et") {
	$t =~ s/\blk\.?\s*(\d|[IVX]+\b)/lk $1/g;
    } elsif ($lang eq "sl") {
	$t =~ s/\bstr\.\s*(\d|[IVX]+\b)/str. $1/g;
    } elsif ($lang eq "ga") {
	$t =~ s/\blch\.\s*(\d|[IVX]+\b)/lch. $1/g;
    } elsif ($lang eq "lv") {
	$t =~ s/(\d|\b[IVX]+)\.\s*lpp\./$1. lpp./g;
    } elsif ($lang eq "hu") {
	$t =~ s/(\d|\b[IVX]+)\.\s*o\./$1. o./g;
    }

    if ($lang eq "cs") {
	# one-letter conjunctions and prepositions
	$t =~ s/([^\.\w])([aikosuvz]) /$1$2 /gi;
	$t =~ s/^([aikosuvz]) /$1 /gi;
	# other prepositions
	$t =~ s/\b(ve|ke|ku|že|na|do|od|pod) /$1 /gi; 
	# abbreviations
	$t =~ s/\ba\. s\./a. s./g;
	$t =~ s/\bakad\.[  ](arch|mal|soch)\.[  ]/akad. $1 /g;
	$t =~ s/\b(arch|brig|č|čísl|čl|DiS|[dD]oc|[dD]r|DrSc|gen|hl|Ing|JUDr|kap|max|Mgr|mil|min|mj|mld|M[UV]Dr|např|nazv|nepov|nerozl|obč|obch|obj|obr|odb|odst|ozn|p|p\.a|PaedDr|Ph[DM]r|písm|plk|polit|[pP]opř|pov|[pP]ozn|pplk|[pP]rof|[pP]říp|pův|r|[rR]eg|resp|RNDr|rozl|s|Sb|spol|[sS]rov|stol|[sS]tr|[sS]v|sz|[tT]ab|tech|[tT]el|tis|[tT]j|[tT]z[nv]|[úÚ]ř|v|[vV]ar|vč|věst|zákl|[zZ]ejm|[zZ]kr|[zZ]n)\. /$1. /g;
	$t =~ s/\b([dD]r)\.[  ]h\.[  ]c\.[  ]/$1. h. c. /g;
	$t =~ s/\bJ\.[  ]E\.[  ]/J. E. /g;
	$t =~ s/\bJ\.[  ]K\.[  ]([JV])\.[  ]/J. K. $1. /g;
	$t =~ s/\bj\.[  ]n\.[  ]/j. n. /g;
	$t =~ s/\bk\.[  ]s\.[  ]/k. s. /g;
	$t =~ s/\bn\.[  ]m\.[  ]/n. m. /g;
	$t =~ s/\bo\.[  ]p\.[  ]s\.[  ]/o. p. s. /g;
	$t =~ s/\bPh\.[  ]D\.[  ]/Ph. D. /g;
	$t =~ s/\b([sS])\.[  ]p\.[  ]/$1. p. /g;
	$t =~ s/\b([sS])\.[  ]r\.[  ]o\./$1. r. o./g;
	$t =~ s/\bt\.[  ]r\.[  ]/t. r. /g;
	$t =~ s/\b([vV]) z\. /$1 z. /g;
	$t =~ s/\bv\.[  ]o\.[  ]s\.[  ]/v. o. s. /g;
	$t =~ s/\bv\.[  ]r\.[  ]/v. r. /g;
    }

    # add non-breaking space between number and degree
    $t =~ s/(\d)\s*(°[CF]\b)/$1 $2/g;

    if ($lang eq "hu") {
	# in Hungarian, there should not be a space before the percent sign
	$t =~ s/(\d)\s*\%/$1%/g;
    } else {
	# replace space before percent sign by non-breaking spaces; add if none present
	$t =~ s/(\d)\s*\%/$1 %/g;
    }

    # add non-breaking space between inequality sign and number
    $t =~ s/([<>≤≥])[  ]*(\d)/$1 $2/g;

    if ($lang eq "bg") {
	# add non-breaking space between year and "г."
	$t =~ s/\b(\d{4})[  ]*г\./$1 г./g;
	# add non-breaking space between "№" and number
	$t =~ s/№[  ]*(\d)/№ $1/g;
    }

    if ($lang eq "de") {
	# add non-breaking space in certain German abbreviations
	$t =~ s/\bd\. ?h\.(\W)/d. h.$1/g;
	$t =~ s/\bu\. ?([aUäÄ])\.(\W)/u. $1.$2/g;
	$t =~ s/\bz\. ?([BETZ])\.(\W)/z. $1.$2/g;
    }

    return $t;
}

1;
