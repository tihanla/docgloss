package Normalise::Units;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_euro normalise_square_cubic normalise_geocoord);

sub normalise_euro {
    my ($t, $lang) = @_;

    # Euro
    $t =~ s/[\x{0080}€]/EUR/g;
    $t =~ s/(\d)EUR\b/$1 EUR/g;
    $t =~ s/\bEUR(\d)/EUR $1/g;

    return $t;
}

sub normalise_square_cubic {
    my ($t) = @_;

    # square and cubic
    $t =~ s/\b([mcdk]?m)(?:2|[  ]²)([^\p{IsAlnum}])/$1²$2/g;
    $t =~ s/\b([mcdk]?m)(?:2|[  ]²)$/$1²/g;
    $t =~ s/\b([mcdk]?m)(?:3|[  ]³)([^\p{IsAlnum}])/$1³$2/g;
    $t =~ s/\b([mcdk]?m)(?:3|[  ]³)$/$1³/g;
    $t =~ s/(\d)([  ]?[mcdk]?m)(?:2|[  ]²)([^\p{IsAlnum}])/$1 $2²$3/g; # non-breaking space
    $t =~ s/(\d)([  ]?[mcdk]?m)(?:2|[  ]²)$/$1 $2²/g; # non-breaking space
    $t =~ s/(\d)([  ]?[mcdk]?m)(?:3|[  ]³)([^\p{IsAlnum}])/$1 $2³$3/g; # non-breaking space
    $t =~ s/(\d)([  ]?[mcdk]?m)(?:3|[  ]³)$/$1 $2³/g; # non-breaking space

    return $t;
}

sub normalise_geocoord {    
    my ($t) = @_;
    
    # geographic coordinates with U+2032 and U+2033
    $t =~ s/(\d)\s*°\s*([\d\.,]+)[‘’\']/$1° $2′/g;
    $t =~ s/(\d)° (\d+)′\s*([\d\.,]+)(?:[“”\"]|[‘’\']{2})/$1° $2′ $3″/g;

    return $t;
}

1;
