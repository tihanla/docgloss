package Normalise::Quotes;

use strict;
use utf8;

use Normalise::Predefregexp qw(vowels);

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes_apostrophes opening_quotes closing_quotes);

sub normalise_quotes_apostrophes {
    my ($t, $lang) = @_;

    # list of some quotation characters potentially appearing in the corpus:
    #
    # " U+0022 QUOTATION MARK
    # $ U+0024 DOLLAR SIGN
    # ' U+0027 APOSTROPHE
    # , U+002C COMMA
    # < U+003C LESS-THAN SIGN
    # > U+003E GREATER-THAN SIGN
    # ` U+0060 GRAVE ACCENT
    # « U+00AB LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
    # ´ U+00B4 ACUTE ACCENT
    # » U+00BB RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
    # ¿ U+00BF INVERTED QUESTION MARK
    # ˝ U+02DD DOUBLE ACUTE ACCENT
    # ‘ U+2018 LEFT SINGLE QUOTATION MARK
    # ’ U+2019 RIGHT SINGLE QUOTATION MARK
    # ‚ U+201A SINGLE LOW-9 QUOTATION MARK
    # ‛ U+201B SINGLE HIGH-REVERSED-9 QUOTATION MARK
    # “ U+201C LEFT DOUBLE QUOTATION MARK
    # ” U+201D RIGHT DOUBLE QUOTATION MARK
    # „ U+201E DOUBLE LOW-9 QUOTATION MARK
    # ‟ U+201F DOUBLE HIGH-REVERSED-9 QUOTATION MARK
    # ′ U+2032 PRIME
    # ‹ U+2039 SINGLE LEFT-POINTING ANGLE QUOTATION MARK
    # › U+203A SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
    
    # apostrophes
    my $vowels = &vowels();
    $t =~ s/([\p{IsAlpha}])[´\'‘\`](s\b)/$1’$2/gi;
    $t =~ s/([\p{IsAlpha}])[´\'‘\`]($vowels)/$1’$2/gi;

    # double acute accent
    unless ($lang eq "hu") {
	$t =~ s/˝/"/g;
    }

    if (eval "use Normalise::Quotes::$lang qw(normalise_quotes); 1") {
	$t = &normalise_quotes($t);
    }

    # apostrophes again
    $t =~ s/s\' /s’ /gi;
    
    return $t;
}

sub opening_quotes {
    my %opening_quotes = ();

    # Swedish and Finnish are not mentioned here because they use the same character for both opening and closing quotes

    # « U+00AB
    foreach (qw(es el fr it pt ro)) {
	$opening_quotes{"«"}{$_} = 1;
    }
    # » U+00BB
    foreach (qw(da hu pl)) {
	$opening_quotes{"»"}{$_} = 1;
    }
    # ‘ U+2018
    foreach (qw(es el en fr ga it mt pt)) {
	$opening_quotes{"‘"}{$_} = 1;
    }
    # ‚ U+201A
    foreach (qw(cs de sk sl)) {
	$opening_quotes{"‚"}{$_} = 1;
    }
    # “ U+201C
    foreach (qw(es el en fr ga it lv mt pt)) {
	$opening_quotes{"“"}{$_} = 1;
    }
    # „ U+201E
    foreach (qw(bg cs de et lt hu nl pl ro sk sl)) {
	$opening_quotes{"„"}{$_} = 1;
    }
    
    return \%opening_quotes;
}

sub closing_quotes {
    my %closing_quotes = ();

    # Swedish and Finnish are not mentioned here because they use the same character for both opening and closing quotes

    # « U+00AB
    foreach (qw(da hu pl)) {
	$closing_quotes{"«"}{$_} = 1;
    }
    # » U+00BB
    foreach (qw(es el fr it pt ro)) {
	$closing_quotes{"»"}{$_} = 1;
    }
    # ‘ U+2018
    foreach (qw(cs de sk sl)) {
	$closing_quotes{"‘"}{$_} = 1;
    }
    # ’ U+2019
    foreach (qw(es el en fr ga it mt pt)) {
	$closing_quotes{"’"}{$_} = 1;
    }
    # “ U+201C
    foreach (qw(bg cs de lt sk sl)) {
	$closing_quotes{"“"}{$_} = 1;
    }
    # ” U+201D
    foreach (qw(es et el en fr ga it lv hu mt nl pl pt ro)) {
	$closing_quotes{"”"}{$_} = 1;
    }
    
    return \%closing_quotes;
}

1;
