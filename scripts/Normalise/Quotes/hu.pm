package Normalise::Quotes::hu;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # Hungarian usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x”   U+0022 x U+201D (mapped to preferred_double)
    # «x»   U+00AB x U+00BB (mapped to preferred_second_level)
    # »x«   U+00BB x U+00AB (preferred_second_level)
    # ‘x’   U+2018 x U+2019 (mapped to preferred_third_level)
    # ’x’   U+2019 x U+2019 (preferred_third_level)
    # ‚x’   U+201A x U+2019 (mapped to preferred_third_level)
    # “x”   U+201C x U+201D (mapped to preferred_double)
    # „x"   U+201E x U+0022 (mapped to preferred_double)
    # „x”   U+201E x U+201D (preferred_double)

    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)
    
    if ($t =~ /^[^«»]*(?:«[^«»]*»[^«»]*)+$/) {
	$t =~ s/«[  ]*/üüü/g;
	$t =~ s/[  ]*»/«/g;
	$t =~ s/üüü/»/g;
    }
    $t =~ s/^«/»/;
    $t =~ s/»$/«/;
    $t =~ s/»(\s*[\.;])$/«$1/;

    if ($t =~ /^[^\"“„”]*(?:[\"“][^\"“„”]*”[^\"“„”]*)+$/) {
	$t =~ s/[\"“]([^\"“„”]*”)/„$1/g;
    }
    if ($t =~ /^[^\"„”]*(?:„[^\"„”]*\"[^\"„”]*)+$/) {
	$t =~ s/(„[^\"„”]*)\"/$1”/g;
    }
    my @dq = ($t =~ /\"/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\"(.*?)\"/„$1”/g;
    }
    $t =~ s/^[\"“]/„/;
    $t =~ s/\"$/”/;
    $t =~ s/\"(\s*[\.;])$/”$1/;

    $t =~ s/[\'‘‚]/’/g;

    return $t;
}

1;
