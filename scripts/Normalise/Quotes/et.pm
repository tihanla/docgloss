package Normalise::Quotes::et;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # Estonian usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped)
    # "x“   U+0022 x U+201C (mapped)
    # "x”   U+0022 x U+201D (mapped)
    # «x»   U+00AB x U+00BB (mapped)
    # »x«   U+00BB x U+00AB (mapped)
    # ‘x’   U+2018 x U+2019 (mapped)
    # ’x’   U+2019 x U+2019 (mapped)
    # “x"   U+201C x U+0022 (mapped)
    # „x"   U+201E x U+0022 (mapped)
    # „x“   U+201E x U+201C (mapped)
    # „x”   U+201E x U+201D (preferred)

    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ` U+0060 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)

    $t =~ s/‘([^‘’]+)’/„$1”/g;
    $t =~ s/([^\p{IsAlpha}])’([^‘’“„”\"«»]+)’([^\p{IsAlpha}])/$1„$2”$3/g;
    $t =~ s/['`´]/’/g;
    if ($t =~ /^[^\"“]*(?:“[^\"“]*\"[^\"“]*)+$/) {
	$t =~ s/“/„/g;
	$t =~ s/\"/”/g;
    }
    $t =~ s/“/”/g;
    if ($t =~ /^[^\"„]*(?:„[^\"„]*\"[^\"„]*)+$/) {
	$t =~ s/\"/”/g;
    }
    if ($t =~ /^[^\"”]*(?:\"[^\"”]*”[^\"”]*)+$/) {
	$t =~ s/\"/„/g;
    }
    my @dq = ($t =~ /\"/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\"([^\"]+)\"/„$1”/g;
    }
    if ($t =~ /^[^«»]*(?:«[^«»]*»[^«»]*)+$/) {
	$t =~ s/«[  ]*/„/g;
	$t =~ s/[  ]*»/”/g;
    } elsif ($t =~ /^[^«»]*(?:»[^«»]*«[^«»]*)+$/) {
	$t =~ s/»[  ]*/„/g;
	$t =~ s/[  ]*«/”/g;
    }
    $t =~ s/^[\"«»‘’“„]/„/;
    $t =~ s/[\"«»“”’]$/”/;
    $t =~ s/[\"«»“”’](\s*[\.;])$/”$1/;

    return $t;
}

1;
