package Normalise::Quotes::nl;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # Dutch usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped)
    # "x''  U+0022 x U+0027 U+0027 (mapped)
    # 'x'   U+0027 x U+0027 (mapped)
    # 'x´   U+0027 x U+00B4 (mapped)
    # ''x"  U+0027 U+0027 x U+0022 (mapped)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped)
    # `x'   U+0060 x U+0027 (mapped)
    # `x`   U+0060 x U+0060 (mapped)
    # `x´   U+0060 x U+00B4 (mapped)
    # «x»   U+00AB x U+00BB (mapped)
    # ¿x¿   U+00BF x U+00BF (mapped)
    # ,,x'' U+002C U+002C x U+0027 U+0027 (mapped)
    # ‘x’   U+2018 x U+2019 (mapped)
    # “x”   U+201C x U+201D (mapped)
    # „x”   U+201E x U+201D (preferred)
    # ‹x›   U+2039 x U+203A (mapped)

    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ` U+0060 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ¿ U+00BF (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)

    $t =~ s/[«“‹]/„/g;
    $t =~ s/[›»]/”/g;
    $t =~ s/‘([^‘’]+)’/„$1”/g;
    $t =~ s/,,([^ ][^']+)''/„$1”/g;
    $t =~ s/,,+/,/g;
    $t =~ s/([\p{IsAlpha}])[\'\`´¿]([\p{IsAlpha}])/$1’$2/g;
    $t =~ s/ [\'\`´¿](\d|[\p{IsAlpha}]\b)/ ’$1/g;
    $t =~ s/[\`´¿]/\"/g;
    my @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/„$1”/g;
    }
    my @sq = ($t =~ /\'/g);
    if ($#sq > 0 && ($#sq+1) % 2 == 0) {
	$t =~ s/\'([^\']*)\'/„$1”/g;
    }
    $t =~ s/^(?:\'\'|[\"\'‘])/„/;
    $t =~ s/(?:\'\'|[\"\'’])$/”/;
    $t =~ s/(?:\'\'|[\"\'’])(\s*[\.;])$/”$1/;

    return $t;
}

1;
