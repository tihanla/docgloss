package Normalise::Quotes::el;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # Greek usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x''  U+0022 x U+0027 U+0027 (mapped to preferred_double)
    # ''x"  U+0027 U+0027 x U+0022 (mapped to preferred_double)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped to preferred_double)
    # «x»   U+00AB x U+00BB (preferred_double)
    # ‘x’   U+2018 x U+2019 (mapped to preferred_single)
    # “x''  U+201C x U+0027 U+0027 (mapped to preferred_double)
    # “x”   U+201C x U+201D (preferred_single)
    # „x“   U+201E x U+201C (mapped to preferred_double)

    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ` U+0060 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)
    
    # double quotes
    if ($t =~ /^[^\"“”«»„]*(?:„[^\"“”«»„]*“[^\"“”«»„]*)+$/) {
	$t =~ s/„([^\"“”«»„]*)“/«$1»/g;
    }
    if ($t =~ /^[^\"“”«»]*(?:“[^\"“”«»]*?\'\'[^\"“”«»]*)+$/) {
	$t =~ s/“([^\"“”«»]*?)\'\'/«$1»/g;
    }
    my @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/«$1»/g;
    }
    $t =~ s/^(?:[\"„]|\'\')/«/;
    $t =~ s/(?:[\"“]|\'\')$/»/;
    $t =~ s/(?:[\"“]|\'\')(\s*[\.·])$/»$1/;

    # single quotes
    if ($t =~ /^[^‘’“”«»]*(?:‘[^‘’“”«»]*’[^‘’“”«»]*)+$/) {
	$t =~ s/‘([^‘’“”«»]*)’/“$1”/g;
    }
    $t =~ s/^‘/“/;
    $t =~ s/’$/”/;
    $t =~ s/’(\s*[\.·])$/”$1/;

    # apostrophes
    $t =~ s/[\'`´]/’/g;

    return $t;
}

1;
