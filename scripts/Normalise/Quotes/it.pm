package Normalise::Quotes::it;

use strict;
use utf8;

use Normalise::Predefregexp qw(vowels);

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;
    
    # Italian usage 'in the wild':
    #
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x''  U+0022 x U+0027 U+0027 (mapped to preferred_double)
    # "x”   U+0022 x U+201D (mapped to preferred_double)
    # 'x'   U+0027 x U+0027 (mapped to preferred_single)
    # 'x`   U+0027 x U+0060 (mapped to preferred_single)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped to preferred_double)
    # `x'   U+0060 x U+0027 (mapped to preferred_single)
    # «x»   U+00AB x U+00BB (preferred_double)
    # ‘x’   U+2018 x U+2019 (preferred_third_level aka preferred_single)
    # “x"   U+201C x U+0022 (mapped to preferred_double)
    # “x“   U+201C x U+201C (mapped to preferred_double)
    # “x”   U+201C x U+201D (preferred_second_level but mostly used as double; thus mapped to preferred_double)
    # ”x”   U+201D x U+201D (mapped to preferred_double)
    # „x"   U+201E x U+0022 (mapped to preferred_double)
    # „x“   U+201E x U+201C (mapped to preferred_double)
    # ‹‹x›› U+2039 U+2039 x U+203A U+203A (should be mapped to preferred_double) VERY RARE, MAPPING OMITTED
    #
    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # '' U+0027 U+0027 (mapped to preferred_apostrophe)
    # ` U+0060 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ‘ U+2018 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)
    
    # apostrophes
    $t =~ s/(\p{IsAlnum})[\'\`´‘]+(\p{IsAlnum})/$1’$2/g;
    $t =~ s/\b(ann[io])\s*[\'\`´‘]([\d])/$1 ’$2/gi;
    
    # double quotes
    my @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/«$1»/g;
    }
    $t =~ s/“([^“”]+)”/«$1»/g;
    $t =~ s/”([^”]+)”/«$1»/g;
    $t =~ s/„([^„\"“]+)[\"“]/«$1»/g;
    $t =~ s/“([^“\"]+)[“\"]/«$1»/g;
    $t =~ s/\"([^\"”]+)”/«$1»/g;
    $t =~ s/^[\"“]/«/;
    $t =~ s/[\"”]$/»/;
    $t =~ s/[\"”](\s*[\.;])$/»$1/;
    @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/«$1»/g;
    }
    
    # grave accents
    $t =~ s/[aà]\`+/à/g;
    $t =~ s/[eè]\`+/è/g;
    $t =~ s/[iì]\`+/ì/g;
    $t =~ s/[oò]\`+/ò/g;
    $t =~ s/[uù]\`+/ù/g;
    $t =~ s/[AÀ]\`+/À/g;
    $t =~ s/[EÈ]\`+/È/g;
    $t =~ s/[IÌ]\`+/Ì/g;
    $t =~ s/[OÒ]\`+/Ò/g;
    $t =~ s/[UÙ]\`+/Ù/g;
    
    # single quotes
    if ($t =~ /^[^\`\']*\`/) {
	$t =~ s/\`([^\`\']+)\'/‘$1’/g;
    } elsif ($t =~ /^[^\`\']*\'/) {
	$t =~ s/\'([^\`\']+)\`/‘$1’/g;
    }
    my @sq = ($t =~ /\'/g);
    if ($#sq > 0 && ($#sq+1) % 2 == 0) {
	$t =~ s/\'([^\']*)\'/‘$1’/g;
    }
    $t =~ s/^\'/‘/;
    $t =~ s/\'$/’/;
    $t =~ s/\'(\s*[\.;])$/’$1/;
    
    # remaining apostrophes
    my $vowels = &vowels();
    $t =~ s/(\p{IsAlnum})\'\s*($vowels)/$1’$2/gi;
    $t =~ s/(\p{IsAlnum})\'\s*([«‘])\s*($vowels)/$1’$2$3/gi;
    $t =~ s/\'([0-9])/’$1/g;
    
    # remaining grave accents
    $t =~ s/a\' /à /g;
    $t =~ s/e\' /è /g;
    $t =~ s/i\' /ì /g;
    $t =~ s/o\' /ò /g;
    $t =~ s/u\' /ù /g;
    $t =~ s/A\' /À /g;
    $t =~ s/E\' /È /g;
    $t =~ s/I\' /Ì /g;
    $t =~ s/O\' /Ò /g;
    $t =~ s/U\' /Ù /g;
    
    return $t;
}

1;
