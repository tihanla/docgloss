package Normalise::Quotes::cs;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # Czech usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x“   U+0022 x U+201C (mapped to preferred_double)
    # ,,x"  U+002C U+002C x U+0022 (mapped to preferred_double)
    # ,,x“  U+002C U+002C x U+201C (mapped to preferred_double)
    # «x»   U+00AB x U+00BB (mapped to preferred_single)
    # »x«   U+00BB x U+00AB (mapped to preferred_single)
    # ‚x‘   U+201A x U+2018 (preferred_single)
    # ‚x‛   U+201A x U+201B (mapped to preferred_single)
    # “x”   U+201C x U+201D (mapped to preferred_double)
    # „x"   U+201E x U+0022 (mapped to preferred_double)
    # „x“   U+201E x U+201C (preferred_double)
    # „x”   U+201E x U+201D (mapped to preferred_double)

    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)

    # single quotes
    $t =~ s/‛/‘/g;
    if ($t =~ /^[^«»]*(?:«[^«»]*»[^«»]*)+$/) {
	$t =~ s/«[  ]*/‚/g;
	$t =~ s/[  ]*»/‘/g;
    } elsif ($t =~ /^[^«»]*(?:»[^«»]*«[^«»]*)+$/) {
	$t =~ s/»[  ]*/‚/g;
	$t =~ s/[  ]*«/‘/g;
    }
    $t =~ s/^[«»]/‚/;
    $t =~ s/[«»]$/‘/;
    $t =~ s/[«»](\s*[\.;])$/‘$1/;

    # double quotes
    $t =~ s/„([^„\"”]+)[\"”]/„$1“/g;
    $t =~ s/“([^“”]+)”/„$1“/g;
    $t =~ s/,,([^\"“„]+)[\"“]/„$1“/g;
    $t =~ s/\"([^\"“„]+)“/„$1“/g;
    if ($t =~ /^[^\"]*(?:\"[^\"]*\"[^\"]*)+$/) {
	$t =~ s/\"([^\"]+)\"/„$1“/g;
    }
    $t =~ s/^(?:,,|[\"“])/„/;
    $t =~ s/[\"”]$/“/;
    $t =~ s/[\"”](\s*[\.;])$/“$1/;

    # apostrophes
    $t =~ s/[\'´]/’/g;
    
    return $t;
}

1;
