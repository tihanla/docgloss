package Normalise::Quotes::de;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;
    
    # German usage 'in the wild':
    #
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x''  U+0022 x U+0027 U+0027 (mapped to preferred_double)
    # "x“   U+0022 x U+201C (mapped to preferred_double)
    # "x”   U+0022 x U+201D (mapped to preferred_double)
    # $x`   U+0024 x U+0060 (mapped to preferred_single)
    # 'x'   U+0027 x U+0027 (mapped to preferred_single)
    # 'x`   U+0027 x U+0060 (mapped to preferred_single)
    # ''x"  U+0027 U+0027 x U+0022 (mapped to preferred_double)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped to preferred_double)
    # ,x`   U+002C x U+0060 (mapped to preferred_single)
    # ,x‘   U+002C x U+2018 (mapped to preferred_single)
    # ,,x"  U+002C U+002C x U+0022 (mapped to preferred_double)
    # ,,x'' U+002C U+002C x U+0027 U+0027 (mapped to preferred_double)
    # ,,x“  U+002C U+002C x U+201C (mapped to preferred_double)
    # `x"   U+0060 x U+0022 (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # `x'   U+0060 x U+0027 (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # `x`   U+0060 x U+0060 (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # `x´   U+0060 x U+00B4 (mapped to preferred_single)
    # «x«   U+00AB x U+00AB (mapped to preferred_double)
    # «x»   U+00AB x U+00BB (mapped to preferred_double)
    # ´x`   U+00B4 x U+0060 (mapped to preferred_single)
    # »x«   U+00BB x U+00AB (mapped to preferred_double)
    # ‘x‘   U+2018 x U+2018 (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # ‘x’   U+2018 x U+2019 (mapped to preferred_single)
    # ’x‘   U+2019 x U+2018 (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # ’x’   U+2019 x U+2019 (mapped to preferred_single)
    # ‚x'   U+201A x U+0027 (mapped to preferred_single)
    # ‚x‘   U+201A x U+2018 (preferred_single)
    # ‚x‛   U+201A x U+201B (mapped to preferred_single)
    # “x"   U+201C x U+0022 (mapped to preferred_double)
    # “x“   U+201C x U+201C (mapped to preferred_double)
    # “x”   U+201C x U+201D (mapped to preferred_double)
    # ”x”   U+201D x U+201D (mapped to preferred_double)
    # „x"   U+201E x U+0022 (mapped to preferred_double)
    # „x`   U+201E x U+0060 (should be mapped to preferred_double) VERY RARE, MAPPING OMITTED
    # „x“   U+201E x U+201C (preferred_double)
    # „x”   U+201E x U+201D (mapped to preferred_double)
    # ‹x›   U+2039 x U+203A (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # ›x‹   U+203A x U+2039 (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    #
    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ` U+0060 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ‘ U+2018 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)
    
    # double quotes
    $t =~ s/„([^„\"”]+)[\"”]/„$1“/g;
    $t =~ s/,,([^ ][^\'\"“]*)(\'\'|[\"“])/„$1“/g;
    my @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/„$1“/g;
    }
    $t =~ s/[\"“]([^\"“”]+)”/„$1“/g;
    @dq = ($t =~ /”/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/”([^”]*)”/„$1“/g;
    }
    $t =~ s/“([^ „“\"][^„“\"]*)[“\"]/„$1“/g;
    $t =~ s/\"([^ „“\"][^„“\"]*)“/„$1“/g;
    if ($t =~ /^[^«»]*«/) {
	$t =~ s/«([^«»]+)»/„$1“/g;
    } elsif ($t =~ /^[^«»]*»/) {
	$t =~ s/»([^«»]*)«/„$1“/g;
    }
    $t =~ s/«([^«»]+)«/„$1“/g;
    $t =~ s/^([\"«»“”]|\'\'|,,)/„/;
    $t =~ s/([\"«»”]|\'\')$/“/;
    $t =~ s/([\"«»”]|\'\')(\s*[\.;])$/“$2/;
    
    # single quotes
    $t =~ s/‛/‘/g;
    $t =~ s/\$([^\$\` ][^\$\`]*)\`/‚$1‘/g;
    $t =~ s/,([^,‘\` ][^,‘\`]*)[‘\`]/‚$1‘/g;
    $t =~ s/‚([^‚\'‘]+)\'/‚$1‘/g;
    $t =~ s/‘([^‘’]+)’/‚$1‘/g;
    $t =~ s/’([^’]+)’/‚$1‘/g;
    if ($t =~ /^[^\`´]*\`/) {
	$t =~ s/\`([^\`´]+)´/‚$1‘/g;
    } elsif ($t =~ /^[^\`´]*´/) {
	$t =~ s/´([^\`´]+)\`/‚$1‘/g;
    }
    $t =~ s/\'([^\'\`]+)\`/‚$1‘/g;
    my @sq = ($t =~ /\'/g);
    if ($#sq > 0 && ($#sq+1) % 2 == 0) {
	$t =~ s/\'([^\']+)\'/‚$1‘/g;
    }
    $t =~ s/^[\',\`´‘’]/‚/;
    $t =~ s/[\'\`´’‛]$/‘/;
    $t =~ s/[\'\`´’‛](\s*[\.;])$/‘$1/;
    
    return $t;
}

1;
