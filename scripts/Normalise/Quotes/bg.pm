package Normalise::Quotes::bg;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;
    
    # Bulgarian usage 'in the wild':
    #
    # "x"   U+0022 x U+0022 
    # "x''  U+0022 x U+0027 U+0027
    # "x”   U+0022 x U+201D
    # "x“   U+0022 x U+201C
    # ``x'' U+0060 U+0060 x U+0027 U+0027
    # ''x'' U+0027 U+0027 x U+0027 U+0027
    # ''x"  U+0027 U+0027 x U+0022 
    # ,,x"  U+002C U+002C x U+0022 
    # ,,x“  U+002C U+002C x U+201C 
    # «x»   U+00AB x U+00BB 
    # 'x'   U+0027 x U+0027 
    # ‘x’   U+2018 x U+2019 
    # ‘‘x’’ U+2018 U+2018 x U+2019 U+20199
    # ‚x‘   U+201A x U+2018 
    # ‚x‛   U+201A x U+201B 
    # “x"   U+201C x U+0022 
    # “x“   U+201C x U+201C VERY RARE 
    # “x”   U+201C x U+201D
    # ”x”   U+201D x U+201D 
    # „x"   U+201E x U+0022
    # „x“   U+201E x U+201C (preferred double quotes)
    # „x”   U+201E x U+201D 
    # „‘x’  U+201E U+2018 x U+2019
    #
    # apostrophe: 
    # in Bulgarian apostrophes are not used. Nevertheless they might be seen in the data when foreign names, etc. are used.
    # ' U+0027 (mapped to preferred_apostrophe)
    # ` U+0060 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)
    
    ## double quotes
    
    # double quotes that start with U+0022
    $t =~ s/"([^\"]+)[”\"]/„$1“/g;
    $t =~ s/"([^\"]+)''/„$1“/g;
    
    # double quotes that start with U+0027 U+0027
    $t =~ s/''([^\"('')]+)['\"]'?/„$1“/g;
    
    # double quotes that start with U+0060 U+0060
    $t =~ s/``([^\"('')]+)''/„$1“/g;
    
    # double quotes that start with U+002C U+002C
    $t =~ s/,,([^\"]+)[“\"]/„$1“/g;
    
    # double quotes that start with U+201E 
    $t =~ s/„‘?([^\"”“’]+)[\"”“’]/„$1“/g;
    
    # double quotes that start with U+201D
    $t =~ s/”([^”]+)”/„$1“/g;
    
    # double quotes that start with U+201A
    $t =~ s/‚([^‘‛]+)[“‘‛]/„$1“/g;
    
    #double quotes that start with U+201C
    $t =~ s/“([^”“\"]+)[“\"”]/„$1“/g;
    
    #double quotes that start with U+2018
    $t =~ s/‘‘?([^’]+)’’?/„$1“/g;
    
    #double quotes that start with U+2027
    $t =~ s/'([^'])'/„$1“/g;
    
    #double quotes that start with U+00AB x U+00BB
    $t =~ s/«x([^»])»/„$1“/g;
    
    #apostrophes
    $t =~ s/([\p{IsAlpha}])[\`'’]([\p{IsAlpha}])/$1’$2/;
    $t =~ s/([\p{IsAlpha}])[\`'’](\s*[\.;])$/$1’$2/;
    
    return $t;
}

1;
