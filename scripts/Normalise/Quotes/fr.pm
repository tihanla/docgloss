package Normalise::Quotes::fr;

use strict;
use utf8;

use Normalise::Predefregexp qw(vowels);

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;
    
    # French usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x''  U+0022 x U+0027 U+0027 (mapped to preferred_double)
    # "x»   U+0022 x U+00BB (mapped to preferred_double)
    # 'x'   U+0027 x U+0027 (mapped to preferred_single)
    # ''x"  U+0027 U+0027 x U+0022 (mapped to preferred_double)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped to preferred_double)
    # ''x”  U+0027 U+0027 x U+201D (mapped to preferred_double)
    # `x¿   U+0060 x U+00BF (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # ``x¿  U+0060 U+0060 x U+00BF (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # «x»   U+00AB x U+00BB (preferred_double)
    # <x>   U+003C x U+003E (mapped to preferred_single)
    # <<x>> U+003C U+003C x U+003E U+003E (mapped to preferred_double)
    # ‘x’   U+2018 x U+2019 (preferred_third_level aka preferred_single)
    # ‛x’   U+201B x U+2019 (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # ‛x′  U+201B x U+2032 (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # “x“   U+201C x U+201C (mapped to preferred_double)
    # “x”   U+201C x U+201D (preferred_second_level but mostly used as double; thus mapped to preferred_double)
    # „x"   U+201E x U+0022 (mapped to preferred_double)
    # „x“   U+201E x U+201C (mapped to preferred_double)
    # „x„   U+201E x U+201E (mapped to preferred_double)
    # ‹‹x›› U+2039 U+2039 x U+203A U+203A (mapped to preferred_double)
    #
    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ` U+0060 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ¿ U+00BF (mapped to preferred_apostrophe)
    # ‘ U+2018 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)
    # ‛ U+201B (should be mapped to preferred_apostrophe) VERY RARE, MAPPING OMITTED
    # ′U+2032 (mapped to preferred_apostrophe) VERY RARE, MAPPING OMITTED
    
    # apostrophes
    $t =~ s/(\p{IsAlnum})[\'\`´¿‘]+(\p{IsAlnum})/$1’$2/g;
    
    # double quotes
    my @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/«$1»/g;
    }
    $t =~ s/\"([^\"«»]+)»/«$1»/g;
    $t =~ s/\'\'([^\'”]+)”/«$1»/g;
    $t =~ s/<<([^<>]+)>>/«$1»/g;
    $t =~ s/‹‹([^‹›]+)››/«$1»/g;
    $t =~ s/“([^“”]+)”/«$1»/g;
    $t =~ s/„([^„\"“]+)[\"“]/«$1»/g;
    $t =~ s/„([^„]+)„/«$1»/g;
    $t =~ s/“([^“]+)“/«$1»/g;
    @dq = ($t =~ /(\"|\'\')/g);
    $t =~ s/^([\"“„]|<<|‹‹)/«/;
    $t =~ s/([\"”“„]|>>|››)$/»/;
    $t =~ s/([\"”“„]|>>|››)(\s*[\.;])$/»$2/;
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/«$1»/g;
    }
    
    # single quotes
    $t =~ s/<([^<>]+)>/‘$1’/g;
    my @sq = ($t =~ /\'/g);
    if ($#sq > 0 && ($#sq+1) % 2 == 0) {
	$t =~ s/\'([^\']*)\'/‘$1’/g;
    }
    $t =~ s/^[\'<]/‘/;
    $t =~ s/[\'>]$/’/;
    $t =~ s/[\'>](\s*[\.;])$/’$1/;
    
    # remaining apostrophes
    my $vowels = &vowels();
    $t =~ s/(\p{IsAlnum})[\'\`´¿‘]\s*($vowels)/$1’$2/gi;
    $t =~ s/(\p{IsAlnum})[\'\`´¿‘]\s*([«‘])\s*($vowels)/$1’$2$3/gi;
    
    return $t;
}

1;
