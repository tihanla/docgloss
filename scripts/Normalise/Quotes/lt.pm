package Normalise::Quotes::lt;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # Lithuanian usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped)
    # ,,x“  U+002C U+002C x U+201C (mapped)
    # ,,x”  U+002C U+002C x U+201D (mapped)
    # «x»   U+00AB x U+00BB (mapped)
    # ‘x’   U+2018 x U+2019 (mapped)
    # “x”   U+201C x U+201D (mapped)
    # „x"   U+201E x U+0022 (mapped)
    # „x“   U+201E x U+201C (preferred)
    # „x”   U+201E x U+201D (mapped)

    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ‘ U+2018 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)

    $t =~ s/«/„/g;
    $t =~ s/»/“/g;
    if ($t =~ /^[^\"„“]*(?:\"[^\"„“]*\"[^\"„“]*)+$/) {
	$t =~ s/\"([^\"„“]+)\"/„$1“/g;
    }
    $t =~ s/(„[^\"„“”]+)[\"”]/$1“/g;
    $t =~ s/,,([^„“”]+)[“”]/„$1“/g;
    $t =~ s/“([^„“”]+)”/„$1“/g;
    if ($t =~ /^[^‘’]*(?:‘[^‘’„“]*’[^‘’]*)+$/) {
	$t =~ s/‘([^‘’„“]*)’/„$1“/g;
    }
    $t =~ s/^(?:[\"«‘“]|,,)/„/;
    $t =~ s/[\"»’”]$/“/;
    $t =~ s/[\"»’”](\s*[\.;])$/“$1/;
    
    $t =~ s/[\'‘]/’/g;
    
    return $t;
}

1;
