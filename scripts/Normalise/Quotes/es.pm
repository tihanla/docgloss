package Normalise::Quotes::es;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;
    
    # Spanish usage 'in the wild':
    #
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x''  U+0022 x U+0027 U+0027 (mapped to preferred_double)
    # 'x'   U+0027 x U+0027 (mapped to preferred_single)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped to preferred_double)
    # `x´   U+0060 x U+00B4 (mapped to preferred_single)
    # «x»   U+00AB x U+00BB (preferred_double)
    # ‘x’   U+2018 x U+2019 (preferred_third_level aka preferred_single)
    # “x”   U+201C x U+201D (preferred_second_level but mostly used as double; thus mapped to preferred_double)
    # „x"   U+201E x U+0022 (mapped to preferred_double)
    # „x“   U+201E x U+201C (mapped to preferred_double)
    # „x”   U+201E x U+201D (mapped to preferred_double)
    
    # double quotes
    my @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/«$1»/g;
    }
    $t =~ s/“([^“”]+)”/«$1»/g;
    $t =~ s/„([^„\"“”]+)[\"“”]/«$1»/g;
    $t =~ s/^[\"“]/«/;
    $t =~ s/[\"”]$/»/;
    $t =~ s/[\"”](\s*[\.;])$/»$1/;
    @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/«$1»/g;
    }
    
    # single quotes
    $t =~ s/\`([^\`\´]+)\´/‘$1’/g;
    my @sq = ($t =~ /\'/g);
    if ($#sq > 0 && ($#sq+1) % 2 == 0) {
	$t =~ s/\'([^\']*)\'/‘$1’/g;
    }
    $t =~ s/^\'/‘/;
    $t =~ s/\'$/’/;
    $t =~ s/\'(\s*[\.;])$/’$1/;
    
    return $t;
}

1;
