package Normalise::Quotes::en;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # English usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x''  U+0022 x U+0027 U+0027 (mapped to preferred_double)
    # 'x'   U+0027 x U+0027 (mapped to preferred_single)
    # ''x"  U+0027 U+0027 x U+0022 (mapped to preferred_double)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped to preferred_double)
    # 'x`   U+0027 x U+0060 (mapped to preferred_single)
    # 'x´   U+0027 x U+00B4 (mapped to preferred_single)
    # 'x‚   U+0027 x U+201A (mapped to preferred_single)
    # <x>   U+003C x U+003E (should be mapped to preferred_single) VERY RARE, MAPPING OMITTED
    # «x»   U+00AB x U+00BB (mapped to preferred_double)
    # ´x'   U+00B4 x U+0027 (mapped to preferred_single)
    # ´´x'' U+00B4 U+00B4 x U+0027 U+0027 (mapped to preferred_double)
    # »x«   U+00BB x U+00AB (should be mapped to preferred_double) VERY RARE, MAPPING OMITTED
    # ‘x’   U+2018 x U+2019 (preferred_single)
    # ‛x’   U+201B x U+2019 (mapped to preferred_single)
    # “x"   U+201C x U+0022 (mapped to preferred_double)
    # “x“   U+201C x U+201C (mapped to preferred_double) TODO!
    # “x”   U+201C x U+201D (preferred_double)
    # „x“   U+201E x U+201C (mapped to preferred_double)
    # „x”   U+201E x U+201D (mapped to preferred_double)
    #
    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)
    
    
    # double quotes
    $t =~ s/´´([^´\']+)\'\'/“$1”/g;
    my @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/“$1”/g;
    }
    $t =~ s/^\"/“/;
    $t =~ s/\"$/”/;
    $t =~ s/\"(\s*[\.;])$/”$1/;
    $t =~ s/„([^„“”]+)[“”]/“$1”/g;
    $t =~ s/«([^«»]+)»/“$1”/g;
    
    # single quotes
    $t =~ s/^\'([^\'\`]+)[\`´]([ \)\.,;\?\!:])/‘$1’$2/g;
    $t =~ s/([ \(])\'([^\'\`]+)[\`´]$/$1‘$2’/g;
    $t =~ s/([ \(])\'([^\'\`]+)[\`´]([ \)\.,;\?\!:])/$1‘$2’$3/g;
    $t =~ s/^´([^´\']+)\'([ \)\.,;\?\!:])/‘$1’$2/g;
    $t =~ s/([ \(])´([^´\']+)\'$/$1‘$2’/g;
    $t =~ s/([ \(])´([^´\']+)\'([ \)\.,;\?\!:])/$1‘$2’$3/g;
    $t =~ s/\'([^\'\`]+)[\`´]/‘$1’/g;
    $t =~ s/´([^´\']+)\'/‘$1’/g;
    my @sq = ($t =~ /\'/g);
    if ($#sq > 0 && ($#sq+1) % 2 == 0) {
	$t =~ s/\'([^\']*)\'/‘$1’/g;
    }
    $t =~ s/‛([^‛’]+)’/‘$1’/g;
    $t =~ s/^\'/‘/;
    $t =~ s/\'$/’/;
    $t =~ s/\'(\s*[\.;])$/’$1/;
    $t =~ s/\'([^\'‚]+)‚/‘$1’/g;
 
    return $t;
}

1;
