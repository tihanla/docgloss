package Normalise::Quotes::fi;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # Finnish usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x”   U+0022 x U+201D (mapped to preferred_double)
    # 'x'   U+0027 x U+0027 (mapped to preferred_single)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped to preferred_double)
    # `x`   U+0060 x U+0060 (mapped to preferred_single)
    # `x´   U+0060 x U+00B4 (mapped to preferred_single)
    # «x»   U+00AB x U+00BB (mapped to preferred_double)
    # »x«   U+00BB x U+00AB (mapped to preferred_double)
    # ’x’   U+2019 x U+2019 (preferred_single)
    # “x”   U+201C x U+201D (mapped to preferred_double)
    # ”x"   U+201D x U+0022 (mapped to preferred_double)
    # ”x”   U+201D x U+201D (preferred_double)
    # „x"   U+201E x U+0022 (mapped to preferred_double)
    # „x“   U+201E x U+201C (mapped to preferred_double)

    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)

    # double quotes
    $t =~ s/(?:[\"«»“„]|\'\')/”/g;

    # single quotes and apostrophes
    $t =~ s/[\'\`‘´]/’/g;

    return $t;
}

1;
