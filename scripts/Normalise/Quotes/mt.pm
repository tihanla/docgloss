package Normalise::Quotes::mt;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_quotes);

sub normalise_quotes {
    my ($t) = @_;

    # Maltese usage 'in the wild':
    #
    # quotations:
    # "x"   U+0022 x U+0022 (mapped to preferred_double)
    # "x''  U+0022 x U+0027 U+0027 (mapped to preferred_double)
    # "x”   U+0022 x U+201D (mapped to preferred_double)
    # ''x"  U+0027 U+0027 x U+0022 (mapped to preferred_double)
    # ''x'' U+0027 U+0027 x U+0027 U+0027 (mapped to preferred_double)
    # 'x'   U+0027 x U+0027 (mapped to preferred_single)
    # 'x`   U+0027 x U+0060 (mapped to preferred_single)
    # 'x´   U+0027 x U+00B4 (mapped to preferred_single)
    # `x`   U+0060 x U+0060 (mapped to preferred_single)
    # x     U+0093 x U+0094 (mapped to preferred_single)
    # «x»   U+00AB x U+00BB (mapped to preferred_double)
    # «xğ   U+00AB x U+011F (mapped to preferred_double)
    # ´x´   U+00B4 x U+00B4 (mapped to preferred_single)
    # Ğx»   U+011E x U+00BB (mapped to preferred_double)
    # Ğxğ   U+011E x U+011F (mapped to preferred_double)
    # ‘x`   U+2018 x U+0060 (mapped to preferred_single)
    # ‘x’   U+2018 x U+2019 (preferred_single)
    # ‛x’   U+201B x U+2019 (mapped to preferred_single)
    # “x"   U+201C x U+0022 (mapped to preferred_double)
    # “x”   U+201C x U+201D (preferred_double)
    # „x“   U+201E x U+201C (mapped to preferred_double)
    #
    # apostrophe:
    # ' U+0027 (mapped to preferred_apostrophe)
    # ''U+0027 U+0027 (mapped to preferred_apostrophe)
    # ` U+0060 (mapped to preferred_apostrophe)
    # ´ U+00B4 (mapped to preferred_apostrophe)
    # ‘ U+2018 (mapped to preferred_apostrophe)
    # ’ U+2019 (preferred_apostrophe)


    # some frequent apostrophes
    $t =~ s/\b([bfmtx]|[tm]a|erba|seba|disa|raba|[njt]ista)(?:[‘´\'\`]|\'\')/$1’/gi;
    $t =~ s/ (?:[‘´\'\`]|\'\')(il) / ’$1 /gi;
    $t =~ s/\b(s)(?:[‘´\'\`]|\'\')(issa)\b/$1’$2/gi;

    # either apostrophe or closing single quote but always U+2019
    $t =~ s/([\p{IsAlpha}])[´\'\`]([^\'])/$1’$2/g;
    $t =~ s/([\p{IsAlpha}])[´\'\`]$/$1’/g;
    $t =~ s/([\p{IsAlpha}])[´\'\`]([\p{IsAlpha}])/$1’$2/g;
    # opening single quote
    $t =~ s/([^\p{IsAlpha}\']|^[^\p{IsAlpha}\']?)[´\'\`]([\p{IsAlpha}])/$1‘$2/g;


    # repair some common ill-formatted quotes
    $t =~ s/[Ğ«]/“/g;
    $t =~ s/[ğ»]/”/g;
    $t =~ s/[\x{0093}‛]/‘/g;
    $t =~ s/\x{0094}/’/g;

    # double quotes
    $t =~ s/„([^„“]+)“/“$1”/g;
    my @dq = ($t =~ /(\"|\'\')/g);
    if ($#dq > 0 && ($#dq+1) % 2 == 0) {
	$t =~ s/\'\'/\"/g;
	$t =~ s/\"([^\"]*)\"/“$1”/g;
    }
    $t =~ s/^\"/“/;
    $t =~ s/\"$/”/;
    $t =~ s/\"(\s*[\.;])$/”$1/;

    # single quotes
    my @sq = ($t =~ /\'/g);
    if ($#sq > 0 && ($#sq+1) % 2 == 0) {
	$t =~ s/\'([^\']*)\'/‘$1’/g;
    }
    $t =~ s/^\'/‘/;
    $t =~ s/\'$/’/;
    $t =~ s/\'(\s*[\.;])$/’$1/;

    return $t;
}

1;
