package Normalise::Spelling::en;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;
    
    $t =~ s/\b([cC]onsist(?:s|ed|ing)?) in\b/$1 of/g; # replace "consist in" by "consist of"

    # -ize --> -ise
    my @words = split (/\b/, $t);
    my $new;
    foreach my $word (@words) {
	if ($word =~ /^(.*[iy])(z)(e[ds]?|ers?|ings?|ations?)$/i && $1 !~ /^(?:pr|se|ass|s|caps|outs|downs|overs|micros|ma|bel|tre)i$/i) {
	    my ($pre, $z, $post) = ($1, $2, $3);
	    $z =~ tr/zZ/sS/;
	    $new .= $pre.$z.$post;
	} else {
	    $new .= $word;
	}
    }
    $t = $new;

# exceptions:
#prize
#seize
#assize
#size
#capsize
#outsize
#downsize
#oversize
#microsize
#maize
#Belize
#treize

    return $t;
}

1;
