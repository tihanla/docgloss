package Normalise::Spelling::de;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

    # replace erroneous beta by eszett
    $t =~ s/([a-zäöü])β/$1ß/gi;

    # some rules about hyphens
    $t =~ s/(\d)(jährig)/$1-$2/gi;
    $t =~ s/(\d)(mal)\b/$1-$2/gi;
    $t =~ s/(\d)-(er[ns]?)\b/$1$2/gi;
    $t =~ s/(\d)-(fach)/$1$2/gi;

    # COM abbreviation
    $t =~ s/\bCOM\(/KOM(/g;

    # some spelling corrections (from OJ-Format)
    $t =~ s/\b[eE]-?[mM]ail\b/E-Mail/g;
    $t =~ s/\bgmbh\b/GmbH/gi;
    $t =~ s/\bu\.?s\.?w\.?(\W)/usw.$1/g;
    $t =~ s/\bu\.?s\.?w\.?$/usw./g;
    $t =~ s/\bad-hoc-/Ad-hoc-/g;
    $t =~ s/\bam siebenten\b/am siebten/g;
    $t =~ s/\bbezug\b/Bezug/g;
    $t =~ s/\bdaß\b/dass/g;
    $t =~ s/\b[EÄ][kq]uador\b/Ecuador/g;
    $t =~ s/\bex-(ante|post)-/Ex-$1-/g;
    $t =~ s/\bGebietes\b/Gebiets/g;
    $t =~ s/\bHong[ \-]Kong\b/Hongkong/g;
    $t =~ s/\bMitgliedsstaa/Mitgliedstaa/g;
    $t =~ s/\bMitgliedstaates\b/Mitgliedstaats/g;
    $t =~ s/\bm([ia])kro-ök/m$1kroök/g;
    $t =~ s/\bmuß\b/muss/g;
    $t =~ s/\bNR\./Nr./g;
    $t =~ s/\bRats\b/Rates/g;
    $t =~ s/\bsozio-(pol|ök|kul)/sozio$1/g;
    $t =~ s/\bSpiegelstrich\b/Gedankenstrich/g;
    $t =~ s/\bVertrages\b/Vertrags/g;
    $t =~ s/\bzufriedenstell/zufrieden stell/g;
    $t =~ s/\bzu Grunde\b/zugrunde/g;
    $t =~ s/\bzugrundelieg/zugrunde lieg/g;
    $t =~ s/\bam(\b| (?:dritten|siebten|zwanzigsten)) Tage\b/am$1 Tag/g;
    $t =~ s/\bBeitrittsakte 2003\b/Beitrittsakte von 2003/g;
    $t =~ s/\bEntscheidung der Kommission (\d+\/\d+\/EW?G)\b/Entscheidung $1 der Kommission/g;
    $t =~ s/\bGemeinsame(n?) (Agrar|Fischerei)politik\b/gemeinsame$1 $2politik/g;
    $t =~ s/\b([iI])m Jahre\b/$1m Jahr/g;
    $t =~ s/\bin Anhang I und II\b/in den Anhängen I und II/g;
    $t =~ s/\bRichtlinie des Rates (\d+\/\d+\/EW?G)\b/Richtlinie $1 des Rates/g;
    $t =~ s/\bVerordnung (der Kommission|des Rates)(\b| \(EW?G\)) Nr\. (\d+\/\d+)\b/Verordnung$2 Nr. $3 $1/g;
    $t =~ s/\bWirtschaftsjahr(e?s?) (19|20)(\d\d)\/\2(\d\d)\b/Wirtschaftsjahr$1 $2$3\/$4/g;
    $t =~ s/\bWirtschaftsjahr(e?s?) 1999\/00\b/Wirtschaftsjahr$1 1999\/2000/g;
    $t =~ s/[  ]% ?igen\b/%igen/g;

    return $t;
}

1;
