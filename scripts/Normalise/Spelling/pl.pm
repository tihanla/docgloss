package Normalise::Spelling::pl;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

# in the corpus:

#¹->ą
#ê->ę
#æ->ć
#³->ł
#ñ->ń
#(ó->ó)
#œ->ś
#Ÿ->ź
#¿->ż

#¥->Ą
#Ê->Ę
#Æ->Ć
#£->Ł
#Ñ->Ń
#(Ó->Ó)
#Œ->Ś
#->Ź
#¯->Ż

#http://en.wikipedia.org/wiki/ISO/IEC_8859
#confusion between 2 and 15? -> not quite; 2x3 exceptions

# implement if necessary

    return $t;
}

1;
