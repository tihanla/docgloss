package Normalise::Spelling::ga;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);


sub normalise_sp {
    my ($t) = @_;

    # capitalise "Fómhair" in names of months
    $t =~ s/\b(meán|deireadh) f(ómhair)\b/ucfirst($1)." F".$2/egi;

# the accent is sometimes misplaced, e.g.
# trı´d an me´id

# often, a grave accent is used instead of an acute accent, e.g.
# nò, dà

# implement when necessary
    
    return $t;
}

1;
