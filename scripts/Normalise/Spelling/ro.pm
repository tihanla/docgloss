package Normalise::Spelling::ro;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

    # Romanian characters
    # cedilla -> comma
    $t =~ s/ş/ș/g;
    $t =~ s/Ş/Ș/g;
    $t =~ s/ţ/ț/g;
    $t =~ s/Ţ/Ț/g;
    # caron -> breve
    $t =~ s/ǎ/ă/g;
    $t =~ s/Ǎ/Ă/g;
    # tilde -> breve (skipping some sequences that are typically Portuguese and thus correct)
    $t =~ s/ã([^oe])/ă$1/g;
    $t =~ s/ã$/ă/;
    $t =~ s/Ã([^OE])/Ă$1/g;
    $t =~ s/Ã$/Ă/;

    # avoid dative form of EC at start of sentence
    # TODO: only in postprocessing
    $t =~ s/^[cC]omisiei\b/Comisia/g;
    $t =~ s/^[cC]omisiei [eE]uropene\b/Comisia Europeană/g;
    
    return $t;
}

1;
