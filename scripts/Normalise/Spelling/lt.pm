package Normalise::Spelling::lt;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

    # In the corpus, Lithuanian characters are often encoded incorrectly -> repair:
    $t =~ s/\x{008a}/Š/g;
    $t =~ s/˛/ž/g;
    $t =~ s/þ/ž/g;
    $t =~ s/ˇ/Ž/g;
    $t =~ s/ë/ė/g;
    $t =~ s/ø/ų/g;
    $t =~ s/æ/ę/g;
    $t =~ s/à/ą/g;
    $t =~ s/á/į/g;
    $t =~ s/ð/š/g;
    $t =~ s/û/ū/g;
    $t =~ s/Á/Į/g;
    $t =~ s/è/č/g;
    $t =~ s/¹/ą/g;

    return $t;
}

1;
