package Normalise::Spelling::mt;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp normalise_sp_end);

sub normalise_sp {
    my ($t) = @_;

    # In the corpus, Maltese characters are often encoded incorrectly -> repair:
    $t =~ s/ˇ/Ħ/g;
    $t =~ s/±([\p{IsAlpha}±])/ħ$1/g;
    $t =~ s/([\p{IsAlpha}±])±/$1ħ/g;
    $t =~ s/ʕh?/ħ/g;
    $t =~ s/[Őʈ]/Ġ/g;
    $t =~ s/[őʉ]/ġ/g;
    $t =~ s/[Ĺɦ]/Ċ/g;
    $t =~ s/ĺ|ɧc?/ċ/g;
    $t =~ s/\x{0379}/Ż/g;
    $t =~ s/[¿\x{0380}]/ż/g;
    $t =~ s/Ȥ/à/g;

    # one Portuguese character is also replaced:
    $t =~ s/\x{f7fa}/ã/g;

    # grave accents in words of Italian origin:
    $t =~ s/(i|je|bbil|r|ul)ta[´\'\`’]([^\p{IsAlpha}])/$1tà$2/g;
    $t =~ s/(I|JE|BBIL|R|UL)TA[´\'\`’]([^\p{IsAlpha}])/$1TÀ$2/g;
    $t =~ s/\bvirtu[´\'\`’]/virtù/g;
    $t =~ s/\bVIRTU[´\'\`’]/VIRTÙ/g;
    $t =~ s/\bkafe[´\'\`’]/kafè/g;
    $t =~ s/\bKAFE[´\'\`’]/KAFÈ/g;
    $t =~ s/\b([pP])ero[´\'\`’]/$1erò/g;
    $t =~ s/\bPERO[´\'\`’]/PERÒ/g;
    $t =~ s/\bdiġa[´\'\`’]/diġà/g;
    $t =~ s/\bDIĠA[´\'\`’]/DIĠÀ/g;
    $t =~ s/\bcioe[´\'\`’]/cioè/g;
    $t =~ s/\bCIOE[´\'\`’]/CIOÈ/g;

    # new orthography rules
    $t =~ s/\b([sS])kond\b/$1kont/g;
    $t =~ s/\bSKOND\b/SKONT/g;
    $t =~ s/\b([aA])wissu\b/$1wwissu/g;
    $t =~ s/\bAWISSU\b/AWWISSU/g;
    $t =~ s/\b(ewwel|tieni|tielet|aħħar)(nett)\b/$1 $2/gi;
    $t =~ s/\btvalja\b/dvalja/g;
    $t =~ s/\briżq\b/risq/g;
    $t =~ s/\bdettal\b/dettall/g;
    $t =~ s/\b(dettal)l(ji)\b/$1$2/gi;
    $t =~ s/\b(iniz)z(jattiva)\b/$1$2/gi;
    $t =~ s/\b[Ii]s(lam(?:iku)?)\b/iż$1/g;
    $t =~ s/\b(kar)r(ozza)\b/$1$2/gi;
    $t =~ s/\b(kuri)d(uri?)\b/$1t$2/g;
    $t =~ s/\b(dgħ)j(ufija)\b/$1$2/gi;
    $t =~ s/\b(għaġ)ġ(la)\b/$1$2/gi;
    $t =~ s/\bxbiha\b/xbieha/g;
    $t =~ s/\b(bħal) (ma)\b/$1$2/gi;
    $t =~ s/\b(bil)-(lejl|qiegħda|wieqfa)\b/$1$2/gi;
    $t =~ s/\b(bi) (nhar)\b/$1$2/gi;
    $t =~ s/\b(dal)-(għodu)\b/$1$2/gi;
    $t =~ s/\b(daqs) (li) (kieku)\b/$1$2$3/gi;
    $t =~ s/\b(fil)-(għaxija|għodu)\b/$1$2/gi;
    $t =~ s/\b(għal) (daqstant)\b/$1$2/gi;
    $t =~ s/\b(in) (ġenerali)\b/$1$2/gi;
    $t =~ s/\b(kem)m(xejn)\b/$1$2/gi;
    $t =~ s/\b(kul)l(ħadd)\b/$1$2/gi;
    $t =~ s/\b(la) (darba)\b/$1$2/gi;
    $t =~ s/\b(il)-(bieraħ|lejla)\b/$1$2/gi;
    $t =~ s/\b(nofs) (il)-(lejl)\b/$1$2$3/gi;
    $t =~ s/\b(per) (eżempju)\b/$1$2/gi;
    $t =~ s/\b(qabel) (xejn)\b/$1$2/gi;
    $t =~ s/\b(sa) (kemm)\b/$1$2/gi;
    $t =~ s/\b(bħal)(xejn)\b/$1 $2/gi;
    $t =~ s/\b(fil)(fatt)\b/$1-$2/gi;
    $t =~ s/\bminfejn\b/minn fejn/g;
    $t =~ s/\b(sa)(fejn)\b/$1 $2/gi;

    return $t;
}

sub normalise_sp_end {
    my ($t) = @_;

    # new orthography rules
    $t =~ s/\b([tm]a)[  ]?’[  ]?(l-)/$1$2/gi;
    $t =~ s/\b(sa)[  ]+(l-)\b/$1$2/gi;
    $t =~ s/\b(m)’(h)(i(?:ja)?|(?:ij|uw)iex)\b/$1$2$3/gi;

    # definite article
    $t =~ s/([aeiouàèòù][  ])i([ċdlnrstxż]-)/$1$2/gi;
    $t =~ s/^([ċdlnrstxż]-)/"I".lc($1)/egi;
    $t =~ s/([\.:!\?][  ]*)([ċdlnrstxż]-)/$1."I".lc($2)/egi;
    $t =~ s/([,\(\)\[\];\/][  ]*)([ċdlnrstxż]-)/$1i$2/g;
    $t =~ s/([,\(\)\[\];\/][  ]*)([ĊDLNRSTXŻ]-)/$1I$2/g;
    $t =~ s/\bi(l-(?:[aeiouh]|għ))/$1/gi;

    return $t;
}

1;
