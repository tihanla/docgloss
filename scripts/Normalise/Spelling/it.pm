package Normalise::Spelling::it;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;
    
    # Italian accents
    $t =~ s/\b[EÈ][´\'‘\`] /È /g;
    $t =~ s/\b[eè][´\'‘\`] /è /g;
    $t =~ s/\b([pP]u?)[oò][´\'‘\`] /$1ò /g;
    $t =~ s/\b([pP]i)[uù][´\'‘\`] /$1ù /g;
    $t =~ s/\b([cC]i)[oò][´\'‘\`] /$1ò /g;
    $t =~ s/\b([gG]i)[aà][´\'‘\`] /$1à /g;
    $t =~ s/\bI[´\'‘\`]([aeiouAEIOU])/l’$1/g;
    $t =~ s/([cC]omunit|[aA]ttivit|[sS]ociet|[sS]anit|[vV]ariet|[mM]odalit|[cC]onformit|[qQ]ualit|[aA]mmissibilit|[uU]nit|[rR]esponsibilit)a[´\'‘\`]/$1à/g;
    $t =~ s/(COMMUNIT|ATTIVIT|SOCIET|SANIT|VARIET|MODALIT|CONFORMIT|QUALIT|AMMISSIBILIT|UNIT|RESPONSIBILIT)A[´\'‘\`]/$1À/g;
    $t =~ s/([gG]iovent)u[´\'‘\`]/$1ù/g;
    $t =~ s/(GIOVENT)U[´\'‘\`]/$1Ù/g;

    # double "l" before numbers starting with a vowel
    $t =~ s/\b(su|a|de|da)(l) +8/$1$2$2’8/gi;
    $t =~ s/\b(su|a|de|da)(l) +11(\D)/$1$2$2’11$3/gi;
    # the first of a month should usually be written as "1º" in our data
    $t =~ s/(\D)1 +(gennaio|febbraio|marzo|aprile|maggio|giugno|luglio|agosto|settembre|ottobre|novembre|dicembre)/${1}1º $2/gi;

    return $t;
}

1;
