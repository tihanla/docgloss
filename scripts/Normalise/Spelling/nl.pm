package Normalise::Spelling::nl;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

    # "IJsland" instead of "Ijsland"
    $t =~ s/Ij/IJ/g;
    # no special ligature characters
    $t =~ s/Ĳ/IJ/g;
    $t =~ s/ĳ/ij/g;

    return $t;
}

1;
