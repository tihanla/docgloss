package Normalise::Spelling::es;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;
    
    $t =~ s/\bComision\b/Comisión/g; # correct spelling for "Comisión"
    $t =~ s/\b([aA]rt)i(culos?\b)/$1í$2/g; # correct spelling for "artículo"
    $t =~ s/\b([cC]ap)i(tulos?\b)/$1í$2/g; # correct spelling for "capítulo"
    $t =~ s/\by[  ]?\/[  ]?o\b/o/g; # according to feedback from ES translators, "y/o" should always be replaced by "o"
    $t =~ s/\b([sS])ólo\b/$1olo/g;
    $t =~ s/\b([pP])eriodo\b/$1eríodo/g;

    return $t;
}

1;
