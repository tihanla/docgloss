package Normalise::Spelling::et;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

    $t =~ s/˛/ž/g;

    return $t;
}

1;
