package Normalise::Spelling::lv;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

    # The letter ŗ has been replaced by r in a 1946 spelling reform
    $t =~ s/ŗ/r/g;
    $t =~ s/Ŗ/R/g;
    
    return $t;
}

1;
