package Normalise::Spelling::el;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

    # accents
    $t =~ s/^´[ΑΆ]/Ά/;
    $t =~ s/([^\p{IsAlpha}])´[ΑΆ]/${1}Ά/g;
    $t =~ s/^´[ΕΈ]/Έ/;
    $t =~ s/([^\p{IsAlpha}])´[ΕΈ]/${1}Έ/g;
    $t =~ s/^´[ΗΉ]/Ή/;
    $t =~ s/([^\p{IsAlpha}])´[ΗΉ]/${1}Ή/g;
    $t =~ s/^´[ΙΊ]/Ί/g;
    $t =~ s/([^\p{IsAlpha}])´[ΙΊ]/${1}Ί/g;
    $t =~ s/^´[ΟΌ]/Ό/g;
    $t =~ s/([^\p{IsAlpha}])´[ΟΌ]/${1}Ό/g;
    $t =~ s/^´[ΥΎ]/Ύ/g;
    $t =~ s/([^\p{IsAlpha}])´[ΥΎ]/${1}Ύ/g;
    $t =~ s/^´[ΩΏ]/Ώ/g;
    $t =~ s/([^\p{IsAlpha}])´[ΩΏ]/${1}Ώ/g;
#    ΪΪ́
#    ΫΫ́

    # 'ano teleia'
    $t =~ s/·/·/g;
    # some mixed Greek/Greeklish abbreviations
    $t =~ s/\b[EΕ][KΚ]\b/ΕΚ/g;
    $t =~ s/\b[EΕ][OΟ][KΚ]\b/ΕΟΚ/g;
    $t =~ s/\b[EΕ][OΟ][XΧ]\b/ΕΟΧ/g;
    $t =~ s/\b[KΚ][EΕ]ΠΠ[AΑ]\b/ΚΕΠΠΑ/g;
    # May
    $t =~ s/\b([μΜ])αίου\b/$1αΐου/g;

    # Greeklish
    $t =~ s/(\p{Greek})A/$1Α/g;
    $t =~ s/(\p{Greek})B/$1Β/g;
    $t =~ s/(\p{Greek})E/$1Ε/g;
    $t =~ s/(\p{Greek})Z/$1Ζ/g;
    $t =~ s/(\p{Greek})H/$1Η/g;
    $t =~ s/(\p{Greek})I/$1Ι/g;
    $t =~ s/(\p{Greek})K/$1Κ/g;
    $t =~ s/(\p{Greek})M/$1Μ/g;
    $t =~ s/(\p{Greek})N/$1Ν/g;
    $t =~ s/(\p{Greek})O/$1Ο/g;
    $t =~ s/(\p{Greek})P/$1Ρ/g;
    $t =~ s/(\p{Greek})T/$1Τ/g;
    $t =~ s/(\p{Greek})Y/$1Υ/g;
    $t =~ s/(\p{Greek})X/$1Χ/g;
    $t =~ s/(\p{Greek})k/$1κ/g;
    $t =~ s/(\p{Greek})v/$1ν/g;
    $t =~ s/(\p{Greek})o/$1ο/g;
    $t =~ s/(\p{Greek})ó/$1ό/g;

    $t =~ s/A(\p{Greek})/Α$1/g;
    $t =~ s/B(\p{Greek})/Β$1/g;
    $t =~ s/E(\p{Greek})/Ε$1/g;
    $t =~ s/Z(\p{Greek})/Ζ$1/g;
    $t =~ s/H(\p{Greek})/Η$1/g;
    $t =~ s/I(\p{Greek})/Ι$1/g;
    $t =~ s/K(\p{Greek})/Κ$1/g;
    $t =~ s/M(\p{Greek})/Μ$1/g;
    $t =~ s/N(\p{Greek})/Ν$1/g;
    $t =~ s/O(\p{Greek})/Ο$1/g;
    $t =~ s/P(\p{Greek})/Ρ$1/g;
    $t =~ s/T(\p{Greek})/Τ$1/g;
    $t =~ s/Y(\p{Greek})/Υ$1/g;
    $t =~ s/X(\p{Greek})/Χ$1/g;
    $t =~ s/k(\p{Greek})/κ$1/g;
    $t =~ s/v(\p{Greek})/ν$1/g;
    $t =~ s/o(\p{Greek})/ο$1/g;
    $t =~ s/ó(\p{Greek})/ό$1/g;

    return $t;
}

1;
