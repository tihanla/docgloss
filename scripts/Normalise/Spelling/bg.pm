package Normalise::Spelling::bg;

use strict;
use utf8;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(normalise_sp);

sub normalise_sp {
    my ($t) = @_;

    # replace BYELORUSSIAN-UKRAINIAN I by LATIN I
    $t =~ s/І/I/g;
    $t =~ s/і/i/g;
    # replace CYRILLIC LETTER DZE by LATIN S
    $t =~ s/Ѕ/S/g;
    $t =~ s/ѕ/s/g;

    # °C
    $t =~ s/(\d)[  ]*[оo][СC]\b/$1 °C/g;

    # some mixed Latin/Cyrillic abbreviations
    # the following should be in all Cyrillic
    $t =~ s/\b[ЕE][ОO]\b/ЕО/g;
    $t =~ s/\b[ЕE]И[ОO]\b/ЕИО/g;
    $t =~ s/\b[ЕE][СC]\b/ЕС/g;
    $t =~ s/\b[ОO][ВB]\b/ОВ/g;
    $t =~ s/\b[сc]т[рp]\b/стр/g;
    # the following should be in all Latin
    $t =~ s/\bЕUR\b/EUR/g;

    # BG letters:
    # А а Б б В в Г г Д д Е е Ж ж З з И и Й й К к Л л М м Н н О о П п Р р С с Т т У у Ф ф Х х Ц ц Ч ч Ш ш Щ щ Ъ ъ Ь ь Ю ю Я я
 
    # BG letters that resemble latin letters:
    my $amb_cyr = '[АаВЕеКкМНОоРрСсТуХх]';
    my $amb_lat = '[AaBEeKkMHOoPpCcTyXx]';
    my %amb_lat_cyr = qw(A А a а B В E Е e е K К k к M М H Н O О o о P Р p р C С c с T Т y у X Х x х); # order: latin cyrillic
    my %amb_cyr_lat = reverse %amb_lat_cyr;

    # BG letters that don't resemble latin letters:
    my $unamb_cyr = '[БбвГгДдЖжЗзИиЙйЛлмнПптУФфЦцЧчШшЩщЪъЬьЮюЯя]';

    # Latin letters that don't resemble BG letters:
    my $unamb_lat = '[bDdFfGghIiJjLlmNnQqRrSstUuVvWwYZzäëïöüáéíóúàèìùòâêîôûãẽĩõũăĕĭŏŭőűąęįųāēīōūėœøåÄËÏÖÜÁÉÍÓÚÀÈÌÙÒÂÊÎÔÛÃẼĨÕŨĂĔĬŎŬŐŰĄĘĮŲĀĒĪŌŪĖŒØÅßČĎĚŇŘŠŤŮŽÆÑÇĢĶĻŅĊĠĦŻĆŁŃŚŹĂȘȚĹĽŔÝčďěňřšťůžæñçģķļņċġħżćłńśźășțĺľŕý]';

    # replace ambiguous latin letters in cyrillic context
    while ($t =~ /$unamb_cyr+$amb_cyr*?$amb_lat/) {
	$t =~ s/($unamb_cyr+$amb_cyr*?)($amb_lat)/$1$amb_lat_cyr{$2}/g;
    }
    while ($t =~ /$amb_lat$amb_cyr*?$unamb_cyr/) {
	$t =~ s/($amb_lat)($amb_cyr*?$unamb_cyr)/$amb_lat_cyr{$1}$2/g;
    }
    # replace ambiguous cyrillic letters in latin context
    while ($t =~ /$unamb_lat+$amb_lat*?$amb_cyr/) {
	$t =~ s/($unamb_lat+$amb_lat*?)($amb_cyr)/$1$amb_cyr_lat{$2}/g;
    }
    while ($t =~ /$amb_cyr$amb_lat*?$unamb_lat/) {
	$t =~ s/($amb_cyr)($amb_lat*?$unamb_lat)/$amb_cyr_lat{$1}$2/g;
    }

    # short form of direct object
    $t =~ s/ й\b/ ѝ/g;

    return $t;
}

1;
