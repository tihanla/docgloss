
use strict;
#prints out the columns of table in the specified order

my $order;
my $table;
my $ordersize;
my $insize;
my @inarray;
my $argc;
my $in;
my $indb;
my $dbfile;
my $orderi;
#my $srclang;
 #my $lang;	#trglang
my $langcol;
my $idcol;
my %idhash;
my $idcoldb;
my $langcoldb;
my $srclangdb;
my $trglangdb;
my $fixcol;
my $i;

my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 9)
	{
	print  "Usage:\n";
	print  "perl getidlangline idcol input db_idcol db_langcol db_src_lang db_trg_lang db_input output fixcol\n";
	#perl %script_path%\getidlangline.pl  %idcol%  %work_path%\found-src.txt  %langcol%  %src% %trg% "%dicinput_path%\%dicinput%" "%work_path%\target.tsv"
	exit ;
	}

$idcol =  $ARGV[0];
open(my $infile, "<", $ARGV[1] ) || die "can't open $ARGV[1]";
open(my $dbfile, "<", $ARGV[6] ) || die "can't open $ARGV[6]";
open(my $outfile, ">", $ARGV[7] ) || die "can't open $ARGV[7]";

$fixcol =  $ARGV[8];

$idcoldb =  $ARGV[2];
$langcoldb =  $ARGV[3];
$srclangdb =  $ARGV[4];
$trglangdb =  $ARGV[5];

#print "source_langdb=$srclangdb, target_lang=$trglangdb\n";
#print "idcoldb=$idcoldb, lancoldb=$langcoldb\n";

#exit;

#binmode($infile, ":encoding(UTF-16LE)");


#print "order=$order, ";
#print "ordersize=$ordersize\n";

my $count =1;

$count=1;
#CHANGED!!! $indb = <$dbfile>;
$count++;
print $outfile  "$indb";

while ($in = <$infile>)
	{
	#print $outfile  $in;
	#next;
	chomp($in);
	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	$idhash{$inarray[$idcol-1]}=1;

	#print src terms
	#CHANGED!!!::: print $outfile  "$in\n";
	
	$count++;
	}	

#reset input, we read it again

#print header line:

while ($indb = <$dbfile>)
	{
	#DEBUG:
	#print  "count=$count, $indb";

	@inarray = split ('\t', $indb, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	#if((exists $idhash{$inarray[$idcoldb-1]} ) && (($inarray[$langcoldb-1] eq $srclangdb) || (($inarray[$langcoldb-1] eq $trglangdb))))
	if((exists $idhash{$inarray[$idcoldb-1]} ) && (($inarray[$langcoldb-1] eq $trglangdb)))
		{
		#chomp($indb);
		$indb =~ s/[\r\n]//g;
		print $outfile "$indb";
		#caller may add extra colums as to get same number of colums as source side
		for ($i=0; $i != $fixcol; $i++)
			{
			print $outfile  "\t";
			}
		print $outfile "\n";
		#print "found: $indb\n";  #DEBUG
		}
	else
		{
		;#print "idhash=$idhash{$inarray[$idcoldb-1]},  lang=$inarray[$langcoldb-1]\n";
		}
		
	$count++;
	}	


close $infile;
close $dbfile;
close $outfile;
