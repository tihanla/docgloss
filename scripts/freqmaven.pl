
#source: http://perlmaven.com/count-words-in-text-using-perl

use strict;
use warnings;
     
my $lc=1;
my $lck;
my %count;
my $file;
my $out;

if ( @ARGV ne 2)
	{
	print "Usage:\nfreqmaven.pl textin freqout\n";
	exit;
	}

open ($file, "<", $ARGV[0])|| die "can't open $ARGV[0]";
open ($out, ">", $ARGV[1])|| die "can't open $ARGV[1]";

#$file = shift or die "Usage: $0 FILE\n";
#open my $fh, '<', $file or die "Could not open '$file' $!";

while (my $line = <$file>) 
	{
	chomp $line;
	$line =~ s/xC2\xA0/ /g;            #V3: normalize hard spaces 
	$line =~ s/\xE2\x80\x99/'/g;           #normalize apostrophs from typographic (8217) to ASCII (')
	foreach my $str (split / +/, $line)  # does not work  frq1
		{
		 $count{$str}++;
		}

	$lc++;
	$lck = int $lc/100000;
	if($lc == $lck*100000) 		
		{print "$lc\n";}
	}        

foreach my $word (reverse sort { $count{$a} <=> $count{$b} } keys %count) 
	{
	print $out "$word\t$count{$word}\n";
	}

#  sort by words:
#    foreach my $str (sort keys %count) 
#	{printf "%-31s %s\n", $str, $count{$str};}
