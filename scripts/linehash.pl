#!/usr/bin/perl 
#checks if input line exists in DB (pair of tokenised and parsed lines) 
#if exists then prints existing tokenized and parsed pair, if not prints the tokenized input into separate files

if($ARGV[0] eq "")
{
print "Usage: searched_keys DB-keys DB-data found_DB_data found_DB_keys notfound_searched_keys\n";
}

my $line;
open (INPUT, "<", $ARGV[0]) || die "I can't open $ARGV[0]"; 
open (TOKENDB, "<", $ARGV[1])|| die "I can't open $ARGV[1]"; 
open (PARSEDB, "<", $ARGV[2])|| die "I can't open $ARGV[2]"; 
open (FOUNDPARSE, ">", $ARGV[3])|| die "I can't open $ARGV[3]"; 
open (FOUNDTOKEN, ">", $ARGV[4])|| die "I can't open $ARGV[4]"; 
open (MISSTOKEN, ">", $ARGV[5])|| die "I can't open $ARGV[5]"; 
#open (LOG, ">", $ARGV[6]) || die "I can't open $ARGV[6]"; 


#build hash:
$line=1;
while (my $tokendb = <TOKENDB>)
	{
	chomp $tokendb;
	my $parsedb = <PARSEDB>;
	chomp $parsedb;
	$linehash {$tokendb} = $parsedb; 

#	$test=$linehash {$tokendb};
#	if ($test ne $parsedb)
#		{
#		print LOG "$line NOT STORED: $parsedb\n";
#		}
	$line++;
	}

#check input lines:
while (my $input = <INPUT>)
	{
	chomp $input;
	if(exists $linehash{$input} )
		{
		print FOUNDTOKEN "$input\n";
		print FOUNDPARSE "$linehash{$input}\n" ;
#		print LOG "FOUND: $input\n";
#		print LOG "RESUL: $linehash{$input}\n";
		}
	else
		{
		print MISSTOKEN "$input\n";
#		print LOG "MISS: $input\n";

		}
	}
