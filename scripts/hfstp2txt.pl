use strict;

=pod
#example  input:
_eol:
COMMISSION:
	commission[N]+N+GEN	0.0
	commission[V]+V+INF	0.0

IMPLEMENTING:
	implement[N]+ING[N/N]+N+GEN	0.0
	implement[V]+V+PROG	0.0

REGULATION:
	regulate[V]+ION[V/N]+N+GEN	0.0
	regulation[N]+N+GEN	0.0
	regulation[ADJ]+ADJ+SUPER	0.0

(:
EU:
	eu[N]+N+GEN	0.0

example output:

^COMMISSION/commission[N]+N+GEN/commission[V]+V+INF$ ^IMPLEMENTING/implement[N]+ING[N/N]+N+GEN/implement[V]+V+PROG$ ^REGULATION/regulate[V]+ION[V/N]+N+GEN/regulation[N]+N+GEN/regulation[ADJ]+ADJ+SUPER$ ( ^EU/eu[N]+N+GEN$ )
=cut

my $infile;
my $outfile;
my $statoutfile;
#open($outfile, ">:raw:utf8", "hfstp2txt.stat" ) || die "can't open hfstp2txt";
#open($outfile, "hfstp2txt.stat" ) || die "can't open hfstp2txt";

my $argc=@ARGV;
if ( $argc ne 3)
	{
	print  "Usage:\n";
	print  "perl hfstptxt.pl input output statoutput\n";
	exit ;
	}


open(my $infile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
open(my $outfile, ">", $ARGV[1] ) || die "can't open $ARGV[1]";
open(my $statoutfile, ">", $ARGV[2] ) || die "can't open $ARGV[2]";

#use strict;

my $surf;
my $ana;
my $anas;
my $unkcnt;

while(<$infile>)
	{
	chomp();
	if (s/_eol//)
		{
		if($surf ne "")   #previous word had no ana, print it
			{
			print $outfile "$surf ";
			print $statoutfile "$surf\n";
			$surf = "";
			$unkcnt++;
			}
		print $outfile "\n";
		}
	elsif (s/^([^\t\n]+):/\1/)
		{
		#print "SURF: $surf\n";
		if($surf ne "")   #previous word had no ana, print it
			{
			print $outfile "$surf ";
			print $statoutfile "$surf\n";
			$unkcnt++;
			}
		$surf = $_;
		#print "SURF: $_\n";
		$anas="";
		}
		
	elsif (s/^\t([^\n\t]+)\t([^\n\t]+)/\1/)
		{
		$ana = $_;
		$anas = $anas . "/" .$ana; 
		#print "ANA: $_\n";
		}
	elsif(s/^$//)
		{
		print $outfile "^$surf$anas\$ ";	
		$surf=""; 
		}
	else
		{
		print "unknown input line: $_\n";
		}

	}       

#print $statoutfile "unknown=$unkcnt\n";
