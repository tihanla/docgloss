if ($#ARGV != 2 ) {
	print "usage: input output delimiter\n";
	print "use \\ to quote \(\)\[\]\{\}";
#	print "( =\\x28, )=\\x29, [=\\x5b ]=\\x5d ";
	exit;
	}

$verb=80; #80;  #verbosity expects rule ID
open (INFILE,  "<", $ARGV[0]) || die "file not found: $ARGV[0]\n", $!;
open (OUTFILE, ">", $ARGV[1])|| die "file not found: $ARGV[1]\n", $!;
$delim = $ARGV[2];

#print "delimiter=\"$delim\" \n";

while(<INFILE>)
	{
	@darray=split($delim, $_);
	print OUTFILE scalar @darray, "\n";
	}