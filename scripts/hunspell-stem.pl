# reads the HFST analysed text (one segment one line) and the a file which contains unknown words word exported and analysed by Hunspell (one word by line)
# merge the two files: takes words which HFST could not identify but had a succesfull and usabel Hunspell analysis


#English morphology:

#Prefixes:
#PRE0: detach 
#_HYP  
my $pre0="O";
=pod
O=non--
=cut

#PRE1: do not detach if we have the literal and another without detach:
my $pre1="[ACEFIKUacefc4]";
=pod
A=re-
C=de-
E=dis-
F=com-,co-,cor-,con-
I=im-,in-
K=pre-
U=un-
a=mis-
c=over-
e=out-
f=under-
c=over-
4=trans-
=cut

#Suffixes
#SUF0: detach (mostly inflectional):
my $suf0="[DGHST12]";

#SUF1: do not detach if we have a undetached version with higher score (mostly derivational):
my $suf1="[BDJLNOPQRVWXYZbdghijklmnopqrstuvwxyz765839\+\-]";
=pod
B=-abilty
D=-ed
J=-ings                                   
L=-ment,-ments
N=-ion
O=-al
P=-ness
Q=-ised
R=-er
V=-ive
W=-ic,
X=-issons
Y=-ly,-ally,-y
Z=-y
b=-ible
d=-d
g=-ability
h=-edly
i=-ness
j=-fully
k=-ingly
l=-ably
m=-man
n=-ation
o=-ally
p=-less
q=-isation
r=-er
s=-iser
t=isable
u=-ivness
v=-ively
w=-ical
x=-ional
z=-ly
7=-able
6=-ful
5=-woman
8=-ized
3=-ist
9=-izer
+=-able,ability
-=-ization
=cut

use strict;
#$|++;

#where SUF1, PRE-1 is rather to be detached then SUF1 and PRE1
my %mt;   #morphological type
$mt{'UD'}=120;
$mt{'BLOCK'}=115;                 #

 $mt{'STEM'}=110;                  
 $mt{'pre1-STEM'}=105;             
 $mt{'PRE0-STEM'}=100;             

 $mt{'STEM-SUF0'}=90;             
# $mt{'pre1-STEM-SUF0'}=80;      #na: would require 2 fl: tags
$mt{'PRE0-STEM-SUF0'}=70;

 $mt{'STEM-SUF1'}=60;              #na: would require 2 fl: tags
# $mt{'pre1-STEM-SUF1'}=50;
$mt{'PRE0-STEM-SUF1'}=40;

 $mt{'PRE1-STEM'}=30;             #stem
 $mt{'PRE1-STEM-SUF0'}=25;         #stem
 $mt{'PRE1-STEM-SUF1'}=20;

 $mt{'SURF'}=10;


#HUngarian morphology



my $mtype;
     
my $file;
my $out;
my $si;
my $line;
my $outline;
my $outfile;
my $stmcnt;
my $lastst;
my @resarr;
my $resarrsize;
my @marr;
my $marrsize;
my @earr;
my $block;
my $earrsize;
my $i;
my $stem;
my $lang;
my $altmor;
my $mi;
my $ri;
my $res;
my $ctype;  #current type value

if ( @ARGV ne 3)
	{
	print "Usage:\nhunspell.pl lang input out\n";
	exit;
	}

$lang=$ARGV[0];

open ($file, "<", $ARGV[1])|| die "can't open $ARGV[1]";
#open ($hspfile, "<", $ARGV[2])|| die "can't open $ARGV[2]";
open ($outfile, ">", $ARGV[2])|| die "can't open $ARGV[3]";
                                        

my $cnt=1;

while($line = <$file>)
	{
	chomp($line);
	if($line !~ /[ \t]st:/)         #if the line was not parsed by hunspell
		{
		print $outfile "$line\n";
		}
	else
		{
		if($lang eq "en" )   #hu, cs
			{
			@resarr =split('\t', $line, -1);   #res element to morpholigical elements separated by space
			$resarrsize=@resarr;
			$mtype=$mt{'SURF'};
			$ctype='SURF';
			$res= $line;
			
			for($ri=1; $ri!=$resarrsize; $ri++)	 #iterate on alternative morph anylysis separated by tab, and select the most promising  (first is the surface)
				{
				#print "resarr[$ri]=$resarr[$ri]\n";
				#blocking types: #dont use the result if the surface contains number, Hunspell forgerts to return the numbers in stem
				if($resarr[0] =~ /[0-9]/)   #simple hunspell results
					{
					#if($mt{'BLOCK'} > $mtype) 		
					if($mt{'BLOCK'} > $mt{$ctype}) 		
				
						{ 
						#$mtype =$mt{'BLOCK'} ;
						$ctype ='BLOCK';

						#$res="BLOCK:$1\t\n";  #DEBUG
						$ri=$resarrsize-1;
						}	
					}       	
				elsif($resarr[$ri] =~ s/^ st:([^\t ]+)$/$1/)   #simple hunspell results
					{
					#if($mt{'STEM'} > $mtype) 		
					if($mt{'STEM'} > $mt{$ctype}) 		
						{ 
						#$mtype =$mt{'STEM'} ;
						$ctype ='STEM';
						$res="$resarr[0]\t$1";
						}
					}
				elsif($resarr[$ri] =~ s/^ st:([^\t ]+) fl:($suf0)$/$1$2/)   
					{
					if($mt{'STEM-SUF0'} > $mt{$ctype}) 		
						{ 
						$ctype='STEM-SUF0';
						$res="$resarr[0]\t$1";
						}
					}
				elsif($resarr[$ri] =~ s/^ st:([^\t ]+) fl:($suf1)$/$1$2/)   
					{                                                                             
					if($mt{'STEM-SUF1'} > $mt{$ctype}) 		
						{ 
						$ctype='STEM-SUF1';
						$res="$resarr[0]\t$1";
						}
					}
				elsif($resarr[$ri] =~ s/^ fl:([^\t ]+) st:($pre1)$/$1$2/)   
					{
					if($mt{'PRE1-STEM'} > $mt{$ctype}) 		
						{ 
						$ctype='PRE1-STEM';
						$res="$resarr[0]\t$2";
						}
					}
				elsif($resarr[$ri] =~ s/^ fl:([^\t ]+) st:($pre0)$/$1$2/)   
					{
					if($mt{'PRE0-STEM'} > $mt{$ctype}) 		
						{ 
						$ctype='PRE0-STEM';
						$res="$resarr[0]\t$2";
						}
					}
				elsif($resarr[$ri] =~ s/^ fl:($pre1) st:([^\t ]+) fl:($suf0)$/$1$2$3/)   
					{
					if($mt{'PRE1-STEM-SUF0'} > $mt{$ctype}) 		
						{ 
						$ctype='PRE1-STEM-SUF0';
						$res="$resarr[0]\t$2";
						}
					}
				elsif($resarr[$ri] =~ s/^ fl:($pre0) st:([^\t ]+) fl:($suf0)$/$1$2$3/)   
					{
					if($mt{'PRE0-STEM-SUF0'} > $mt{$ctype}) 		
						{ 
						$ctype='PRE0-STEM-SUF0';
						$res="$resarr[0]\t$2";
						}
					}
				elsif($resarr[$ri] =~ s/^ fl:($pre0) st:([^\t ]+) fl:($suf1)$/$1$2$3/)   
					{
					if($mt{'PRE0-STEM-SUF1'} > $mt{$ctype}) 		
						{ 
						$ctype='PRE0-STEM-SUF1';
						$res="$resarr[0]\t$2";
						}
					}
				elsif($resarr[$ri] =~ s/^([a-z\-]+) st:([^\t ]+) fl:($pre1)$/$1$2$3/)     #reappointed	re st:appointed fl:A
					{
					if($mt{'pre1-STEM'} > $mt{$ctype}) 		
						{ 
						$ctype='pre1-STEM';
						$res="$resarr[0]\t$1$2";
						}
					}
				elsif($resarr[$ri] =~ s/^([a-z\-]+) st:([^\t ]+) fl:($pre0)$/$1$2$3/)     #reappointed	re st:appointed fl:A
					{
					if($mt{'PRE0-STEM'} > $mt{$ctype}) 		
						{ 
						$ctype='PRE0-STEM';  #ne need for pre0-STEM, as it is to be deleted, not like per1
						$res="$resarr[0]\t$2";
						}
					}
				elsif($resarr[$ri] =~ s/^ fl:($pre1) st:([^\t ]+) fl:($suf1)$/$1$2$3/)   
					{
					if($mt{'PRE1-STEM-SUF1'} > $mt{$ctype}) 		
						{ 
						$ctype='PRE1-STEM-SUF1';
						$res="$resarr[0]\t$2";
						}
					}
				else
					{                                                                             
					$res="UNHANDLED: $line";                                                     
					$mtype="0";
					}
				}

			#print $outfile "$ctype: ";  #DEBUG
			print $outfile "$res\n";
				
			}
		#if lang=hu
		#hyphenated: dont use (hyph compounds, suffixed numbers, foreign words with numbers)  (hu,cs)
		#list with pipes: take the first  (hu, cs:na)
		#list with tabs: take the first  (hu,cs)
		#concat: st:  (hu,cs)
		#concat: sp:  (hu, cs:na)
		if($lang ne "en" )   #hu, cs
			{
			$block=0;
			$outline="";
			$stem="";
			@resarr =split('\t', $line, -1);   #morpholigical alternatives  separated tabs we use only the first
			$resarrsize=@resarr;
			if ($resarr[0] =~ /-/)  #  block everything with hyphen. Numbers with suffix, hyphenated compounds are usually wrong 
				{
				$block = 1;
				}
			
			#print "resarr[0]=$resarr[0]\n";  #DEBUG

			for($i=1; $i!=2; $i++)	 #WE Use only the first analysis (the second (i=1) in this list as the first is the surface)
				{
				#print "resarr[$i]=$resarr[$i]\n";   #DEBUG

				@marr =split(' ', $resarr[$i], -1);   #res element to morpholigical elements separated by space
				$marrsize=@marr;

				#print "marrsize=$marrsize\n";
				$altmor=0;   #alternative morph elements in the for of (x|y)  we take the first by stopping at the pipe
				for($mi=0; $mi!=$marrsize; $mi++)	 #iterate on morphs separated by space
					{
					#print " marr[$mi]=$marr[$mi]\n";   #DEBUG
					if($altmor == 1)
						{
						next;
						}
					@earr =split(':', $marr[$mi], -1);   #res element to morpholigical elements separated by space
					$earrsize=@earr;         
					#print "  earrsize=$earrsize earr[0]=$earr[0]  earr[1]=$earr[1] \n";    #DEBUG
					if($earrsize < 2)
						{
						if($earr[0] eq "\|")
							{
							$altmor =1;
							}
						next;  
						}

					if( $earr[0] =~ /(st|sp)/)  #st: stem, sp: surface prefix
						{
						# if($stem ne "") {$stem = $stem . "+";}  #mark compund segments with a + sign: not used anymorem we need valid stems
						$stem = $stem . $earr[1];
						#print "stem=$stem\n";
						}

					if( $earr[0] =~ /pa/)  #pa: part of compound, one stem part might missing  (da,de,et,nl,sv), (hu is correct...)
						{
						if ($lang = /(da|de|et|nl|sv)/)
							{
							$block=1;
							#print "stem=$stem\n";
							}
					#else 	{print "non st: $earr[0]\n";}							
						}
					}
				}


			if( ($stem ne "") && ($block == 0))  
				{
				$outline =  $resarr[0] . "\t" . $stem;
				#print "stem=$stem\n";  #DEBUG
				}
			else	#we cannot use the hunspell result
				{
				$outline = $resarr[0] . "\t"; #. $resarr[0];
				#print "line=$outline\n";
				}
			print $outfile "$outline\n";
			}  #hu

		}  #if we try to use hunspell results 

	}

