#creates hash from target ids and writes out  source and target lines tht has this id
#created by modification of getidlangline.pl

use strict;
use Getopt::Long;
#prints out the columns of table in the specified order


my $order;
my $table;
my $ordersize;
my $insize;
my @inarray;
my $argc;
my $in;
my $indb;
my $dbfile;
my $orderi;
#my $srclang;
 #my $lang;	#trglang
my $langcol;
my $idcol;
my %idhash;
my $idcoldb;
my $langcoldb;
my $srclangdb;
my $trglangdb;

my $mul="N";

my $result = GetOptions (
"mul=s" => \$mul
);

my $argc=@ARGV;

#print "argc=$argc\n";
#print "mul=$mul\n";  #DEBUG

if ( ($argc ne 6 ) || ($mul eq "" ))
	{
	print  "Usage:\n";
	print  "perl getidlangline db_idcol db_langcol db_src_lang db_trg_lang db_input output [-mul=Y/N]\n";
	#perl %script_path%\getidlangline.pl  %idcol%  %work_path%\found-src.txt  %langcol%  %src% %trg% "%dicinput_path%\%dicinput%" "%work_path%\target.tsv"
	exit ;
	}

$idcoldb =  $ARGV[0];
$langcoldb =  $ARGV[1];
$srclangdb =  $ARGV[2];
$trglangdb =  $ARGV[3];
open(my $dbfile, "<", $ARGV[4] ) || die "can't open $ARGV[4]";
open(my $outfile, ">", $ARGV[5] ) || die "can't open $ARGV[5]";


#print "source_langdb=$srclangdb, target_lang=$trglangdb\n";
#print "idcoldb=$idcoldb, lancoldb=$langcoldb\n";

#exit;

#print "order=$order, ";
#print "ordersize=$ordersize\n";

my $count =1;

$count=1;
$indb = <$dbfile>;
$count++;
print $outfile  "$indb";

while ($in = <$dbfile>)
	{
	#print $outfile  $in;
	#next;
	chomp($in);
	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	if(($inarray[$langcoldb-1] eq "mul") && ($mul eq "Y"))
		{
		$inarray[$langcoldb-1] = "$trglangdb";
		}

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	if($inarray[$langcoldb-1] eq $trglangdb)
		{
		$idhash{$inarray[$idcoldb-1]}=1;
		}
	#print   "$inarray[$idcol-1]\n"; DEBUF

	#print src terms
	#OLD print $outfile  "$in\n";
	
	$count++;
	}	

#reset input, we read it again
close $dbfile;
open(my $dbfile, "<", $ARGV[4] ) || die "can't open $ARGV[4]";

#print header line:

while ($indb = <$dbfile>)
	{                                ##
	#DEBUG:
	#print  "count=$count, $indb";

	@inarray = split ('\t', $indb, -1);
	$insize=@inarray;

	if(($inarray[$langcoldb-1] eq "mul") && ($mul eq "Y"))  #this definetely creates a pair, at least with itself (or maybe also with additionals)
		{
		;
		$inarray[$langcoldb-1] = "$trglangdb";
		$indb = join ("\t",@inarray) ;
		print $outfile "!s$indb"; 
		$inarray[$langcoldb-1] = "$srclangdb";
		$indb = join ("\t",@inarray) ;
		print $outfile  "!t$indb";
		}
	else		
		{		                                 
		#print "insize=$insize\n";
		#small hack to supprot merge: if the first c{olumn is TERM and it is to be moved than rename it to "EN"
		#if((exists $idhash{$inarray[$idcoldb-1]} ) && (($inarray[$langcoldb-1] eq $srclangdb) || (($inarray[$langcoldb-1] eq $trglangdb))))
		if((exists $idhash{$inarray[$idcoldb-1]} ) && ( ($inarray[$langcoldb-1] eq $srclangdb) ||  ($inarray[$langcoldb-1] eq $trglangdb) ))
			{
			print $outfile  "$indb";
			#print "inarray[$idcoldb-1]=$inarray[$idcoldb-1], idhash=$idhash{$inarray[$idcoldb-1]},  lang=$inarray[$langcoldb-1]\n";  #DEBUG
			#print "found: $indb\n";  #DEBUG
			}
		#else
		#		{; #DEBUG print "idhash=$idhash{$inarray[$idcoldb-1]},  lang=$inarray[$langcoldb-1]\n";}
		}
		
	$count++;
	}	


close $dbfile;
close $outfile;
