#sorts a doc. spec IATE database where terms are either in term field or in form field

use strict;
#prints out the columns of table in the specified order

my $insize;
my @inarray;
my $argc;
my $in;
my $indb;
#my $dbfile;
#my $srclang;
 #my $lang;	#trglang
my %hash_headword;
my %hash_src_term;
my $termcol;
my $formcol;

my $term;
my $form;
my $headword;
my $index;
my $idkey;
my $idval;

my $argc=@ARGV;
#my $hw;
my $referring;

my $prev_src_term;
my $prev_form;
#print "argc=$argc\n";
my $testformnum;




my $order;
my $table;
my $ordersize;
my $insize;
my @orderarray;
my @inarray;
my @headarray;
my @cfgarray;                                                                                               
my @tigarray;
my $argc;
my $in;
my$orderi;

my $entry_size;
my $lang_size;
my $term_size;
#my $badcom=N;
#my $form=N;

my $i;
my $ti;
my $tigsize;

#my $id_col;
#my $lang_col;
#my $term_col;

my $argc=@ARGV;
#print "argc=$argc\n";


my $prevterm="";
my $entry_level;
my $src_lang_level;
my $src_term_level;
my $trg_lang_level;
my $trg_term_level;

my $src_termt;
my $trg_termt;
my $src_term;
my $trg_term;

my $src_lang;
my $trg_lang;
###


my $count =1;

$count=1;


#beg------------
if ( $argc ne 3)
	{
	print  "Usage:\n";
	print  "perl txt2tbx.pl configfile input.txt output.tbx \n";
	exit ;
	}

open(my $cfgfile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
open(my $infile, "<:encoding(utf8)", $ARGV[1] ) || die "can't open $ARGV[1]";
open(my $outfile, ">:encoding(utf8)", $ARGV[2] ) || die "can't open $ARGV[2]";



print $outfile "<?xml version='1.0'?>\n";
print $outfile "<!DOCTYPE martif SYSTEM \"TBXcoreStructV02.dtd\">\n";
print $outfile "<martif type=\"TBX\" xml:lang=\"en\">\n\n";
print $outfile "<martifHeader>\n<fileDesc>\n<titleStmt>\n\t<title>IATE extraction</title>\n</titleStmt>\n";
print $outfile "<sourceDesc>\n\t<p>This termbase has been distributed by the Terminology Coordination Unit of the Commission</p>\n";
print $outfile "\t<p>DTD source: http:\/\/www.ttt.org\/oscarStandards\/tbx\/TBXcoreStructV02.dtd</p>\n</sourceDesc>\n</fileDesc>\n</martifHeader>\n\n";


print $outfile "<text>\n";
 print $outfile "<body>\n\n";



while ($in = <$cfgfile>)
	{
	#if ($in =~ s/^set //)
	$in =~ s/^set //;     #also works for linux config, without 'set' keyword
		#{
		@cfgarray = split ('=', $in, -1);
		if($cfgarray[0] eq "entry")
			{
			$entry_size=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "lang")
			{
			$lang_size=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "term")
			{
			$term_size=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "formc")
			{
			$formcol=$cfgarray[1];
			}
		elsif ($cfgarray[0] eq "termc")
			{
			$termcol=$cfgarray[1];
			}
		#}
	}
#end---------

$count=1;



while ($in = <$infile>)
	{
	#print $outfile  $in;
	#next;
	chomp($in);

	@inarray = split ('\t', $in, -1);
	$insize=@inarray;


	#save field names in header:
	if( ($count == 1) )
		{
		@headarray=@inarray;
		$count++;
		next;
		}

	
	$form=$inarray[$formcol-1];
	$term=$inarray[$termcol-1];

	$testformnum=$form;
	if( ($testformnum =~ s/^[0-9]+$// ) || ($form eq "") )
	#if( (($form > 0 ) && ($form < 1000 )) || ($inarray[$formcol-1] eq "0"))
		{
		$headword=$term;
		#print "headword-term=$headword\n";  #DEBUG
		}
	
	else
		{
		$headword=$form;
		#print "headword-form=$headword\n";  #DEBUG
		}


	for ($index=0;;)
		{
		$idkey = $headword . "-" .  $index;
		#print "idkey=$idkey\n";
		if(exists  $hash_headword{$idkey})
			{
			#if we have a key, check if the surf is the same (this may happen because of lowercasing)
			#if identical then increase the frequency  otherwise creta new index item
			$index++;
			}
		else
			{
			$hash_headword{$idkey}=  $in;
			#print "hash_headword{$idkey}=$hash_headword{$idkey}  in=$in";  DEBUG
			$count++;
			last;
			}
		}

	$count++;
	}	


	


	
#reset input, we read it again

my $count =1;
foreach my $head (sort  keys %hash_headword) 
	{
	
#while ($in = <$infile>)
#	{
	$in= $hash_headword{$head};

	chomp($in);
	$in =~ s/\&([a-z]+[^a-z;])/&amp;\1/ig;  #quote & if it soes not look like a character entity
	$in =~ s/\&([a-z]+[^a-z;])/&amp;\1/ig;  #needs duplication because of &X& constructions  (X is any single character)

	$in =~ s/\&([^a-z])/&amp;/ig;           #as a result both & and &amp;  will be &amp;  on the output! 

#print "in=$in\n";  #DEBUG

	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print $outfile "in=$in\n";  #DEBUG


	# entry_size + 1 (TERM) + lang_size + term_size-1  | 1 (TERM) + lang_size + term_size-1 
	
	#read out: current term and find out if we create a new entry or concat to previous
	#print $outfile "<termEntry>\n";	
	$entry_level="";
	for($i=0 ;$i != $entry_size; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        $entry_level = $entry_level . "\t\t<descrip type=\"" . $headarray[$i] . "\">";
			$entry_level = $entry_level . $inarray[$i];
	        	$entry_level = $entry_level . "</descrip>\n";

			}
		}


	#source side----------------------------
        #print $outfile "<langSet xml:lang=\"", $inarray[$entry_size+1], "\">\n";
	$src_lang = "<langSet xml:lang=\""; 
	$src_lang = $src_lang . $inarray[$entry_size+1];
	$src_lang = $src_lang .  "\">\n";

	$src_lang_level="";
	for($i=$entry_size+2;$i != $entry_size+1+$lang_size; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        $src_lang_level = $src_lang_level .  "\t\t<descrip type=\"" . $headarray[$i] . "\">";
			$src_lang_level = $src_lang_level .  $inarray[$i];
	        	$src_lang_level = $src_lang_level .  "</descrip>\n";

			}
		}




	$src_term_level="";
	for($i=$entry_size+1+$lang_size;$i != $entry_size+$lang_size+$term_size; $i++)	
		{
		@tigarray = split ('\|', $inarray[$i], -1);   #input does not contain | (replaced to ,)
		if ($headarray[$i] eq "") {;}
		elsif ($headarray[$i] eq "COM_BAD_MT") {;}
		elsif ($inarray[$i] eq "") {;}
		else
			{
			if($inarray[$i] ne "")
				{
			        $src_term_level = $src_term_level .  "\t\t<descrip type=\"" . $headarray[$i] . "\">";
				$src_term_level = $src_term_level . $inarray[$i];
			        $src_term_level = $src_term_level . "</descrip>\n";
				}
			}
		}
	

	#target side-------------------------------
	my $t=$entry_size+$lang_size+$term_size;

        #print $outfile "<langSet xml:lang=\"", $inarray[$t+1], "\">\n";
        
	$trg_lang_level="";

	$trg_lang = "<langSet xml:lang=\""; 
	$trg_lang = $trg_lang . $inarray[$t+1];
	$trg_lang = $trg_lang .  "\">\n";


	for($i=$t+2;$i != $t+$lang_size+1; $i++)	
		{
		if($inarray[$i] ne "")
			{
		        $trg_lang_level = $trg_lang_level . "\t\t<descrip type=\"" . $headarray[$i] . "\">";
			$trg_lang_level = $trg_lang_level .  $inarray[$i];
	        	$trg_lang_level = $trg_lang_level .  "</descrip>\n";
			}
		}


	
	
      	$trg_term =   $inarray[$t];

	$trg_term_level="";
	for($i=$t+$lang_size+1; $i != $t+$lang_size+$term_size; $i++)	
		{
		if ($headarray[$i] eq "") {;}
		elsif ($headarray[$i] eq "COM_BAD_MT") {;}
		elsif ($inarray[$i] eq "") {;}
		else
			{
	
			if($inarray[$i] ne "")
				{
		        	$trg_term_level = $trg_term_level . "\t\t<descrip type=\"" . $headarray[$i] . "\">";
				$trg_term_level = $trg_term_level . $inarray[$i];
        			$trg_term_level = $trg_term_level . "</descrip>\n";
				}
			}
		}
#
	#print results
	#$hw = $head;
	#$hw =~ s/\-[0-9]+$//;


	$src_term =  $inarray[$entry_size];
	$form=$inarray[$formcol-1];
	$referring=1;

	$testformnum=$form;
	#if( $testformnum =~ s/^[0-9]+$//  )
	if( ($testformnum =~ s/^[0-9]+$// ) || ($form eq "") )

	#if(($form > 0 ) && ($form < 100 ))
		{$referring=0;}

	
	#print "$head, $hw\n";

#	if 

	#print $outfile "referring\t form\t prev_form\t src_term\t prev_src_term\n";
	#print $outfile "$referring\t$form\t$prev_form\t$src_term\t$prev_src_term\n";
	
	#do not print a new wntry if it is a referring, with identical soure_term and form:
	if(($referring == 0) || ($form ne $prev_form) || ($src_term ne $prev_src_term))
		{

		if(($referring == 1 && $form ne $prevterm) || ($referring == 0 && $src_term ne $prevterm))
		
				{
				if( $prevterm ne "" ) 	 #if not the first one
					{
					print $outfile "</termEntry>\n";	
					}
				print $outfile "<termEntry>\n";	
				%hash_src_term = ();   #empty hash

			        }

	#		print  "src_term=$src_term,form=$form\n";   #DEBUG
	                #do not print an identical headword
			if(! exists $hash_src_term{$src_term})
#			if( $src_term ne $prev_src_term)
				{
	 			print $outfile "$src_lang";
				print $outfile "\t<tig>\n";	
				print $outfile "\t\t<term>$src_term</term>\n";
				#if(($form > 0 ) && ($form < 100 ))
				$testformnum=$form;
				if( ($testformnum =~ s/^[0-9]+$// ) || ($form eq "") )

					{
					print $outfile "\t\t<descrip type=\"Frequency\">$form</descrip>\n";
					}
				else
					{
					#original form draws away attention, instead we give only Termtype: Lookup information, 
					#needed to indicate that IT IS NOT a term to be used 
					#print $outfile "\t\t<descrip type=\"Lookup\">$form</descrip>\n";
					print $outfile "\t\t<descrip type=\"Termtype\">Lookup</descrip>\n";
					}
				print $outfile "\t</tig>\n";	
			
				print $outfile "</langSet>\n";	
				$hash_src_term{$src_term}=1;
			         }

		#if not morph variant:
		#if(($form > 0 ) && ($form < 100 ))
		$testformnum=$form;
		if( ($testformnum =~ s/^[0-9]+$// ) || ($form eq "") )

			{
		 	print $outfile $trg_lang;
			print $outfile "\t<tig>\n";	
			print $outfile "\t\t<term>$trg_term</term>\n";
			print $outfile "$entry_level\n";
			print $outfile "$trg_lang_level\n";
			print $outfile "$trg_term_level\n";
		
			#print $outfile "\t\t<descrip type=\"****\">$src_term</descrip>\n";
			#print $outfile "$src_lang_level\n";
			#print $outfile "$src_term_level\n";	

			print $outfile "\t</tig>\n";	
			print $outfile "</langSet>\n";	
			print $outfile  "\n";
			}	

		}

	$prev_src_term= $src_term;
	$prev_form=$form;

	if ($referring==1)
		{
		$prevterm = $form;
		}
	else
		{
		$prevterm=$src_term;
		
		}
	$count++;

	}	

print $outfile "</termEntry>\n";	

print $outfile "</body>\n";
print $outfile "</text>\n";
print $outfile "</martif>\n";

close $infile;
close $outfile;
