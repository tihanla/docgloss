#converts the piped list format of iate table into tab separated pairs (generating all possible pairs of src and trg)
#input format is created by iate_convert.bat or .sh to be used in SDLTB-s
#author: Laszlo Tihanyi, tihanyi1123@gmail.com

use strict;


=pod
#tipical input fields (the src is in field 5 and trg in field 21)

 1 IATE-ID	
 2 Domains	
 3 Domain note	
 4 Primary	
 5 EN	
 6 LANGUAGE_CODE	
 7 Definition	
 8 Definition reference	
 9 Definition note	
10 Related material	
11 Formatted term	
12 Term type	
13 Reliability	
14 Term reference	
15 Context	
16 Context reference	
17 Evaluation	
18 Term note	
19 Institution	
20 COM_BAD_MT	
21 HR	
22 LANGUAGE_CODE	
23 Definition	
24 Definition reference	
25 Definition note	
26 Related material	
27 Formatted term	
28 Term type	
29 Reliability	
30 Term reference	
31 Context	
32 Context reference	
33 Evaluation	
34 Term note	
35 Institution	
36 COM_BAD_MT	
=cut

my $srccol; #=5;
my $trgcol; #=21;

my $srcout;
my $trgout;
my $i;
my $j;
my $in;
my @inarr;
my @srcarr;
my @trgarr;
my $src;
my $trg;
my $srcarrn;
my $trgarrn;
my $insize;

my $argc=@ARGV;

if ( $argc ne 4)
	{
	print  "Usage:\n";
	print  "perl txt2lst.pl srccol trgcol input output \n";
	exit ;
	}

$srccol = $ARGV[0];
$srccol--;  #we count from 1;
$trgcol = $ARGV[1];
$trgcol--;  #we count from 1;

open(my $infile, "<", $ARGV[2] ) || die "can't open $ARGV[2]";
open(my $src_outfile, ">", $ARGV[3] ) || die "can't open $ARGV[3]";


<$infile>;   #jump the header

while ($in = <$infile>)
	{
	s/\t/ /g;   #for sake of safety...
	s/[ \t]+//g;

	chomp($in);
	@inarr = split ('\t', $in, -1);
	$insize=@inarr;
	$src=$inarr[$srccol];
	$trg=$inarr[$trgcol];

	@srcarr = split ('\|', $src, -1);
	$srcarrn=@srcarr;
	@trgarr = split ('\|', $trg, -1);
	$trgarrn=@trgarr;

	
#	print $src_outfile "*** $src ---- $trg\n";  #DBG

	#seach for identical terms (tipically capitalized abbreviations) write out them and and delete from pairing
	for($i=0;$i!=$srcarrn;$i++)
		{
		for($j=0;$j!=$trgarrn;$j++)
			{
			#print $src_outfile "$srcarr[$i]\n";
			if($srcarr[$i] eq $trgarr[$j] )
				{
				print $src_outfile "$srcarr[$i]\t$trgarr[$j]\n";    #DBG
				$srcarr[$i] = "";				
				$trgarr[$j] = "";				
				}
			}
		}

	#write out pairs if not empty strings:
	for($i=0;$i!=$srcarrn;$i++)
		{
		for($j=0;$j!=$trgarrn;$j++)
			{
			if(($srcarr[$i] ne "") && ($trgarr[$j] ne ""))
				{
				print $src_outfile "$srcarr[$i]\t$trgarr[$j]\n";    #DBG
				}
			}
		}
	}

close $infile;
close $src_outfile;

