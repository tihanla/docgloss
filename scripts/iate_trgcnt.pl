#creates hash from target ids and writes out  source and target lines tht has this id
#created by modification of getidlangline.pl

use strict;
#prints out the columns of table in the specified order

my $order;
my $table;
my $ordersize;
my $insize;
my @inarray;
my $argc;
my $in;
my $indb;
my $dbfile;
my $orderi;
#my $srclang;
 #my $lang;	#trglang
my $langcol;
my $idcol;
my %idhash;
my $idcoldb;
my $langcoldb;
my $srclangdb;
my $trglangdb;

my @meancnt;
my $lastid;
my $mcnt;
my $mcntmax;
my $srccnt=0;
my $trgcnt=0;

my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 6)
	{
	print  "Usage:\n";
	print  "perl getidlangline db_idcol db_langcol db_src_lang db_trg_lang db_input output \n";
	#perl %script_path%\getidlangline.pl  %idcol%  %work_path%\found-src.txt  %langcol%  %src% %trg% "%dicinput_path%\%dicinput%" "%work_path%\target.tsv"
	exit ;
	}

$idcoldb =  $ARGV[0];
$langcoldb =  $ARGV[1];
$srclangdb =  $ARGV[2];
$trglangdb =  $ARGV[3];
open(my $dbfile, "<", $ARGV[4] ) || die "can't open $ARGV[4]";
open(my $outfile, ">", $ARGV[5] ) || die "can't open $ARGV[5]";


#print "source_langdb=$srclangdb, target_lang=$trglangdb\n";
#print "idcoldb=$idcoldb, lancoldb=$langcoldb\n";

#exit;

#print "order=$order, ";
#print "ordersize=$ordersize\n";

my $count =1;

$count=1;
$indb = <$dbfile>;    #header
$count++;
#print $outfile  "$indb";

while ($in = <$dbfile>)
	{
	#print  $in;   DEBUG
	#next;
	chomp($in);
	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	#print "$langcoldb= $langcoldb-1, inarray[$langcoldb-1]=$inarray[$langcoldb-1],  trglangdb=$trglangdb\n";

	if($inarray[$langcoldb-1] eq $srclangdb)
		{
		$idhash{$inarray[$idcoldb-1]}=1;
		#print   "added: $inarray[$idcol-1]\n"; #DEBUG
		}

	#print src terms
	#OLD print $outfile  "$in\n";
	
	$count++;
	}	

#reset input, we read it again
close $dbfile;
open(my $dbfile, "<", $ARGV[4] ) || die "can't open $ARGV[4]";

print "$count added\n";  #DEBUG

#print header line:

$lastid=0;
$mcnt=0;
$mcntmax=0;
$indb = <$dbfile>;  #header

while ($indb = <$dbfile>)
	{
	#DEBUG:
	#print  "count=$count, $indb";

	@inarray = split ('\t', $indb, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	#if((exists $idhash{$inarray[$idcoldb-1]} ) && (($inarray[$langcoldb-1] eq $srclangdb) || (($inarray[$langcoldb-1] eq $trglangdb))))
	if( exists $idhash{$inarray[$idcoldb-1]} ) 
		{
		if( $inarray[$langcoldb-1] eq $srclangdb )
			{
			#print $outfile  "$indb";
			#stats:
			$srccnt++;
			}
		elsif ( $inarray[$langcoldb-1] eq $trglangdb) 
		        {
			#print $outfile  "$indb";
		
			#stats:
			$trgcnt++;
			#print $outfile "id=$inarray[$idcoldb-1], lang=$inarray[$langcoldb-1], lastid=$lastid\n";  #DEBUG
			if($inarray[$langcoldb-1] eq $trglangdb)
				{
				#print $outfile "lastid=$lastid, inarray[$idcoldb-1]=$inarray[$idcoldb-1]\n";
				if(($lastid == $inarray[$idcoldb-1]) || ($lastid == 0))
					{
					$mcnt++;
					if($mcnt > $mcntmax)
						{
						$mcntmax=$mcnt;
						}
					#print $outfile "mcnt++ => $mcnt, id=$inarray[$idcoldb-1], \n";  #DEBUG
				        }
				else
					{
					$meancnt[$mcnt]++;
					#print $outfile  "meancnt[$mcnt]=$meancnt[$mcnt]\n";  #DEBUG
					$mcnt=1;
					}
				$lastid= $inarray[$idcoldb-1];
				}
			#print "found: $indb\n";  #DEBUG
			}
		}
	else
		{
		$lastid= $inarray[$idcoldb-1];
		if($mcnt > 0)   
			{
			$meancnt[$mcnt]++;
			}
        	$mcnt=0;
		#print "idhash=$idhash{$inarray[$idcoldb-1]},  lang=$inarray[$langcoldb-1]\n";
		}
		
	$count++;
	}	


#if last ID had translation
if($mcnt > 0)   
	{
	$meancnt[$mcnt]++;
	}

#stats: count number of translations of terms
print $outfile "src\t$srccnt\n";
print $outfile "trg\t$trgcnt\n";

for($mcnt=1; $mcnt <= $mcntmax; $mcnt++)
	{
	print $outfile "$mcnt\t$meancnt[$mcnt]\n";
	}

close $dbfile;
close $outfile;
