#Anything in this file, followed by a period (and an upper-case word), does NOT indicate an end-of-sentence marker.
#Special cases are included for prefixes that ONLY appear before 0-9 numbers.
#
#any single upper case letter  followed by a period is not a sentence ender
#usually upper case letters are initials in a name
A
B
Ċ
C
D
E
F
Ġ
G
Għ
H
Ħ
I
J
K
L
M
N
O
P
Q
R
S
T
U
V
W
X
Y
Ż
Z


# Period-final abbreviation list for Maltese
a.m
art
Art
cf
Co
eċċ
Eċċ
eż
Fig
i.e
ikomp
Ill
ink
inkl
Ir-Reg
kap
Kap
kont
l-Art
nru
Nru
p
p.e
p.m
para
pġ
pp
Prof
Reg
Tab
tel
var
vol
Vol
