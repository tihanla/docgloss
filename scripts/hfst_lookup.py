#!/usr/bin/python

import itertools
import sys
import os
from shared import Header, Alphabet, LetterTrie
from transducer import Transducer
from transducer import TransducerW

class OlTransducer:
    def __init__(self, filename):
        '''Read a transducer from filename
        '''
        handle = open(filename, "rb")
        self.header = Header(handle)
        self.alphabet = Alphabet(handle, self.header.number_of_symbols)
        if self.header.weighted:
            self.transducer = TransducerW(handle, self.header, self.alphabet)
        else:
            self.transducer = Transducer(handle, self.header, self.alphabet)
        handle.close()
    def analyse(self, string):
        '''Take string to analyse, return a vector of (string, weight) pairs.
        '''
        if transducer.analyze(string):
            return transducer.displayVector
        else:
            return []

_INPUT_ENCODING = 'utf-8'
def check_case(string_decoded):
    cases = itertools.imap(lambda c:c.isupper(), string_decoded)
    try:
        if not cases.next(): return False
    except StopIteration:
        return False

    try:
        pivot = cases.next()
    except StopIteration:
        return True

    for c_case in cases:
        if c_case is not pivot:
            return False

    return True


def handle_uppercase(string):
    string_decoded = string.decode(_INPUT_ENCODING)
    if check_case(string_decoded):
        lower = string_decoded.lower().encode(_INPUT_ENCODING)
        if lower != string:
            doAnalysis(lower)

def doAnalysis(string):
    if transducer.analyze(string):
        if transducer.analysisIsEmpty():
            handle_uppercase(string)
        else:
            transducer.printAnalyses()
            print
    else:
        # tokenization failed
        handle_uppercase(string)
        
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: python HfstRuntimeReader FILE"
        sys.exit()
    transducerfile = open(sys.argv[1], "rb")
    header = Header(transducerfile)
    #print "header read"
    alphabet = Alphabet(transducerfile, header.number_of_symbols)
    #print "alphabet read"
    if header.weighted:
        transducer = TransducerW(transducerfile, header, alphabet)
    else:
        transducer = Transducer(transducerfile, header, alphabet)
    #print "transducer ready"
    #print

    while True:
        try:
            string = raw_input()
        except EOFError:
            sys.exit(0)
        print string + ":"
        if string:
            doAnalysis(string)
        else:
            print

