use strict;

my @linearr;
my $srfstm;
my $srf;
my $stm;
my $lcnt=1;
my $i;
my $linesize;
my @srfstm;
my $out;

while(<>)
	{

        s/\\\//_clean_slash_/g;

	@linearr=split(' ', $_, -1);
	$linesize=@linearr;
	#print "linesize=$linesize\n";
	$stm="";
	$srf="";
	for ($i=0; $i != $linesize; $i++)
		{
		#print "line=$lcnt i=$i\n";
		@srfstm=split('/', $linearr[$i], -1);

		if( ($i > 0) && ($i < ($linesize-1)) )
			{
			$stm= $stm . " ";
			$srf= $srf . " ";
		        }

		if($srfstm[1] eq "")  #if no stem
			{
			$stm= $stm . $srfstm[0];
			}
		else
			{
			$stm= $stm . $srfstm[1];
			}
		$srf= $srf . $srfstm[0];

		}
	$out = $stm . "\t" . $srf;
	$out =~ s/_clean_slash_/\\\//g;                 #restore \/
	$out =~ s/\\\/\/\\\//\//g;                     #replace:  \//\/  with /
	$out =~ s/([^\\])\\/\1/g;                      # \[  -> [
	$out =~ s/^\\//g;                              # ^\  ->  del

	print "$out\n";
	$lcnt++;
	}