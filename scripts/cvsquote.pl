use strict;

my $src;
my $trg;

if (@ARGV != 2 )  
	{
	print  "Usage:\n";
	print  "perl cvsquote.pl input output  \n";
	exit 1;
	}                  

open $src, "<$ARGV[0]";
open $trg, ">$ARGV[1]";

binmode ($src);
binmode ($trg);
my $field;

while ( <$src> )
	{
	chomp();
	my @qtarr=split('\t', $_, -1);
	my $qtuarrlen = $#qtarr;
	if($qtuarrlen > 0)
		{
		for(my $ti = 0; $ti != $qtuarrlen + 1; $ti++)
			{
			#print $_;
			$field="";
			my @qarr=split('\"', $qtarr[$ti], -1);
			my $quarrlen = $#qarr;
			#print "qaurrlen=$quarrlen\n";
			if($quarrlen > 0)
				{
				$field = $field . "\"" ; 
				for(my $i = 0; $i != $quarrlen + 1; $i++)
					{
					#print "\n$i=$qarr[$i]\n";
					$field = $field . $qarr[$i];
					if($i != $quarrlen)
						{
						$field = $field . "\"\"";  
						}
					}
				$field = $field . "\"" ;
				}
			else
				{
				$field = $qtarr[$ti];
				}

			#fields startng with = + - sign: 
			#we need also when starts with double quote,otherwise fields longer then 256 char will be truncated
			#'-dasdsa
			$field =~ s/^(\"?)([=+-])/$1'$2/;

			#save dates from conversion: 
			#'-3356/2/09  -> 6356.02.09. 0:00:00
			#2014/12/30
			#6356/02/09  
			#31/12/31
			#5/1/3
			#""'2014/12/30
			$field =~ s/^([0-9][0-9]?[0-9]?[0-9]?\/[0-9][0-9]?\/[0-9][0-9]?[0-9]?[0-9]?)$/'$1/;

			#save decimal separator on Hungarian Windows:
			$field =~ s/^([0-9]+\.[0-9]+)$/'$1/;

			# avoid: (20)  -> -20
			$field =~ s/^(\"?)(\([0-9]+\)\"?)$/$1'$2/;

			#avoid date  (on windows locale)
			$field =~ s/^([0-9][0-9]?\.?[ ]?)(January|February|March|April|May|June|July|August|September|October|November|December)$/'$1$2/;
			$field =~ s/^(janu�r|febru�r|m�rcius|�prilis|m�jus|j�nius|j�lius|augusztus|szeptember|okt�ber|november|december|Janu�r|Febru�r|M�rcius|�prilis|M�jus|J�nius|J�lius|Augusztus|Szeptember|Okt�ber|November|December) ([0-9][0-9]?\.?)$/'$1 $2/;


			#=("-asdsad asdsa dsad saddd dddd rrrr ")
			#=("+asdsad asdsa dsad saddd dddd rrrr ")

			#print "$field\n";
			print $trg $field;
			if($ti != $qtuarrlen)
				{
				print $trg  "\t";  
				}
			}
		}

	print $trg "\n";
	#exit;
	}

close $src;
close $trg;
