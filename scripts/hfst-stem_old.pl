#extracts stems of various HFST morphological analysers for the EU24 languages
#languages implicitly assumes that they were analysed by one specific morphological engine (like: HFST, Apertium, hunmorph-foma, open-sfst, galliatekno)
#see input syntax of various languages at the end of file
#options: 
#returns part-of-speech (pos)
#returns compound constituents

# tihanyi@ext.ec.europa.eu


use strict;

my $lang;
my $si;
my $wi;
my $li;
my $ci;
my $mi;
my $sarrsize;
my $line;
my $wlen;
my $lexform;
my $word;
my $lexhashkey;
my @lexarr;
my %lexhash;
my %lexhashpart;
my $lexhashkeys;
my $lfsize;
my $lexhashkey;
my $poshashkey;
my $mainstem;
my $poshashcnt;
my $listchar;
my $pos;
my $part;
my $udic;
my $ud;
my %udichash;
my $stem;
my $srf;
my $pos;
my $first;  #ruturns only the first stemming result  /simple disambiguation/
my $word1;

my $wordh;
my $wh;
my $warrsize;
my $wout;
my $wsurfout;
my $wanaout;
my $wunknown;

my @word1arr;

use Getopt::Long;
my $result = GetOptions (
"lang=s" =>\$lang,
"udic=s" =>\$udic,
"srf" =>\$srf,
"pos" =>\$pos ,
"part" =>\$part,
"first" =>\$first
);  # flag

my $argc=@ARGV;

if ( ($argc ne 0) || ($lang eq "" ) )
	{
	if($lang eq "" ){print "lang is not specified!\n";}
	print  "Usage:\n";
	print  "perl hfst-stem.pl -lang=LANG [-pos] [-srf] [-part] [-first]\n";
	print  "-lang: required: iso-639-2 lanuguage code of the EU language\n";                              
	print  "-pos: prints out part of speech info additionally to stems\n";                              
	print  "-srf: prints out surface form additionally to stems\n";                              
	print  "-part: prints out parts of stems additionally, e.g. each element of a compound word\n"; 
	print  "-first: prints out only the first result\n";                              
	print  "-udic: name of user dictionary\n";                              
	print  "reads standard input and writes to standard output\n";
	print  "more info:  laszlo.tihanyi\@ext.ec.europa.eu\n";

	exit;
	}                                                                                 
#print "pos=$pos\n";
#print "part=$part\n";
$stem=1;


#initiate user dictionary
if($udic ne "")
	{
	#if(open $ud, "<$udic")
	open(my $ud, "<", $udic)  or die "cannot open > $udic $!";
	my $ui=0;
	while ($word = <$ud>)
		{
		chomp($word);
		my @uarr =split('\t', $word, -1);   #break words to ana
		my $uarrsize=@uarr;
		#my $key= lc ($uarr[0]);
		my $key= $uarr[0];
		$udichash{$key} =  $uarr[1];     #build a hash from elements to avoid duplicates
		$ui++;
		#print "$ui word=$word key=$key  val=$uarr[1]\n";  #DEBUG
		}	
	close $ud;
	}



my $lout="";
while (<>)
	{
	#array from lines
	chomp();   #chomp new line
	$line=$_;
	#print "line0=$line\n";  #DEBUG

	if (substr($line,0,1) eq "]")
		{
		$line=substr($line,1);  #chomp leading ]
		}
	if (substr($line,-1) eq "[")
		{
		$line=substr($line,0,-1);  #chomp trailing [
		}
	

	#print "line=$line\n";  #DEBUG

	#replace spaces used as stem/pos/cat separator in fi
	if($lang eq "fi")
		{
		$line =~ s/([^\$]) ([^\\^])/\1\+\2/g;
		$line =~ s/([^\$]) ([^\\^])/\1\+\2/g;   #need repetition becasuse of single character pos, could be written more nicely with context regexp...
		}

	if($lang eq "de")
		{
		$line =~ s/<([^>]+)\/([^>]+)>/<\1-\2>/g;     #replace / to - in tags
	        }

	if($lang eq "en")
		{
		$line =~ s/\[([^\[\] ]+)\/([^[] ]+)\]/\[\1-\2\]/g;     #replace / to - in tags
	        }

	#print "line=$line\n";   #DEBUG

	my @sarr =split(' ', $line, -1);   #break line to words
	my $sarrsize=@sarr;
	#print "s length=$sarrsize\n";  #DEBUG

	
	for($si=0;$si!=$sarrsize; $si++)	 #iterate on words
		{
		if($sarr[$si] eq "")
			{
			next;    #we have a closing space in every line which generates an additional empty sarr item, skip it
			}



		#print "sarr[$si]=$sarr[$si]\n";  #DEBUG!
		$wordh = $sarr[$si];
		#print "wordh=$wordh\n";  #DEBUG
		#no if(((substr($wordh, 0,1)  ne "^")) || (substr($wordh, -1) ne "\$"))  #if there is no analyis then print without change
		#	{
		#	#print "word=$word ";   #DEBUG
		#	print "$wordh";
		#	next;
		#	}

        	$wordh =~ s/^\^([^\^])/\^\^\1/g;  #double the (unquoted) ana opening ^-s for first words in the line
        	$wordh =~ s/([^\^])\^/\1\^\^/g;  #double the (unquoted) ana opening ^-s in the line
		$wordh =~ s/([^\$])\$/\1\$\$/g;  #double the (unquoted) ana cloding $-s


	        #no $wordh  =~ s/\-\^/\@-\@^/g;   #mark hyphenated compounds with @-@
		#print "wordh=$wordh\n";   #DEBUG
	       #break word to hyphenated components (if any)
		my @wharr =split('\^\^', $wordh, -1);   #hyphenated compounds to words

		my $wharrsize=@wharr;
		#print "wharrsize=$wharrsize\n"; #DEBUG		

		$wsurfout="";
		$wanaout="";
		$wunknown=0;

		$wsurfout=$wharr[0];  #parts before the ana-s or an unanalysed words
		#$wanaout=$wharr[0];  we do not put extra elements into ana
		if($wharrsize eq 1)
			{
			$wunknown=1;
			}

		
		for($wh=1;$wh!=$wharrsize; $wh++)	 #iterate on ana parts
			{
			$wout=""; 
			$word = $wharr[$wh];

			#print "wh=$wh, wharr[$wh]=$wharr[$wh]\n";  #DEBUG!

			#no if(((substr($word, 0,1)  ne "^")) || (substr($word, -1) ne "\$"))  #if there is no analyis then print without change

			@word1arr =split('\$\$', $word, -1);   #split ana segments into ana part and any leftover garbage like a hyphen
			my $word1arrsize=@word1arr;
			if ($word1arrsize == 1)  #if no $$ in $word
					{
					print "assertion:  wrong format by HFST, opening ana marker without close a marker: $wordh\n";
					exit;
					}

                        $word1=$word1arr[0];
			
			#if( substr($word, -2) ne "\$\$")  #if it is the first (or only) part without a closing sign: not analysed
			#	{
			#	#print "word=$word ";   #DEBUG
			#	print "segment not endig with \$\$:$word\n";
			#	$wout= $wout . "$word";
			#	$wsurfout= $wsurfout . "$word";
			#	$wanaout = "$wanaout" . "$wout";
			#	print "wout=$wout\n";
			#	#no next;
			#	}
			#else            #if we have an analysis
			#{
			#no $word=substr( $word, 1, -1);  #chop leading ^ and closing $
			#no chopped already
			$word1=substr($word, 0, -2) ;

			#print "word1=$word1\n";  #DEBUG!

			my @warr =split('/', $word1, -1);   #break words to ana
			my $warrsize=@warr;
			#print "warr[0]=";              #DEBUG
			if($srf)
				{
				#print "$warr[0]";              #print surface form
				#old: $lout= $lout . "$warr[0]";
				$wsurfout= $wsurfout . "$warr[0]";
				if($wharrsize != 1 && $wh != ($wharrsize-1) )
					{
					$wsurfout= $wsurfout . $word1arr[1];
					}

				}
			#$mainstem="";

			#lookup user dictionary:   
			my $unknown=0;
			my $uidfound=0;

			my $key = $warr[0];

			#print "key=$key\n";  #DEBUG

			my $key1lc=lc(substr ($key,0,1)) . substr ($key,1);
			if(exists $udichash{$key})
				{
				$lexhash{$udichash{$key}} = 1;
				#print "\n warr[0]=$warr[0]";  #DEBUG
				#print " udichash{$key}=$udichash{$key}  \n";  #DEBUG
				$uidfound = 1;
				#print "uid1=$uidfound\n";   #DEBUG

				}
			elsif(exists $udichash{$key1lc})     # if we have it by lowercasing the firts letter  (ALLCAPS are not supported, making to much noise)
				{
				$lexhash{$udichash{$key1lc}} = 1;
				#print "\n warr[0]=$warr[0]";  #DEBUG
				#print " udichash{$key}=$udichash{$key}  \n";  #DEBUG
				$uidfound = 1;
				#print "uid 1lc=$uidfound\n";   #DEBUG
				}
			elsif( $key =~ s/^[0-9]+[a-z]?$//i)
				{
				#print "NUMBER: $warr[0], key=$key, lexhash{$warr[0]}=$lexhash{$warr[0]}\n";
				if($key eq "")
					{
					$lexhash{$warr[0]} = 1;   #
					$uidfound = 1;
					}
				}
		
			#print "uidfound=$uidfound\n";   #DEBUG

			for($wi=1;(!$uidfound) && ($wi < $warrsize); $wi++)	 #iterate on words
				{
	
				#workaround for de to get the stem of the first ana, not tried...
				# if($wi >1 && $lang eq "de" &&  we_already_have_a_lexhash) {next;	}

				$lexform = $warr[$wi];
				#print "wi=$wi, lexform=$lexform\n";   #DEBUG
																														 
				#OLD: if( ($wi == 1) && (substr($lexform,0,1) eq "\*"))
				if( substr($lexform,0,1) eq "\*")
					{
					$wout=$wout . "$warr[$wi-1]"; 
					#print "wout=$wout\n";   #mark unknown DEBUG
					$unknown=1;
					$wunknown=1;
					}
				else
					{
					#print "$lexform ";
					if( ($lang eq "hu") || ($lang eq "fr") ||
						($lang eq "it") ||
						($lang eq "bg")||($lang eq "cs")||($lang eq "da")||($lang eq "el")||($lang eq "ga")||($lang eq "hr")||($lang eq "lv")||($lang eq "lt") ||
						($lang eq "mt")||($lang eq "nl")||($lang eq "pl")||($lang eq "pt")||($lang eq "ro")||($lang eq "sk")||($lang eq "sl")||($lang eq "es") ||
						($lang eq "en"))
						{

						if( $lang eq "it")
							# a#p:c1+c2  -> a+p
							#regolamentare#VER:ind+pres+1+s
							{
							$lexform =~ s/^([^:]+):[^\n]+/$1/;
							$lexform =~ s/#/\+/g;
							}

						elsif ( ($lang eq "bg")||($lang eq "cs")||($lang eq "da")||($lang eq "el")||($lang eq "ga")||($lang eq "hr")||($lang eq "lv")||($lang eq "lt") ||
							($lang eq "mt")||($lang eq "nl")||($lang eq "pl")||($lang eq "pt")||($lang eq "ro")||($lang eq "sk")||($lang eq "sl")||($lang eq "es"))
							{
							# a<p><c1><c2>  -> a+p
							$lexform =~ s/</\+/g;
							$lexform =~ s/>//g;
							}
						elsif( $lang eq "en")
							{
							#international/INTER+nation[N]+AL[N/ADJ]+ADJ/INTER+national[ADJ]+ADJ/INTER+national[N]+N/international[ADJ]+ADJ/international[N]+N$ 
							#->international/international[ADJ]+ADJ/international[N]+N  -> international+ADJ/international+N

							#print "\nlexform=$lexform\n";  DEBUG
		
							my $prefixflag = 0;
							my $suffixflag = 0;
							my @marr =split('\+', $lexform, -1);   #break words to ana
							my $morfsize=@marr;
							my $lexpartcnt=0;
							my $segmcnt = 0;
							for($mi=0; $mi < $morfsize; $mi++ )  
								{
#								print "\nmarr[$mi]=$marr[$mi]\n";  #DEB

								my @lparr =split('\[', $marr[$mi], -1);   #break morf into lex and cat 
								my $lpsize=@lparr;
#								print "\nlpsize=$lpsize\n";  #DEBUG
								#ignore analysis that has more then one literal element (prefix, derivation) usually garbage:
								if($lpsize > 1)  #if contains []
									{
									if(substr ($marr[$mi],0,1 ) ne "["  )  #if there is lexical part but it also contains category info
										{
										#$lexpartcnt++;	
										#$lexhashkey =$lparr[0];
										#$poshashkey = "";   #pos may start only behind some lex
										$lexform=$lparr[0] ; 
										$segmcnt++;
										}
									#else  : does not happen: +[]
									}
								else          # = 1:  an element without  []
									{
									if ($segmcnt == 0)   #if not before lex, wthout [] then it is a prefix (otherwise suffix) 
										{
										$prefixflag=1;
										}
									else     #if already have lex, then the first of such elements
										{
										if($suffixflag == 0)
											{
											$lexform = $lexform . "+" .  $marr[$mi];
											$suffixflag = 1; 
											}
										#print ("marr[$mi]($marr[$mi], suffflag=$suffixflag\n");
										}
									}						
										
								}
							
							 #print "lexform2=$lexform, segmct=$segmcnt, prefixflag=$prefixflag\n";

							if($segmcnt != 1 || ($prefixflag != 0))
								{
								next;    #we sipmpl ignore everthing if have prefix or compound  (they are garbage)
								}
												}
						#else:  hu, fr: nothing to do

						my @marr =split('\+', $lexform, -1);   #break words to ana


						my $morfsize=@marr;
						if($stem)
					
							{
							#print "$marr[0]";    #print lexical form  
							$lexhashkey = $marr[0];
							$lexhash{$lexhashkey} =  1;     #build a hash from elements to avoid duplicates
							}
						elsif($stem && $pos == 1)
							{
							#print "$marr[0]";    #print lexical form  
							#print "<$marr[1]>";    #print lexical form  
							$lexhashkey= $marr[0] . "<". $marr[1] . ">"; 
							$lexhash{$lexhashkey} =  1;
							}
						}


					if( ($lang eq "et" ) ||  ($lang eq "sv" )  ||  ($lang eq "fi" )  ||  ($lang eq "de" ))
					#orig:
					#processed:
						{
						#saablexform1=saama+V+Pers+Prs+Ind+Sg3+Aff
						#iseloomulexform1=ise+A#loom+N+Sg+Gen

				
						if( $lang eq "et" )
							{ 
							# a+p1#b+p2+c1+c2 -> a+p1+b+p2
							my @marr =split('#', $lexform, -1);   #breaklexform into stems
							my $msize=@marr;
							my $lexform1="";
							for($mi=0; $mi < $msize; $mi++ )
								{
								$marr[$mi] =~ s/^([^\+]+\+[^\+]+)[^\n]*/$1/;
								if($lexform1 ne "")
									{
									$lexform1  = $lexform1 . "+";
									}
								$lexform1  = $lexform1  . @marr[$mi];
								}
							$lexform=$lexform1;
							}
						if( $lang eq "sv" )
							{
							# a<p1>b<p2><c1><c2>   -> a+p1+b+p2
							my @marr =split('><', $lexform, -1);   #break words to stem and suffixes
							$lexform = @marr[0];    #assume that a non-last stem part never get suffix /usually they just are called "prefix"/
							$lexform=~ s/</\+/g;
							$lexform =~ s/>/\+/g;   
							}

						if( $lang eq "fi" )
							{
							# a#b+p+c1+c2  ->  a++b+p
							$lexform =~  s/(\+[^\+]+)[^\n]+/$1/;
								$lexform =~  s/#/\+\+/g;
							}

						if( $lang eq "de")
							{
							#a<p1>b<p2><c1><c2> ->a+p1+b+p2
							$lexform =~ s/<\+([^>]+)>/<\1>/g;              #replace + to ! in tags
							$lexform =~ s/<CAP>//g;              #delete <CAP>  it show that word in text was capitalized (not supported by other systems)
							$lexform =~ s/<CARD>//g;              #delete <CARD>  from ein<CARD><NN> 
							$lexform =~ s/<[^<>]+><SUFF>/<SUFF>/g;   #delete pos before SUFF:  <V><SUFF>   -> <SUFF>
							$lexform =~ s/>$//;

							my @marr =split('><', $lexform, -1);   #break words to ana
							my $morfsize=@marr;
							#for($mi=0; $mi < $morfsize; $mi++ )             #DEBUG
							#	{print "\nmarr[$mi]=$marr[$mi]"; }      #DEBUG
					
							$marr[0] =~ s/[<>]/\+/g;   
													$lexform=$marr[0];
							}



						#print "\nlexform==$lexform\n";    #DEBUG

						my @carr =split('\+', $lexform, -1);   #break words to stem and suffixes
						my $compsize=@carr;
						$lexhashkey="";
						for($ci=0; $ci < $compsize; $ci++ )
							{
							#print "ci=$ci carr[$ci]=$carr[$ci] --- ci=($ci+1) carr[$ci+1]=$carr[$ci+1]  \n";    #DEBUG
							#we have no pos info for the elemenets:
							if( ($lang eq "de") && (($carr[$ci+1] eq "SUFF") || ($carr[$ci+1] eq "PREF")))
								{
								$ci++;
								next;
								}
							if($stem)
								{
								if($lexhashkey ne "")
									{
									$lexhashkey = $lexhashkey . "+" ;
									}
								$lexhashkey=$lexhashkey . $carr[$ci];      
								$ci++;
								#print "$marr[0]";    #print lexical form  
								#$lexhash{$lexhashkey} =  1;     #build a hash from elements to avoid duplicates
								}
							elsif($stem && $pos)
								{ 
								if($lexhashkey ne "")
									{
									$lexhashkey = $lexhashkey . "+" ;
									}
								$lexhashkey = $lexhashkey . $carr[$ci];
								$ci++; 
								$lexhashkey = $lexhashkey ."<". $carr[$ci] . ">"; 
								}
							#print("lexhashkey===$lexhashkey\n");   #DEBUG
							}
						if($lexhashkey eq "")
							{
							next;  #if we skipped all ci in "de" because of SUFF/PREF
							}
						$lexhash{$lexhashkey} =  1;     #build a hash from elements to avoid duplicates

						if($part eq 1)
								{
							for($ci=0; $ci < $compsize; $ci++ )
								{
								$lexhashkey="";
								#print "ci=$ci carr[$ci]=$carr[$ci]\n";
								#we have no pos info for the elemenets:
								if( ($lang eq "de") && (($carr[$ci+1] eq "SUFF") || ($carr[$ci+1] eq "PREF")))
									{
									$ci++;
									next;
									}
								if($stem)

									{
									$lexhashkey=$lexhashkey . $carr[$ci];      
									#print "$marr[0]";    #print lexical form  
									#$lexhash{$lexhashkey} =  1;     #build a hash from elements to avoid duplicates
									$ci++;  #to be checked!
									}
								elsif( $stem && $pos )
									{ 
									$lexhashkey = $lexhashkey . $carr[$ci];
									$ci++; 
									$lexhashkey = $lexhashkey ."<". $carr[$ci] . ">"; 
									}
								#print("lexhashkey===$lexhashkey\n");
								if(! exists $lexhash{$lexhashkey})
									{
									$lexhashpart{$lexhashkey} =  1;     #build a hash from elements to avoid duplicates
									}
								}
							}
						}
					}
				}
			@lexarr = keys %lexhash;
			$lfsize=@lexarr;
			#print "lfsize=$lfsize\n"; #DEBUG
			if(!$unknown && $lfsize == 0)    #only against stupid english segmentations, where the only result is an unusable "compound"  eg: ^eco/ec[N]+O[N/N]+N$
				{

				if($srf)
					{
						#print ("/* ");
					#$wout=$wout . "/";
					$wout=$wout . $warr[0];
					$wunknown=1;   #use the surface instead  
					my $whmax=$wharrsize-1;
					if(($wharrsize == 1) && ($wh == $whmax ))
						{
						#print "\nwharssize=$wharrsize, wh=$wh\n";   #DEBUG
						$wout=$wout . " ";
						}
					}
				else
					{
					 #$wout=$wout . "**";    #mark unknown for DEVELOPMENT/DEBUG
					$wout=$wout . "$warr[0] ";
					}
				}
			else
				{
				for ($li=0;$li!=$lfsize;$li++)
					{
					#if($srf)
					#	{
						# OLD$wout=$wout . "/";
					#	}
					#print "lexarr[$li]=$lexarr[$li]\n";    #DEBUG
					if($first && $li!=0)
						{
						next;
						}
					#restore ALLCAPS: we want original form 'CAR' instead of 'car'
					#check for allcaps:
					my $allcaps=1;
					my  $i;
					if(length ($lexarr[$li]) != length ($warr[0]))
						{
						$allcaps=0;
						}
					for( $i = 0 ; $i < length ($lexarr[$li]); $i++)
						{
						my $surfch=substr($warr[0], $i,1);
						my $lexch=substr($lexarr[$li], $i,1);
						#print ("surfch=$surfch, lexch=$lexch\n");

						if( ($surfch ne $lexch) && (lc($surfch) eq $lexch) && ($allcaps==1)  )
							{
							$allcaps=1;	
							}
						else
							{
							#print "i=$i, $lexarr[$li], $warr[0], substr($lexarr[$li],$i,1), substr($warr[0], $i,1), allcaps=$allcaps, \n";
							$allcaps=0;
							}
						}
					#print " allcaps=$allcaps\n";
					if( $allcaps )
						{
						$lexarr[$li]=$warr[0];    #if allcaps then use surface
						}
					$wout=$wout . $lexarr[$li];
					}
				if($part eq 1)
					{
					@lexarr = keys %lexhashpart;
					$lfsize=@lexarr;
					for ($li=0;$li!=$lfsize;$li++)
						{
						#if($srf)	
						#	{
						#	#print ("/");
						#	$wsurfout=$wsurfout . "/";
						#	}
						if($first && $li!=0)
							{
							next;
							}
						#print "$lexarr[$li]";
						$wout=$wout . $lexarr[$li];
						}
					}
				}

			undef %lexhash;
			undef %lexhashpart;

			if($word1arr[1] ne "")
				{
				$wout=$wout . $word1arr[1];
				#print "extra ana part added to wout=$wout\n";
				}

			$wanaout = $wanaout . $wout;
			}  #hyphenated parts

		#last part might contain some ignored chars:
		
		$wsurfout = $wsurfout  .  $word1arr[1];   #where word1arr in rarely cases might contain some leftover like an "."
		#print "wsurfout=$wsurfout,  word1arr[1]=$word1arr[1]\n";   #DEBUG

		$lout= $lout . $wsurfout .  "/" ;   

		#1. if compound, then look the whole input in the user dict
		# might be a compound with partial analysis (typical case: ABBR-*suff)
		#or even a analysis with all elements recognized, but which requests different result for the whole.
		#2. or there was some leftover in $word1arr
		if(($wharrsize > 2 )|| ($word1arr[1] ne "" ))
			{
			my $wsurfoutlc=lc(substr ($wsurfout,0,1)) . substr ($wsurfout,1);
			if(exists $udichash{$wsurfout})
				{
				$wanaout=$udichash{$wsurfout};
				$wunknown=0;
				}
			elsif(exists $udichash{$wsurfoutlc})     # if we have it by lowercasing the firts letter  (ALLCAPS are not supported, making to much noise)
				{
				$wanaout=$udichash{$wsurfoutlc};
				$wunknown=0;
				}
			#print "wharrsize=$wharrsize, unknown=$wunknown, udichash{$wsurfout}=$udichash{$wsurfout}\n";   #DEBUG
			}

		if($wunknown)
			{
			$lout= $lout .  "*" ;
			}
		$lout= $lout . $wanaout . " ";
	
        	}  #words
	#print("\n");
	$lout =~ s/ +/ /;
	$lout =~ s/ +$//;
	$lout=$lout . "\n";
	print $lout;
	$lout="";
	}



# hunmorph (hu)
#lexform: stem "+" pos ("+" cat)*

# apertium (hr, cs, bg, da, nl, el, ga, lv, lt, mt, pl, pt, ro, sk, sl, es)
#lexform: stem  ("<" pos ">") ("<" cat ">")*

# giellatekno (et)
#lexform: stem pos (" " cat)* (# pos (" " cat)* ))* 

# giellatekno (fi)
#lexform: stem (# stem )* " " pos (" " cat)*

# hfst (en)
#lexform: (PREFIX "+" )? stem "[" pos "]" "+" (DERIV "[" pos "]" )?  "+" pos ("+" cat)*

# hfst (fr)
#lexform: stem "+" pos ("+" cat)*

# hfst (it): 
#lexform: stem "#" pos ":" ("+" cat)*

# hfst (sv):
#lexform: stem  ("<" pos ">") ("<" cat ">")*

# hfst (de):
#lexform:  (  stem "<" "+" pos ">" ("<" cat ">")*  )   |   ( (stem "<" pos ">") + ("<" cat ">")* ("<" "+" pos ">") ("<" cat ">")* )) 
 