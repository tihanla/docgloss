#filters those lines of "stem TAB surf" type files
#in which surf is ALLCAP or Firstcap if there is a lowercased version with the sane stem

use strict;
use utf8;  #not necsseary
#prints out the columns of table in the specified order

my $key;
my $index;
my $idkey;
my $index;
my %term1hash;
my %termshash;
my %casehash;
my $casetype;

my $insize;
my @inarray;
my $in;

my $surf;
my $surf1;
my $surfrest;
my $first_upper; 
my $first_lower; 
my $rest_one_upper;
my $rest_all_upper;
my $rest_one_lower;
my $rest_all_lower;
my $casetype;  
my $ix;
my $ik;

#0=miXed, 123
#1=lower
#2=UPPER
#3=Firstupper


my $argc=@ARGV;
#print "argc=$argc\n";

if ( $argc ne 2)
	{
	print  "Usage:\n";
	print  "perl filter_upperterm.pl input output \n";
	#perl %script_path%\getidlangline.pl  %idcol%  %work_path%\found-src.txt  %langcol%  %src% %trg% "%dicinput_path%\%dicinput%" "%work_path%\target.tsv"
	exit ;
	}

open(my $infile, "<:encoding(utf8)", $ARGV[0] ) || die "can't open $ARGV[0]";
open(my $outfile, ">:encoding(utf8)", $ARGV[1] ) || die "can't open $ARGV[1]";

my $count =1;

#line:  stem TAB surf
while ($in = <$infile>)
	{
	#print  $in;   DEBUG
	#next;
	chomp($in);
	@inarray = split ('\t', $in, -1);
	$insize=@inarray;

	#print "insize=$insize\n";
	#small hack to supprot merge: if the first column is TERM and it is to be moved than rename it to "EN"
	#print "$langcol= $langcol, inarray[$langcol]=$inarray[$langcol],  trglang=$trglang\n";

	$term1hash{$inarray[0]} = 1;

	#create index, grouped by stems and store its capitalization type
	for ($index=0;;)
		{
		$idkey =$inarray[0] . "-" .  $index;
		#print "idkey=$idkey\n";
		if(exists  $termshash{$idkey})
			{
			$index++;
			}
		else
			{
			$surf=$inarray[1];
			$surf1=$surf;
			$surf1 =~ s/^(.)[^\n]+/$1/;
			$surfrest=$surf;
			$surfrest =~ s/^.([^\n]+)/$1/;
			$first_upper=0;
			$first_lower=0;
			$rest_all_upper=0;
			$rest_all_lower=0;

			if ($surf1 =~ /\p{Uppercase}/) {$first_upper=1};
			if ($surf1 =~ /\p{Lowercase}/) {$first_lower=1};
			if ($surfrest =~ /^[ \p{Uppercase}]+$/) {$rest_all_upper=1}
			if ($surfrest =~ /^[ \p{Lowercase}]+$/) {$rest_all_lower=1}

			#print (">>>$inarray[0], $inarray[1]\n");  #DEBUG
			#print ("first_upper=$first_upper\n");
			#print ("first_lower=$first_lower\n");
			#print ("rest_all_upper=$rest_all_upper\n");
			#print ("rest_all_lower=$rest_all_lower\n");



			#print ("$idkey, $surf\n");
			$casetype=0;  #mixed
			if($first_lower && $rest_all_lower )
				{
				$casetype=1;   #lower
				}
			if($first_upper && $rest_all_lower)
				{
				$casetype=2;  #Firstcap
				}
			if($first_upper && $rest_all_upper)
				{
				$casetype=3;     #UPPER
				}
			#print ("casetype=$casetype\n");                            #DEBUG

			$termshash{$idkey}=  $inarray[1];
			$casehash{$idkey} = $casetype;
			#print "casehash{$idkey}=$casehash{$idkey}\n";	#DEBUG

			#$used_src_hash{$inarray[$termcol]}=0;
			last;
			}
		}
	#print src terms
	#OLD print $outfile  "$in\n";
	                                                                                                                                      
	$count++;
	}	



        #iterate on stem groups, find Firstcap and ALLCAP surface forms, and check for lowercased alternatives
	while( my ($key, $val) = each %term1hash)
		{
	
		#itearte on surface forms belonging to this stem
		#these tell if any of the surface form is lowercase, upprcase, firstcap, or mixed
		#if so, they get the index value of the surface (starting with 1)
		my $all_lower=0;
		my $firstcap=0;
		my $all_upper=0;
		my $mixed=0;
			

		for ($index=0;;)
			{
			$idkey = $key . "-" .  $index;
			#print "idkey=$idkey\n";

			#lookup last word in STEM
			if(exists $termshash{$idkey} )
				{

				#if the stem is does not contain uppercased chaharcter, write all forms
				#if the surfase is "Abc" and there is "abc" then skip
				#if the surcase is "ABC" and  there is either "Abc" "aBC" or "abc" then skip
				$surf=$termshash{$idkey};
				$surf1=$surf;
				$surf1 =~ s/^(.)[^\n]+/$1/;
				$surfrest=$surf;
				$surfrest =~ s/^.([^\n]+)/$1/;

				$first_upper=0;
				$first_lower=0;
				$rest_all_upper=0;
				$rest_all_lower=0;
				
				
				if ($surf1 =~ /\p{Uppercase}/) {$first_upper=1};
				if ($surf1 =~ /\p{Lowercase}/) {$first_lower=1};
				if ($surfrest =~ /^[ \p{Uppercase}]+$/) {$rest_all_upper=1}
				if ($surfrest =~ /^[ \p{Lowercase}]+$/) {$rest_all_lower=1}

				#print ("$idkey, $surf\n");  #DEBUG
				#print ("first_upper=$first_upper\n");
				#print ("first_lower=$first_lower\n");
				#print ("rest_all_upper=$rest_all_upper\n");
				#print ("rest_all_lower=$rest_all_lower\n");

				$casetype=0;  #mixed
				if($first_lower && $rest_all_lower )
					{
					$casetype=1;   #lower
					}
				if($first_upper && $rest_all_lower)
					{
					$casetype=2;  #Firstcap
					}
				if($first_upper && $rest_all_upper)
					{
					$casetype=3;     #UPPER
					}
				#print ("casetype2=$casetype\n");  #DEBUG
				my $skip=0;
				if($casetype == 2 || $casetype == 3 )  #either Firstcap or ALLCAP , we might have to skip
					{
					for ($ix=0;;)
						{
						$ik = $key . "-" .  $ix;
						if(exists $termshash{$ik} )
							{
							#print "ik=$ik, casehash{$ik}=$casehash{$ik}\n";  #DEBUG
							if( ($casetype == 2)  && ($casehash{$ik} == 1 ))  #Apple->apple
								{
								$skip=1; 
								}
							if( ($casetype == 3)  && (($casehash{$ik} == 1 ) || ($casehash{$ik} == 2 ))) #APPLE->Apple or apple
								{
								$skip=1; 
								}
							$ix++;
							}
						else
							{
							last;
							}	
						}
					}

				#print "skip=$skip\n";  #DEBUG
				if(!$skip)
					{
					print ($outfile "$key\t$termshash{$idkey}\n");
					}	
				$index++;
				}
			else
				{
				last;
				}
			}
		}	

close $infile;
close $outfile;

