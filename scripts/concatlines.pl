use strict;

# A program to concatenate two files.
# For files FILE1 & FILE2, the output is
# FILE1 <tab> FILE2
# on each line.

die "Usage: $0 file1 file2\nwhere file1 and file2 are files with equal
number of lines.\n" if @ARGV < 2;

my $tab = "\t";

my $line1;
my $line2;

open(FILE1, $ARGV[0]);
open(FILE2, $ARGV[1]);

while (1) {
    $line1 = <FILE1>;
    $line2 = <FILE2>;
	
    chomp($line1);
    chomp($line2);
    
   if(($line1 eq "") && ($line2 eq ""))
	{
	last;
	}
   elsif($line1 eq "") 
	{
	print STDOUT $tab . $line2 . "\n";
	}
   elsif($line2 eq "") 
	{
	print STDOUT $line1 . $tab . "\n";
	}
    else
	{
	print STDOUT $line1 . $tab . $line2 . "\n";
	}
	
	
    #$line =~ s/\n//;
    #print STDOUT $line . $tab . <FILE2>
}
