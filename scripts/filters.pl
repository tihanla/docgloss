use strict;

my $argc=@ARGV;
my $outfile;
my $outfile2;
my $infile;
my $reliability_pos;
my $evaluation_pos;
my $pifs_pos;
my $prim_pos;
my $language;
my $reliabilitylang_pos;

my $in;
use Getopt::Long;
                                                                    
my $result = GetOptions (
"f_prim=s" =>\$prim_pos,
"f_pifs=s" =>\$pifs_pos,
"f_evaluation=s" =>\$evaluation_pos,
"f_reliability=s" =>\$reliability_pos,
"f_reliabilitylang=s" =>\$reliabilitylang_pos,
"f_language=s" =>\$language,
);  



my $reliability_data1 = "Not Verified";
my $reliability_data2 = "Reliability not verified";
my $reliability_data3 = "Downgraded prior to deletion"; 

my $evaluation_data1 = "Deprecated";
my $evaluation_data2 = "Obsolete";
#Reliability and Evaluation is not used in  version 2014.10.09
my $pifs_data = "Y";
my $prim_data = "dummy";  #not filters if not specified

if ($argc <= 3 )
	{print "argc=$argc\n";                                  
	print  "Usage:\n";
	print  "perl filters.pl input output filtered [-f_prim=N] [-f_pifs=N] [-f_evaluation=N] [-f_reliability=N] [-f_reliabilitylang=N] [-f_language=ID]\n";
	print  "more info:  laszlo.tihanyi\@ext.ec.europa.eu\n";
	exit 1;
	}                  
	                                         


open($infile, "<", $ARGV[0] ) || die "can't open $ARGV[0]";
#binmode($infile, ":encoding(UTF-8)");
open($outfile, ">", $ARGV[1] ) || die "can't open $ARGV[1]";
#binmode($outfile, ":raw:encoding(UTF-8)");
open($outfile2, ">", $ARGV[2] ) || die "can't open $ARGV[2]";
#binmode($outfile2, ":raw:encoding(UTF-8)");

#print "reliability_pos=$reliability_pos\n";
#print "evaluation_pos=$evaluation_pos\n";

my $counter = 1;
while ($in = <$infile>)
	{
	chomp($in);
	#print "counter=$counter\n";

	if($counter == 1)
		{
		#print "src: langData=$langData\n";
		print $outfile "$in\n";
		print $outfile2 "$in\n";
		$counter++;
		next;
		}

	my @inarray = split ('\t', $in, -1);         
	
	#"Not Verified";
	if(($inarray[$reliability_pos-1] eq $reliability_data1) && ($inarray[$reliabilitylang_pos-1] eq $language))
		{
		print $outfile2 "1 $in\n";
		}		
	#"Reliability not verified" (CdT)
	elsif(($inarray[$reliability_pos-1] eq $reliability_data2)&& ($inarray[$reliabilitylang_pos-1] eq $language))
		{
		print $outfile2 "2 $in\n";
		}	
	#"Downgraded prior to deletion"	
	elsif($inarray[$reliability_pos-1] eq $reliability_data3) 
		{
		print $outfile2 "3 $in\n";
		}		
	#"Deprecated";	
	elsif($inarray[$evaluation_pos-1] eq $evaluation_data1)
		{
		print $outfile2 "4 $in\n";
		}		
	#"Obsolete"
	elsif($inarray[$evaluation_pos-1] eq $evaluation_data2)
		{
		print $outfile2 "5 $in\n";
		}		
	#"PIFS=Y"
	elsif($inarray[$pifs_pos-1] eq $pifs_data)
		{
		print $outfile2 "6 $in\n";
		}		
	elsif($inarray[$prim_pos-1] eq $prim_data)
		{
		print $outfile2 "7 $in\n";
		}		
	else
		{
		print $outfile "$in\n";
		}	
	#dbg: print "pifpos=$pifpos, inarray[$pifpos-1]=$inarray[$pifpos-1],  pifdata=$pifdata\n";

	$counter++;
	}


