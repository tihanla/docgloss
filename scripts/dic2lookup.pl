use strict;

my @arr;
my @arrlookup;
my $arrsize;
my $arrlookupsize;
my $stempos;
my $langpos;
my $termpos;
my $lookuppos;
my $idpos;
my $lookuppos;  
my $i;
my $li;
my $lang;

use Getopt::Long;
my $result = GetOptions (
"lang=s" =>\$lang,
"idpos=i" =>\$idpos,
"langpos=i" =>\$langpos,
"termpos=i" =>\$termpos,
"lookuppos=i" =>\$lookuppos,
);  # flag

my $argc=@ARGV;

if ( ($argc ne 0) || ($lang eq "" ) )
	{
	if($lang eq "" ){print "lang is not specified!\n";}
	print  "Usage:\n";
	print  "perl dic2lookup.pl -lang=LANG [-idpos] [-langpos] [-termpos] [-lookuppos]\n";
	print  "reads standard input and writes to standard output\n";
	print "prints aout same table put with content only: id, lang, term and lookup\n";
	print "lookup columns is moved to the last column\n",
	print  "more info:  laszlo.tihanyi\@ext.ec.europa.eu\n";

	exit;
	}                                                                                 

$langpos--;
$idpos--;
$termpos--;
$lookuppos--;
my $cnt=0;

while (<>)
	{
	#array from lines
	chomp();   #chomp new line

	@arr =split('\t', $_, -1);   #break line to words
	$arrsize=@arr;
	#print "arrsize=$arrsize\n";  #DEBUG

	#print "lang=$lang, [-idpos=$idpos] [-langpos=$langpos] [-termpos=$termpos] [-lookuppos=$lookuppos]\n";
	#print "arr[1]=$arr[1]\n";

	

	if ( ($arr[$langpos] eq $lang ) && ($arr[$lookuppos] ne ""))
		{
		#fix  BADLY USED COMMA!!!
		$arr[$lookuppos] =~ s/[ ]*[,;][ ]*/;/g;
		@arrlookup =split(';', $arr[$lookuppos], -1);   #break lookup into phrases
		$arrlookupsize=@arrlookup;

		for($li=0; $li!=$arrlookupsize; $li++)
			{
			#print "$lang\n";
			for($i=0;$i!=$arrsize; $i++)	 #iterate on words
				{
				if( ($i== $idpos) || ($i== $langpos) || ($i== $termpos))
					{
					print "$arr[$i]";
					}
				#other content is emptied, lookup column not printed in the table (shifted to the last)
				if( ($i < ($arrsize-1) ) && ($i != $lookuppos) )
					{
					print "\t";
					$cnt++;
					}
		        	}
			#print("\t$arr[$lookuppos]\n");
			print("\t$arrlookup[$li]\n");
			}
		}
	#print "$langpos,$arr[$langpos], $lang, $_\n";
	}

