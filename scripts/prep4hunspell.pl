use strict;
my $li;
my @larr;
my $larrsize;
my @warr;
my $warrsize;
my $ch1;
my $linecnt=1;

open(my $infile, "<:encoding(utf8)", $ARGV[0] ) || die "can't open input: $ARGV[0]";
open(my $outfile, ">:encoding(utf8)", $ARGV[1] ) || die "can't open output: $ARGV[1]";

while(<$infile>)
	{
	chomp();
	my @larr =split(' ', $_, -1);   #break line to words
	$larrsize=@larr;
	#print "s length=$sarrsize\n";  #DEBUG

	
	for($li=0;$li < $larrsize; $li++)	 #iterate on words
		{
		#double / if not preceeded by a quoting \
		$larr[$li] =~ s/([^\\])\//\1\/\//g;    #double 
		#double / if precedded by a literal blackslash (\\):
		$larr[$li] =~ s/\\\\\//\\\\\/\//g;    #double 

		#print "larr[$li]=$larr[$li]\n";  #DEBUG

		@warr =split('\/\/', $larr[$li], -1);   #break line to words
		$warrsize=@warr;
		if($warrsize != 2)
			{
			print "Error in line $linecnt, number of / in word $larr[$li] is $warrsize\n";
			next;
			}
		$ch1 = substr ($warr[1], 0, 1);

		if( $ch1 eq "\*" )  #if the analysis starts with a *  \x2A
			{
			#$warr[1] = substr ($warr[1], 1);
			#$warr[0] =~ s/^[^\pL\n]+\n/\n/;

			print $outfile $warr[0];
			}
		print $outfile "\n";		
	        }
	print $outfile "\n";	
	$linecnt++;

	}