#destxt.pl adds necessary qutes to special character
#also lowercase long (>15 char) capitalized German words that blocks hfst processing, if the first 15 char of the word is all uppercased
#also reduce XXXXXXXXXX+  in Latvian to 10 X-s

#German 
#Examples to all-cap words crashing HFST:
#s/EU-KONFORMITÄTSERKLÄRUNG/EU-konformitätserklärung/g;
#s/LEISTUNGSBESCHREIBUNG/Leistingsbeschreibung/g;
#s/DURCHFÜHRUNGSVERORDNUNG/Durchfürungsverordnung/g; 
#s/KOMMISSIONSDIENSTSTELLEN/Kommissionsdienststellen/g;
#s/INFORMATIONSGESELLSCHAFT/Informationsgesellschaft/g;
#s/SCHATTENBERICHTERSTATTER/SCHATTENBERICHTERSTATTER/g
#"VETERIN�RBESCHEINIGUNGf�r"
#NATRIUMMETHYL-p-HYDROXYBENZOAT
#SozialpolitikzurGEMEINSCHAFTSSTRATEGIE

#Latvian:
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXX

use strict;
#use utf8;

my $len;
my $lang;
my $lowercased;
my $str;
my $i;
my @strarr;
my $strarrlen;
my $allcap;
my $lci;
my $out;
my $li;
my $checklen=10;   #ad hoc constant to lowarcase even word like:  "VETERIN�RBESCHEINIGUNGf�r" 
my $forcecap;
my $linecount;
my $capcnt;
my $capcntmax=5;

use Getopt::Long;
my $result = GetOptions (
"lang=s" =>\$lang,
);  # flag


open(my $infile, "<:encoding(utf8)", $ARGV[0] ) || die "can't open input: $ARGV[0]";
open(my $outfile, ">:encoding(utf8)", $ARGV[1] ) || die "can't open output: $ARGV[1]";

my $argc=@ARGV;

if ( ($argc ne 2) || ($lang eq "" ) )
	{
	if($lang eq "" ){print "lang is not specified!\n";}
	print  "Usage:\n";
	print  "perl destxt.pl -lang=LANG input output\n";
	print  "-lang: required: iso-639-2 lanuguage code of the EU language\n";                              
	exit;
	}                                                                                 

$linecount=1;

while(<$infile>)
	{
	
	#delete BOM:
	if($linecount == 1)  
		{
	        s/\x{FEFF}//g;
		}
	
	s/([^ ])\//\1 \//g;    # tokenizr detach internal / but not at the beginning or end of words
	s/\/[^ \n]/\/ \1/g;

	#apertium_destxt.cc:
	# if(regcomp(&escape_chars, "[][\\\\/@<>^${}]", REG_EXTENDED))
	s/([\\\/\[\]@<>^\${}])/\\$1/g; 

	#does it work,???
	s/xC2\xA0/ /g;            #V3: normalize hard spaces 
	s/\xE2\x80\x99/'/g;           #normalize apostrophs from typographic (8217) to ASCII (')


	my @larr =split(' ', $_, -1);   #break line to words
	my $larrsize=@larr;
	#print "s length=$sarrsize\n";  #DEBUG

	
	for($li=0;$li < $larrsize-1; $li++)	 #iterate on words
		{
		#print $outfile "larr[$li]=$larr[$li]\n";
		$len=length($larr[$li]);
		#print $outfile "len=$len\n";
		
		if (($lang eq "de") && ($len > $checklen))
			{
			@strarr = split ('', $larr[$li]);
			$strarrlen=@strarr;
			$allcap=1;
			$forcecap=0;
			#print $outfile "strarrlen=$strarrlen\n";
			$capcnt=0;
			for  ($i=0; $i <= $strarrlen ; $i++)
				{
				#print $outfile "strarr[$i]=$strarr[$i]\n";
				#if it contains at least one character that is lowercased, then it is not an all-cap, otherwise lowercase the all-cap 
				#the problem happened in long compounds with UNG and S, so it is not a problem that accented cannot be lowercased...
				$lci=lc($strarr[$i]);
				if(($lci eq "-") || ($lci eq "�") || ($lci eq "_") )
					{
					next;
					}

				if(($lci eq "/") || ($lci eq "\\") )
					{
					$forcecap=1;
					next;
					}

				# the / sign behaves as  lowercaser since it might start a new word that can be bloclking
				if( $lci eq $strarr[$i])  
					{
					$allcap=0;
					#print $outfile "i=$i, lci=$lci, strarr[$i]=$strarr[$i]\n";  #DEBUG
					#print $outfile "allcap=$allcap\n";  #DEBUG
					next;
					}
				else
					{
					$capcnt++;		
					}
				}
			#print $outfile "allcap=$allcap\n";
			if($allcap || $forcecap || $capcnt > $capcntmax)    #if all or at least 5 were capitals
				{
				$out =  substr ($larr[$li],0, 1);
				$out =  $out . lc (substr ($larr[$li], 1));
				#print "lower=$out\n";
				}
			else	
				{
				$out = $larr[$li];
				}
			print $outfile $out;
			#print $outfile "\n---\n";  #DEBUG
			}
		elsif ( $lang eq "lv" )    
			{
			$larr[$li] =~ s/XXXXXXXXXXX+/XXXXXXXXXX/;  #reduce long X-s to 10 X
			print $outfile $larr[$li];
			}
		else
			{
			print $outfile $larr[$li];
			}

		#needed for lines ending with a ".", where HFST loose new line without this workaround:
		#s/\n/ \n/;
		print $outfile " ";   #we need spaces even at line end

		}  #words loop

	print $outfile "\n";   
	$linecount++;
	}  #line loop
