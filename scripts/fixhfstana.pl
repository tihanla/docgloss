use strict;

while(<>)
	{
	#$_ =~ s/\$ +/\$/g;     #chomp closing spaces
        $_ =~ s/[ ]+([\r\n])/\1/g;     #chomp closing spaces

	#fixing a nasty but frequent error in many lexicons: lexical form might contain spaces... 
	$_ =~ s/ (\/\*[^ \$\^]+) \$/\1\$/g;  #eg eg: Polish: ^trzÍsienia /*trzÍsienia $
	$_ =~ s/ \//\//g;                    #or any surf/ana separator / preceeded by a space 
	$_ =~ s/ </</g;                      #or any space before tags
	$_ =~ s/\$†\^/\$\^/g;                # remaining space between two ana elements 

	#only with specific language
	$_ =~ s/<presens particip>//g;       #sv: swedish lemmas comtaining morphological info
	$_ =~ s/<perfekt particip>//g;       #sv: swedish lemmas comtaining morphological info
	$_ =~ s/([^\\]\/ex) /\1/g;           #es: spanish ex
	$_ =~ s/\/teagascs n="vblex"\/>/\/teagascs/g;   #mt: maltese lex error
	$_ =~ s/\/t. i./\/t.i./g;            #lv: latvian t.i. abbrev
	$_ =~ s/([^\\])\/per /\1\/per/g;           #mt: maltese for examples
	$_ =~ s/\/zich inlaten/\/inlaten/g;  #nl: dutch 
	$_ =~ s/compra e venda/compra/g;     #pt: portuguese
	$_ =~ s/([^\\])\/se /\1\/se/g;       #fr: french se 
	$_ =~ s/<\^(ABK|VPRES)>/<\1>/g;             #de:  start symbol used in tags

	$_ =~ s/\\\\/^\\\\\/\\\\\$/g;   	    #\\ left non-analysed
	print "$_" ;

	}

