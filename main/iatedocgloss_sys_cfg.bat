set docgloss_path=%IATE_DOCGLOSS_PATH%
rem set docgloss_path=C:\Pgm\DGTapps\IATEdocgloss_win

set perl_path=%docgloss_path%\..\Perl516\bin
set work_path=%docgloss_path%\tmp
set out_path=%docgloss_path%\pub
set iate_dict=%docgloss_path%\data\20150831_en.txt

set iateconf_path=%docgloss_path%
set script_path=%docgloss_path%\scripts
set hfst_prg_path=%docgloss_path%\bin
set hfst_dic_path=%docgloss_path%\data
set hunspell_prg_path=%docgloss_path%\bin
set hunspell_dict_path=%docgloss_path%\data

rem out_format_gloss: SMT_OUT or TB_OUT
set out_format_gloss=TB_OUT
rem out_format: SDLTB or TBX
set out_format=SDLTB
set convert_tbxid=NO
set convert_zip=NO

set convert_stat=NO
:set convert_stat=YES  todo

set convert_log=YES
set convert_tmpfiles=YES
set use_timestamp=NO

