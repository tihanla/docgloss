if [ ! "$#" == 4  ]; then
	echo Usage: IATEconvert.sh YYYY MM DD input 
	echo input is the input document with full path name 
	exit
fi

year=$1
#set year=2015

month=$2
#set month=06

#set day=01
day=$3

dirdate=$year-$month-$day

#mode=pair
mode=list
#rem if mode==pair
#mode1=-pair
#rem else
#mode1=
#rem endif

infname=$4
fullpath=$infname

    filename="${fullpath##*/}"                      # Strip longest match of */ from start
    dir="${fullpath:0:${#fullpath} - ${#filename}}" # Substring from 0 thru pos of filename
    base="${filename%.[^.]*}"                       # Strip shortest match of . plus at least one non-dot char from end
    ext="${filename:${#base} + 1}"                  # Substring from len of base thru end
    if [[ -z "$base" && -n "$ext" ]]; then          # If we have an extension and no base, it's really the base
        base=".$ext"
        ext=""
    fi

#    echo -e "$fullpath:\n\tdir  = \"$dir\"\n\tbase = \"$base\"\n\text  = \"$ext\""




outfname=iate_$day-$month-$year_$mode

#input_path=C:/PGM/DGTapps/IATEsdl/work/$dirdate_DGT
input_path=$dir
infname=$filename
  
echo input_path=$input_path
echo infname=$infname


#input_path=C:/Pgm/DGTapps/IATEdocgloss/data
#input_path=P:/Terminology_Coordination/IATE_SDL/Development/IATE_extracts_txt/test_init_Dimitris_SQL

#output_path=C:/PGM/DGTapps/IATEsdl/work/$dirdate_DGT
output_path=$5


pdir=C:/PGm/DGTapps/IATEconvert
pdir=dir


convert_zip=YES
convert_stat=YES
convert_log=YES
convert_tmpfiles=NO
convert_tbxid=YES

#TODO TBX_OUT=YES

#test:
$pdir/iate_convert.sh en hu TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
exit

#:TBX
#13 new EU langs: TBX
$pdir/iate_convert.sh en bg TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname 
$pdir/iate_convert.sh en cs TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en et TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en ga TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en hr TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en hu TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en mt TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en sk TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en ro TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en pl TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en lv TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en lt TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en sl TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew.sh $input_path/$infname $output_path $outfname
#10 old EU langs: TBX only primaries
$pdir/iate_convert.sh en da TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en de TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en pt TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en fi TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en el TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en es TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en fr TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en it TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en nl TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
$pdir/iate_convert.sh en sv TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtold.sh $input_path/$infname $output_path $outfname
#Maltese TBX: filtered for the COM:
$pdir/iate_convert.sh en mt TXT TBX $mode $pdir/dcfg-base.sh  $pdir/cfg-dgtnew-mtcom.sh  $input_path/$infname $output_path $outfname


