rem FOR %%c in (%in_path%\*.doc) DO echo %%c
rem %script_path%\word2txt.vbs %in_path%\%in%%in_ext% %work_path%\text.txt 
if "%2"=="" goto terminate

set input=%1
set in_path=%~dp1
set in_path=C:\Pgm\DGTApps\IATEdocgloss\test\ELARG-2014-80031-80034-en-pt

set script_path=C:\Pgm\DGTApps\IATEdocgloss\scripts

:dir_input
for %%f in (%in_path%\*.doc*) do (
echo %%~nf
%script_path%\word2txt.vbs "%in_path%\%%~nf.doc" %in_path%\%%~nf.docs2txt" 
)

copy /b "%in_path%\*.docs2txt" %2

:del "%in_path%\*.docs2txt"

goto end
:terminate
echo Usage:  doc2txt.bat path outfile
echo where the path is a folder containing doc or docx files from which the program generates a text file named outfile

:end
