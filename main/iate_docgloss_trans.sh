set -x

src=$1
trg=$2
work_path=$3
infile=$4
project=$5
reference=$6



####work_path=/ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest
#work_path=/ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/markupx-$src-$trg

###script_path=/ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest
script_path=/ec/dgt/local/exodus/user/tihanla/IATEdocgloss/scripts

moses_path=/ec/dgt/shared/exodus/MosesSuite_installations/MosesSuite_2a3c9fc679_2012-12-07/bin
moses_work_path=/ec/dgt/shared/exodus-archive/build_2014_0003/$src$trg-all
para=/ec/dgt/shared/exodus/environments/training/ec.dgt.moses.infrastructure.translation_prod/bin/para
#info:/ec/dgt/shared/exodus/environments/training/ec.dgt.moses.infrastructure.training_installations/1.11/scripts/evaluate.bash
p4mt_path=/ec/dgt/shared/exodus/environments/training/ec.dgt.moses.helpers_prod/scripts
eval_path=/ec/dgt/shared/exodus/environments/training/MosesSuite_prod/scripts/generic

#preprocess source:
cd $moses_work_path

if [ "$DEBUG" ]; then

#baseline input:
./translate -p   < $infile  > $work_path/$project.$src.prep.txt  2>$work_path/$project.$src.prep.err

#xml-markup for exclusive and inclusive input:
perl $script_path/iatexmarkup.pl $work_path/$project-$src-$trg-1to1.tsv $work_path/$project.$src.prep.txt  $work_path/$project.$src-$trg.markup.txt 


#xml-markup for bottom-baseline:
perl  $script_path/iatexmarkupxxx.pl $work_path/$project.$src-$trg.markup.txt  > $work_path/$project.$src-$trg.markupxxx.txt 
#xml-markup for input with target morphological alternatives with probabilities: (done on win)
perl $script_path/iatexmarkupfrq.pl $work_path/$project-$src-$trg-1to1.tsv  $work_path/$trg-stm-num-lc.tsv  $work_path/$project.$src.prep.txt  $work_path/$project.$src-$trg.markup_frq.txt 

#cutting into pieces with 1000 lines to aviud moses crash (without any error meassage)
perl $script_path/part.pl    1  1000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.0001-1000.txt
perl $script_path/part.pl 1001  2000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.1001-2000.txt
perl $script_path/part.pl 2001  3000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.2001-3000.txt
perl $script_path/part.pl 3001  4000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.3001-4000.txt
perl $script_path/part.pl 4001  5000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.4001-5000.txt
perl $script_path/part.pl 5001  6000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.5001-6000.txt
perl $script_path/part.pl 6001  7000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.6001-7000.txt
perl $script_path/part.pl 7001  8000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.7001-8000.txt
perl $script_path/part.pl 8001  9000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.8001-9000.txt
perl $script_path/part.pl 9001 10000  $work_path/$project.$src-$trg.markup_frq.txt  $work_path/$project.$src-$trg.markup_frq.9001-10000.txt

#preprocess reference:
./translate -p  < $reference > $work_path/$project.$trg.prep.txt  2>$work_path/$project.$trg.prep.err

fi #DEBUG


#baseline:
#nohup $para 14 $moses_path/moses -f moses.ini   <$work_path/$project.$src.prep.txt  > $work_path/$project.$src-$trg.txt 2>$work_path/$project.$src-$trg.moses.log
python $p4mt_path/prep4mteval.py -r $work_path/$project.$trg.prep.txt -s $work_path/$project.$src.prep.txt -t $work_path/$project.$src-$trg.txt --src $src --trg $trg --sys nolookup --docid test
perl $eval_path/mteval-v12.pl -r $work_path/$project.$trg.prep.txt.sgm -s $work_path/$project.$src.prep.txt.sgm -t $work_path/$project.$src-$trg.txt.sgm > $work_path/$project.$src-$trg.scores

#target side morphological alternatives with probabilities, used in exclusive mode, para cannot be used: crash moses in every 300-400 lines
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.0001-1000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.0001-1000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.0001-1000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.1001-2000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.1001-2000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.1001-2000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.2001-3000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.2001-3000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.2001-3000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.3001-4000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.3001-4000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.3001-4000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.4001-5000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.4001-5000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.4001-5000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.5001-6000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.5001-6000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.5001-6000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.6001-7000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.6001-7000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.6001-7000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.7001-8000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.7001-8000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.7001-8000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup_frq.8001-9000.txt  > $work_path/$project.$src-$trg.markup_frq.exl.8001-9000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.8001-9000.moses.log
#nohup $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive <$work_path/$project.$src-$trg.markup_frq.9001-10000.txt  >$work_path/$project.$src-$trg.markup_frq.exl.9001-10000.txt 2>$work_path/$project.$src-$trg.markup_frq.exl.9001-10000.moses.log

#cat $work_path/$project.$src-$trg.markup_frq.exl.*-*.txt  > $work_path/$project.$src-$trg.markup_frq.exl.txt

python $p4mt_path/prep4mteval.py -r $work_path/$project.$trg.prep.txt -s $work_path/$project.$src.prep.txt -t $work_path/$project.$src-$trg.markup_frq.exl.txt --src $src --trg $trg --sys nolookup --docid test
perl $eval_path/mteval-v12.pl -r $work_path/$project.$trg.prep.txt.sgm -s $work_path/$project.$src.prep.txt.sgm -t $work_path/$project.$src-$trg.markup_frq.exl.txt.sgm > $work_path/$project.$src-$trg.markup_frq.exl.scores

#baseline bottom:
#nohup $para 14 $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markupxxx.txt  > $work_path/$project.$src-$trg.markupxxx.exl.txt 2>$work_path/$project.$src-$trg.markupxxx.exl.moses.log
python $p4mt_path/prep4mteval.py -r $work_path/$project.$trg.prep.txt -s $work_path/$project.$src.prep.txt -t $work_path/$project.$src-$trg.markupxxx.exl.txt --src $src --trg $trg --sys nolookup --docid test
perl $eval_path/mteval-v12.pl -r $work_path/$project.$trg.prep.txt.sgm -s $work_path/$project.$src.prep.txt.sgm -t $work_path/$project.$src-$trg.markupxxx.exl.txt.sgm > $work_path/$project.$src-$trg.markupxxx.exl.scores

#exclusive:
#nohup $para 14 $moses_path/moses -f $moses_work_path/moses.ini -xml-input  exclusive  <$work_path/$project.$src-$trg.markup.txt  > $work_path/$project.$src-$trg.markup.exl.txt 2>$work_path/$project.$src-$trg.markup.exl.moses.log
python $p4mt_path/prep4mteval.py -r $work_path/$project.$trg.prep.txt -s $work_path/$project.$src.prep.txt -t $work_path/$project.$src-$trg.markup.exl.txt --src $src --trg $trg --sys nolookup --docid test
perl $eval_path/mteval-v12.pl -r $work_path/$project.$trg.prep.txt.sgm -s $work_path/$project.$src.prep.txt.sgm -t $work_path/$project.$src-$trg.markup.exl.txt.sgm > $work_path/$project.$src-$trg.markup.exl.scores

#inclusive:
#nohup $para 14 $moses_path/moses -f $moses_work_path/moses.ini -xml-input  inclusive  <$work_path/$project.$src-$trg.markup.txt  > $work_path/$project.$src-$trg.markup.inc.txt 2>$work_path/$project.$src-$trg.markup.inc.moses.log
python $p4mt_path/prep4mteval.py -r $work_path/$project.$trg.prep.txt -s $work_path/$project.$src.prep.txt -t $work_path/$project.$src-$trg.markup.inc.txt --src $src --trg $trg --sys nolookup --docid test
perl $eval_path/mteval-v12.pl -r $work_path/$project.$trg.prep.txt.sgm -s $work_path/$project.$src.prep.txt.sgm -t $work_path/$project.$src-$trg.markup.inc.txt.sgm > $work_path/$project.$src-$trg.markup.inc.scores

