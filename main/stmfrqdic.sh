set -x

#input is a frequency list made from Euramis: surf TAB frq
#output is the same list with stems:   surf TAB stem TAB frq
#:input is not lowercased
#input may contain placeholders and character entities



if [ ! "$#" == 4  ]; then
	echo Usage: stmfrqdic.sh lang  path inputpath input
	exit
fi

lang=$1
work_path=$2
input_path=$3
input=$4


#informat values: informat_vocab, inputformat_frq, informat_txt, informat_token, informat_doc
informat=informat_frq

#ok WIN:
#prg_path=C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\exe
#dic_path=C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\data\hfst
#dic_path=C:\Pgm\DGTApps\IATEdocgloss\data
#script_path=C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\scripts
#script2_path=C:\Pgm\DGTApps\IATEdocgloss\scripts
#dict_path_hunspell=C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\data\hunspell

#ok CYGWIN
#prg_path=/cygdrive/c/Pgm/DGTApps/IATEdocgloss/bin
#prg_path_hunspell=/cygdrive/c/DGT/shared/nlp_installations/nlp_tools-2014-10-30/exe
#script_path=/cygdrive/c/Pgm/DGTApps/IATEdocgloss/scripts
#perldir=/cygdrive/c/Pgm/DGTapps/Perl516/bin
#perldir=/usr/bin
#python_path=/cygdrive/c/Python27
#java_path="/cygdrive/c/Program Files (x86)/Java/jre1.8.0_31/bin"
#hfst_dic_path=/cygdrive/c/Pgm/DGTApps/IATEdocgloss/data
#hfst_dic_winpath=C:\\Pgm\\DGTApps\\IATEdocgloss\\data
#dict_path_hunspell=/cygdrive/c/Pgm/DGTApps/IATEdocgloss/data

#ok LINUX:
source sys_cfg.sh


# work_path=C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\test\hfst-hunspell-euramis
# udic_path=C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\test\hhi-euramis-stat\udic



#rem delete placeholders and convert character entities back to normal characters:
$perldir/perl $script_path/frq2normalized.pl $input_path/$input   >  $work_path/$lang1.vcb

#rem separate frequency info column:
$perldir/perl -pe "s/^([^\t]+)\t([^\n]+)/\1/" $work_path/$lang1.vcb > $work_path/$lang.wrd.vcb
$perldir/perl -pe "s/^([^\t]+)\t([^\n]+)/\2/" $work_path/$lang1.vcb > $work_path/$lang.num.vcb


#:HFST***
#:rem prepare text
$perldir/perl $script_path/destxt.pl  -lang=$lang $work_path/$lang.wrd.vcb  $work_path/$lang.dtok



#rem HFST analyse:
$prg_path/hfst-proc --apertium --dictionary-case  $hfst_dic_path/$lang.ana.hfstol $work_path/$lang.dtok > $work_path/$lang.ana


#delete the last space in line
$perldir/perl $script_path/fixhfstana.pl $work_path/$lang.ana  > $work_path/$lang.nosp.ana
#check:
#grep " "  $work_path/$lang.nosp.ana > $work_path/$lang.sp.err

#rem stem:
$perldir/perl $script_path/hfst-stem.pl  -lang=$lang -udic=$hfst_dic_path/hfst-$lang.udic -srf  -first <$work_path/$lang.nosp.ana  >$work_path/$lang-hfst.stm
#replace per (/) -> tab
$perldir/perl $script_path/prep4hsp.pl   "$work_path/$lang-hfst.stm"  "$work_path/$lang-4hsp.stm"  



#Hunspell***
#analyse:
$prg_path_hunspell/analyze_dgt a $dict_path_hunspell/$src.aff $dict_path_hunspell/$src.dic  $work_path/$lang-4hsp.stm  >$work_path/$lang-hsp.ana

#stem:
$perldir/perl $script_path/hunspell-stem.pl  $lang "$work_path/$lang-hsp.ana"  "$work_path/$lang-hsp.stm"

#head -1000 "$work_path/$lang-hsp.ana" > "$work_path/$lang-hsp.ana-1000"  
#head -1000 "$work_path/$lang-hsp.stm" > "$work_path/$lang-hsp.stm-1000"

#merge back frequency info:
$perldir/perl $script_path/adlinesbytab.pl   "$work_path/$lang-hsp.stm" "$work_path/$lang.num.vcb"  "$work_path/$lang-stm-num.tsv"


#create lowercase frequency list
$perldir/perl $script_path/freq2lower.pl  "$work_path/$lang-stm-num.tsv"   "$work_path/$lang-stm-num-lc.tsv"



