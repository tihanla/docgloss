<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="no" />

<xsl:strip-space elements="*"/>

<xsl:variable name="lilid">n</xsl:variable>
<!-- set lilid=y to dispay IATE-ID-s -->

<xsl:template match="martif">
<martif type="TBX-Basic" xml:lang="en-US">
<xsl:text>&#xa;</xsl:text>
<xsl:apply-templates/>
<xsl:text>&#xa;</xsl:text>
</martif>
</xsl:template>

<xsl:template match="martifHeader">
<martifHeader>
<xsl:text>&#xa;</xsl:text>
<xsl:apply-templates/>
</martifHeader>
<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="fileDesc">
<fileDesc>
<xsl:text>&#xa;</xsl:text>
<xsl:apply-templates/>
</fileDesc>
<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="titleStmt">
<titleStmt>
<xsl:apply-templates/>
</titleStmt>
<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="title">
<title>
<xsl:apply-templates/>
</title>
</xsl:template>

<xsl:template match="sourceDesc">
<sourceDesc>
<xsl:apply-templates/>
</sourceDesc>
<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="p">
<p>
<xsl:apply-templates/>
</p>
</xsl:template>

<xsl:template match="text">
<text>
<xsl:text>&#xa;</xsl:text>
<xsl:apply-templates/>
</text>
</xsl:template>

<xsl:template match="body">
<body>
<xsl:text>&#xa;</xsl:text>
<xsl:text>&#xa;</xsl:text>
<xsl:apply-templates/>
</body>
<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="termEntry">
<termEntry>
<xsl:text>&#xa;</xsl:text>
<!--xsl:apply-templates-->
   <!--xsl:with-param name="lilid" select = "y" /-->
<!--/xsl:apply-templates-->
<xsl:apply-templates/>
</termEntry>
<xsl:text>&#xa;</xsl:text>
<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="langSet">
<xsl:text disable-output-escaping="yes"><![CDATA[<]]></xsl:text>
<xsl:text>langSet xml:lang="</xsl:text>
<xsl:value-of select="@xml:lang"/>
<xsl:text disable-output-escaping="yes">"<![CDATA[>]]></xsl:text>
<xsl:text>&#xa;</xsl:text>
<xsl:apply-templates/>
<xsl:text disable-output-escaping="yes"><![CDATA[<]]></xsl:text>
<xsl:text>/langSet</xsl:text>
<xsl:text disable-output-escaping="yes"><![CDATA[>]]></xsl:text>
<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="tig">
<tig>
<xsl:text>&#xa;</xsl:text>
<!--xsl:apply-templates-->
   <!--xsl:with-param name="lilid" select = "n" /-->
<!--/xsl:apply-templates-->
<xsl:apply-templates/>
</tig>
<xsl:text>&#xa;</xsl:text>
</xsl:template>

<xsl:template match="term">
<term>
	<xsl:value-of select="."/>
</term>
<xsl:text>&#xa;</xsl:text>
</xsl:template>



<xsl:template match="descrip">
<xsl:choose>
	<xsl:when test="$lilid='y'">
		<xsl:if test="@type='IATE-ID'">
		<descrip type="IATE-ID">
		     <xsl:apply-templates/>
		</descrip>
		<xsl:text>&#xa;</xsl:text>
		</xsl:if>
	</xsl:when>
	<xsl:otherwise>
		<!--nothing to do -->
	</xsl:otherwise>
</xsl:choose>
</xsl:template>
 

</xsl:stylesheet>
