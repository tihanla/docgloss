#set -x

#acubens:
#docgloss_path=/ec/dgt/local/exodus/user/tihanla/IATEdocglossx
#docgloss_path=/ec/dgt/local/exodus/user/tihanla/IATEdocgloss_unx
#perl_path=/ec/dgt/shared/exodus/local/perl/bin


#cygwin:
#under cygwin it runs only from the current folder:
docgloss_path=.
perl_path=/cygdrive/c/Pgm/DGTapps/Perl516/bin

#ubuntu
#docgloss_path=/mnt/Pgm/DGTapps/IATEdocgloss_unx
#perl_path=/usr/bin

#Conversion Server
#docgloss_path=/ec/dev/app/euramis/home/iatetrid/IATEgloss
#perl_path=/ec/dev/app/euramis/home/iatetrid/perl/ActivePerl-5.18/bin


work_path=$docgloss_path/tmp
#work_path=./tmp

out_path=$docgloss_path/pub
iate_dict=$docgloss_path/data/iate_extract.txt

iateconf_path=$docgloss_path
script_path=$docgloss_path/scripts

hfst_prg_path=$docgloss_path/bin
hfst_dic_path=$docgloss_path/data
hunspell_prg_path=$docgloss_path/bin
hunspell_dict_path=$docgloss_path/data
bin_path=$docgloss_path/bin

#out_format_gloss: SMT_OUT or TB_OUT
out_format_gloss=TB_OUT
#out_format: SDLTB or TBX
out_format=TBX
convert_tbxid=NO
convert_zip=NO

convert_stat=NO
convert_log=NO
convert_tmpfiles=NO
use_prefix=YES

