call IATEdocgloss.bat en de C:\PGM\DGTApps\IATEdocgloss\test\GROW-2015-80018-00-00-EN-ORI-00-en-de\GROW-2015-80018-00-00-EN-ORI-00.DOCX

:call IATEdocgloss.bat hu en C:\PGM\DGTApps\IATEdocgloss\test\GROW-2015-00728-00-00-HU-PRT\GROW-2015-00728-00-00-HU-PRT-00.DOCX

:call IATEdocgloss.bat en mt C:\Pgm\DGTApps\IATEdocgloss\test\JRC-2015-00178-00-00-EN-ORI\JRC-2015-00178-00-00-EN-ORI-00.doc
:call IATEdocgloss.bat en mt C:\Pgm\DGTApps\IATEdocgloss\test\JRC-2015-00178-00-00-EN-REF\JRC-2015-00178-00-00-EN-REF-00.docx

:call IATEdocgloss.bat en pl C:\PGM\DGTApps\IATEdocgloss\test\SANTE-2014-80140-en-pl\SANTE-2015-80140-00-00_01-EN-ORI-00.txt
:call IATEdocgloss.bat en pl C:\PGM\DGTApps\IATEdocgloss\test\SANTE-2014-97160-en-pl\SANTE-2014-97160-00-00-EN-ORI-00.DOCX


:call IATEdocgloss.bat en mt G:\Pgm\DGTApps\IATEdocgloss\test\JRC-2015-00178-00-00-EN-REF\JRC-2015-00178-00-00-EN-REF-00.docx
:call IATEdocgloss.bat en da C:\PGM\DGTApps\IATEdocgloss\test\GROW-2015-80020-en-da\GROW-2015-80020.txt
:call IATEdocgloss.bat en lt C:\Pgm\DGTApps\IATEdocgloss\test\TAXUD-2015\TAXUD-2015-00390-01-00-EN-ORI-00.docx


:call IATEdocgloss.bat en sv  C:\Pgm\DGTApps\IATEdocgloss\test\ENER-2015-80021-00-00_01-00-SV\ENER-2015-80021-00-00_01-00.txt

:create document glossaries for the 10k test set of moses  7th generation, out_format_gloss must be set to SMT_OUT in IATEdocgloss.bat manually
:13 new langs list full
:call IATEdocgloss.bat en bg C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en cs C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en et C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en ga C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en hr C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:exit
:call IATEdocgloss.bat en mt C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en sk C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en ro C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en pl C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en lv C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en lt C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en sl C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:TODO 10 old EU langs list prim
:call IATEdocgloss.bat en da C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en el C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en es C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en fi C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en it C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en nl C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en pt C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 
:call IATEdocgloss.bat en sv C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k.txt 

:Maria Machado
:call IATEdocgloss.bat en pt C:\Pgm\DGTApps\IATEdocgloss\test\ELARG-2014-80031-80034-en-pt\ELARG-2014-80031-80034.txt


:CdT test for comaprison to Paula:
:PRIMARY
:perl -pe "s/set primary_no=./set primary_no=N/" C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.bat
:del C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.bat
:mv C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.tmp C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.bat
:todo sdltb:
:OK call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\prim\ECHA_2012_00730000_EN_ORI_prim.doc  -admin
:OK call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\prim\ECHA_2012_00730000_EN_ORI_prim.doc  -admin
:OK call IATEdocgloss.bat en et C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\prim\ECHA_2012_00730000_EN_ORI_prim.doc  -admin
:OK call IATEdocgloss.bat en mt C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\prim\ECHA_2012_00730000_EN_ORI_prim.doc  -admin
:OK call IATEdocgloss.bat en pl C:\Pgm\DGTApps\IATEdocgloss\test\CDT\EBA_2014_00040000\prim\EBA_2014_00040000_EN_ORI_prim.docx -admin
:OK call IATEdocgloss.bat en it C:\Pgm\DGTApps\IATEdocgloss\test\CDT\EBA_2014_00040000\prim\EBA_2014_00040000_EN_ORI_prim.docx -admin
:OK call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\CDT\OHMI_2014_01500000\prim\OHMI_2014_01500000_EN_ORI_prim.docx -admin
:OK call IATEdocgloss.bat en es C:\Pgm\DGTApps\IATEdocgloss\test\CDT\OHMI_2014_01500000\prim\OHMI_2014_01500000_EN_ORI_prim.docx -admin
:FULL
:regenerated (2015.04..03)   with two fixes: no Fistcap/ALLCAP  if not necessary: use new Hunspell, debeleoped for test10k.txt (old folder not kept)
:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\full\ECHA_2012_00730000_EN_ORI_full.doc  -admin

:perl -pe "s/set primary_no=./set primary_no=Y/" C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.bat   C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.tmp
:del C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.bat
:mv C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.tmp C:\Pgm\DGTApps\IATEconvert\dgt-txt_base2form4CDT_cfg.bat
:runing without sdltb
:call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\full\ECHA_2012_00730000_EN_ORI_full.doc  -admin
:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\full\ECHA_2012_00730000_EN_ORI_full.doc  -admin
:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\full-allcap\ECHA_2012_00730000_EN_ORI_full.doc  -admin
:OK call IATEdocgloss.bat en et C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\full\ECHA_2012_00730000_EN_ORI_full.doc  -admin
:OK call IATEdocgloss.bat en mt C:\Pgm\DGTApps\IATEdocgloss\test\CDT\ECHA_2012_00730000\full\ECHA_2012_00730000_EN_ORI_full.doc  -admin
:OK call IATEdocgloss.bat en pl C:\Pgm\DGTApps\IATEdocgloss\test\CDT\EBA_2014_00040000\full\EBA_2014_00040000_EN_ORI_full.docx -admin
:OK call IATEdocgloss.bat en it C:\Pgm\DGTApps\IATEdocgloss\test\CDT\EBA_2014_00040000\full\EBA_2014_00040000_EN_ORI_full.docx -admin
:OK call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\CDT\OHMI_2014_01500000\full\OHMI_2014_01500000_EN_ORI_full.docx -admin
:OK call IATEdocgloss.bat en es C:\Pgm\DGTApps\IATEdocgloss\test\CDT\OHMI_2014_01500000\full\OHMI_2014_01500000_EN_ORI_full.docx -admin


:call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\mttest1k\test1k.en.txt
:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\mttest1k\test1k.en.txt

:call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\ECHA_2012_00730000\prim\ECHA_2012_00730000_EN_ORI_prim.doc  -admin
:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\ECHA_2012_00730000\prim\ECHA_2012_00730000_EN_ORI_prim.doc  -admin
:call IATEdocgloss.bat en et C:\Pgm\DGTApps\IATEdocgloss\test\ECHA_2012_00730000\prim\ECHA_2012_00730000_EN_ORI_prim.doc  -admin
:call IATEdocgloss.bat en mt C:\Pgm\DGTApps\IATEdocgloss\test\ECHA_2012_00730000\prim\ECHA_2012_00730000_EN_ORI_prim.doc  -admin
:call IATEdocgloss.bat en pl C:\Pgm\DGTApps\IATEdocgloss\test\EBA_2014_00040000\prim\EBA_2014_00040000_EN_ORI_prim.docx -admin
:call IATEdocgloss.bat en it C:\Pgm\DGTApps\IATEdocgloss\test\EBA_2014_00040000\prim\EBA_2014_00040000_EN_ORI_prim.docx -admin
:call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\OHMI_2014_01500000\prim\OHMI_2014_01500000_EN_ORI_prim.docx -admin
:call IATEdocgloss.bat en es C:\Pgm\DGTApps\IATEdocgloss\test\OHMI_2014_01500000\prim\OHMI_2014_01500000_EN_ORI_prim.docx -admin



:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\GROW-2015-80018-00-00-EN-ORI-00-en-de\GROW-2015-80018-00-00-EN-ORI-00.DOCX

:call IATEdocgloss.bat en pt C:\Pgm\DGTApps\IATEdocgloss\test\COMP-2015-00119-00-00-EN-ORI-00-en-pt\COMP-2015-00119-00-00-EN-ORI-00.DOCX


:docs2txt.bat  C:\Pgm\DGTApps\IATEdocgloss\test\Parliament-en-fr-pt-hu\  C:\Pgm\DGTApps\IATEdocgloss\test\Parliament-en-fr-pt-hu\EP_GGTEST_04-MAR-2015.txt
:call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\Parliament-en-fr-pt-hu\EP_GGTEST_04-MAR-2015.txt 
:call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\Parliament-en-fr-pt-hu\EP_GGTEST_04-MAR-2015.txt 
:call IATEdocgloss.bat en pt C:\Pgm\DGTApps\IATEdocgloss\test\Parliament-en-fr-pt-hu\EP_GGTEST_04-MAR-2015.txt 
:perl -pe "s/set iate_cfg=dgt-txt_form_2015-03-02_cfg.bat/set iate_cfg=dgt-txt_form_2015-03-02_oldlng_cfg.bat/"   IATEdogloss.bat  > IATEdogloss.tmp
:mv IATEdogloss.tmp IATEdogloss.bat
:call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\Parliament-en-fr-pt-hu\EP_GGTEST_04-MAR-2015-prim.txt 
:call IATEdocgloss.bat en fr C:\Pgm\DGTApps\IATEdocgloss\test\Parliament-en-fr-pt-hu\EP_GGTEST_04-MAR-2015-prim.txt 
:call IATEdocgloss.bat en pt C:\Pgm\DGTApps\IATEdocgloss\test\Parliament-en-fr-pt-hu\EP_GGTEST_04-MAR-2015-prim.txt 


:call IATEdocgloss.bat en lt C:\Pgm\DGTApps\IATEdocgloss\test\SG-2015-00141-00-00-en-lt\SG-2015-00141-00-00-EN-ORI-00.DOC -admin

:call IATEdocgloss.bat en sl C:\Pgm\DGTApps\IATEdocgloss\test\TAXUD\TAXUD-2015-80016-00-00.txt -admin

:call IATEdocgloss.bat hu en C:\Pgm\DGTApps\IATEdocgloss\test\mttest\test.hu.txt -admin

:call IATEdocgloss.bat hu en C:\Pgm\DGTApps\IATEdocgloss\test\OP-2014-00092-01-03-HU-TRA-00-hu-en-2\OP-2014-00092-01-03-HU-TRA-00.DOCX -admin

:rem repeate generation with improved morphology (fixed / in hfst out) + hunspell  2015.02.24.
:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\SANCO-2014-80581-00-00-EN-ORC-en-de-2\SANCO-2014-80581-00-00-EN-ORC-00.DOCX  -admin

:rem repeate generation with improved morphology (fixed / in hfst out) + hunspell  2015.02.23.
:call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\COMP-2015-00119-00-00-EN-ORI-00-en-hu-2\COMP-2015-00119-00-00-EN-ORI-00.DOC -admin

:rem by skipping text->doc generation, copying the 4 files together manually:
:call IATEdocgloss.bat en lt C:\Pgm\DGTApps\IATEdocgloss\test\SG-2015-80039-ENER-2015-80010-en-lt\SG-2015-80039-ENER-2015-80010.doc

:call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\COMP-2015-00119-00-00-EN-ORI-00-en-hu\COMP-2015-00119-00-00-EN-ORI-00.DOC
:call IATEdocgloss.bat hu en C:\Pgm\DGTApps\IATEdocgloss\test\OP-2014-00092-01-03-HU-TRA-00-hu-en\OP-2014-00092-01-03-HU-TRA-00.DOCX
:call IATEdocgloss.bat en hu  C:\Pgm\DGTApps\IATEdocgloss\test\OP-2014-00092-01-03-HU-TRA-00-en-hu\OP-2014-00092-01-03-EN-ORI-00.DOCX

:----

:call IATEdocgloss.bat en de C:\Pgm\DGTApps\IATEdocgloss\test\SANCO-2014-80581-00-00-EN-ORC-en-de\SANCO-2014-80581-00-00-EN-ORC-00.DOCX

:OK call IATEdocgloss.bat hu en C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-02-14-HU-en\MARKT-2014-80060-02-14-HU-TRA-00.DOC 
:long call IATEdocgloss.bat de en C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-11-00-DE\MARKT-2014-80060-11-00-DE-TRA-00.DOC

:call IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-02-14-EN-hu\MARKT-2014-80060-00-14-EN-ORI-00.DOC

:call IATEdocgloss.bat cs en C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-02-14-CS-en\MARKT-2014-80060-02-14-CS-TRA-00.DOC 
:call IATEdocgloss.bat en cs C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-02-14-EN-cs\MARKT-2014-80060-00-14-EN-ORI-00.DOC

:call IATEdocgloss.bat de en C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-02-14-DE-en\MARKT-2014-80060-02-14-DE-TRA-00.DOC 
:call IATEdocgloss.bat en cs C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-02-14-EN-cs\MARKT-2014-80060-00-14-EN-ORI-00.DOC

:call IATEdocgloss.bat de en C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-11-00-DE\MARKT-2014-80060-11-00-DE-TRA-00.DOC


:IATEdocgloss.bat de en C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060-02-14-DE\MARKT-2014-80060-03-12-DE-TRA-00.DOC	       
:IATEdocgloss.bat en hu C:\Pgm\DGTApps\IATEdocgloss\test\MARKT-2014-80060\MARKT-2014-80060-00-00-ORC837916.docx
