# iate_convert.bat
# converts multilingual IATE termbases to bilingual dictionaries by associating source and target terms with same IATE ID
# Usage:
# iate_convert.bat source_lang target_lang source_format target_format mode config_file input_file output_dir out_filename
# Parameter values:
# source_lang, target_lang: ISO 639 two letter language codes, lowercased 
# source_format:TXT, XLS or XML
# target_format: TXT, XLS, SDLTB
# mode: list or pair (list: terms with same ID creates one entry, pair: terms with same ID creates N x M single synonym entries, where N and M are the number of source and target side terms)
# more info: laszlo.tihanyi@ext.ec.europa.eu

#blacklist=yes
# blacklist=no
#blacklistdir="C:\Pgm\DGTApps\IATEconvert\blacklist"
blacklistdir="/cygdrive/c/Pgm/DGTApps/IATEconvert/blacklist"


if [ ! "$#" == 9  ]; then
	echo Converts DGT IATE export to SDL Trados Studio - Glossary converter import TXT format
	echo usage:
	echo iate_convert.bat SRC TRG SRC_FORMAT TRG_FORMAT pair_mode config.bat inputfile output_dir output_filename
	echo SR and TG are 2-letter IATE identifiers of source and target languages 
	echo more info: tihanyi@ext.ec.europa.eu
	exit
fi




src=$1
trg=$2
informat=$3
outformat=$4
mode=$5
iateconfig=$6
input=$7
workdir=$8
dic=$9


source "$iateconfig"



echo src=$src >"$workdir/iate_convert.log"
echo trg=$trg >>"$workdir/iate_convert.log"
echo informat=$informat >>"$workdir/iate_convert.log"
echo outformat=$outformat >>"$workdir/iate_convert.log"
echo mode=$mode >>"$workdir/iate_convert.log"
echo iateconfig=$iateconfig >>"$workdir/iate_convert.log"
echo input=$input >>"$workdir/iate_convert.log"
echo workdir=$workdir >>"$workdir/iate_convert.log"
echo dic=$dic >>"$workdir/iate_convert.log"

if [ ! -f "$input" ]; then
	echo File not found: "$input" 
	exit
fi

if [ "$DEBUG" ]; then


if [ "$informat" == "TXT" ]; then
	if [ ! -f "$workdir/preproc.tsv" ]; then
		$perldir/perl $progdir/txtclean.pl -domainpos=$domainpos -instpos=$instpos -termpos=$termpos -primarypos=$primarypos -preiatepos=$preiatepos -historypos=$historypos -evalpos=$evalpos -relypos=$relypos -columns=$columns "$input" > "$workdir/preproc.tsv"
	fi
fi

if [ "$informat" == "XML" ]; then
	$perldir/perl $progdir/xmlclean.pl "$input" >"$workdir/preproc1.xml"
	$perldir/perl $progdir/xml2lines.pl "$workdir/preproc1.xml" > "$workdir/preproc2.xml"
	$perldir/perl $progdir/iate_xml2sql.pl "$workdir/preproc2.xml" "$workdir/preproc.tsv" $xml2sql
fi

#else 
#echo UNKNOWN format: $informat >> "$workdir/iate_convert.log"


$perldir/perl $progdir/filtersrclang.pl $idpos $langpos $src $trg "$workdir/preproc.tsv" "$workdir/$dic-filt.tsv" 


$perldir/perl $progdir/ordertermcol.pl "$workdir/$dic-filt.tsv" $order "$workdir/$dic.ordered.tsv" 


if [ "$blacklist" == "yes"  ]; then
	$perldir/perl $progdir/mergetermextra-u8.pl "$workdir/$dic.ordered.tsv" $termcol "$blacklistdir/exclude.$trg.txt" 1 "$workdir/$dic.ordered.found.tsv" "$workdir/$dic.ordered.filt.tsv"
else
	cp  "$workdir/$dic.ordered.tsv" "$workdir/$dic.ordered.filt.tsv"  
fi



if [ ! "$primary_no" == "Y" ]; then
	$perldir/perl $progdir/filters.pl  "$workdir/$dic.ordered.filt.tsv"  "$workdir/$dic.ordered.filt1.tsv" "$workdir/$dic.ordered.prim.tsv" -f_prim=$primarycol
	$perldir/perl $progdir/iate_trgcnt.pl $idcol $langcol $src $trg "$workdir/$dic.ordered.filt1.tsv"  "$workdir/$dic-transcnt.stat.tsv"
else
	$perldir/perl $progdir/iate_trgcnt.pl $idcol $langcol $src $trg "$workdir/$dic.ordered.filt.tsv"  "$workdir/$dic-transcnt.stat.tsv"
fi



$perldir/perl $progdir/iatepair.pl "$workdir/$dic.ordered.filt.tsv" "$workdir/$dic-$mode.tsv" $src $trg -mode=$mode -entry=$entry -lang=$lang -term=$term -idcol=$idcol -langcol=$langcol -termcol=$termcol  -termtypecol=$termtypecol -evalcol=$evalcol -relycol=$relycol -primarycol=$primarycol -badcomcol=$badcomcol -rely_mini=$rely_mini -rely_notverified=$rely_notverified -primary_no=$primary_no -filter_badcom=$filter_badcom


fi #DEBUG


if [ "$mode" == "list" ]; then
$perldir/perl $progdir/cvsquote.pl "$workdir/$dic-$mode.tsv" "$workdir/$dic-$mode.csv"
$perldir/perl $progdir/utf8to16.pl "$workdir/$dic-$mode.csv" "$workdir/$dic.txt" 


$perldir/perl $progdir/txt2tbx.pl  "$iateconfig" "$workdir/$dic-$mode.tsv"  "$workdir/$dic.tbx"

#filter fields of TBX defined by an XSLT:
# cgwin_path=C:/cygwin64/bin
# $cygwin_path/$xmllint.exe  -noout --dtdvalid TBXcoreStructV02.dtd "$workdir/$dic.tbx"  2>  "$workdir/$dic.tbx.err"
# saxon_path=C:/Pgm/DGTApps/Saxonica/SaxonHE9.5N/bin
# $saxon_path/Transform.exe -s:$workdir/$dic.tbx -xsl:$progdir/tbx_filter.xslt  -o:$workdir/$dic_term.tbx

fi  #list

if [ "$mode" == "pair" ]; then
	$perldir/perl $progdir/txt2tbx.pl "$iateconfig" "$workdir/$dic-$mode.tsv"  "$workdir/$dic-$mode.tbx"
	# xmllint.exe  -noout --dtdvalid TBXcoreStructV02.dtd "$workdir/$dic-$mode.tbx" 2>  "$workdir/$dic-$mode.tbx.err"
fi  #pair

exit

if [ "$outformat" == "XLS_OUT" ]; then
	$progdir/txt2xls.vbs "$workdir/$dic.txt" "$workdir/$dic-$mode.xls"
fi

if [ "$outformat" == "SDLTB_OUT" ]; then
	$GLOSSCONV "$workdir/$dic.tbx"
fi

#else 
#echo UNKNOWN format: $outformat  >>"$workdir/iate_convert.log"

if [ "$zipfile" == "y" ]; then
	gzip -c  "$workdir/$dic.sdltb" "$workdir/$dic-*.mtf"  "$workdir/$dic-*.mdf"  > "$workdir/$dic.sdltb.zip"
	gzip -c  "$workdir/$dic.tbx" > "$workdir/$dic.tbx.zip"
fi




if [ "$XML_IN_CLEAN" == "y" ]; then
	rm "$workdir/$dic-preproc1.xml"
	rm "$workdir/$dic-preproc2.xml"
	rm "$workdir/$dic-$mode.tsv.err"
fi

if [ "$XLS_IN_CLEAN" == "y" ]; then
	rm "$workdir/$dic-preproc1.tsv"
	rm "$workdir/$dic-preproc2.tsv"
	rm "$workdir/$dic-preproc.tsv"
	rm "$workdir/$dic-$mode.tsv.err"
fi

# rm "$workdir/$dic-$mode.tsv"
# rm "$workdir/$dic-$mode.csv"
# rm "$workdir/$dic.ordered.found.tsv" 
# rm "$workdir/$dic-filt.tsv"
# rm "$workdir/iate_convert.log"


