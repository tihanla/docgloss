set perldir=C:\Pgm\DGTapps\Perl516\bin
set progdir=C:\Pgm\DGTapps\IATEconvert
set GLOSSCONV=C:\Pgm\DGTapps\IATEconvert\GlossaryConverter\GlossaryConverter.exe

rem iate_xml2sql.pl:
set xml2sql=iatepublic

rem ordertermcol.pl:
rem 1:LIL_RECORD_ID   2:SUBJECT_DOMAIN   3:SUBJECT_DOMAIN_NOTE   4:LANGUAGE_CODE   5:TERM   6:termformat   7:TERM_TYPE   8:RELIABILITY_VALUE   9:TERM_EVALUATION
set order=1-2-3-4-5-6-7-8-9

rem iatepair.pl:
rem 1:LIL_RECORD_ID   2:SUBJECT_DOMAIN   3:SUBJECT_DOMAIN_NOTE   4:LANGUAGE_CODE   5:TERM   6:termformat   7:TERM_TYPE   8:RELIABILITY_VALUE   9:TERM_EVALUATION
set entry=3
set lang=1
set term=5
set idcol=1
set langcol=4
set termcol=5
set termtypecol=7
set relycol=8
set evalcol=9
rem nonfileterd:
rem set relycol=0
rem set evalcol=0
