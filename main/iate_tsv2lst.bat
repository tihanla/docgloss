:set dname=iate_06-10-2016
set sc=5
set tc=21

set perl_path=C:\Pgm\DGTapps\Perl516\bin
set progdir=C:\PGM\DGTApps\IATEconvert_win\scripts

set src=%1
set trg=%2

:preprocessing steps betwwen the IATE source table to ID based pair lists (from iate_convert.bat):
::%perl_path%\perl %progdir%\txtclean.pl -domainpos=%domainpos% -instpos=%instpos% -termpos=%termpos% -primarypos=%primarypos% -preiatepos=%preiatepos% -historypos=%historypos% -evalpos=%evalpos% -relypos=%relypos% -columns=%columns% "%input%" > "%work_path%\%dicts%-preproc.tsv"
::%perl_path%\perl %progdir%\filtersrclang.pl %idpos% %langpos% %src% %trg% "%work_path%\%dicts%-preproc.tsv" "%work_path%\%diclf%-filt.tsv" -mul=%mul% 
::%perl_path%\perl %progdir%\ordertermcol.pl "%work_path%\%diclf%-filt.tsv" %order% "%work_path%\%diclf%.ordered.tsv" 
::%perl_path%\perl %progdir%\mergetermextra-u8.pl "%work_path%\%diclf%.ordered.tsv" %termcol% "%blacklistdir%\exclude.%trg%.txt" 1 "%work_path%\%diclf%.ordered.found.tsv" "%work_path%\%diclf%.filtered.tsv"
::%perl_path%\perl %progdir%\iatepair.pl "%work_path%\%diclf%.table.tsv" "%work_path%\%diclf%.tsv" %src% %trg% -mode=%mode2% -entry=%entry% -lang=%lang% -term=%term% -idcol=%idcol% -langcol=%langcol% -termcol=%termcol%  -termtypecol=%termtypecol% -evalcol=%evalcol% -relycol=%relycol% -primarycol=%primarycol% -badcomcol=%badcomcol% -rely_mini=%rely_mini% -rely_notverified=%rely_notverified% -primary_no=%primary_no% -filter_badcom=%filter_badcom%

set diclf=%3
set work_path=%4

%perl_path%\perl %progdir%\iate_tsv2lst.pl %sc% %tc% "%work_path%\%diclf%.tsv" "%work_path%\iate_%src%-%trg%"
dos2unix  "%work_path%\iate_%src%-%trg%"

