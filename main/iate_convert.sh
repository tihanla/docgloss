#set -x

# iate_convert.sh
# converts multilingual IATE termbases to bilingual dictionaries by associating source and target terms with same IATE ID
# Usage:
# iate_convert.sh source_lang target_lang source_format target_format mode config_file input_file output_dir out_filename
# Parameter values:
# source_lang, target_lang: ISO 639 two letter language codes, lowercased 
# source_format:TXT, XLS or XML
# target_format: TXT, XLS, SDLTB
# mode: list or pair (list: terms with same ID creates one entry, pair: terms with same ID creates N x M single synonym entries, where N and M are the number of source and target side terms)
# more info: laszlo.tihanyi@ext.ec.europa.eu



if [ ! "$#" == 10  ]; then
	echo Converts DGT IATE export to SDL Trados Studio - Glossary converter import TXT format
	echo usage:
	echo iate_convert.sh SRC TRG SRC_FORMAT TRG_FORMAT pair_mode dictfield.config filter.config inputfile output_dir output_filename
	echo SR and TG are 2-letter IATE identifiers of source and target languages 
	echo more info: tihanyi@ext.ec.europa.eu
	exit
fi

src=$1
trg=$2
informat=$3
outformat=$4
mode=$5
dcfg=$6
fcfg=$7
input=$8
out_path=$9
dic=${10}

if [ "$mode" == "l" ]; then
	mode2=l

fi

if [ "$mode" == "p" ]; then
	mode2=p
fi

if [ "$mode" == "h" ]; then
	mode2=p
fi



#if NOT DEFINED work_path set work_path=$out_path
if [ -z $work_path ]; then 
	work_path=$out_path; 
fi

#echo work_path=$work_path
#echo out_path=$out_path

mypath=$(dirname "$0")
#echo mypath=$mypath


source "$mypath/iateconvert_sys_cfg.sh"

#echo mypath=$mypath
#echo black_dic_path=$black_dic_path

#db fields defined in dcfg-form.sh:
#echo dcfg=$dcfg
source "$dcfg"

#db filter settings in cfg-dgtnew.sh or cfg-dgtold.sh:
source "$fcfg"


dicl=$dic-$src-$trg
if [ "$primary_no" == "Y" ]; then
	dicl=$dicl
fi 


if [ "$primary_no" == "N" ]; then
	dicl=$dicl-prim
fi

if [ "filter_badcom" == "Y" ]; then
	dicl=$dicl-COM
fi

if [ "$mode" == "p" ]; then
	dicl=$dicl-p
fi 

if [ "$mode" == "h" ]; then
	dicl=$dicl-h
fi 

diclf=$dicl
dicts=$dic



echo src=$src >"$work_path/$diclf-iate_convert.log"
echo trg=$trg >>"$work_path/$diclf-iate_convert.log"
echo informat=$informat >>"$work_path/$diclf-iate_convert.log"
echo outformat=$outformat >>"$work_path/$diclf-iate_convert.log"
echo mode=$mode >>"$work_path/$diclf-iate_convert.log"
echo dcfg=$dcfg >>"$work_path/$diclf-iate_convert.log"
echo fcfg=$fcfg >>"$work_path/$diclf-iate_convert.log"
echo input=$input >>"$work_path/$diclf-iate_convert.log"
echo work_path=$work_path >>"$work_path/$diclf-iate_convert.log"
echo dic=$diclf >>"$work_path/$diclf-iate_convert.log"

if [ ! -f "$input" ]; then
	echo File not found: "$input" 
	exit
fi

#if [ "$DEBUG" ]; then
#fi #DEBUG


if [ "$informat" == "TXT" ]; then
	if [ ! -f "$work_path/$dic-preproc.tsv" ]; then
		$perl_path/perl $progdir/txtclean.pl -domainpos=$domainpos -instpos=$instpos -termpos=$termpos -primarypos=$primarypos -preiatepos=$preiatepos -historypos=$historypos -evalpos=$evalpos -relypos=$relypos -columns=$columns "$input" > "$work_path/$dic-preproc.tsv"
	fi
fi



if [ "$informat" == "XML" ]; then
	$perl_path/perl $progdir/xmlclean.pl "$input" >"$work_path/$diclf-preproc1.xml"
	$perl_path/perl $progdir/xml2lines.pl "$work_path/$diclf-preproc1.xml" > "$work_path/$diclf-preproc2.xml"
	$perl_path/perl $progdir/iate_xml2sql.pl "$work_path/$diclf-preproc2.xml" "$work_path/$dic-preproc.tsv" $xml2sql
fi

#else 
#echo UNKNOWN format: $informat >> "$work_path/iate_convert.log"

if [ "$outformat" == "LOOKUP" ]; then
	$perl_path/perl $progdir/filterlookup.pl $src $idpos $langpos $termpos $lookuppos $primarypos $instpos2 "$work_path/$dicts-preproc.tsv" "$work_path/$dic-$src.lookup.tsv" 
exit
fi

$perl_path/perl $progdir/filtersrclang.pl $idpos $langpos $src $trg "$work_path/$dic-preproc.tsv" "$work_path/$diclf-filt.tsv" 


$perl_path/perl $progdir/ordertermcol.pl "$work_path/$diclf-filt.tsv" $order "$work_path/$diclf.ordered.tsv" 


if [ "$blacklist" == "Y"  ]; then
	
	if [ -f "$black_dic_path/exclude.$trg.txt"  ]; then
		$perl_path/perl $progdir/mergetermextra-u8.pl "$work_path/$diclf.ordered.tsv" $termcol "$black_dic_path/exclude.$trg.txt" 1 "$work_path/$diclf.ordered.found.tsv" "$work_path/$diclf.filtered.tsv"
	else
		cp  "$work_path/$diclf.ordered.tsv" "$work_path/$diclf.filtered.tsv"  
	fi
else
	cp  "$work_path/$diclf.ordered.tsv" "$work_path/$diclf.filtered.tsv"  
fi


if [ ! -f "$lookupdir/lookup.$src.tsv" ]; then
#perl compatibility problem on ubuntu:
# $perl_path/perl $progdir/lookupform_add.pl "$work_path/$diclf.filtered.tsv" "$work_path/$diclf.table.tsv" $src $trg -do_hyphen=$hyphen_lookup -do_udic=$udic_lookup -udic=$lookupdir/lookup.$src.tsv -mode=$mode2 -entry=$entry -lang=$lang -term=$term -idcol=$idcol -langcol=$langcol -termcol=$termcol  -termtypecol=$termtypecol -evalcol=$evalcol -relycol=$relycol -primarycol=$primarycol -badcomcol=$badcomcol -lookupcol=$formc 
	cp  "$work_path/$diclf.filtered.tsv" "$work_path/$diclf.table.tsv" 
else
	cp  "$work_path/$diclf.filtered.tsv" "$work_path/$diclf.table.tsv" 
fi


$perl_path/perl $progdir/iatepair.pl "$work_path/$diclf.table.tsv" "$work_path/$diclf.tsv" $src $trg -mode=$mode2 -entry=$entry -lang=$lang -term=$term -idcol=$idcol -langcol=$langcol -termcol=$termcol  -termtypecol=$termtypecol -evalcol=$evalcol -relycol=$relycol -primarycol=$primarycol -badcomcol=$badcomcol -rely_mini=$rely_mini -rely_notverified=$rely_notverified -primary_no=$primary_no -filter_badcom=$filter_badcom

mkdir -p $out_path

if [ "$mode" == "l" ]; then
	$perl_path/perl $progdir/txt2tbx.pl  "$dcfg" "$work_path/$diclf.tsv"  "$out_path/$dicl.tbx"
fi  #list

if [ "$mode" == "p" ]; then
	$perl_path/perl $progdir/txt2tbx.pl "$dcfg" "$work_path/$diclf.tsv"  "$out_path/$diclf.tbx"
	# xmllint.exe  -noout --dtdvalid TBXcoreStructV02.dtd "$work_path/$diclf-$mode.tbx" 2>  "$work_path/$diclf-$mode.tbx.err"
fi  #pair

if [ "$mode" == "h" ]; then
	$perl_path/perl $progdir/txt2tbx_headword.pl "$dcfg" "$work_path/$diclf.tsv"  "$out_path/$diclf.tbx"
	# xmllint.exe  -noout --dtdvalid TBXcoreStructV02.dtd "$work_path/$diclf-$mode.tbx" 2>  "$work_path/$diclf-$mode.tbx.err"
fi  #pair


if [ "$outformat" == "XLS_OUT" ]; then
	#$progdir/txt2xls.vbs "$work_path/$diclf.txt" "$work_path/$diclf-$mode.xls"
	echo "XLS is unsupported in Linux version"
fi

if [ "$outformat" == "SDLTB_OUT" ]; then
	echo "SDLTB is unsupported in Linux version"
	#$GLOSSCONV "$work_path/$diclf.tbx"
fi

if [ "$outformat" == "TBX" ]; then
	if [ "$convert_zip" == "YES" ]; then
		gzip -c "$out_path/$diclf.tbx" > "$out_path/$diclf.tbx.zip"
	fi

if [ "$convert_tbxid" == "YES" ]; then
	$perl_path/perl $progdir/tbxfilter.pl $out_path/$diclf.tbx > $out_path/$diclf_term-id.tbx
	if [ "$convert_zip" == "YES" ]; then
		gzip -c "$out_path/$diclf_term-id.tbx" > "$out_path/$diclf_term-id.tbx.zip"
	#:skipzip_tbxid
	fi

fi
fi  #TBX_OUT



#else 
#echo UNKNOWN format: $outformat  >>"$work_path/iate_convert.log"

if [ "$zipfile" == "y" ]; then
	gzip -c  "$work_path/$diclf.sdltb" "$work_path/$diclf-*.mtf"  "$work_path/$diclf-*.mdf"  > "$work_path/$diclf.sdltb.zip"
	gzip -c  "$work_path/$diclf.tbx" > "$work_path/$diclf.tbx.zip"
fi



if [ "$XML_IN_CLEAN" == "y" ]; then
	rm "$work_path/$diclf-preproc1.xml"
	rm "$work_path/$diclf-preproc2.xml"
	rm "$work_path/$diclf-$mode.tsv.err"
fi

if [ "$XLS_IN_CLEAN" == "y" ]; then
	rm "$work_path/$diclf-preproc1.tsv"
	rm "$work_path/$diclf-preproc2.tsv"
	rm "$work_path/$diclf-preproc.tsv"
	rm "$work_path/$diclf-$mode.tsv.err"
fi

# rm "$work_path/$diclf-$mode.tsv"
# rm "$work_path/$diclf-$mode.csv"
# rm "$work_path/$diclf.ordered.found.tsv" 
# rm "$work_path/$diclf-filt.tsv"
# rm "$work_path/iate_convert.log"



if [ ! $docgloss_path == ""  ]; then
	rm $work_path/$dic-preproc.tsv
else
	echo  $work_path/$dic-preproc.tsv was not removed
fi
