:@echo off
if "%3"=="" goto terminate0

rem set year=2015
set year=%1

rem set month=04
set month=%2

rem set day=%3%
set day=%3

rem set infname=%year%%month%%day%_en.txt
rem set infname=20150331_en.txt
set infname=%4

set src=%5

set dirdate=%year%-%month%-%day%

rem l=list, p=pair, h=headkey
set mode=l
rem set mode=p
rem set mode=h

set informat=TXT
set outformat=SDLTB
:set outformat=LOOKUP


set outfname=iate_%day%-%month%-%year%

rem set input_path=C:\Pgm\DGTapps\IATEdocgloss\data
rem set input_path=P:\Terminology_Coordination\IATE_SDL\Development\IATE_extracts_txt\test_init_Dimitris_SQL
set input_path=.\data

rem set output_path=C:\PGM\DGTapps\IATEsdl\work\%dirdate%_DGT
set output_path=.\regextract\%dirdate%_DGT_%src%

rem set pdir=C:\PGm\DGTapps\IATEconvert
set pdir=.


:if EXIST %output_path%\!%dirdate%_complete.txt goto terminate

set convert_zip=YES
set convert_stat=YES
set convert_log=YES
set convert_tmpfiles=NO
set convert_tbxid=YES


:SDLTB
:13 new EU langs: SDLTB

call %pdir%\iate_convert.bat %src% bg %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname% 
call %pdir%\iate_convert.bat %src% cs %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% et %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% ga %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% hr %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% hu %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% lt %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% ro %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% pl %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% lv %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% sk %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew-sk.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% sl %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:Maltese SDLTB filtered for the COM:
call %pdir%\iate_convert.bat %src% mt %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% mt %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew-mtcom.bat  %input_path%\%infname% %output_path% %outfname%


:10 old EU langs: %outformat% only primaries
call %pdir%\iate_convert.bat %src% da %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% de %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% pt %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% fi %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% el %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% es %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% fr %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% it %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% nl %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%
call %pdir%\iate_convert.bat %src% sv %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%

:filtered with Polish
:call %pdir%\iate_convert.bat %src% da %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:call %pdir%\iate_convert.bat %src% de %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:call %pdir%\iate_convert.bat %src% pt %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:call %pdir%\iate_convert.bat %src% fi %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:call %pdir%\iate_convert.bat %src% el %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:call %pdir%\iate_convert.bat %src% es %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:call %pdir%\iate_convert.bat %src% fr %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%

:lets try only primaries:
:call %pdir%\iate_convert.bat %src% fr %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%

:call %pdir%\iate_convert.bat %src% it %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:call %pdir%\iate_convert.bat %src% nl %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%
:call %pdir%\iate_convert.bat %src% sv %informat% %outformat% %mode% %pdir%\dcfg-base.bat  %pdir%\cfg-dgtnew.bat %input_path%\%infname% %output_path% %outfname%


call %pdir%\stat.bat

echo  %dirdate% package generated successfully >%output_path%\!%dirdate%_complete.txt

:terminate0
echo usage:  
echo call_iate_convert.bat YYYY MM DD
goto end
:terminate  
echo Nothing to do: files are up to date.
goto end
echo Generation completed successfully!
:end
