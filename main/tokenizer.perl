#!/usr/bin/perl -w

# Sample Tokenizer
# written by Josh Schroeder, based on code by Philipp Koehn
#
# modified by Michael Jellinghaus
# modified by Laszlo Tihanyi 

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
# $|=1; # use '-b' instead

use FindBin qw($Bin);
use strict;
use utf8;
use Normalise::Predefregexp qw(notouch_patterns);

my $mydir = "$Bin/nonbreaking_prefixes";

my %NONBREAKING_PREFIX = ();
my $language = "en";
my $QUIET = 0;
my $HELP = 0;
my $AGGRESSIVE = 0;

while (@ARGV) {
	$_ = shift;
	/^-b$/ && ($| = 1, next);
	/^-l$/ && ($language = shift, next);
	/^-q$/ && ($QUIET = 1, next);
	/^-h$/ && ($HELP = 1, next);
	/^-a$/ && ($AGGRESSIVE = 1, next);
	/^-ad$/ && ($AGGRESSIVE = 2, next);  #agressive and deleting
}

if ($HELP) {
	print "Usage ./tokenizer.perl (-l [en|de|...]) < textfile > tokenizedfile\n";
        print "Options:\n";
        print "  -q  ... quiet.\n";
        print "  -a  ... aggressive hyphen splitting.\n";
        print "  -ad ... aggressive hyphen splitting with deletion of hyphen.\n";
        print "  -b  ... disable Perl buffering.\n";
	exit;
}
if (!$QUIET) {
	print STDERR "Language: $language\n";
}

load_prefixes($language,\%NONBREAKING_PREFIX);

if (scalar(%NONBREAKING_PREFIX) eq 0){
	print STDERR "Warning: No known abbreviations for language '$language'\n";
}

while(<STDIN>) {
    #don't try to tokenize XML/HTML tag lines
    unless (/^<.+>$/ || /^\s*$/) {
	my @parts = ();
	my $notouch_patterns = &notouch_patterns();
	#don't tokenize URIs etc.
	while (/$notouch_patterns/op) {
	    my ($prematch, $match, $postmatch) = (${^PREMATCH}, ${^MATCH}, ${^POSTMATCH});
	    $match =~ s/^ *//;
	    $match =~ s/ *$//;
	    push (@parts, &tokenize($prematch), $match, " ");
	    $_ = $postmatch;
	}
	$_ = join('',(@parts, &tokenize($_)));

	#ensure final line break
	s/\s*$/\n/;

    }
    print;
}


sub tokenize {
	my($text) = @_;
	if ($text =~ /^\s*$/) {
	    return "";
	}
	chomp($text);
	$text = " $text ";
	
	# seperate out all "other" special characters
	$text =~ s/([^\p{IsAlnum}\s\.\'\`\,\-\/’_²³:°])/ $1 /g;

	# in Swedish and Finnish, the colon is used in abbreviations
	if ($language =~ /^sv|fi$/) {
	    $text =~ s/([^\p{IsAlnum}]):/$1 : /g;
	    $text =~ s/:([^\p{IsAlnum}])/ : $1/g;
	} else {
	    $text =~ s/:/ : /g;
	}
	
	# separate out "/" if surrounded by letters
	$text =~ s/(\p{IsAlpha})\/(\p{IsAlpha})/$1 \/ $2/g;

	# aggressive hyphen splitting
        if ($AGGRESSIVE == 1) {
	   $text =~ s/([\p{IsAlnum}])\-([\p{IsAlnum}])/$1 \@-\@ $2/g;
        }
        if ($AGGRESSIVE == 2) {
	   $text =~ s/-/ /g;
        }

	#multi-dots stay together
	$text =~ s/\.([\.]+)/ DOTMULTI$1/g;
	while($text =~ /DOTMULTI\./) {
		$text =~ s/DOTMULTI\.([^\.])/DOTDOTMULTI $1/g;
		$text =~ s/DOTMULTI\./DOTDOTMULTI/g;
	}

	# seperate out "," except if within numbers (5,300)
	$text =~ s/([^\p{IsN}])[,]([^\p{IsN}])/$1 , $2/g;
	# separate , pre and post number
	$text =~ s/([\p{IsN}])[,]([^\p{IsN}])/$1 , $2/g;
	$text =~ s/([^\p{IsN}])[,]([\p{IsN}])/$1 , $2/g;
	      
	# turn `into '
	$text =~ s/\`/\'/g;
	
	#turn '' into "
	$text =~ s/\'\'/ \" /g;

	$text =~ s/([^\p{IsAlpha}])([\'’])([^\p{IsAlpha}])/$1 $2 $3/g;
	if ($language eq "mt") {
	    # in Maltese, ’ should often be considered part of the word
	    $text =~ s/([^\p{IsAlpha}\p{IsN}])([\'’])([\p{IsAlpha}])/$1 $2 $3/g;
	    $text =~ s/ ([\'’]) (i?l) / $1$2 /gi;
	    $text =~ s/([\p{IsAlpha}])([\'’])([^\p{IsAlpha}])/$1$2 $3/g;
	} else {
	    unless ($language eq "nl") {
		# Dutch has expressions like " 's avonds" -> keep everything as it is
		$text =~ s/([^\p{IsAlpha}\p{IsN}])([\'’])([\p{IsAlpha}])/$1 $2 $3/g;
	    }
	    $text =~ s/([\p{IsAlpha}])([\'’])([^\p{IsAlpha} ])/$1 $2 $3/g;
	}
	if ($language =~ /^en|da|pl|et|lt$/) {
		#split contractions right
		$text =~ s/([\p{IsAlpha}])([\'’])([\p{IsAlpha}])/$1 $2$3/g;
		#special case for "1990's"
		$text =~ s/([\p{IsN}])([\'’])([s])/$1 $2$3/g;
	} elsif ($language eq "nl") {
	    # Dutch has plurals like "foto's" -> don't split "contractions"
	} else {
		#split contractions left	
		if ($language =~ /^it|fr$/) {
		    # hack for things like "dell' ____article"
		    $text =~ s/([\p{IsAlpha}])([\'’])( ____)/$1$2üüü$3/g;
		}
		$text =~ s/([\p{IsAlpha}])([\'’])([\p{IsAlpha}])/$1$2 $3/g;
		if ($language =~ /^it|fr$/) {
		    # undo hack
		    $text =~ s/([\p{IsAlpha}])([\'’]) üüü( ____)/$1$2 $3/g;
		}
	}

	#word token method
	my @words = split(/[ \n\r\t]+/,$text); # not using "\s" here because we don't want to split on non-breaking space
	$text = "";
	for (my $i=0;$i<(scalar(@words));$i++) {
		my $word = $words[$i];
		if ($language eq "el" && $word =~ /^(.*[\p{Greek}]’ )([\p{Greek}].*)$/) {
		    # Greek words ending in an apostrophe are followed by a non-breaking space but should still be treated as a separate token
		    $word = $1." ".$2;
		}
		if ( $word =~ /^(\S+)\.$/) {
			my $pre = $1;
			if (($pre =~ /\./ && $pre =~ /\p{IsAlpha}/) || ($NONBREAKING_PREFIX{$pre} && $NONBREAKING_PREFIX{$pre}==1) || ($i<scalar(@words)-1 && ($words[$i+1] =~ /^[\p{IsLower}]/))) {
				#no change
			} elsif (($NONBREAKING_PREFIX{$pre} && $NONBREAKING_PREFIX{$pre}==2) && ($i<scalar(@words)-1 && ($words[$i+1] =~ /^[0-9]+/))) {
				#no change
			} else {
				$word = $pre." .";
			}
		}
		$text .= $word." ";
	}		

	# clean up extraneous spaces and ensure final space
	$text =~ s/ +/ /g;
	$text =~ s/^ //;
	$text =~ s/\s*$/ /;

	#restore multi-dots
	while($text =~ /DOTDOTMULTI/) {
		$text =~ s/DOTDOTMULTI/DOTMULTI./g;
	}
	$text =~ s/DOTMULTI/./g;
	
	return $text;
}

sub load_prefixes {
	my ($language, $PREFIX_REF) = @_;
	
	my $prefixfile = "$mydir/nonbreaking_prefix.$language";
	
	#default back to English if we don't have a language-specific prefix file
	if (!(-e $prefixfile)) {
		$prefixfile = "$mydir/nonbreaking_prefix.en";
		print STDERR "WARNING: No known abbreviations for language '$language', attempting fall-back to English version...\n";
		die ("ERROR: No abbreviations files found in $mydir\n") unless (-e $prefixfile);
	}
	
	if (-e "$prefixfile") {
		open(PREFIX, "<:utf8", "$prefixfile");
		while (<PREFIX>) {
			my $item = $_;
			chomp($item);
			if (($item) && (substr($item,0,1) ne "#")) {
				if ($item =~ /(.*)[\s]+(\#NUMERIC_ONLY\#)/) {
				    $PREFIX_REF->{$1} = 2;
				} else {
				    $PREFIX_REF->{$item} = 1;
				}
			}
		}
		close(PREFIX);
	}
}
