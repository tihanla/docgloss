:@echo off 
rem iate_convert.bat
rem converts multilingual IATE termbases to bilingual dictionaries by associating source and target terms with same IATE ID
rem Usage:
rem iate_convert.bat source_lang target_lang source_format target_format mode config_file input_file output_dir out_filename
rem Parameter values:
rem source_lang, target_lang: ISO 639 two letter language codes, lowercased 
rem source_format:TXT, XLS or XML
rem target_format: TXT, TBX, XLS, SDLTB
rem mode: list or pair (list: terms with same ID creates one entry, pair: terms with same ID creates N x M single synonym entries, where N and M are the number of source and target side terms)
rem more info: laszlo.tihanyi@ext.ec.europa.eu

rem set blacklist=N
rem set blacklistdir="%~p0\blacklist"

if "%iateconvertjava%"=="1" goto javacall 

if %1=="" goto terminate
if %2=="" goto terminate
if %3=="" goto terminate
if %4=="" goto terminate
if %5=="" goto terminate
if %6=="" goto terminate
if %7=="" goto terminate
if %8=="" goto terminate
if %9=="" goto terminate


set src=%1
set trg=%2
set informat=%3
set outformat=%4
set mode=%5
set dcfg=%~6
set fcfg=%~7
set input=%~8
set out_path=%~9
:shift
:shift
:shift
:shift
:shift
:shift
:shift                                                                                         
:shift
shift
set dic=%~9

if %mode%==l set mode2=l
if %mode%==p set mode2=p
if %mode%==h set mode2=p



if NOT DEFINED work_path set work_path=%out_path%

rem echo work_path=%work_path%
rem echo out_path=%out_path%

set mypath=%~dp0
set mypath=%mypath:~0,-1%
rem echo mypath = %mypath%
call "%mypath%\iateconvert_sys_cfg.bat"

rem db fields defined in dcfg-form.bat:
rem echo dcfg=%dcfg%
call "%dcfg%"

rem db filter settings in cfg-dgtnew.bat or cfg-dgtold.bat:
call "%fcfg%"

rem javacall

set dicl=%dic%-%src%-%trg%
if %primary_no%==Y set dicl=%dicl%
if %primary_no%==N set dicl=%dicl%-prim
if %filter_badcom%==Y set dicl=%dicl%-COM

rem if %mode%==l set modepub=
if %mode%==p set dicl=%dicl%-p
if %mode%==h set dicl=%dicl%-h
rem set dic=%dic%%modepub%

set diclf=%dicl%
set dicts=%dic%


if "%docgloss_path%"=="" goto skiptstamp
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "tstamp=%YYYY%%MM%%DD%-%HH%%Min%%Sec%"
if %use_timestamp%==YES set diclf=%diclf%-%tstamp%
set dicts=%dic%
if %use_timestamp%==YES set dicts=%dicts%-%tstamp%

:skiptstamp

echo src=%src% >"%work_path%\%diclf%-iate_convert.log"
echo trg=%trg% >>"%work_path%\%diclf%-iate_convert.log"
echo informat=%informat% >>"%work_path%\%diclf%-iate_convert.log"
echo outformat=%outformat% >>"%work_path%\%diclf%-iate_convert.log"
echo mode=%mode% >>"%work_path%\%diclf%-iate_convert.log"
echo dcfg=%dcfg% >>"%work_path%\%diclf%-iate_convert.log"
echo fcfg=%fcfg% >>"%work_path%\%diclf%-iate_convert.log"
echo input=%input% >>"%work_path%\%diclf%-iate_convert.log"
echo work_path=%work_path% >>"%work_path%\%diclf%-iate_convert.log"
echo diclf=%diclf% >>"%work_path%\%diclf%-iate_convert.log"
echo lookupdir=%lookupdir% >>"%work_path%\%diclf%-iate_convert.log"




:goto jump

if NOT EXIST "%input%" ( echo File not found: "%input%" >> "%work_path%\%diclf%-iate_convert.log" goto end)

mkdir "%work_path%" 2>nul
mkdir "%out_path%" 2>nul

rem delete empty files To be tested:
rem FOR %%F IN ("%work_path%\*.*") DO (
rem IF "%%~zF" LSS 1 DEL "%%F"
rem )


if  EXIST "%work_path%\%dicts%-preproc.tsv"  goto skip_txtclean

if %informat%==TXT goto TXT_IN
if %informat%==XML goto XML_IN
if %informat%==XLS ( goto XLS_IN
) else (
echo UNKNOWN format: %informat% >> "%work_path%\%diclf%-iate_convert.log"
exit
)


:TXT_IN
if EXIST "%work_path%\%dicts%-preproc.tsv" goto skip_preproc
%perl_path%\perl %progdir%\txtclean.pl -domainpos=%domainpos% -instpos=%instpos% -termpos=%termpos% -primarypos=%primarypos% -preiatepos=%preiatepos% -historypos=%historypos% -evalpos=%evalpos% -relypos=%relypos% -columns=%columns% "%input%" > "%work_path%\%dicts%-preproc.tsv"
:skip_preproc
goto inend


:XML_IN
%perl_path%\perl %progdir%\xmlclean.pl "%input%" >"%work_path%\%diclf%-preproc1.xml"
%perl_path%\perl %progdir%\xml2lines.pl "%work_path%\%diclf%-preproc1.xml" > "%work_path%\%diclf%-preproc2.xml"
%perl_path%\perl %progdir%\iate_xml2sql.pl "%work_path%\%diclf%-preproc2.xml" "%work_path%\%diclf%-preproc.tsv" %xml2sql%
goto inend

:XLS_IN
%progdir%\xls2txt.vbs "%input%" "%work_path%\%diclf%-preproc1.tsv"
%perl_path%\perl %progdir%\utf16to8.pl "%work_path%\%diclf%-preproc1.tsv" "%work_path%\%diclf%-preproc2.tsv"
%perl_path%\perl %progdir%\txtclean.pl -domainpos=%domainpos% -instpos=%instpos% -termpos=%termpos% -primarypos=%primarypos% -preiatepos=%preiatepos% -historypos=%historypos% -columns=%columns% "%work_path%\%diclf%-preproc2.tsv" >"%work_path%\%diclf%-preproc.tsv"
goto inend

:inend
rem save time : assumes common file for languages and preproc methods in one folder
:skip_txtclean



if NOT %outformat%==LOOKUP  goto skip_lookup
%perl_path%\perl %progdir%\filterlookup.pl %src% %idpos% %langpos% %termpos% %lookuppos% %primarypos% %instpos2% "%work_path%\%dicts%-preproc.tsv" "%work_path%\%dic%-%src%.lookup.tsv" 
exit
:skip_lookup



if EXIST "%work_path%\%diclf%-filt.tsv" goto skip_filterlang
%perl_path%\perl %progdir%\filtersrclang.pl %idpos% %langpos% %src% %trg% "%work_path%\%dicts%-preproc.tsv" "%work_path%\%diclf%-filt.tsv" -mul=%mul% 
:skip_filterlang

:Polish filter 
:filter en for which we have polish:
if EXIST "%work_path%\%diclf%-filt-pl0-en.tsv" goto skip_filterlang1
%perl_path%\perl %progdir%\filtersrclang.pl %idpos% %langpos% en pl "%work_path%\%dicts%-preproc.tsv" "%work_path%\%diclf%-filt-pl0.tsv" -mul=%mul% 
%perl_path%\perl -pe "s/^[0-9]+\tpl[^\n]+\n//"  "%work_path%\%diclf%-filt-pl0.tsv"  > "%work_path%\%diclf%-filt-pl0-en.tsv"
:skip_filterlang1

:filter old lang from orig
if EXIST "%work_path%\%diclf%-filt.tsv" goto skip_filterlang2
%perl_path%\perl  %progdir%\filtpl.pl %trg%  "%work_path%\%diclf%-filt.tsv"  "%work_path%\%diclf%-filt-pl0-%trg%.tsv"
copy /b   "%work_path%\%diclf%-filt-pl0-en.tsv" + "%work_path%\%diclf%-filt-pl0-%trg%.tsv" "%work_path%\%diclf%-filt.tsv"
:skip_filterlang2


%perl_path%\perl %progdir%\ordertermcol.pl "%work_path%\%diclf%-filt.tsv" %order% "%work_path%\%diclf%.ordered.tsv" 

if %blacklist%==Y  goto blacklist
copy  "%work_path%\%diclf%.ordered.tsv" "%work_path%\%diclf%.filtered.tsv"  >nul
goto blacklistend
:blacklist
if EXIST  "%blacklistdir%\exclude.%trg%.txt" (
%perl_path%\perl %progdir%\mergetermextra-u8.pl "%work_path%\%diclf%.ordered.tsv" %termcol% "%blacklistdir%\exclude.%trg%.txt" 1 "%work_path%\%diclf%.ordered.found.tsv" "%work_path%\%diclf%.filtered.tsv"
) ELSE (
copy  "%work_path%\%diclf%.ordered.tsv" "%work_path%\%diclf%.filtered.tsv"  >nul
)
:blacklistend


if EXIST  "%lookupdir%\lookup.%src%.tsv" (
%perl_path%\perl %progdir%\lookupform_add.pl "%work_path%\%diclf%.filtered.tsv" "%work_path%\%diclf%.table.tsv" %src% %trg% -do_hyphen=%hyphen_lookup% -do_udic=%udic_lookup% -udic=%lookupdir%\lookup.%src%.tsv -mode=%mode2% -entry=%entry% -lang=%lang% -term=%term% -idcol=%idcol% -langcol=%langcol% -termcol=%termcol%  -termtypecol=%termtypecol% -evalcol=%evalcol% -relycol=%relycol% -primarycol=%primarycol% -badcomcol=%badcomcol% -lookupcol=%formc% 
) ELSE (
copy  "%work_path%\%diclf%.filtered.tsv" "%work_path%\%diclf%.table.tsv"  >nul
)



%perl_path%\perl %progdir%\iatepair.pl "%work_path%\%diclf%.table.tsv" "%work_path%\%diclf%.tsv" %src% %trg% -mode=%mode2% -entry=%entry% -lang=%lang% -term=%term% -idcol=%idcol% -langcol=%langcol% -termcol=%termcol%  -termtypecol=%termtypecol% -evalcol=%evalcol% -relycol=%relycol% -primarycol=%primarycol% -badcomcol=%badcomcol% -rely_mini=%rely_mini% -rely_notverified=%rely_notverified% -primary_no=%primary_no% -filter_badcom=%filter_badcom%




if not %mode%==l goto skiplist_l
%perl_path%\perl %progdir%\txt2tbx.pl  "%dcfg%" "%work_path%\%diclf%.tsv"  "%out_path%\%dicl%.tbx"
rem %xmlvalid%  -noout --dtdvalid TBXcoreStructV02.dtd "%out_path%\%diclf%.tbx"  2>  "%work_path%\%diclf%.tbx.err"
:skiplist_l

if not %mode%==p goto skippair_p
%perl_path%\perl %progdir%\txt2tbx.pl "%dcfg%" "%work_path%\%diclf%.tsv"  "%work_path%\%dicl%.tbx"
rem %xmlvalid%  -noout --dtdvalid TBXcoreStructV02.dtd "%work_path%\%dicl%-%mode%.tbx" 2>  "%work_path%\%dicl%-%mode%.tbx.err"
:skippair_p

if not %mode%==h goto skiplist_h
%perl_path%\perl %progdir%\txt2tbx_headword.pl "%dcfg%" "%work_path%\%diclf%.tsv"  "%out_path%\%dicl%.tbx"
:skiplist_h



if %outformat%==SDLTB goto SDLTB_OUT
if %outformat%==MTTXT goto SDLTB_OUT
if %outformat%==TXT goto TXT_OUT
if %outformat%==TBX goto TBX_OUT
if %outformat%==XLS ( goto XLS_OUT
) else (
echo UNKNOWN format: %outformat%  >>"%work_path%\%diclf%-iate_convert.log"
exit
)
goto end


:XLS_OUT
%perl_path%\perl %progdir%\cvsquote.pl "%work_path%\%diclf%.tsv" "%work_path%\%diclf%.csv"
%perl_path%\perl %progdir%\utf8to16.pl "%work_path%\%diclf%.csv" "%work_path%\%diclf%.txt" 
%progdir%\txt2xls.vbs "%work_path%\%diclf%.txt" "%out_path%\%dicl%.xls"
goto CLEAN


:!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  EXPERIMENT: POLISH FILTER


:SDLTB_OUT
%GLOSSCONV% "%out_path%\%dicl%.tbx"



:add link:
if NOT exist %access%  goto skip_addlink
copy /Y %out_path%\%dicl%.sdltb  %progdir%\iate_mdblink.sdltb
%access% %progdir%\mdblink.accdb /x mdblink_macro
mv -f %progdir%\iate_mdblink.sdltb  %out_path%\%dicl%.sdltb
:skip_addlink


if NOT "%docgloss_path%"=="" goto skipconvzip

:SETLOCAL ENABLEDELAYEDEXPANSION
:set savepath=%cd%
:cd %work_path%

:@ECHO OFF 
: FOR /f "tokens=*" %%a IN ('dir /b iate_%day%-%month%-%year%-%src%-%trg%*.mtf') DO ( 
: SET "oldName=%%a"                               
: SET "newName=!oldName:%day%-%month%-%year%-=!" 
: :ECHO Rename !oldName! to !newName! 
: RENAME "!oldName!" "!newName!" 
: ) 

: FOR /f "tokens=*" %%a IN ('dir /b iate_%day%-%month%-%year%-%src%-%trg%*.mdf') DO ( 
: SET "oldName=%%a" 
: SET "newName=!oldName:%day%-%month%-%year%-=!" 
: :ECHO Rename !oldName! to !newName! 
: RENAME "!oldName!" "!newName!" 
: ) 

:we need * as we do not know if prim or not
: FOR /f "tokens=*" %%a IN ('dir /b iate_%day%-%month%-%year%-%src%-%trg%*.sdltb') DO ( 
: SET "oldName=%%a" 
: SET "newName=!oldName:%day%-%month%-%year%-=!" 
: :ECHO Rename !oldName! to !newName! 
: RENAME "!oldName!" "!newName!" 
: ) 

:cd %savepath%


:setdate.bat "%out_path%\iate_%day%-%month%-%year%-%src%-%trg%" 2015-10-05T09:00:00  en
:call setdate_sdl.bat 2015-10-05T09:00:00  %src% %trg% %out_path% %day% %month% %year%
:call powershell $(Get-Item %f%).lastwritetime=$(Get-Date "%d%")

:jump

if NOT %convert_zip%==YES goto skipzip_sdltb_c
%zip% a -tzip "%out_path%\%dicl%.sdltb.zip" "%out_path%\iate_%day%-%month%-%year%-%src%-%trg%*.sdltb" "%out_path%\iate_%day%-%month%-%year%-%src%-%trg%*.mtf"  "%out_path%\iate_%day%-%month%-%year%-%src%-%trg%*.mdf"
:no date name: %zip% a -tzip "%out_path%\%dicl%.sdltb.zip" "%out_path%\iate_%src%-%trg%*.sdltb" "%out_path%\iate_%src%-%trg%*.mtf"  "%out_path%\iate_%src%-%trg%*.mdf"
:skipzip_sdltb_c
:skipconvzip



if NOT "%docgloss_path%"=="" goto skipglosszip
if NOT %convert_zip%==YES goto skipzip_sdltb_g
%zip% a -tzip "%out_path%\%dicl%.sdltb.zip" "%out_path%\%dicl%.sdltb" "%out_path%\%dicl%-*.mtf"  "%out_path%\%dicl%-*.mdf"
:skipzip_sdltb_g
:skipglosszip


rem goto CLEAN
rem workaround SDLTB also generates TBX and TBX-ID outputs (zipped/unzipped)


:TBX_OUT
if NOT %convert_zip%==YES goto skipzip_tbx
%zip% a -tzip "%out_path%\%dicl%.tbx.zip"  "%out_path%\%dicl%.tbx"
:skipzip_tbx


if NOT %convert_tbxid%==YES goto skip_tbxid
%perl_path%\perl %progdir%\tbxfilter.pl %out_path%\%dicl%.tbx > %out_path%\%dicl%_term.tbx

if NOT %convert_zip%==YES goto skipzip_tbxid
%zip% a -tzip "%out_path%\%dicl%_term.tbx.zip"  "%out_path%\%dicl%_term.tbx"

:to be DELETED:
goto end

:skipzip_tbxid
:skip_tbxid


goto CLEAN

:TXT_OUT
:nothing to do
echo TXT_OUT  >> "%work_path%\%diclf%-iate_convert.log"
echo informat=%informat%  >> "%work_path%\%diclf%-iate_convert.log"
goto CLEAN

:CLEAN
if %convert_tmpfiles%==YES goto skipclean

if %informat%==TXT goto TXT_IN_CLEAN
if %informat%==XML goto XML_IN_CLEAN
if %informat%==XLS goto XLS_IN_CLEAN


:TXT_IN_CLEAN
goto MAIN_CLEAN
:XML_IN_CLEAN
del "%work_path%\%diclf%-preproc1.xml"
del "%work_path%\%diclf%-preproc2.xml"
del "%work_path%\%diclf%.tsv.err"
goto MAIN_CLEAN
:XLS_IN_CLEAN
del "%work_path%\%diclf%-preproc1.tsv"
del "%work_path%\%diclf%-preproc2.tsv"
del "%work_path%\%diclf%.tsv.err"
goto MAIN_CLEAN
:MAIN_CLEAN
if %mode%==p goto skipdeltsv2
del "%work_path%\%diclf%.tsv"
del "%work_path%\%diclf%.csv"

del "%work_path%\%diclf%.ordered.found.tsv" 2>nul
del "%work_path%\%diclf%-filt.tsv"

if NOT "%docgloss_path%"=="" del "%work_path%\%dicts%-preproc.tsv"

:the input: del "%work_path%\%diclf%.tsv"

del "%work_path%\%diclf%.txt"

del "%work_path%\%diclf%.filtered.tsv"
del "%work_path%\%diclf%.ordered.tsv"
:skipdeltsv2
:skipclean

if %convert_log%==YES goto skipdellog
del "%work_path%\%diclf%-iate_convert.log"
del "%work_path%\%diclf%-list.tsv.log
:skipdellog

if %convert_stat%==YES goto skipdelstat
del "%work_path%\%diclf%-list.tsv.stat
del "%work_path%\%diclf%-transcnt.stat.tsv
:skipdelstat

goto end

:terminate
echo Converts DGT IATE export (UTF-8 TXT) to SDL Trados Studio - Glossary converter import TXT format
echo usage:
echo iate_convert.bat SRC TRG SRC_FORMAT TRG_FORMAT pair_mode config.bat inputfile output_dir output_filename
echo SR and TG are 2-letter IATE identifiers of source and target languages 
echo more info: tihanyi@ext.ec.europa.eu
:end

rem pause
@echo on
