rem INPUT for txtclean.pl:
rem 1:LIL_RECORD_ID	
rem 2:LANGUAGE_CODE
rem 3:DOMAINS
rem 4:DOMAIN_NOTE
rem 5:IS_PRIMARY
rem 6:DEFINITION
rem 7:DEFINITION_REF
rem 8:LL_COMMENT
rem 9:RELATED_MATERIAL
rem 10:TERM
rem 11:TERMTYPE
rem 12:RELIABILITY
rem 13:TERM_REF
rem 14:CONTEXT
rem 15:CONTEXT_REF
rem 16:EVALUATION
rem 17:PIFS
rem 18:TL_COMMENT
rem 19 LOOKUP_FORM
rem 20:TL_INST
rem 21:COM_BAD_MT
set idpos=1
set langpos=2
set domainpos=3
set instpos=20
set termpos=10
set relypos=12
set evalpos=16
set primarypos=5
set preiatepos=17
set historypos=0
set columns=21
rem Adds 11:TERM_FORMAT

rem INPUT for ordertermcol.pl:
rem 1:LIL_RECORD_ID	
rem 2:LANGUAGE_CODE
rem 3:DOMAINS
rem 4:DOMAIN_NOTE
rem 5:IS_PRIMARY
rem 6:DEFINITION
rem 7:DEFINITION_REF
rem 8:LL_COMMENT
rem 9:RELATED_MATERIAL
rem 10:TERM
rem 11:TERM_FORMAT
rem 12:TERMTYPE
rem 13:RELIABILITY
rem 14:TERM_REF
rem 15:CONTEXT
rem 16:CONTEXT_REF
rem 17:EVALUATION
rem 18:PIFS
rem 19:TL_COMMENT
rem 20: LOOKUP_FORM
rem 21:TL_INST
rem 22:COM_BAD_MT
rem SKIP: 19:PIFS: 21 LOOKUP_FORM
set order=1-3-4-5-2-6-7-8-9-10-11-12-13-14-15-16-17-19-21-22
set order_mt=1-2-10
set lookuppos=20
set instpos2=21


rem INPUT for iatepair.pl
rem iatepairs.pl:
rem 1:IATE_ID
rem 2:DOMAIN
rem 3:DOMAIN_NOTE
rem 4:IS_PRIMARY
rem 5:LANGUAGE_CODE
rem 6:DEFINITION
rem 7:DEFINITION_REF
rem 8:LL_COMMENT
rem 9:RELATED_MATERIAL
rem 10:TERM
rem 11:TERM_FORMAT
rem 12:TERMTYPE
rem 13:RELIABILITY
rem 14:TERM_REF
rem 15:CONTEXT
rem 16:CONTEXT_REF
rem 17:EVALUATION
rem 18:TL_COMMENT
rem 19:INST
rem 20:COM_BAD_MT
set entry=4
set lang=5
set term=11
set idcol=1
set langcol=5
set termcol=10
set termtypecol=12
set relycol=13
set evalcol=17
set primarycol=4
set badcomcol=20

set formc=21
