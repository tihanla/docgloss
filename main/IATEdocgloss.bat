:@echo off

if "%3"=="" goto terminate

set TEXTGENSTEP=1
set DICTGENSTEP=1
set ADDFORMSTEP=1
set FILTERSTEP=1
set IATECONVERTSTEP=1


rem values: Python, C, Java
set STEMMING=C
rem set STEMMING=Python
rem set STEMMING=Java

set lookupform=no

call "%~p0\iatedocgloss_sys_cfg.bat"

if not defined perl_path goto missingenv
if not defined docgloss_path goto missingenv
if not defined work_path goto missingenv
if not defined out_path  goto missingenv
if not defined iate_dict goto missingenv
goto skipmissingenv
:missingenv 
echo set environment variables in iatedocgloss_sys_cfg.bat
goto end

:skipmissingenv

rem set iate_cfg=dgt-txt_form_2015-04-30_oldlng_cfg.bat
rem obsolete call %iateconf_path%\%iate_cfg%

set src=%1
set trg=%2
set in_fullname=%~3
set in_path=%~dp3
set in=%~n3
set in_ext=%~x3
rem truncate the trailing backlash: 
set in_path=%in_path:~0,-1%
rem set work_path=%in_path%

if %trg%==da goto old
if %trg%==de goto old
if %trg%==el goto old
if %trg%==es goto old
if %trg%==fi goto old
if %trg%==fr goto old
if %trg%==it goto old
if %trg%==nl goto old
if %trg%==pt goto old
if %trg%==sv goto old

if %src%==da goto old
if %src%==de goto old
if %src%==el goto old
if %src%==es goto old
if %src%==fi goto old
if %src%==fr goto old
if %src%==it goto old
if %src%==nl goto old
if %src%==pt goto old
if %src%==sv goto old
set cfg-dgt=%iateconf_path%\cfg-dgtnew.bat
goto skip_old
:old
set cfg-dgt=%iateconf_path%\cfg-dgtold.bat
:skip_old

call %cfg-dgt%

call %iateconf_path%\dcfg-form.bat  


rem if NOT %src%==en goto setlookend
rem if %langroup%==old goto setlookold
rem set lus=-eu13
rem goto setlookend
rem :setlookold
rem rem look-up  specifyer:
rem set lus=-prim
rem :setlookend

rem if no surface print only the first ana:
if NOT DEFINED srf (
set firstana=-first
)

rem delete zero size txt files from target folder
set filemask="%work_path%\*.txt"
for %%A in (%filemask%) do if %%~zA==0 del "%%A"

set filemask="%work_path%\*.tsv"
for %%A in (%filemask%) do if %%~zA==0 del "%%A"



set p=%in%
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "timestamp=%YYYY%%MM%%DD%-%HH%%Min%%Sec%"
if %use_timestamp%==YES set p=%p%-%timestamp%



:goto jump

mkdir  %work_path%  2>nul

rem --------------------------------------------------------------------
if NOT DEFINED TEXTGENSTEP goto skiptextgen
echo Get stemmed phrases from source document

rem if skip if we alread parsed the text
if EXIST "%work_path%\%p%-textsrf.stm-%src%.ng" GOTO skiptextgen

del "%work_path%\%p%-text.txt"  2>nul
rem extension is doc or docx:
if /I NOT %in_ext%==.DOCX  goto skip_docx 
%script_path%\word2txt.vbs "%in_path%\%in%%in_ext%" "%work_path%\%p%-text.txt"
:skip_docx
if /I NOT %in_ext%==.DOC  goto skip_doc
%script_path%\word2txt.vbs "%in_path%\%in%%in_ext%" "%work_path%\%p%-text.txt"
:skip_doc
if /I NOT %in_ext%==.txt  goto skip_txt
copy %in_path%\%in%%in_ext% "%work_path%\%p%-text.txt"  >nul
:skip_txt


if NOT EXIST "%work_path%\%p%-text.txt"  goto terminate

rem deletion of simple XML tags (without attributes)
%perl_path%\perl -pe "s/<[^ >\n]+>//g" < "%work_path%\%p%-text.txt" >  "%work_path%\%p%-textnotag.txt"
set orig_path=%CD%
cd %iateconf_path%
%perl_path%\perl %iateconf_path%\tokenizer.perl -l %src% -q -a < "%work_path%\%p%-textnotag.txt" >  "%work_path%\%p%-text.tok"
cd %orig_path%


rem ONLY for debugging purpoces:
rem dos2unix  %work_path%\%p%-text.tok


rem C HFST analyser:
if NOT %STEMMING%==C goto skip_c
%perl_path%\perl %script_path%\destxt.pl  -lang=%src% "%work_path%\%p%-text.tok"  "%work_path%\%p%-text.dtok1"
%perl_path%\perl %script_path%\dos2unix.pl   "%work_path%\%p%-text.dtok1" >"%work_path%\%p%-text.dtok"
%hfst_prg_path%\hfst-proc.exe --apertium --dictionary-case  "%hfst_dic_path%\%src%.ana.hfstol" "%work_path%\%p%-text.dtok" > "%work_path%\%p%-text-hfst.ana"
:skip_c

rem Python HFST  analyser:
if NOT %STEMMING%==Python goto skip_python
%perl_path%\perl -pe "s/\n/ _eol\n/;s/^[ ]+\n/\n/;s/[ ]+/\n/g;s/^\n//"  "%work_path%\%p%-text.tok" > "%work_path%\%p%-textl1.dtok"
rem %perl_path%\perl -pe "s/^[ ]+\n/\n/;s/[ ]+/\n/g;s/^\n//"  %work_path%\%p%-text.tok > %work_path%\%p%-textl1.dtok
%python_path%\python %script_path%\hfst_lookup.py %hfst_dic_path%\%src%.ana.hfstol   <"%work_path%\%p%-textl1.dtok"   > "%work_path%\%p%-text-p.ana" 2>"%work_path%\%p%-text-p.err"
%perl_path%\perl %script_path%\hfstp2txt.pl  "%work_path%\%p%-text-p.ana" "%work_path%\%p%-text-hfst.ana" "%work_path%\%p%-text.hfst-ana.stat"


if NOT %convert_stat%==YES goto skipstats2
%hfst_prg_path%\sortx.exe  -f "%work_path%\%p%-text.hfst-ana.stat" | %script_path%\uniq.pl | %hfst_prg_path%\sortx  -gr  > "%work_path%\%p%-text.hfst-ana.freq.stat"

:skipstats2
:skip_python

rem Java HFST analyser:
if NOT %STEMMING%==Java goto skip_java
%perl_path%\perl -pe "s/[ ]+/\n/g"  "%work_path%\%p%-text.tok" > "%work_path%\%p%-textl1.dtok"
%java_path%\java -jar %script_path%\hfst-ol.jar "%hfst_dic_path%\%src%.ana.hfstol"   <"%work_path%\%p%-textl1.dtok"   > "%work_path%\%p%-text-hfst-j.ana"
rem TODO hfstj2txt.pl text-hfst-j.ana text-hfst.ana
:skip_java


rem OLD: HFST Stemming+ Hunspell:
rem conversion of ana to stm form + recognition user dictionary words and non-alfanumeric char words + protection against hunspell crash in case of some hu/sv words
rem %perl_path%\perl %script_path%\hfst-stem.pl  -lang=%src% -srf -udic=%hfst_dic_path%\hfst-%src%.udic %pos% %part% %srf% %firstana% < "%work_path%\%p%-text-hfst.ana"  >"%work_path%\%p%-text-hfst.stm"
rem break to lines with the unknown words:
rem :%perl_path%\perl %script_path%\prep4hunspell.pl   "%work_path%\%p%-text-hfst.stm"  "%work_path%\%p%-textsrf-4hsp.stm"
rem Hunspell analyser:  analyse words with a version of hunspell which expects 1 word per line and returns surface:
rem :%hfst_prg_path%\hunspell_m2w.exe -i utf-8 -m -d %hunspell_dict_path%\%src%  < "%work_path%\%p%-textsrf-4hsp.stm"  >"%work_path%\%p%-text-hsp.ana"



rem delete the last space in line
%perl_path%\perl %script_path%\fixhfstana.pl "%work_path%\%p%-text-hfst.ana"  > "%work_path%\%p%-text.nosp.ana"
rem check:
rem grep " "  %work_path%\%p%-%src%.nosp.ana > %work_path%\%p%-%src%.sp.err
rem stem:
%perl_path%\perl %script_path%\hfst-stem.pl  -lang=%src% -udic=%hfst_dic_path%\hfst-%src%.udic -srf  -first < "%work_path%\%p%-text.nosp.ana"  > "%work_path%\%p%-text-hfst.stm"
rem replace per (/) -> tab
%perl_path%\perl %script_path%\prep4hunsp.pl   "%work_path%\%p%-text-hfst.stm"  "%work_path%\%p%-text-4hsp.stm"  

rem Hunspell***
rem analyse:

%hunspell_prg_path%\analyze_dgt.exe a %hunspell_dict_path%\%src%.aff %hunspell_dict_path%\%src%.dic  "%work_path%\%p%-text-4hsp.stm"  >"%work_path%\%p%-text-hsp.ana"

rem Hunspell stem:  
%perl_path%\perl %script_path%\hunspell-stem.pl  %src%  "%work_path%\%p%-text-hsp.ana"  "%work_path%\%p%-text-hsp.line.stm"

rem restore lines:
%perl_path%\perl %script_path%\restoreline.pl "%work_path%\%p%-text-hsp.line.stm"  > "%work_path%\%p%-text-hsp.stm"


rem statistics on unknown words:
if NOT %convert_stat%==YES goto skipstats3

rem  %perl_path%\perl %script_path%\prep4hunspell.pl  "%work_path%\%p%-text-hsp.stm"  "%work_path%\%p%-unknown-%src%.stm"
rem  %perl_path%\perl -pe "s/^\n//"  "%work_path%\%p%-unknown-%src%.stm" > "%work_path%\%p%-unknown-%src%.err"
rem  %hfst_prg_path%\sortx.exe  -f "%work_path%\%p%-unknown-%src%.err" | uniq   -c | %hfst_prg_path%\sortx  -gr  > "%work_path%\%p%-unknown-%src%.err.stat"
:skipstats3



rem connect with old system:
copy "%work_path%\%p%-text-hsp.stm"  "%work_path%\%p%-textsrf.stm" >nul
%perl_path%\perl %script_path%\cleanstemmed.pl   "%work_path%\%p%-text-hsp.stm"   > "%work_path%\%p%-text.stm"


rem stem suffix array to creat phrases to be looked up in the dictionary, also its frequency infois added to the dictionary:
set orig_path=%CD%
cd %iateconf_path%
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 1 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-01.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 2 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-02.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 3 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-03.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 4 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-04.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 5 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-05.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 6 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-06.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 7 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-07.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 8 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-08.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 9 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-09.txt" "%work_path%\%p%-text.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 10 --token %script_path%\satoken.txt "%work_path%\%p%-text.stm-10.txt" "%work_path%\%p%-text.stm"
copy /b "%work_path%\%p%-text.stm-*.txt"  "%work_path%\%p%-text.stm.ngf-%src%.txt"   >nul


:normalize hyphenated compounds:
%perl_path%\perl %script_path%\cleanhypcomplex.pl  "%work_path%\%p%-text.stm.ngf-%src%.txt" > "%work_path%\%p%-text.stm.ngf-%src%.hc.txt"
%perl_path%\perl -pe "s/ [0-9]+ +\n/\n/"            "%work_path%\%p%-text.stm.ngf-%src%.hc.txt" > "%work_path%\%p%-text.stm-%src%.ng"
%perl_path%\perl -pe "s/^[^\n]+ ([0-9]+) +\n/\1\n/" "%work_path%\%p%-text.stm.ngf-%src%.hc.txt" > "%work_path%\%p%-text.num-%src%.ng"

rem morphological variants: create surface=stem pairs by suffix array using the stemmed suffix array
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 1 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-01.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 2 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-02.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 3 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-03.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 4 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-04.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 5 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-05.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 6 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-06.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 7 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-07.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 8 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-08.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 9 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-09.txt" "%work_path%\%p%-textsrf.stm"
%perl_path%\perl %iateconf_path%\array-suffix-driver.pl --newLine --ngram 10 --token %script_path%\satoken.txt "%work_path%\%p%-textsrf.stm-10.txt" "%work_path%\%p%-textsrf.stm"
copy /b "%work_path%\%p%-textsrf.stm-*.txt"  "%work_path%\%p%-textsrf.stm.ngf-%src%.txt"   >nul
rem: delete lines with numbers, delete * unknown word markers
cd %iateconf_path%

%perl_path%\perl -pe "s/ [0-9]+ +\n/\n/"           "%work_path%\%p%-textsrf.stm.ngf-%src%.txt" > "%work_path%\%p%-textsrf.stm-%src%.ng"
rem convert / to tab
%perl_path%\perl %script_path%\textsrf2tab.pl      "%work_path%\%p%-textsrf.stm-%src%.ng" > "%work_path%\%p%-textsrftab.stm-%src%.caps.ng"
%perl_path%\perl %script_path%\filter_upperterm.pl "%work_path%\%p%-textsrftab.stm-%src%.caps.ng"  "%work_path%\%p%-textsrftab.stm-%src%.caps.4hc.ng"

%perl_path%\perl %script_path%\cleanhypcompsurf.pl             "%work_path%\%p%-textsrftab.stm-%src%.caps.4hc.ng" > "%work_path%\%p%-textsrftab.stm-%src%.ng"


rem debug:
rem %perl_path%\perl %script_path%\textsrf2tab.pl  "%work_path%\%p%-textsrf.stm-%src%.ng" > "%work_path%\%p%-textsrftab.stm-%src%.ng"
rem sort "%work_path%\%p%-textsrftab.stm-%src%.caps.ng" > "%work_path%\%p%-textsrftab.stm-%src%.caps.ng.srt"
rem sort "%work_path%\%p%-textsrftab.stm-%src%.ng" > "%work_path%\%p%-textsrftab.stm-%src%.ng.srt"

rem :not needed: %perl_path%\perl -pe "s/^[^\n]+ ([0-9]+) +\n/\1\n/" %work_path%\%p%-textsrf.stm.ngf-%src%.txt > %work_path%\%p%-textsrf.num.ng



rem not used creates only noise:
rem set orig_path=%CD%
rem cd  %script_path%
rem call sar.bat  %work_path%\%p%-text.tok
rem cd %orig_path%
rem returns %work_path%\%p%-text.tok.ng

:skiptextgen

rem --------------------------------------------------------------------


if NOT DEFINED DICTGENSTEP goto skipdictgen
echo Create a full stemmed IATE list for the source language 


rem we need to generate dic file 1. if the server source file has been updated 2. the dic file does not exist 3. or this script is changed (if so delete dic)
rem if EXIST %iate_dict% goto chkdicindate
rem copy  %dicin_path_remote%\%dicin% %iate_dict%  >nul
rem echo file copied...
rem goto endupdatedicin
rem :chkdicindate
rem echo Check if server dictionary is updated
rem copy  %dicin_path_remote%\%dicin% %iate_dict%-2chk  >nul
rem FOR /F %%i IN ('DIR %iate_dict%* /B /O:D') DO SET NEWEST=%%i
rem rem ECHO %NEWEST% is newer
rem if %NEWEST%==%dicin% goto endupdatedicin1
rem del %iate_dict%
rem mv  %iate_dict%-2chk %iate_dict%
rem rem echo Dictionary updated...
rem goto dictgen
rem :endupdatedicin1
rem del %iate_dict%-2chk   2>nul
rem rem echo Test copy file deleted
rem :endupdatedicin
rem rem Do not generate dict stem list if already exists: 
rem rem if EXIST %iate_dic_path%\dict-%src%.stm echo dict phrase list already exist, generation skipped
set filemask=%hfst_dic_path%\dict-%src%.stm
for %%A in (%filemask%) do if %%~zA==0 del "%%A"


rem delete if zero size 
for /F %%A in ("%hfst_dic_path%\dict-%src%.stm") do If %%~zA equ 0 del "%hfst_dic_path%\dict-%src%.stm"  2>nul

if EXIST %hfst_dic_path%\dict-%src%.stm GOTO skipdictgen
rem :dictgen


set DICTGENRUN=1


%perl_path%\perl %script_path%\gettermcol.pl  %src% %langpos% %termpos% "%iate_dict%"   "%work_path%\dict.ord-%src%.tsv" 


rem Tokenization
set orig_path=%CD%
cd %iateconf_path%
rem  NOT USED for IATE dicts: %perl_path%\perl %script_path%\normalise.perl --lang %src% < "%work_path%\dict.ord.tsv" >  "%work_path%\dict.norm"
%perl_path%\perl %iateconf_path%\tokenizer.perl -l %src% -q -ad < "%work_path%\dict.ord-%src%.tsv"  >  "%work_path%\dict-%src%.tok" 
cd %orig_path%
rem ONLY for debugging purpoces:
rem dos2unix  %work_path%\dict-%src%.tok


if NOT %src%==en  goto skipentok
%perl_path%\perl -pe "s/^to //" "%work_path%\dict-%src%.tok" > "%work_path%\dict-%src%.tok1"
copy "%work_path%\dict-%src%.tok1" "%work_path%\dict-%src%.tok"  >nul
:skipentok


rem C HFST analyser:
if NOT %STEMMING%==C goto skip_c2
%perl_path%\perl %script_path%\destxt.pl  -lang=%src%  "%work_path%\dict-%src%.tok"  "%work_path%\dict-%src%.dtok1"
%perl_path%\perl %script_path%\dos2unix.pl "%work_path%\dict-%src%.dtok1" > "%work_path%\dict-%src%.dtok"
%hfst_prg_path%\hfst-proc.exe --apertium --dictionary-case  "%hfst_dic_path%\%src%.ana.hfstol" "%work_path%\dict-%src%.dtok" > "%work_path%\dict-%src%-hfst.ana"
:skip_c2




rem delete the last space in line
%perl_path%\perl %script_path%\fixhfstana.pl "%work_path%\dict-%src%-hfst.ana"  > "%work_path%\dict-%src%.nosp.ana"


rem HFST stem:
%perl_path%\perl %script_path%\hfst-stem.pl  -lang=%src% -udic=%hfst_dic_path%\hfst-%src%.udic -srf  -first < "%work_path%\dict-%src%.nosp.ana"  > "%work_path%\dict-%src%-hfst.stm"


rem replace per (/) -> tab
%perl_path%\perl %script_path%\prep4hunsp.pl   "%work_path%\dict-%src%-hfst.stm"  "%work_path%\dict-%src%-4hsp.stm"  

rem Hunspell
%hunspell_prg_path%\analyze_dgt.exe a %hunspell_dict_path%\%src%.aff %hunspell_dict_path%\%src%.dic  "%work_path%\dict-%src%-4hsp.stm"  >"%work_path%\dict-%src%-hsp.ana"



rem Hunspell stem:  

%perl_path%\perl %script_path%\hunspell-stem.pl  %src%  "%work_path%\dict-%src%-hsp.ana"  "%work_path%\dict-%src%-hsp.line.stm"

rem restore lines:
%perl_path%\perl %script_path%\restoreline.pl "%work_path%\dict-%src%-hsp.line.stm"  > "%work_path%\dict-%src%-hsp.stm"


rem :for development purposes first we create it in a local folder:
%perl_path%\perl %script_path%\cleanstemmed.pl  "%work_path%\dict-%src%-hsp.stm" >  "%work_path%\dict-%src%.stm"


copy  "%work_path%\dict-%src%.stm"  "%hfst_dic_path%\dict-%src%.stm" >nul


rem statistics:
if NOT %convert_stat%==YES goto skipstats4
%perl_path%\perl -pe "s/^\n//"  "%work_path%\dict-%src%-hsp.ana" >"%work_path%\dic-%src%-ana.txt"
%perl_path%\perl -pe "s/[\t ]+/\n/g" "%work_path%\dic-%src%-ana.txt" > "%work_path%\dic-%src%-ana1.tmp"
grep "^fl:" "%work_path%\dic-%src%-ana1.tmp" > "%work_path%\dic-%src%-ana2.tmp"
%perl_path%\perl -pe "s/fl://" "%work_path%\dic-%src%-ana2.tmp" >"%work_path%\dic-%src%-ana3.tmp"
%hfst_prg_path%\sortx.exe  -f "%work_path%\dic-%src%-ana3.tmp" | uniq   -c | %hfst_prg_path%\sortx  -gr  >"%work_path%\dic-%src%-ana-fl.stat"

grep "^..:" "%work_path%\dic-%src%-ana1.tmp" > "%work_path%\dic-%src%-ana10.tmp"
%perl_path%\perl -pe "s/^(..):[^\n]+/\1/" "%work_path%\dic-%src%-ana10.tmp" > "%work_path%\dic-%src%-ana11.tmp"
%hfst_prg_path%\sortx.exe  -f "%work_path%\dic-%src%-ana11.tmp" | uniq   -c | %hfst_prg_path%\sortx  -gr  >"%work_path%\dic-%src%-ana-mcat.stat"
:skipstats4

:skipdictgen

rem --------------------------------------------------------------------

if NOT DEFINED ADDFORMSTEP goto skip_addform 
echo Add morphological forms to source text terms 

if EXIST "%work_path%\found-srcnum-%src%.txt" GOTO skip_addform

%perl_path%\perl %script_path%\header.pl  <"%iate_dict%" >  "%work_path%\%p%-header.tsv"
:old %perl_path%\perl -pe "s/(.)\n/\1\tTL_FORM\n/" "%work_path%\%p%-header0.tsv" > "%work_path%\%p%-header.tsv"
:obsolete: %perl_path%\perl -pe "s/^(([^\t]*\t){17}[^\t\n]*)\n/\1\t\n/i" "%work_path%\%p%-header1.tsv" > "%work_path%\%p%-header.tsv"


rem FILTER IATE SOURCE TERMS with DOCUMENT SOURCE TERMS  [and generate list of morphological variants]
rem                                             in:file1=stemmed_text              in:file2=src_keys_of_stemmed_dict  in:file2_dic  out=FOUND_IN_DICT                           out=NOT_FOUND_IN_DIC                    out=FOUND_DIC_TERMS                   [in=morph variant dict                   in=id    in=lang   in=primary   in=term    out=FOUND_DIC_MORPHVAR]                         LOG
%perl_path%\perl %script_path%\compare_sar.pl  "%work_path%\%p%-text.stm-%src%.ng" "%hfst_dic_path%\dict-%src%.stm"  "%iate_dict%"  "%work_path%\%p%-found_in_text-%src%.txt"  "%work_path%\%p%-not_in_text-%src%.txt" "%work_path%\%p%-found-src-%src%.txt" "%work_path%\%p%-textsrftab.stm-%src%.ng" %idpos% %langpos% %primarypos% %termpos%  "%work_path%\%p%-found-src-%src%-morfvar.txt" > "%work_path%\%p%-compare_sar.log"
 

%perl_path%\perl -pe "s/^(([^\t]*)\t){9}([^\t]+)\t(([^\t]*)\t){11}\3[^\n]*\n//" "%work_path%\%p%-found-src-%src%-morfvar.txt" > "%work_path%\%p%-found-src-%src%-morfvarq.txt"


rem add frequency info to found_dict:
%perl_path%\perl %script_path%\linehash.pl  "%work_path%\%p%-found_in_text-%src%.txt"  "%work_path%\%p%-text.stm-%src%.ng"  "%work_path%\%p%-text.num-%src%.ng" "%work_path%\%p%-found_num.txt" "%work_path%\%p%-found_notused.txt" "%work_path%\%p%-notfound_notused.txt"

rem add vertical: dictionary and morphvar form info
%perl_path%\perl %script_path%\adlinesbytab.pl "%work_path%\%p%-found-src-%src%.txt"  "%work_path%\%p%-found_num.txt"  "%work_path%\%p%-found-srcnum-%src%.txt"

rem Get lookupforms from lookupform dictionary: 
if NOT %lookupform%==yes goto skip_lookupform
%perl_path%\perl %script_path%\getidlangline.pl  %termpos% "%work_path%\%p%-found-srcnum-%src%.txt"  %termpos% %langpos% notused %src%   "%hfst_dic_path%\lookup-%src%%lus%.tsv" "%work_path%\%p%-lookup-dic-%src%-rev.tsv" 0	
%perl_path%\perl %script_path%\ordertermcol.pl  "%work_path%\%p%-lookup-dic-%src%-rev.tsv" 1-2-3-4-5-6-7-8-9-20-11-12-13-14-15-16-17-18-19-10 "%work_path%\%p%-lookup-dic-%src%.tsv"
:skip_lookupform



:skip_addform

rem ---------------------------
if NOT DEFINED FILTERSTEP goto skipfilter 
echo Filter document terms with IATE terms

rem Get target terms based upon source term id-s
%perl_path%\perl %script_path%\getidlangline.pl  %idpos%  "%work_path%\%p%-found-srcnum-%src%.txt"  %idpos% %langpos% %src% %trg% "%iate_dict%" "%work_path%\%p%-dic0-%src%-%trg%.tsv" 1

rem copy together:  header       +src                                     + lookupform                         +trg                              + morf variants
copy /b "%work_path%\%p%-header.tsv" +"%work_path%\%p%-found-srcnum-%src%.txt" +  "%work_path%\%p%-lookup-dic-%src%.tsv"+ "%work_path%\%p%-dic0-%src%-%trg%.tsv" + "%work_path%\%p%-found-src-%src%-morfvarq.txt"  "%work_path%\%in%-%src%-%trg%.tsv" >nul

:skipfilter

rem ---------------------------
if NOT %convert_stat%==YES goto skipstats5
echo Create statistics

rem STATISICS:
rem OLD grep "^[^\n]" "%work_path%\%p%-found_in_text-%src%.txt" > "%work_path%\%p%-foundintext-%src%.txt"
%perl_path%\perl -pe "s/^\n//"  "%work_path%\%p%-found_in_text-%src%.txt" > "%work_path%\%p%-foundintext-%src%.txt"
%perl_path%\perl %script_path%\cntdelim.pl  "%work_path%\%p%-foundintext-%src%.txt" "%work_path%\%p%-foundintext-%src%.tmp" " "
%hfst_prg_path%\sortx.exe  -f "%work_path%\%p%-foundintext-%src%.tmp" | uniq   -c | %hfst_prg_path%\sortx  -gr  >"%work_path%\%p%-foundintext-%src%.len.stat"
del "%work_path%\%p%-foundintext-%src%.tmp"

echo +++DGT recognition+++ > "%work_path%\%in%-%src%-%trg%.stat"
%perl_path%\perl %script_path%\getlangline.pl %src% %langpos% "%work_path%\%in%-%src%-%trg%.tsv" "%work_path%\%in%-%src%.tsv" -nohead
%perl_path%\perl %script_path%\getlangline.pl %trg% %langpos% "%work_path%\%in%-%src%-%trg%.tsv" "%work_path%\%in%-%trg%.tsv" -nohead
%perl_path%\perl %script_path%\ordertermcol.pl  "%work_path%\%in%-%src%.tsv" %termpos% "%work_path%\%in%-%src%-term.txt"
%perl_path%\perl %script_path%\ordertermcol.pl  "%work_path%\%in%-%trg%.tsv" %termpos% "%work_path%\%in%-%trg%-term.txt"

echo source terms: >> "%work_path%\%in%-%src%-%trg%.stat"
wc -l  "%work_path%\%in%-%src%-term.txt" >> "%work_path%\%in%-%src%-%trg%.stat"
echo uniq source terms: >> "%work_path%\%in%-%src%-%trg%.stat"
%hfst_prg_path%\sortx.exe  -u   "%work_path%\%in%-%src%-term.txt" > "%work_path%\%in%-%src%-term.unq"
wc -l  "%work_path%\%in%-%src%-term.unq" >> "%work_path%\%in%-%src%-%trg%.stat"

echo target terms: >> "%work_path%\%in%-%src%-%trg%.stat"
wc -l  "%work_path%\%in%-%trg%-term.txt" >> "%work_path%\%in%-%src%-%trg%.stat"
echo uniq target terms: >> "%work_path%\%in%-%src%-%trg%.stat"
%hfst_prg_path%\sortx.exe -u   "%work_path%\%in%-%trg%-term.txt" > "%work_path%\%in%-%trg%-term.unq"
wc -l  "%work_path%\%in%-%trg%-term.unq" >> "%work_path%\%in%-%src%-%trg%.stat"

echo source terms lenghts: >> "%work_path%\%in%-%src%-%trg%.stat"
copy /b   "%work_path%\%in%-%src%-%trg%.stat" + "%work_path%\%p%-foundintext-%src%.len.stat" "%work_path%\%in%-%src%-%trg%.stat">nul
del "%work_path%\%p%-foundintext-%src%.len.stat"
:skipstats5

rem --------------------------------------------------------------------
if NOT DEFINED IATECONVERTSTEP goto skipiateconvert
echo Generate SDLTB dictionary

:jump

if NOT %out_format_gloss%==TB_OUT  goto skip_sdltbout
call %iateconf_path%\iate_convert.bat %src% %trg% TXT %out_format% h %iateconf_path%\dcfg-form.bat %cfg-dgt% "%work_path%\%in%-%src%-%trg%.tsv" "%out_path%" "%in%
rem example: call %progdir%\iate_convert.bat     en de      TXT SDLTB %mode% %progdir%\dcfg-base.bat  %progdir%\cfg-dgtold.bat %input_path%\%infname% %output_path% %outfname%_prim-en-de%mode1%



:skip_sdltbout

rem --------------------------------------------------------------------

if NOT %out_format_gloss%==SMT_OUT  goto skip_smtout
rem simple filter on Reliability and Evaluation:

%perl_path%\perl %progdir%\filters.pl  "%work_path%\%in%-%src%-%trg%.tsv"  "%work_path%\%in%-%src%-%trg%-fil.tsv" "%work_path%\%in%-%src%-%trg%-filtered.log"  -f_evaluation=%evalpos% -f_reliability=%relypos% 
rem :perl filters.pl input output filtered [-f_prim=N] [-f_pifs=N] [-f_evaluation=N] [-f_reliability=N] [-f_reliabilitylang=N] [-f_language=ID]\n";

%perl_path%\perl %progdir%\ordertermcol.pl "%work_path%\%in%-%src%-%trg%.tsv" 1-2-11  "%work_path%\%in%-%src%-%trg%-ordered-1to1.tsv"
%perl_path%\perl %script_path%\filter_1trg.pl %src% %trg% "%work_path%\%in%-%src%-%trg%-ordered-1to1.tsv" "%work_path%\%in%-%src%-%trg%-1to1.tsv" "%work_path%\%in%-%src%-%trg%-1to1_skip.log" 
dos2unix  "%work_path%\%in%-%src%-%trg%-1to1.tsv"

rem create xml-markups for Moses with terms from IATE and lowercased morphological forms with probabilities of the target phrase:
rem these last steps can be done only on DGT linux server as it calls Moses pipeline prerocessor that inserts place-holders and lowercase it:
rem copy these 3 files to linux server:
rem cp "%work_path%\text.tok"
rem cp "%work_path%\%in%-%src%-%trg%-1to1.tsv"
rem cp "%work2_path%\%trg%-stm-num-lc.tsv"
rem where "%trg%-stm-num-lc.tsv"  was genereted in two steps: 
rem 1  Euramis corpora to %src%.frq on linux, 
rem 2  %src%.frq -> C:\Pgm\DGTApps\IATEdocgloss\test\mttest10k\test10k-en-hu.markup_frq.txt by C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\run\stmfrqdic.bat
rem swith to linux and issue these commands:
rem :preproc   "%work_path%\%p%-text.tok"  "%work_path%\%p%-text.tok.prec"
rem :then create markup for moses test input :
rem for inclusive/exclusive without probabilites:
rem %perl_path%\perl %script_path%\iatexmarkup.pl "%work_path%\%in%-%src%-%trg%-1to1.tsv" "%work_path%\%p%-text.tok"  "%work_path%\%in%-%src%-%trg%.markup.txt"
rem for exclusive with probabiilities:
rem %perl_path%\perl %script_path%\iatexmarkupfrq.pl "%work_path%\%in%-%src%-%trg%-1to1.tsv" "%work2_path%\%trg%-stm-num-lc.tsv" "%work_path%\%p%-text.tok"  "%work_path%\%in%-%src%-%trg%.markup_frq.txt"

:skip_smtout

:skipiateconvert

if %convert_tmpfiles%==YES goto skipcleanup


del "%work_path%\%p%-text.stm-*.sntngram"
del "%work_path%\%p%-text.stm-*.vocab"
del "%work_path%\%p%-text.stm-*.snt"
del "%work_path%\%p%-text.stm-??.txt"
del "%work_path%\%p%-textsrf.stm-*.sntngram"
del "%work_path%\%p%-textsrf.stm-*.vocab"
del "%work_path%\%p%-textsrf.stm-*.snt"
del "%work_path%\%p%-textsrf.stm-??.txt"


rem del "%work_path%\%in%-%src%-%trg%.tsv" 2>nul
del "%work_path%\%p%-text.ana" 2>nul
del "%work_path%\%p%-text.tok" 2>nul
del "%work_path%\%p%-textl1.dtok" 2>nul
del "%work_path%\%p%-text.stm" 2>nul
del "%work_path%\%p%-text.txt" 2>nul


del "%work_path%\%p%-found-src-%src%-%trg%.txt" 2>nul
del "%work_path%\%p%-found_in_text-%src%.txt" 2>nul
del "%work_path%\%p%-not_in_text-%src%-%trg%.txt" 2>nul
del "%work_path%\%in%-%src%-%trg%-list.tsv.stat" 2>nul
del "%work_path%\%in%-%src%-%trg%.ordered.tsv" 2>nul
del "%work_path%\%in%-%src%-%trg%.ordered.filt.tsv" 2>nul

del "%work_path%\%in%-%src%-%trg%.tsv" 2>nul

del "%work_path%\%p%-text.stm-%src%.ng" 2>nul
del "%work_path%\%p%-text.stm.ngf-%src%.txt" 2>nul
del "%work_path%\%p%-textsrf.stm.ng" 2>nul
del "%work_path%\%p%-textsrf.stm.ngf-%src%.txt" 2>nul

del "%work_path%\%p%-found-srcnum-%src%-%trg%.txt" 2>nul
del "%work_path%\%p%-found_notused.txt" 2>nul
del "%work_path%\%p%-found_num.txt" 2>nul

del "%work_path%\%p%-notfound_notused.txt" 2>nul
del "%work_path%\%p%-text.num-%src%.ng" 2>nul
del "%work_path%\%p%-dic0-%src%-%trg%.tsv" 2>nul

del "%work_path%\%p%-found-src-%src%-morfvar.txt" 2>nul
del "%work_path%\%p%-found-src-%src%-morfvarq.txt" 2>nul
del "%work_path%\%p%-header.tsv" 2>nul
del "%work_path%\%p%-header0.tsv" 2>nul
del "%work_path%\%p%-header1.tsv" 2>nul
del "%work_path%\%p%-lookup-dic-%src%-rev.tsv" 2>nul
del "%work_path%\%p%-lookup-dic-%src%.tsv" 2>nul
del "%work_path%\%p%-text.dtok" 2>nul
del "%work_path%\%p%-textsrf.stm" 2>nul
del "%work_path%\%p%-textsrftab.stm.ng" 2>nul

del "%work_path%\%p%-found-src-%src%.txt" 2>nul
del "%work_path%\%p%-found-srcnum-%src%.txt" 2>nul
del "%work_path%\%p%-foundintext-%src%.txt" 2>nul
del "%work_path%\%p%-not_in_text-%src%.txt" 2>nul
del "%work_path%\%p%-text-hfst.ana" 2>nul
del "%work_path%\%p%-text-hfst.stm" 2>nul
del "%work_path%\%p%-text-hsp.ana" 2>nul
del "%work_path%\%p%-text-hsp.stm" 2>nul
del "%work_path%\%p%-text.dtok1" 2>nul
del "%work_path%\%p%-textnotag.txt" 2>nul
del "%work_path%\%p%-textsrf-4hsp.stm" 2>nul
del "%work_path%\%p%-textsrf.stm-%src%.ng" 2>nul
del "%work_path%\%p%-textsrftab.stm-%src%.ng" 2>nul
del "%work_path%\%p%-unknown-%src%.stm" 2>nul

rem dependent removes:
del "%work_path%\%p%-found-srcnum-%src%-%trg%.txt" 2>nul
del "%work_path%\%p%-text-p.ana" 2>nul
del "%work_path%\%p%-text-p.err" 2>nul
del "%work_path%\%p%-text.ana.freq.stat" 2>nul
del "%work_path%\%p%-text.ana.stat" 2>nul
del "%work_path%\%p%-textl1.dtok" 2>nul


del "%work_path%\%p%-text-4hsp.stm"  2>nul
del "%work_path%\%p%-text-hsp.line.stm"  2>nul
del "%work_path%\%p%-text.nosp.ana"  2>nul
del "%work_path%\%p%-textsrftab.stm-%src%.caps.ng"  2>nul


if NOT DEFINED DICTGENSTEP goto skipdictgenrun
del "%work_path%\dict-%src%-%trg%.ana" 2>nul
del "%work_path%\dict-%src%-%trg%.dtok" 2>nul
del "%work_path%\dict-%src%-%trg%.tok" 2>nul
del "%work_path%\dict-%src%-%trg%.tok1" 2>nul
del "%work_path%\dict.ord-%src%-%trg%.tsv" 2>nul
del "%work_path%\dict-%src%-%trg%.stm" 2>nul

del "%work_path%\dict-%src%-4hsp.stm" 2>nul
del "%work_path%\dict-%src%-hfst.ana" 2>nul
del "%work_path%\dict-%src%-hfst.stm"  2>nul
del "%work_path%\dict-%src%-hsp-srf.stm"  2>nul
del "%work_path%\dict-%src%-hsp.ana" 2>nul
del "%work_path%\dict-%src%.dtok" 2>nul
del "%work_path%\dict-%src%.dtok1" 2>nul
del "%work_path%\dict-%src%.stm" 2>nul
del "%work_path%\dict-%src%.tok" 2>nul
del "%work_path%\dict.ord-%src%.tsv" 2>nul
del "%work_path%\found_in_text-%trg%.txt" 2>nul
del "%work_path%\%dict%-%src%-%trg%.txt" 2>nul

del "%work_path%\dict-%src%-hsp.line.stm" 2>nul
del "%work_path%\dict-%src%-hsp.stm" 2>nul
del "%work_path%\dict-%src%.nosp.ana" 2>nul
del "%work_path%\dict-%src%.tok1" 2>nul
del "%work_path%\%p%-compare_sar.log" 2>nul

:skipdictgenrun

if NOT %convert_stat%==YES goto skipstatdel
rem delete temporary files created during statistics generation
del "%work_path%\%p%-foundintext-%src%-%trg%-%src%.txt" 2>nul
del "%work_path%\%in%-%src%-term.txt" 2>nul
del "%work_path%\%in%-%src%-term.unq" 2>nul
del "%work_path%\%in%-%src%.tsv" 2>nul
del "%work_path%\%in%-%trg%-term.txt" 2>nul
del "%work_path%\%in%-%trg%-term.unq" 2>nul
del "%work_path%\%in%-%trg%.tsv" 2>nul
del "%work_path%\%p%-foundintext-%src%-%trg%-%src%.len.stat" 2>nul
del "%work_path%\%p%-found-srcnum-%src%-%trg%.txt"2>nul
del "%work_path%\%in%-%src%-%trg%.stat"
:skipstatdel

:skipcleanup

if NOT %convert_log%==YES goto skiplogdel
del "%work_path%\%in%-%src%-%trg%-list.tsv.log" 2>nul
del "%work_path%\%in%-%src%-%trg%-iate_convert.log
:skiplogdel



goto end
:terminate
echo Usage: IATEdocgloss.bat src trg input
echo where src and trg are the two letter language ID-s
echo input is the input document with full path name 
echo use double quotes for input file names with space or special characters
:end

