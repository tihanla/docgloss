
set prg_path=C:\Pgm\DGTApps\IATEdocgloss\bin
set dic_path=C:\Pgm\DGTApps\IATEdocgloss\data
set script_path=C:\Pgm\DGTApps\IATEdocgloss\scripts
set perldir=C:\Pgm\DGTapps\Perl516\bin
set dict_path_hunspell=C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\data\hunspell

set iateconf_path=C:\Pgm\DGTapps\IATEconvert

set src=%1
set trg=%2
set work_path=C:\Pgm\DGTApps\IATEdocgloss\test\mttest

set dicin_path_remote=C:\DGTapps\IATEsdl\publish\IATE_EXTRACT
:set dicin_path_remote=P:\Terminology_Coordination\IATE_SDL\Development\IATE_extracts_txt\IATE_EXTRACT

:user dictionary from iate:  to be merged with 
set udict_path_iate=C:\DGT\shared\nlp_installations\nlp_tools-2014-10-30\data\iateuid

:rem statistics:
:grep ^[^\n] %work_path%\dict-%src%-%trg%-hsp.ana >%work_path%\dic-%src%-ana.txt
:perl -pe "s/[\t ]+/\n/g" %work_path%\dic-%src%-ana.txt > %work_path%\dic-%src%-ana1.tmp


:grep "^fl:" %work_path%\dic-%src%-ana1.tmp > %work_path%\dic-%src%-ana2.tmp
:perl -pe "s/fl://" %work_path%\dic-%src%-ana2.tmp >%work_path%\dic-%src%-ana3.tmp
:%prg_path%\sortx.exe  -f "%work_path%\dic-%src%-ana3.tmp" | uniq   -c | %prg_path%\sortx  -gr  >"%work_path%\dic-%src%-ana-fl.stat"


grep "^..:" %work_path%\dic-%src%-ana1.tmp > %work_path%\dic-%src%-ana10.tmp
perl -pe "s/^(..):[^\n]+/\1/" %work_path%\dic-%src%-ana10.tmp >%work_path%\dic-%src%-ana11.tmp
%prg_path%\sortx.exe  -f "%work_path%\dic-%src%-ana11.tmp" | uniq   -c | %prg_path%\sortx  -gr  >"%work_path%\dic-%src%-ana-mcat.stat"
