#@echo off 
#iate_convert.bat
#converts multilingual IATE termbases to bilingual dictionaries by associating source and target terms with same IATE ID
#Usage:
#iate_convert.bat source_lang target_lang source_format target_format mode config_file input_file output_dir out_filename
#Parameter values:
#source_lang, target_lang: ISO 639 two letter language codes, lowercased 
#source_format:TXT, XLS or XML
#target_format: TXT, TBX, XLS, SDLTB
#mode: list or pair (list: terms with same ID creates one entry, pair: terms with same ID creates N x M single synonym entries, where N and M are the number of source and target side terms)
#more info: laszlo.tihanyi@ext.ec.europa.eu

#set blacklist=no
#blacklistdir="%~p0/blacklist"

#if "$iateconvertjava"=="1" goto javacall 

if [ ! "$#" == 10  ]; then
	echo Converts DGT IATE export to SDL Trados Studio - Glossary converter import TXT format
	echo usage:
	echo iate_convert.bat SRC TRG SRC_FORMAT TRG_FORMAT pair_mode dictfield.config filter.config inputfile output_dir output_filename
	echo SR and TG are 2-letter IATE identifiers of source and target languages 
	echo more info: tihanyi@ext.ec.europa.eu
	exit
fi


src=$1
trg=$2
informat=$3
outformat=$4
mode=$5
dcfg=$6
fcfg=$7
input=$8
out_path=$9
dic=$10


#if NOT DEFINED work_path set work_path=$out_path
if [ -z "$work_path"} ]; then 
	work_path=$out_path; 
fi

echo work_path=$work_path
echo out_path=$out_path

source "$rootdir/iateconvert_sys_cfg.sh"
blacklistdir="$rootdir/blacklist"

#db fields defined in dcfg-form.bat:
echo dcfg=$dcfg
source "$dcfg"

#db filter settings in cfg-dgtnew.bat or cfg-dgtold.bat:
source "$fcfg"

#javacall

echo src=$src >"$work_path/$dic-iate_convert.log"
echo trg=$trg >>"$work_path/$dic-iate_convert.log"
echo informat=$informat >>"$work_path/$dic-iate_convert.log"
echo outformat=$outformat >>"$work_path/$dic-iate_convert.log"
echo mode=$mode >>"$work_path/$dic-iate_convert.log"
echo dcfg=$dcfg >>"$work_path/$dic-iate_convert.log"
echo fcfg=$fcfg >>"$work_path/$dic-iate_convert.log"
echo input=$input >>"$work_path/$dic-iate_convert.log"
echo work_path=$work_path >>"$work_path/$dic-iate_convert.log"
echo dic=$dic >>"$work_path/$dic-iate_convert.log"

if [ ! -f "$input" ]; then
	echo File not found: "$input" 
	exit
fi

mkdir "$work_path" 2>nul

#delete empty files To be tested:
#FOR %$F IN ("work_path%/*.*") DO (
#IF "%$~zF" LSS 1 DEL "%F"
#)

if  EXIST "$work_path/$dic-preproc.tsv"  goto skip_txtclean

if $informat==TXT goto TXT_IN
if $informat==XML goto XML_IN
if $informat==XLS ( goto XLS_IN
) else (
echo UNKNOWN format: $informat >> "$work_path/$dic-iate_convert.log"
exit
)
:TXT_IN
if EXIST "$work_path/$dic-preproc.tsv" goto skip_preproc
$perl_path/perl $progdir/txtclean.pl -domainpos=$domainpos -instpos=$instpos -termpos=$termpos -primarypos=$primarypos -preiatepos=$preiatepos -historypos=$historypos -evalpos=$evalpos -relypos=$relypos -columns=$columns "$input" > "$work_path/$dic-preproc.tsv"
:skip_preproc

goto inend
:XML_IN
$perl_path/perl $progdir/xmlclean.pl "$input" >"$work_path/$dic-preproc1.xml"
$perl_path/perl $progdir/xml2lines.pl "$work_path/$dic-preproc1.xml" > "$work_path/$dic-preproc2.xml"
$perl_path/perl $progdir/iate_xml2sql.pl "$work_path/$dic-preproc2.xml" "$work_path/$dic-preproc.tsv" $xml2sql
goto inend
:XLS_IN
$progdir/xls2txt.vbs "$input" "$work_path/$dic-preproc1.tsv"
$perl_path/perl $progdir/utf16to8.pl "$work_path/$dic-preproc1.tsv" "$work_path/$dic-preproc2.tsv"
$perl_path/perl $progdir/txtclean.pl -domainpos=$domainpos -instpos=$instpos -termpos=$termpos -primarypos=$primarypos -preiatepos=$preiatepos -historypos=$historypos -columns=$columns "$work_path/$dic-preproc2.tsv" >"$work_path/$dic-preproc.tsv"
goto inend
:inend
#save time : assumes common file for languages and preproc methods in one folder
:skip_txtclean

$perl_path/perl $progdir/filtersrclang.pl $idpos $langpos $src $trg "$work_path/$dic-preproc.tsv" "$work_path/$dic-filt.tsv" 

#goto CLEAN
#:skip_mtout


$perl_path/perl $progdir/ordertermcol.pl "$work_path/$dic-filt.tsv" $order "$work_path/$dic.ordered.tsv" 


if $blacklist==yes  goto blacklist
cp  "$work_path/$dic.ordered.tsv" "$work_path/$dic.ordered.filt.tsv"  >nul
goto blacklistend
:blacklist
if EXIST  "$blacklistdir/exclude.$trg.txt" (
$perl_path/perl $progdir/mergetermextra-u8.pl "$work_path/$dic.ordered.tsv" $termcol "$blacklistdir/exclude.$trg.txt" 1 "$work_path/$dic.ordered.found.tsv" "$work_path/$dic.ordered.filt.tsv"
) ELSE (
cp  "$work_path/$dic.ordered.tsv" "$work_path/$dic.ordered.filt.tsv"  >nul
)
:blacklistend

$perl_path/perl $progdir/iatepair.pl "$work_path/$dic.ordered.filt.tsv" "$work_path/$dic-$mode.tsv" $src $trg -mode=$mode -entry=$entry -lang=$lang -term=$term -idcol=$idcol -langcol=$langcol -termcol=$termcol  -termtypecol=$termtypecol -evalcol=$evalcol -relycol=$relycol -primarycol=$primarycol -badcomcol=$badcomcol -rely_mini=$rely_mini -rely_notverified=$rely_notverified -primary_no=$primary_no -filter_badcom=$filter_badcom


if $mode==pair goto skiplist1
$perl_path/perl $progdir/cvsquote.pl "$work_path/$dic-$mode.tsv" "$work_path/$dic-$mode.csv"
$perl_path/perl $progdir/utf8to16.pl "$work_path/$dic-$mode.csv" "$work_path/$dic.txt" 

$perl_path/perl $progdir/txt2tbx.pl  "$dcfg" "$work_path/$dic-$mode.tsv"  "$out_path/$dic.tbx"

#$xmlvalid  -noout --dtdvalid TBXcoreStructV02.dtd "$out_path/$dic.tbx"  2>  "$work_path/$dic.tbx.err"

:skiplist1

if $mode==list goto skippair1
$perl_path/perl $progdir/txt2tbx.pl "$iateconfig" "$work_path/$dic-$mode.tsv"  "$work_path/$dic-$mode.tbx"
#$xmlvalid  -noout --dtdvalid TBXcoreStructV02.dtd "$work_path/$dic-$mode.tbx" 2>  "$work_path/$dic-$mode.tbx.err"
:skippair1


if $outformat==SDLTB goto SDLTB_OUT
if $outformat==MTTXT goto SDLTB_OUT
if $outformat==TXT goto TXT_OUT
if $outformat==TBX goto TBX_OUT
if $outformat==XLS ( goto XLS_OUT
) else (
echo UNKNOWN format: $outformat  >>"$work_path/$dic-iate_convert.log"
exit
)
goto end


:XLS_OUT
$progdir/txt2xls.vbs "$work_path/$dic.txt" "$out_path/$dic-$mode.xls"
goto CLEAN

:SDLTB_OUT
$GLOSSCONV "$out_path/$dic.tbx"
if NOT $convert_zip==YES goto skipzip_sdltb
$zip a -tzip "$out_path/$dic.sdltb.zip" "$out_path/$dic.sdltb" "$out_path/$dic-*.mtf"  "$out_path/$dic-*.mdf"
:skipzip
goto CLEAN

:TBX_OUT
if NOT $convert_zip==YES goto skipzip_tbx
$zip a -tzip "$out_path/$dic.tbx.zip"  "$out_path/$dic.tbx"
:skipzip_tbx

if NOT $convert_tbxid==YES goto skip_tbxid
$perl_path/perl $progdir/tbxfilter.pl $out_path/$dic.tbx > $out_path/$dic_term-id.tbx

if NOT $convert_zip==YES goto skipzip_tbxid
$zip a -tzip "$out_path/$dic_term-id.tbx.zip"  "$out_path/$dic_term-id.tbx"
:skipzip_tbxid
:skip_tbxid

goto CLEAN

:TXT_OUT
:nothing to do
echo TXT_OUT  >> "$work_path/$dic-iate_convert.log"
echo informat=$informat  >> "$work_path/$dic-iate_convert.log"
goto CLEAN

:CLEAN
if $convert_tmpfiles==YES goto skipclean

if $informat==TXT goto TXT_IN_CLEAN
if $informat==XML goto XML_IN_CLEAN
if $informat==XLS goto XLS_IN_CLEAN

:TXT_IN_CLEAN
goto MAIN_CLEAN
:XML_IN_CLEAN
rm "$work_path/$dic-preproc1.xml"
rm "$work_path/$dic-preproc2.xml"
rm "$work_path/$dic-$mode.tsv.err"
goto MAIN_CLEAN
:XLS_IN_CLEAN
rm "$work_path/$dic-preproc1.tsv"
rm "$work_path/$dic-preproc2.tsv"
rm "$work_path/$dic-$mode.tsv.err"
goto MAIN_CLEAN
:MAIN_CLEAN
if $mode==pair goto skipdeltsv2
rm "$work_path/$dic-$mode.tsv"
rm "$work_path/$dic-$mode.csv"

rm "$work_path/$dic.ordered.found.tsv" 2>nul
rm "$work_path/$dic-filt.tsv"
rm "$work_path/$dic-preproc.tsv"

:the input: del "$work_path/$dic.tsv"

rm "$work_path/$dic.txt"

rm "$work_path/$dic.ordered.filt.tsv"
rm "$work_path/$dic.ordered.tsv"
:skipdeltsv2
:skipclean

if $convert_log==YES goto skipdellog
rm "$work_path/$dic-iate_convert.log"
rm "$work_path/$dic-list.tsv.log
:skipdellog

if $convert_stat==YES goto skipdelstat
rm "$work_path/$dic-list.tsv.stat
rm "$work_path/$dic-transcnt.stat.tsv
:skipdelstat

goto end

:terminate
echo Converts DGT IATE export (UTF-8 TXT) to SDL Trados Studio - Glossary converter import TXT format
echo usage:
echo iate_convert.bat SRC TRG SRC_FORMAT TRG_FORMAT pair_mode config.bat inputfile output_dir output_filename
echo SR and TG are 2-letter IATE identifiers of source and target languages 
echo more info: tihanyi@ext.ec.europa.eu
:end

#pause
@echo on
