@echo off

if "%3"=="" goto terminate

TEXTGENSTEP=1
DICTGENSTEP=1
ADDFORMSTEP=1
FILTERSTEP=1
IATECONVERTSTEP=1


#values: Python, C, Java
STEMMING=C
#set STEMMING=Python
#set STEMMING=Java

lookupform=no

source "%~p0/iatedocgloss_sys_cfg.bat"

if not defined perl_path goto missingenv
if not defined docgloss_path goto missingenv
if not defined work_path goto missingenv
if not defined out_path  goto missingenv
if not defined iate_dict goto missingenv
goto skipmissingenv
:missingenv 
echo set environment variables in iatedocgloss_sys_cfg.bat
goto end

:skipmissingenv

#set iate_cfg=dgt-txt_form_2015-04-30_oldlng_cfg.bat
#obsolete call $iateconf_path/$iate_cfg

src=%1
trg=%2
in_fullname=%~3
in_path=%~dp3
in=%~n3
in_ext=%~x3
#truncate the trailing backlash: 
in_path=$in_path:~0,-1
#set work_path=$in_path

if $trg==da goto old
if $trg==de goto old
if $trg==el goto old
if $trg==es goto old
if $trg==fi goto old
if $trg==fr goto old
if $trg==it goto old
if $trg==nl goto old
if $trg==pt goto old
if $trg==sv goto old
cfg-dgt=$iateconf_path/cfg-dgtnew.bat
goto skip_old
:old
cfg-dgt=$iateconf_path/cfg-dgtold.bat
:skip_old

source $cfg-dgt

source $iateconf_path/dcfg-form.bat  

#if NOT $src==en goto setlookend
#if $langroup==old goto setlookold
#set lus=-eu13
#goto setlookend
#:setlookold
#rem look-up  specifyer:
#set lus=-prim
#:setlookend

#if no surface print only the first ana:
if NOT DEFINED srf (
firstana=-first
)

#delete zero size txt files from target folder
filemask="$work_path/*.txt"
for %$A in (filemask$) do if $~zA==0 del "%A"

filemask="$work_path/*.tsv"
for %$A in (filemask$) do if $~zA==0 del "%A"

if $use_prefix==YES set p=$in

#goto jump

mkdir  $work_path  2>nul

#--------------------------------------------------------------------
if NOT DEFINED TEXTGENSTEP goto skiptextgen
echo Get stemmed phrases from source document

#if skip if we alread parsed the text
if EXIST "$work_path/$p-textsrf.stm-$src.ng" GOTO skiptextgen

rm "$work_path/$p-text.txt"  2>nul
#extension is doc or docx:
if /I NOT $in_ext==.DOCX  goto skip_docx 
$script_path/word2txt.vbs "$in_path/$in$in_ext" "$work_path/$p-text.txt"
:skip_docx
if /I NOT $in_ext==.DOC  goto skip_doc
$script_path/word2txt.vbs "$in_path/$in$in_ext" "$work_path/$p-text.txt"
:skip_doc
if /I NOT $in_ext==.txt  goto skip_txt
cp $in_path/$in$in_ext "$work_path/$p-text.txt"  >nul
:skip_txt

if NOT EXIST "$work_path/$p-text.txt"  goto terminate

orig_path=$CD
cd $script_path
#deletion of simple XML tags (without attributes)
$perl_path/perl -pe "s/<[^ >/n]+>//g" < "$work_path/$p-text.txt" >  "$work_path/$p-textnotag.txt"
$perl_path/perl $script_path/tokenizer.perl -l $src -q < "$work_path/$p-textnotag.txt" >  "$work_path/$p-text.tok"
cd $orig_path
#ONLY for debugging purpoces:
#dos2unix  $work_path/$p-text.tok


#C HFST analyser:
if NOT $STEMMING==C goto skip_c
$perl_path/perl $script_path/destxt.pl  -lang=$src "$work_path/$p-text.tok"  "$work_path/$p-text.dtok1"
$perl_path/perl $script_path/dos2unix.pl   "$work_path/$p-text.dtok1" >"$work_path/$p-text.dtok"
$hfst_prg_path/hfst-proc.exe --apertium --dictionary-case  "$hfst_dic_path/$src.ana.hfstol" "$work_path/$p-text.dtok" > "$work_path/$p-text-hfst.ana"
:skip_c

#Python HFST  analyser:
if NOT $STEMMING==Python goto skip_python
$perl_path/perl -pe "s//n/ _eol/n/;s/^[ ]+/n//n/;s/[ ]+//n/g;s/^/n//"  "$work_path/$p-text.tok" > "$work_path/$p-textl1.dtok"
#$perl_path/perl -pe "s/^[ ]+/n//n/;s/[ ]+//n/g;s/^/n//"  $work_path/$p-text.tok > $work_path/$p-textl1.dtok
$python_path/python $script_path/hfst_lookup.py $hfst_dic_path/$src.ana.hfstol   <"$work_path/$p-textl1.dtok"   > "$work_path/$p-text-p.ana" 2>"$work_path/$p-text-p.err"
$perl_path/perl $script_path/hfstp2txt.pl  "$work_path/$p-text-p.ana" "$work_path/$p-text-hfst.ana" "$work_path/$p-text.hfst-ana.stat"

if NOT $convert_stat==YES goto skipstats2
$hfst_prg_path/sortx.exe  -f "$work_path/$p-text.hfst-ana.stat" | uniq   -c | $hfst_prg_path/sortx  -gr  > "$work_path/$p-text.hfst-ana.freq.stat"
:skipstats2
:skip_python

#Java HFST analyser:
if NOT $STEMMING==Java goto skip_java
$perl_path/perl -pe "s/[ ]+//n/g"  "$work_path/$p-text.tok" > "$work_path/$p-textl1.dtok"
$java_path/java -jar $script_path/hfst-ol.jar "$hfst_dic_path/$src.ana.hfstol"   <"$work_path/$p-textl1.dtok"   > "$work_path/$p-text-hfst-j.ana"
#TODO hfstj2txt.pl text-hfst-j.ana text-hfst.ana
:skip_java


#OLD: HFST Stemming+ Hunspell:
#conversion of ana to stm form + recognition user dictionary words and non-alfanumeric char words + protection against hunspell crash in case of some hu/sv words
#$perl_path/perl $script_path/hfst-stem.pl  -lang=$src -srf -udic=$hfst_dic_path/hfst-$src.udic $pos $part $srf $firstana < "$work_path/$p-text-hfst.ana"  >"$work_path/$p-text-hfst.stm"
#break to lines with the unknown words:
#:$perl_path/perl $script_path/prep4hunspell.pl   "$work_path/$p-text-hfst.stm"  "$work_path/$p-textsrf-4hsp.stm"
#Hunspell analyser:  analyse words with a version of hunspell which expects 1 word per line and returns surface:
#:$hfst_prg_path/hunspell_m2w.exe -i utf-8 -m -d $hunspell_dict_path/$src  < "$work_path/$p-textsrf-4hsp.stm"  >"$work_path/$p-text-hsp.ana"



#delete the last space in line
$perl_path/perl $script_path/fixhfstana.pl "$work_path/$p-text-hfst.ana"  > "$work_path/$p-text.nosp.ana"
#check:
#grep " "  $work_path/$p-$src.nosp.ana > $work_path/$p-$src.sp.err
#stem:
$perl_path/perl $script_path/hfst-stem.pl  -lang=$src -udic=$hfst_dic_path/hfst-$src.udic -srf  -first < "$work_path/$p-text.nosp.ana"  > "$work_path/$p-text-hfst.stm"
#replace per (/) -> tab
$perl_path/perl $script_path/prep4hunsp.pl   "$work_path/$p-text-hfst.stm"  "$work_path/$p-text-4hsp.stm"  

#Hunspell***
#analyse:

$hunspell_prg_path/analyze_dgt.exe a $hunspell_dict_path/$src.aff $hunspell_dict_path/$src.dic  "$work_path/$p-text-4hsp.stm"  >"$work_path/$p-text-hsp.ana"

#Hunspell stem:  
$perl_path/perl $script_path/hunspell-stem.pl  $src  "$work_path/$p-text-hsp.ana"  "$work_path/$p-text-hsp.line.stm"

#restore lines:
$perl_path/perl $script_path/restoreline.pl "$work_path/$p-text-hsp.line.stm"  > "$work_path/$p-text-hsp.stm"


#statistics on unknown words:
if NOT $convert_stat==YES goto skipstats3

# $perl_path/perl $script_path/prep4hunspell.pl  "$work_path/$p-text-hsp.stm"  "$work_path/$p-unknown-$src.stm"
# $perl_path/perl -pe "s/^/n//"  "$work_path/$p-unknown-$src.stm" > "$work_path/$p-unknown-$src.err"
# $hfst_prg_path/sortx.exe  -f "$work_path/$p-unknown-$src.err" | uniq   -c | $hfst_prg_path/sortx  -gr  > "$work_path/$p-unknown-$src.err.stat"
:skipstats3



#connect with old system:
cp "$work_path/$p-text-hsp.stm"  "$work_path/$p-textsrf.stm" >nul
$perl_path/perl $script_path/cleanstemmed.pl   "$work_path/$p-text-hsp.stm"   > "$work_path/$p-text.stm"

orig_path=$CD
cd  $script_path

#stem suffix array to creat phrases to be looked up in the dictionary, also its frequency infois added to the dictionary:
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 1 --token $script_path/satoken.txt "$work_path/$p-text.stm-01.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 2 --token $script_path/satoken.txt "$work_path/$p-text.stm-02.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 3 --token $script_path/satoken.txt "$work_path/$p-text.stm-03.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 4 --token $script_path/satoken.txt "$work_path/$p-text.stm-04.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 5 --token $script_path/satoken.txt "$work_path/$p-text.stm-05.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 6 --token $script_path/satoken.txt "$work_path/$p-text.stm-06.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 7 --token $script_path/satoken.txt "$work_path/$p-text.stm-07.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 8 --token $script_path/satoken.txt "$work_path/$p-text.stm-08.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 9 --token $script_path/satoken.txt "$work_path/$p-text.stm-09.txt" "$work_path/$p-text.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 10 --token $script_path/satoken.txt "$work_path/$p-text.stm-10.txt" "$work_path/$p-text.stm"
cp /b "$work_path/$p-text.stm-*.txt"  "$work_path/$p-text.stm.ngf-$src.txt"   >nul
$perl_path/perl -pe "s/ [0-9]+ +/n//n/" "$work_path/$p-text.stm.ngf-$src.txt" > "$work_path/$p-text.stm-$src.ng"
$perl_path/perl -pe "s/^[^/n]+ ([0-9]+) +/n//1/n/" "$work_path/$p-text.stm.ngf-$src.txt" > "$work_path/$p-text.num-$src.ng"

#morphological variants: create surface=stem pairs by suffix array using the stemmed suffix array
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 1 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-01.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 2 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-02.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 3 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-03.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 4 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-04.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 5 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-05.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 6 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-06.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 7 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-07.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 8 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-08.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 9 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-09.txt" "$work_path/$p-textsrf.stm"
$perl_path/perl $script_path/array-suffix-driver.pl --newLine --ngram 10 --token $script_path/satoken.txt "$work_path/$p-textsrf.stm-10.txt" "$work_path/$p-textsrf.stm"
cp /b "$work_path/$p-textsrf.stm-*.txt"  "$work_path/$p-textsrf.stm.ngf-$src.txt"   >nul
rem: delete lines with numbers, delete * unknown word markers


$perl_path/perl -pe "s/ [0-9]+ +/n//n/"   "$work_path/$p-textsrf.stm.ngf-$src.txt" > "$work_path/$p-textsrf.stm-$src.ng"
#convert / to tab
$perl_path/perl $script_path/textsrf2tab.pl  "$work_path/$p-textsrf.stm-$src.ng" > "$work_path/$p-textsrftab.stm-$src.caps.ng"
$perl_path/perl $script_path/filter_upperterm.pl   "$work_path/$p-textsrftab.stm-$src.caps.ng"  "$work_path/$p-textsrftab.stm-$src.ng"

#debug:
#$perl_path/perl $script_path/textsrf2tab.pl  "$work_path/$p-textsrf.stm-$src.ng" > "$work_path/$p-textsrftab.stm-$src.ng"
#sort "$work_path/$p-textsrftab.stm-$src.caps.ng" > "$work_path/$p-textsrftab.stm-$src.caps.ng.srt"
#sort "$work_path/$p-textsrftab.stm-$src.ng" > "$work_path/$p-textsrftab.stm-$src.ng.srt"

#:not needed: $perl_path/perl -pe "s/^[^/n]+ ([0-9]+) +/n//1/n/" $work_path/$p-textsrf.stm.ngf-$src.txt > $work_path/$p-textsrf.num.ng

cd "$orig_path"


#not used creates only noise:
#set orig_path=$CD
#cd  $script_path
#call sar.bat  $work_path/$p-text.tok
#cd $orig_path
#returns $work_path/$p-text.tok.ng

:skiptextgen

#--------------------------------------------------------------------


if NOT DEFINED DICTGENSTEP goto skipdictgen
echo Create a full stemmed IATE list for the source language 

#we need to generate dic file 1. if the server source file has been updated 2. the dic file does not exist 3. or this script is changed (if so delete dic)
#if EXIST $iate_dict goto chkdicindate
#copy  $dicin_path_remote/$dicin $iate_dict  >nul
#echo file copied...
#goto endupdatedicin
#:chkdicindate
#echo Check if server dictionary is updated
#copy  $dicin_path_remote/$dicin $iate_dict-2chk  >nul
#FOR /F %$i IN ('DIR iate_dict$* /B /O:D') DO SET NEWEST=%i
#rem ECHO $NEWEST is newer
#if $NEWEST==$dicin goto endupdatedicin1
#del $iate_dict
#mv  $iate_dict-2chk $iate_dict
#rem echo Dictionary updated...
#goto dictgen
#:endupdatedicin1
#del $iate_dict-2chk   2>nul
#rem echo Test copy file deleted
#:endupdatedicin
#rem Do not generate dict stem list if already exists: 
#rem if EXIST $iate_dic_path/dict-$src.stm echo dict phrase list already exist, generation skipped
filemask=$hfst_dic_path/dict-$src.stm
for %$A in (filemask$) do if $~zA==0 del "%A"


#delete if zero size 
for /F %$A in ("hfst_dic_path$/dict-src$.stm") do If $~zA equ 0 del "hfst_dic_path$/dict-src%.stm"  2>nul

if EXIST $hfst_dic_path/dict-$src.stm GOTO skipdictgen
#:dictgen

DICTGENRUN=1


$perl_path/perl $script_path/gettermcol.pl  $src $langpos $termpos "$iate_dict"   "$work_path/dict.ord-$src.tsv" 

#Tokenization
orig_path=$CD
cd $script_path
# NOT USED for IATE dicts: $perl_path/perl $script_path/normalise.perl --lang $src < "$work_path/dict.ord.tsv" >  "$work_path/dict.norm"
$perl_path/perl $script_path/tokenizer.perl -l $src -q < "$work_path/dict.ord-$src.tsv"  >  "$work_path/dict-$src.tok" 
cd $orig_path
#ONLY for debugging purpoces:
#dos2unix  $work_path/dict-$src.tok


if NOT $src==en  goto skipentok
$perl_path/perl -pe "s/^to //" "$work_path/dict-$src.tok" > "$work_path/dict-$src.tok1"
cp "$work_path/dict-$src.tok1" "$work_path/dict-$src.tok"  >nul
:skipentok


#C HFST analyser:
if NOT $STEMMING==C goto skip_c2
$perl_path/perl $script_path/destxt.pl  -lang=$src  "$work_path/dict-$src.tok"  "$work_path/dict-$src.dtok1"
$perl_path/perl $script_path/dos2unix.pl "$work_path/dict-$src.dtok1" > "$work_path/dict-$src.dtok"
$hfst_prg_path/hfst-proc.exe --apertium --dictionary-case  "$hfst_dic_path/$src.ana.hfstol" "$work_path/dict-$src.dtok" > "$work_path/dict-$src-hfst.ana"
:skip_c2

#delete the last space in line
$perl_path/perl $script_path/fixhfstana.pl "$work_path/dict-$src-hfst.ana"  > "$work_path/dict-$src.nosp.ana"
#HFST stem:
$perl_path/perl $script_path/hfst-stem.pl  -lang=$src -udic=$hfst_dic_path/hfst-$src.udic -srf  -first < "$work_path/dict-$src.nosp.ana"  > "$work_path/dict-$src-hfst.stm"
#replace per (/) -> tab
$perl_path/perl $script_path/prep4hunsp.pl   "$work_path/dict-$src-hfst.stm"  "$work_path/dict-$src-4hsp.stm"  
#Hunspell
$hunspell_prg_path/analyze_dgt.exe a $hunspell_dict_path/$src.aff $hunspell_dict_path/$src.dic  "$work_path/dict-$src-4hsp.stm"  >"$work_path/dict-$src-hsp.ana"
#Hunspell stem:  
$perl_path/perl $script_path/hunspell-stem.pl  $src  "$work_path/dict-$src-hsp.ana"  "$work_path/dict-$src-hsp.line.stm"

#restore lines:
$perl_path/perl $script_path/restoreline.pl "$work_path/dict-$src-hsp.line.stm"  > "$work_path/dict-$src-hsp.stm"


#:for development purposes first we create it in a local folder:
$perl_path/perl $script_path/cleanstemmed.pl  "$work_path/dict-$src-hsp.stm" >  "$work_path/dict-$src.stm"


cp  "$work_path/dict-$src.stm"  "$hfst_dic_path/dict-$src.stm" >nul


#statistics:
if NOT $convert_stat==YES goto skipstats4
$perl_path/perl -pe "s/^/n//"  "$work_path/dict-$src-hsp.ana" >"$work_path/dic-$src-ana.txt"
$perl_path/perl -pe "s/[/t ]+//n/g" "$work_path/dic-$src-ana.txt" > "$work_path/dic-$src-ana1.tmp"
grep "^fl:" "$work_path/dic-$src-ana1.tmp" > "$work_path/dic-$src-ana2.tmp"
$perl_path/perl -pe "s/fl://" "$work_path/dic-$src-ana2.tmp" >"$work_path/dic-$src-ana3.tmp"
$hfst_prg_path/sortx.exe  -f "$work_path/dic-$src-ana3.tmp" | uniq   -c | $hfst_prg_path/sortx  -gr  >"$work_path/dic-$src-ana-fl.stat"

grep "^..:" "$work_path/dic-$src-ana1.tmp" > "$work_path/dic-$src-ana10.tmp"
$perl_path/perl -pe "s/^(..):[^/n]+//1/" "$work_path/dic-$src-ana10.tmp" > "$work_path/dic-$src-ana11.tmp"
$hfst_prg_path/sortx.exe  -f "$work_path/dic-$src-ana11.tmp" | uniq   -c | $hfst_prg_path/sortx  -gr  >"$work_path/dic-$src-ana-mcat.stat"
:skipstats4

:skipdictgen

#--------------------------------------------------------------------

if NOT DEFINED ADDFORMSTEP goto skip_addform 
echo Add morphological forms to source text terms 

if EXIST "$work_path/found-srcnum-$src.txt" GOTO skip_addform

$perl_path/perl $script_path/header.pl  <"$iate_dict" >  "$work_path/$p-header0.tsv"
$perl_path/perl -pe "s/(.)/n//1/tTL_FORM/n/" "$work_path/$p-header0.tsv" > "$work_path/$p-header.tsv"
:obsolete: $perl_path/perl -pe "s/^(([^/t]*/t){17}[^/t/n]*)/n//1/t/n/i" "$work_path/$p-header1.tsv" > "$work_path/$p-header.tsv"


#FILTER IATE SOURCE TERMS with DOCUMENT SOURCE TERMS  [and generate list of morphological variants]
#                                         in:file1=stemmed_text           in:file2=src_keys_of_stemmed_dict  in:file2_dic              out=FOUND_IN_DICT                      out=NOT_FOUND_IN_DIC                out=FOUND_DIC_TERMS               [in=morph variant dict               in=id    in=lang   in=primary   in=term    out=FOUND_DIC_MORPHVAR]                     LOG

$perl_path/perl $script_path/compare_sar.pl  "$work_path/$p-text.stm-$src.ng" "$hfst_dic_path/dict-$src.stm"  "$iate_dict"  "$work_path/$p-found_in_text-$src.txt"  "$work_path/$p-not_in_text-$src.txt" "$work_path/$p-found-src-$src.txt" "$work_path/$p-textsrftab.stm-$src.ng" $idpos $langpos $primarypos $termpos  "$work_path/$p-found-src-$src-morfvar.txt" > "$work_path/$p-compare_sar.log"


$perl_path/perl -pe "s/^([^/t]+)/t([^/t]+)/t+([^/t]+)/t+/3/n//" "$work_path/$p-found-src-$src-morfvar.txt" > "$work_path/$p-found-src-$src-morfvarq.txt"

#add frequency info to found_dict:
$perl_path/perl $script_path/linehash.pl  "$work_path/$p-found_in_text-$src.txt"  "$work_path/$p-text.stm-$src.ng"  "$work_path/$p-text.num-$src.ng" "$work_path/$p-found_num.txt" "$work_path/$p-found_notused.txt" "$work_path/$p-notfound_notused.txt"

#add vertical: dictionary and morphvar form info
$perl_path/perl $script_path/adlinesbytab.pl "$work_path/$p-found-src-$src.txt"  "$work_path/$p-found_num.txt"  "$work_path/$p-found-srcnum-$src.txt"

#Get lookupforms from lookupform dictionary: 
if NOT $lookupform==yes goto skip_lookupform
$perl_path/perl $script_path/getidlangline.pl  $termpos "$work_path/$p-found-srcnum-$src.txt"  $termpos $langpos notused $src   "$hfst_dic_path/lookup-$src$lus.tsv" "$work_path/$p-lookup-dic-$src-rev.tsv" 0	
$perl_path/perl $script_path/ordertermcol.pl  "$work_path/$p-lookup-dic-$src-rev.tsv" 1-2-3-4-5-6-7-8-9-20-11-12-13-14-15-16-17-18-19-10 "$work_path/$p-lookup-dic-$src.tsv"
:skip_lookupform


:skip_addform

#---------------------------
if NOT DEFINED FILTERSTEP goto skipfilter 
echo Filter document terms with IATE terms

#Get target terms based upon source term id-s
$perl_path/perl $script_path/getidlangline.pl  $idpos  "$work_path/$p-found-srcnum-$src.txt"  $idpos $langpos $src $trg "$iate_dict" "$work_path/$p-dic0-$src-$trg.tsv" 1

#copy together:  header       +src                                     + lookupform                         +trg                              + morf variants
cp /b "$work_path/$p-header.tsv" +"$work_path/$p-found-srcnum-$src.txt" +  "$work_path/$p-lookup-dic-$src.tsv"+ "$work_path/$p-dic0-$src-$trg.tsv" + "$work_path/$p-found-src-$src-morfvarq.txt"  "$work_path/$in-$src-$trg.tsv" >nul

:skipfilter

#---------------------------
if NOT $convert_stat==YES goto skipstats5
echo Create statistics

#STATISICS:
#OLD grep "^[^/n]" "$work_path/$p-found_in_text-$src.txt" > "$work_path/$p-foundintext-$src.txt"
$perl_path/perl -pe "s/^/n//"  "$work_path/$p-found_in_text-$src.txt" > "$work_path/$p-foundintext-$src.txt"
$perl_path/perl $script_path/cntdelim.pl  "$work_path/$p-foundintext-$src.txt" "$work_path/$p-foundintext-$src.tmp" " "
$hfst_prg_path/sortx.exe  -f "$work_path/$p-foundintext-$src.tmp" | uniq   -c | $hfst_prg_path/sortx  -gr  >"$work_path/$p-foundintext-$src.len.stat"
rm "$work_path/$p-foundintext-$src.tmp"

echo +++DGT recognition+++ > "$work_path/$in-$src-$trg.stat"
$perl_path/perl $script_path/getlangline.pl $src $langpos "$work_path/$in-$src-$trg.tsv" "$work_path/$in-$src.tsv" -nohead
$perl_path/perl $script_path/getlangline.pl $trg $langpos "$work_path/$in-$src-$trg.tsv" "$work_path/$in-$trg.tsv" -nohead
$perl_path/perl $script_path/ordertermcol.pl  "$work_path/$in-$src.tsv" $termpos "$work_path/$in-$src-term.txt"
$perl_path/perl $script_path/ordertermcol.pl  "$work_path/$in-$trg.tsv" $termpos "$work_path/$in-$trg-term.txt"

echo source terms: >> "$work_path/$in-$src-$trg.stat"
wc -l  "$work_path/$in-$src-term.txt" >> "$work_path/$in-$src-$trg.stat"
echo uniq source terms: >> "$work_path/$in-$src-$trg.stat"
$hfst_prg_path/sortx.exe  -u   "$work_path/$in-$src-term.txt" > "$work_path/$in-$src-term.unq"
wc -l  "$work_path/$in-$src-term.unq" >> "$work_path/$in-$src-$trg.stat"

echo target terms: >> "$work_path/$in-$src-$trg.stat"
wc -l  "$work_path/$in-$trg-term.txt" >> "$work_path/$in-$src-$trg.stat"
echo uniq target terms: >> "$work_path/$in-$src-$trg.stat"
$hfst_prg_path/sortx.exe -u   "$work_path/$in-$trg-term.txt" > "$work_path/$in-$trg-term.unq"
wc -l  "$work_path/$in-$trg-term.unq" >> "$work_path/$in-$src-$trg.stat"

echo source terms lenghts: >> "$work_path/$in-$src-$trg.stat"
cp /b   "$work_path/$in-$src-$trg.stat" + "$work_path/$p-foundintext-$src.len.stat" "$work_path/$in-$src-$trg.stat">nul
rm "$work_path/$p-foundintext-$src.len.stat"
:skipstats5

#--------------------------------------------------------------------
if NOT DEFINED IATECONVERTSTEP goto skipiateconvert
echo Generate SDLTB dictionary

#:jump


if NOT $out_format_gloss==TB_OUT  goto skip_sdltbout
source $iateconf_path/iate_convert.bat $src $trg TXT $out_format list $iateconf_path/dcfg-form.bat $cfg-dgt "$work_path/$in-$src-$trg.tsv" "$out_path" "$in
#example: call $progdir/iate_convert.bat     en de      TXT SDLTB $mode $progdir/dcfg-base.bat  $progdir/cfg-dgtold.bat $input_path/$infname $output_path $outfname_prim-en-de$mode1


:skip_sdltbout

#--------------------------------------------------------------------

if NOT $out_format_gloss==SMT_OUT  goto skip_smtout
#simple filter on Reliability and Evaluation:

$perl_path/perl $progdir/filters.pl  "$work_path/$in-$src-$trg.tsv"  "$work_path/$in-$src-$trg-fil.tsv" "$work_path/$in-$src-$trg-filtered.log"  -f_evaluation=$evalpos -f_reliability=$relypos 
#:perl filters.pl input output filtered [-f_prim=N] [-f_pifs=N] [-f_evaluation=N] [-f_reliability=N] [-f_reliabilitylang=N] [-f_language=ID]/n";

$perl_path/perl $progdir/ordertermcol.pl "$work_path/$in-$src-$trg.tsv" 1-2-11  "$work_path/$in-$src-$trg-ordered-1to1.tsv"
$perl_path/perl $script_path/filter_1trg.pl $src $trg "$work_path/$in-$src-$trg-ordered-1to1.tsv" "$work_path/$in-$src-$trg-1to1.tsv" "$work_path/$in-$src-$trg-1to1_skip.log" 
dos2unix  "$work_path/$in-$src-$trg-1to1.tsv"

#create xml-markups for Moses with terms from IATE and lowercased morphological forms with probabilities of the target phrase:
#these last steps can be done only on DGT linux server as it calls Moses pipeline prerocessor that inserts place-holders and lowercase it:
#copy these 3 files to linux server:
#cp "$work_path/text.tok"
#cp "$work_path/$in-$src-$trg-1to1.tsv"
#cp "$work2_path/$trg-stm-num-lc.tsv"
#where "$trg-stm-num-lc.tsv"  was genereted in two steps: 
#1  Euramis corpora to $src.frq on linux, 
#2  $src.frq -> ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/test10k-en-hu.markup_frq.txt by C:/DGT/shared/nlp_installations/nlp_tools-2014-10-30/run/stmfrqdic.bat
#swith to linux and issue these commands:
#:preproc   "$work_path/$p-text.tok"  "$work_path/$p-text.tok.prec"
#:then create markup for moses test input :
#for inclusive/exclusive without probabilites:
#$perl_path/perl $script_path/iatexmarkup.pl "$work_path/$in-$src-$trg-1to1.tsv" "$work_path/$p-text.tok"  "$work_path/$in-$src-$trg.markup.txt"
#for exclusive with probabiilities:
#$perl_path/perl $script_path/iatexmarkupfrq.pl "$work_path/$in-$src-$trg-1to1.tsv" "$work2_path/$trg-stm-num-lc.tsv" "$work_path/$p-text.tok"  "$work_path/$in-$src-$trg.markup_frq.txt"

:skip_smtout

:skipiateconvert

#if NOT DEFINED CLEANUPSTEP goto skipcleanup
if $convert_tmpfiles==YES goto skipcleanup


rm "$work_path/$p-text.stm-*.sntngram"
rm "$work_path/$p-text.stm-*.vocab"
rm "$work_path/$p-text.stm-*.snt"
rm "$work_path/$p-text.stm-??.txt"
rm "$work_path/$p-textsrf.stm-*.sntngram"
rm "$work_path/$p-textsrf.stm-*.vocab"
rm "$work_path/$p-textsrf.stm-*.snt"
rm "$work_path/$p-textsrf.stm-??.txt"


#del "$work_path/$in-$src-$trg.tsv" 2>nul
rm "$work_path/$p-text.ana" 2>nul
rm "$work_path/$p-text.tok" 2>nul
rm "$work_path/$p-textl1.dtok" 2>nul
rm "$work_path/$p-text.stm" 2>nul
rm "$work_path/$p-text.txt" 2>nul


rm "$work_path/$p-found-src-$src-$trg.txt" 2>nul
rm "$work_path/$p-found_in_text-$src.txt" 2>nul
rm "$work_path/$p-not_in_text-$src-$trg.txt" 2>nul
rm "$work_path/$in-$src-$trg-list.tsv.stat" 2>nul
rm "$work_path/$in-preproc-$src-$trg.tsv" 2>nul
rm "$work_path/$in-$src-$trg.ordered.tsv" 2>nul
rm "$work_path/$in-$src-$trg.ordered.filt.tsv" 2>nul

rm "$work_path/$in-$src-$trg.tsv" 2>nul

rm "$work_path/$p-text.stm-$src.ng" 2>nul
rm "$work_path/$p-text.stm.ngf-$src.txt" 2>nul
rm "$work_path/$p-textsrf.stm.ng" 2>nul
rm "$work_path/$p-textsrf.stm.ngf-$src.txt" 2>nul

rm "$work_path/$p-found-srcnum-en-hu.txt" 2>nul
rm "$work_path/$p-found_notused.txt" 2>nul
rm "$work_path/$p-found_num.txt" 2>nul

rm "$work_path/$p-notfound_notused.txt" 2>nul
rm "$work_path/$p-text.num-$src.ng" 2>nul
rm "$work_path/$p-dic0-$src-$trg.tsv" 2>nul

rm "$work_path/$p-found-src-$src-morfvar.txt" 2>nul
rm "$work_path/$p-found-src-$src-morfvarq.txt" 2>nul
rm "$work_path/$p-header.tsv" 2>nul
rm "$work_path/$p-header0.tsv" 2>nul
rm "$work_path/$p-header1.tsv" 2>nul
rm "$work_path/$p-lookup-dic-$src-rev.tsv" 2>nul
rm "$work_path/$p-lookup-dic-$src.tsv" 2>nul
rm "$work_path/$p-text.dtok" 2>nul
rm "$work_path/$p-textsrf.stm" 2>nul
rm "$work_path/$p-textsrftab.stm.ng" 2>nul

rm "$work_path/$p-found-src-$src.txt" 2>nul
rm "$work_path/$p-found-srcnum-$src.txt" 2>nul
rm "$work_path/$p-foundintext-$src.txt" 2>nul
rm "$work_path/$p-not_in_text-$src.txt" 2>nul
rm "$work_path/$p-text-hfst.ana" 2>nul
rm "$work_path/$p-text-hfst.stm" 2>nul
rm "$work_path/$p-text-hsp.ana" 2>nul
rm "$work_path/$p-text-hsp.stm" 2>nul
rm "$work_path/$p-text.dtok1" 2>nul
rm "$work_path/$p-textnotag.txt" 2>nul
rm "$work_path/$p-textsrf-4hsp.stm" 2>nul
rm "$work_path/$p-textsrf.stm-$src.ng" 2>nul
rm "$work_path/$p-textsrftab.stm-$src.ng" 2>nul
rm "$work_path/$p-unknown-$src.stm" 2>nul

#dependent removes:
rm "$work_path/$p-found-srcnum-en-de.txt" 2>nul
rm "$work_path/$p-text-p.ana" 2>nul
rm "$work_path/$p-text-p.err" 2>nul
rm "$work_path/$p-text.ana.freq.stat" 2>nul
rm "$work_path/$p-text.ana.stat" 2>nul
rm "$work_path/$p-textl1.dtok" 2>nul


rm "$work_path/$p-text-4hsp.stm"  2>nul
rm "$work_path/$p-text-hsp.line.stm"  2>nul
rm "$work_path/$p-text.nosp.ana"  2>nul
rm "$work_path/$p-textsrftab.stm-en.caps.ng"  2>nul


if NOT DEFINED DICTGENRUN goto skipdictgenrun
rm "$work_path/dict-$src-$trg.ana" 2>nul
rm "$work_path/dict-$src-$trg.dtok" 2>nul
rm "$work_path/dict-$src-$trg.tok" 2>nul
rm "$work_path/dict-$src-$trg.tok1" 2>nul
rm "$work_path/dict.ord-$src-$trg.tsv" 2>nul
rm "$work_path/dict-$src-$trg.stm" 2>nul

rm "$work_path/dict-$src-4hsp.stm" 2>nul
rm "$work_path/dict-$src-hfst.ana" 2>nul
rm "$work_path/dict-$src-hfst.stm"  2>nul
rm "$work_path/dict-$src-hsp-srf.stm"  2>nul
rm "$work_path/dict-$src-hsp.ana" 2>nul
rm "$work_path/dict-$src.dtok" 2>nul
rm "$work_path/dict-$src.dtok1" 2>nul
rm "$work_path/dict-$src.stm" 2>nul
rm "$work_path/dict-$src.tok" 2>nul
rm "$work_path/dict.ord-$src.tsv" 2>nul
rm "$work_path/found_in_text-$trg.txt" 2>nul
rm "$work_path/$dict-$src-$trg.txt" 2>nul

rm "$work_path/dict-$src-hsp.line.stm" 2>nul
rm "$work_path/dict-$src-hsp.stm" 2>nul
rm "$work_path/dict-$src.nosp.ana" 2>nul
rm "$work_path/dict-$src.tok1" 2>nul

:skipdictgenrun

if NOT $convert_stat==YES goto skipstatdel
#delete temporary files created during statistics generation
rm "$work_path/$p-foundintext-$src-$trg-$src.txt" 2>nul
rm "$work_path/$in-$src-term.txt" 2>nul
rm "$work_path/$in-$src-term.unq" 2>nul
rm "$work_path/$in-$src.tsv" 2>nul
rm "$work_path/$in-$trg-term.txt" 2>nul
rm "$work_path/$in-$trg-term.unq" 2>nul
rm "$work_path/$in-$trg.tsv" 2>nul
rm "$work_path/$p-foundintext-$src-$trg-$src.len.stat" 2>nul
rm "$work_path/$p-found-srcnum-$src-$trg.txt"2>nul
rm "$work_path/$in-$src-$trg.stat"
:skipstatdel

:skipcleanup

if NOT $convert_log==YES goto skiplogdel
rm "$work_path/$p-compare_sar.log" 2>nul
rm "$work_path/$in-$src-$trg-list.tsv.log" 2>nul
:skiplogdel



goto end
:terminate
echo Usage: IATEdocgloss.bat src trg input
echo where src and trg are the two letter language ID-s
echo input is the input document with full path name 
echo use double quotes for input file names with space or special characters
:end

