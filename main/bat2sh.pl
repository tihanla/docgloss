#converts  windows batch files into unix shell scripts:

use strict;

while(<>)
	{

	#/ec/dgt/local/exodus/user/tihanla/IATEdocgloss
	s/C:\\PGM\\DGTApps\\IATEdocgloss/ec\/dgt\/local\/exodus\/user\/tihanla\/IATEdocgloss/i;

	#perl
	s/C:\\Pgm\\DGTapps\\Perl516\\bin/\/ec\/dgt\/shared\/exodus\/local\/perl\/bin/i;

	#replace comments:
	s/^rem /#/;

	#replace set with nothing
	s/^set //;

	#replace variable names:
	s/\%([^\%]+)\%/\$$1/g;

	#replace path \ with /
	s/\\/\//g;

	#replace del with  rm
	s/^del /rm /g;

	#replace copy to cp
	s/^copy /cp /g;

	#replace copy to cp
	s/^xcopy \/s /cp \/s /g;

	#call to source
	s/^call /source /g;


	print $_;
	}
	