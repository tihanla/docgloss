#Add terms to text in Moses XML-markup format.
#Used extrnal resources: 
#IATE dictionary: en-*, *-en directions
#Morphologies:  24 EU official languages, HFST and Hunspell dictionaries
#corpora:  24 EU official languages, training sets form Euramis
#markups inserted into morphological varians and translations also contain variant with probabilities calculated from 

set -x

src=$1
trg=$2
workpath=$3
input=$4
project=$5
reference=$6

#src=hu
#trg=en
#workpath=/ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/markupx-hu-en
#input=/ec/dgt/local/exodus/user/tihanla/data/testset/test10k.en.txt
#name=test10k

mkdir $workpath

# 1. create mini termbase from IATE with term of the input, and morphological variants of the source terms
#input:$project $src $trg 
#output: $project-$src-$trg-1to1.tsv
#resources:
#test:
#./IATEdocgloss.sh de en /ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/markupx-de-en /ec/dgt/local/exodus/user/tihanla/data/testset/test.de.txt test10k
nohup ./IATEdocgloss.sh $src $trg $workpath $input $project

# 2.a create target side frequency list from the training corpus
frqdicpath=/ec/dgt/local/exodus/user/tihanla/hhi-euramis-stat
if [$frqdicpath/$trg.zip]
	#skip generation of new freq lists and use existing ones:
	#test:
	#gunzip -c /ec/dgt/local/exodus/user/tihanla/hhi-euramis-stat/en.zip  > /ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/markupx-de-en/en.frq
	gunzip -c /ec/dgt/local/exodus/user/tihanla/hhi-euramis-stat/$trg.zip  > $workpath/$trg.frq
else
	#input: use language model training corpora which are located on local drives of 4 servers
	#copy freqmaven.pl script to the appropriate SERVER-s, run, zip and copy results back to acubens:
	#input:monolingual.$trg.monolingual4lm.rc
	#output:/ec/dgt/local/exodus/user/tihanla/hhi-euramis-stat/$trg.frq
	#script: /ec/dgt/local/exodus/user/tihanla/hhi-euramis-stat/freqmaven.pl  
	#example usage: 
	perl freqmaven.pl /ec/dgt/local/exodus-data01/trainings/build_2014_0003/hr-lm_2014-12-10_15:40:40/prepared-corpus/monolingual.hr.monolingual4lm.rc hr.frq 
	#calls on the servers were done by ./scripts/freq_SERVER.sh scripts
fi

# 2.b create stemmed frequency lists
#in: $trg.frq
#out:$trg-stm-num.tsv
#out:$trg-stm-num.lc.tsv
#test:
#./stmfrqdic.sh en /ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/markupx-de-en /ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/markupx-de-en en.frq 
#nohup ./stmfrqdic.sh $trg $workpath $workpath $trg.frq 

# 3. tag input text with XML-markups containing the term pairs and the morphological alternatives/probabilities of target terms
#  (also contains tokenization step of translation )
#  + translate and test (evaluate blue score)
# inputs: 
# $project.$src.txt: input text to be translated
# $project.$trg.txt:  reference translation
# $project.$src-$trg-1to1.tsv: mini termbase with the terms in the text 
# $trg-stm-num-lc.tsv:  frequency information of word foems with stems of a big (the training) corpus
# outputs:
# :translation
# :evaluation blue-score  
# resources: trained $src-$trg moses engine
#test
#./iate_docgloss_trans.sh de en /ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/markupx-de-en /ec/dgt/local/exodus/user/tihanla/data/testset/test.de.txt test10k
./iate_docgloss_trans.sh $src $trg $workpath $input $project $reference



