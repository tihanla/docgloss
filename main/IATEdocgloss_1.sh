#using 

set -x

if [ ! "$#" == 5  ]; then
	echo Usage: IATEdocgloss.sh src trg work_path input outname
	echo where src and trg are the two letter language ID-s
	echo input is the input document with full path name 
	echo use double quotes for input file names with space or special characters
	exit
fi

TEXTGENSTEP=1
DICTGENSTEP=1
ADDFORMSTEP=1
FILTERSTEP=1
#STATSTEP=1
USEDICTSTEP=1
#

#if [ "$DEBUG" ]; then
#fi #DEBUG

# values: Python, C, Java
STEMMING=C
# STEMMING=Python
# STEMMING=Java

lookupform=NO
#TODO:
source "%~p0/iatedocgloss_sys_cfg.bat"

if [ -z "$perl_path"} ]; then 
	echo perl_path undifined 
	exit
fi
if [ -z "$docgloss_path"} ]; then 
	echo docgloss_path undifined 
	exit
fi
if [ -z "$work_path"} ]; then 
	echo work_path undifined 
	exit
fi
if [ -z "$out_path"} ]; then 
	echo out_path undifined 
	exit
fi
if [ -z "$iate_dict"} ]; then 
	echo iate_dict undifined 
	exit
fi


#old source sys_cfg.sh
#old source $iateconf_path/$iate_cfg


#TODO:
src=%1
trg=%2
in_fullname=%~3
in_path=%~dp3
in=%~n3
in_ext=%~x3
#truncate the trailing backlash: 
in_path=$in_path:~0,-1


if $trg==da goto old
if $trg==de goto old
if $trg==el goto old
if $trg==es goto old
if $trg==fi goto old
if $trg==fr goto old
if $trg==it goto old
if $trg==nl goto old
if $trg==pt goto old
if $trg==sv goto old
cfg-dgt=$iateconf_path/cfg-dgtnew.bat
goto skip_old
:old
cfg-dgt=$iateconf_path/cfg-dgtold.bat
:skip_old

source $cfg-dgt

source $iateconf_path/dcfg-form.bat  

#if NOT $src==en goto setlookend
#if $langroup==old goto setlookold
#set lus=-eu13
#goto setlookend
#:setlookold
#rem look-up  specifyer:
#set lus=-prim
#:setlookend

#if no surface print only the first ana:
if NOT DEFINED srf (
firstana=-first
)

#delete zero size txt files from target folder
filemask="$work_path/*.txt"
for %$A in (filemask$) do if $~zA==0 del "%A"

filemask="$work_path/*.tsv"
for %$A in (filemask$) do if $~zA==0 del "%A"

if $use_prefix==YES set p=$in

#goto jump


# --------------------------------------------------------------------
if [ "$TEXTGENSTEP" ]; then 


echo Get stemmed phrases from source document

#TODO
# if skip if we alread parsed the text
#if EXIST "$work_path/textsrf.stm-$src.ng" GOTO skiptextgen


# rm "$work_path/text.txt"  
# extension is doc or docx:
cp $infile "$work_path/text.txt"  

orig_path="$(pwd)"

cd $tok_path
# deletion of simple XML tags (without attributes):
$perldir/perl $script_path/deltag.pl < "$work_path/text.txt" >  "$work_path/textnotag.txt"
echo $perldir/perl $script_path/tokenizer.perl -l $src -q  "$work_path/textnotag.txt"   "$work_path/text.tok"
$perldir/perl $script_path/tokenizer.perl -l $src -q < "$work_path/textnotag.txt" >  "$work_path/text.tok"
cd $orig_path


# C HFST analyser:
if [ "$STEMMING" == "C" ]; then
	$perldir/perl $script_path/destxt.pl  -lang=$src "$work_path/text.tok"  "$work_path/text.dtok"
	cd $work_path
	$hfst_prg_path/hfst-proc --apertium --dictionary-case  $hfst_dic_path/$src.ana.hfstol $work_path/text.dtok > $work_path/text-hfst.$src.ana
	cd $orig_path
	#:skip_c
fi


# Python HFST  analyser:
if [ "$STEMMING" == "Python" ]; then
	$perldir/perl -pe "s/\n/ _eol\n/;s/^[ ]+\n/\n/;s/[ ]+/\n/g;s/^\n//"  "$work_path/text.tok" > "$work_path/textl1.dtok"
	# $perldir/perl -pe "s/^[ ]+\n/\n/;s/[ ]+/\n/g;s/^\n//"  $work_path/text.tok > $work_path/textl1.dtok
	$python_path/python $script_path/hfst_lookup.py $hfst_dic_path/$src.ana.hfstol   <"$work_path/textl1.dtok"   > "$work_path/text-p.ana" 2>"$work_path/text-p.err"
	$perldir/perl $script_path/hfstp2txt.pl  "$work_path/text-p.ana" "$work_path/text-hfst.ana" "$work_path/text.hfst-ana.stat"
	if [ "$STATSTEP" ]; then
	$prg_path/sort  -f "$work_path/text.hfst-ana.stat" | uniq   -c | $prg_path/sort  -gr  > "$work_path/text.hfst-ana.freq.stat"
	#skipstats2
	fi
	#skip_python
fi

# Java HFST analyser:
if [ "$STEMMING" == "Java" ]; then
	$perldir/perl -pe "s/[ ]+/\n/g"  "$work_path/text.tok" > "$work_path/textl1.dtok"
	$java_path/java -jar $script_path/hfst-ol.jar "$hfst_dic_path/$src.ana.hfstol"   <"$work_path/textl1.dtok"   > "$work_path/text-hfst-j.ana"
	# TODO hfstj2txt.pl text-hfst-j.ana text-hfst.ana
	:skip_java
fi



# delete the last space in line
$perldir/perl $script_path/fixhfstana.pl "$work_path/text-hfst.$src.ana"  > "$work_path/text.nosp.$src.ana"
# check:
# grep " "  $work_path/$src.nosp.$src.ana > $work_path/$src.sp.$src.err
# stem:
$perldir/perl $script_path/hfst-stem.pl  -lang=$src -udic=$hfst_dic_path/hfst-$src.udic -srf  -first < "$work_path/text.nosp.$src.ana"  > "$work_path/text-hfst.$src.stm"
# replace per (/) -> tab
$perldir/perl $script_path/prep4hunsp.pl   "$work_path/text-hfst.$src.stm"  "$work_path/text-4hsp.$src.stm"  


# Hunspell***
# analyse:
# updated and re-complile /ec/dgt/local/exodus/user/tihanla/hunspell-1.3.3/src/tools/analyze.cxx  <-analyze_dgt.cxx
$prg_path_hunspell/analyze_dgt a $dict_path_hunspell/$src.aff $dict_path_hunspell/$src.dic  "$work_path/text-4hsp.$src.stm"  >"$work_path/text-hsp.$src.ana"



# Hunspell stem:  
$perldir/perl $script_path/hunspell-stem.pl  $src  "$work_path/text-hsp.$src.ana"  "$work_path/text-hsp.line.$src.stm"

# restore lines:
$perldir/perl $script_path/restoreline.pl "$work_path/text-hsp.line.$src.stm"  > "$work_path/text-hsp.$src.stm"


# statistics on unknown words:
#if NOT DEFINED STATSTEP goto skipstats3
#  $perldir/perl $script_path/prep4hunspell.pl  "$work_path/text-hsp.stm"  "$work_path/unknown-$src.stm"
#  $perldir/perl -pe "s/^\n//"  "$work_path/unknown-$src.stm" > "$work_path/unknown-$src.err"
#  $prg_path/sort  -f "$work_path/unknown-$src.err" | uniq   -c | $prg_path/sort  -gr  > "$work_path/unknown-$src.err.stat"
#:skipstats3


# connect with old system:
cp "$work_path/text-hsp.$src.stm"  "$work_path/textsrf.stm" 
$perldir/perl $script_path/cleanstemmed.pl   "$work_path/text-hsp.$src.stm"   > "$work_path/text.stm"



orig_path="$(pwd)"
cd  $script_path

# stem suffix array to creat phrases to be looked up in the dictionary, also its frequency infois added to the dictionary:
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 1 --token $script_path/satoken.txt "$work_path/text.stm-01.txt" "$work_path/text.stm"

$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 2 --token $script_path/satoken.txt "$work_path/text.stm-02.txt" "$work_path/text.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 3 --token $script_path/satoken.txt "$work_path/text.stm-03.txt" "$work_path/text.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 4 --token $script_path/satoken.txt "$work_path/text.stm-04.txt" "$work_path/text.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 5 --token $script_path/satoken.txt "$work_path/text.stm-05.txt" "$work_path/text.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 6 --token $script_path/satoken.txt "$work_path/text.stm-06.txt" "$work_path/text.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 7 --token $script_path/satoken.txt "$work_path/text.stm-07.txt" "$work_path/text.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 8 --token $script_path/satoken.txt "$work_path/text.stm-08.txt" "$work_path/text.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 9 --token $script_path/satoken.txt "$work_path/text.stm-09.txt" "$work_path/text.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 10 --token $script_path/satoken.txt "$work_path/text.stm-10.txt" "$work_path/text.stm"
cat $work_path/text.stm-*.txt  > $work_path/text.stm.ngf-$src.txt
$perldir/perl -pe "s/ [0-9]+ +\n/\n/" "$work_path/text.stm.ngf-$src.txt" > "$work_path/text.stm-$src.ng"
$perldir/perl -pe "s/^[^\n]+ ([0-9]+) +\n/\1\n/" "$work_path/text.stm.ngf-$src.txt" > "$work_path/text.num-$src.ng"


# morphological variants: create surface=stem pairs by suffix array using the stemmed suffix array
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 1 --token $script_path/satoken.txt "$work_path/textsrf.stm-01.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 2 --token $script_path/satoken.txt "$work_path/textsrf.stm-02.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 3 --token $script_path/satoken.txt "$work_path/textsrf.stm-03.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 4 --token $script_path/satoken.txt "$work_path/textsrf.stm-04.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 5 --token $script_path/satoken.txt "$work_path/textsrf.stm-05.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 6 --token $script_path/satoken.txt "$work_path/textsrf.stm-06.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 7 --token $script_path/satoken.txt "$work_path/textsrf.stm-07.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 8 --token $script_path/satoken.txt "$work_path/textsrf.stm-08.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 9 --token $script_path/satoken.txt "$work_path/textsrf.stm-09.txt" "$work_path/textsrf.stm"
$perldir/perl $script_path/array-suffix-driver.pl --newLine --ngram 10 --token $script_path/satoken.txt "$work_path/textsrf.stm-10.txt" "$work_path/textsrf.stm"


cat $work_path/textsrf.stm-*.txt > $work_path/textsrf.stm.ngf-$src.txt
# delete lines with numbers, delete * unknown word markers



$perldir/perl -pe "s/ [0-9]+ +\n/\n/"   "$work_path/textsrf.stm.ngf-$src.txt" > "$work_path/textsrf.stm-$src.ng"
# convert / to tab


$perldir/perl $script_path/textsrf2tab.pl  "$work_path/textsrf.stm-$src.ng" > "$work_path/textsrftab.stm-$src.caps.ng"
$perldir/perl $script_path/filter_upperterm.pl   "$work_path/textsrftab.stm-$src.caps.ng"  "$work_path/textsrftab.stm-$src.ng"

# debug:
# $perldir/perl $script_path/textsrf2tab.pl  "$work_path/textsrf.stm-$src.ng" > "$work_path/textsrftab.stm-$src.ng"
# sort "$work_path/textsrftab.stm-$src.caps.ng" > "$work_path/textsrftab.stm-$src.caps.ng.srt"
# sort "$work_path/textsrftab.stm-$src.ng" > "$work_path/textsrftab.stm-$src.ng.srt"

# :not needed: $perldir/perl -pe "s/^[^\n]+ ([0-9]+) +\n/\1\n/" $work_path/textsrf.stm.ngf-$src.txt > $work_path/textsrf.num.ng

cd $orig_path


rm $work_path/text.stm-*.sntngram.
rm $work_path/text.stm-*.vocab.
rm $work_path/text.stm-*.snt.
rm $work_path/text.stm-??.txt
rm $work_path/textsrf.stm-*.sntngram.
rm $work_path/textsrf.stm-*.vocab.
rm $work_path/textsrf.stm-*.snt.
rm $work_path/textsrf.stm-??.txt
#rm $work_path/textsrftab.stm-$src.caps.ng.srt
#rm $work_path/textsrftab.stm-$src.caps.ng



fi #:skiptextgen

# --------------------------------------------------------------------
if [ "$DICTGENSTEP" ]; then
#echo Create a full stemmed IATE list for the source language 

#if EXIST $hfst_dic_path/dict-$src.stm GOTO skipdictgen
# :dictgen

DICTGENRUN=1

$perldir/perl $script_path/gettermcol.pl  $src $langpos $termpos "$iate_dic_path/$dicin"   "$work_path/dict.ord-$src.tsv" 


# Tokenization

orig_path="$(pwd)"
cd $script_path
$perldir/perl $script_path/tokenizer.perl -l $src -q < "$work_path/dict.ord-$src.tsv"  >  "$work_path/dict-$src.tok" 
cd $orig_path


#IATE specific workaround, not needed for other dictionaries:
if [ "$src" == "en" ]; then
$perldir/perl -pe "s/^to //" "$work_path/dict-$src.tok" > "$work_path/dict-$src.tok1"
read -p "stop"
cp "$work_path/dict-$src.tok1" "$work_path/dict-$src.tok"  
fi




# C HFST analyser:
if [ "$STEMMING" == "C" ]; then
	$perldir/perl $script_path/destxt.pl  -lang=$src  "$work_path/dict-$src.tok"  "$work_path/dict-$src.dtok"
	cd $work_path
	$hfst_prg_path/hfst-proc --apertium --dictionary-case  "$hfst_dic_path/$src.ana.hfstol" "dict-$src.dtok" > "dict-$src-hfst.ana"
	cd $orig_path
	#:skip_c2
fi
#:skipentok


$perldir/perl $script_path/fixhfstana.pl  "$work_path/dict-$src-hfst.ana"  > "$work_path/dict-$src-nosp.ana"





# HFST stem:
$perldir/perl $script_path/hfst-stem.pl  -lang=$src -udic=$hfst_dic_path/hfst-$src.udic -srf  -first < "$work_path/dict-$src-nosp.ana"  > "$work_path/dict-$src-hfst.stm"



# replace per (/) -> tab
$perldir/perl $script_path/prep4hunsp.pl   "$work_path/dict-$src-hfst.stm"  "$work_path/dict-$src-4hsp.stm"  
# Hunspell
$prg_path_hunspell/analyze_dgt a $dict_path_hunspell/$src.aff $dict_path_hunspell/$src.dic  "$work_path/dict-$src-4hsp.stm"  >"$work_path/dict-$src-hsp.ana"
# Hunspell stem:  
$perldir/perl $script_path/hunspell-stem.pl  $src  "$work_path/dict-$src-hsp.ana"  "$work_path/dict-$src-hsp.line.stm"

# restore lines:
$perldir/perl $script_path/restoreline.pl "$work_path/dict-$src-hsp.line.stm"  > "$work_path/dict-$src-hsp.stm"


# :for development purposes first we create it in a local folder:
$perldir/perl $script_path/cleanstemmed.pl  "$work_path/dict-$src-hsp.stm" >  "$work_path/dict-$src.stm"


cp  "$work_path/dict-$src.stm"  "$hfst_dic_path/dict-$src.stm" 


# statistics:
if [ "$STATSTEP" ]; then
	$perldir/perl -pe "s/^\n//"  "$work_path/dict-$src-hsp.ana" >"$work_path/dic-$src-ana.txt"
	$perldir/perl -pe "s/[\t ]+/\n/g" "$work_path/dic-$src-ana.txt" > "$work_path/dic-$src-ana1.tmp"
	grep "^fl:" "$work_path/dic-$src-ana1.tmp" > "$work_path/dic-$src-ana2.tmp"
	$perldir/perl -pe "s/fl://" "$work_path/dic-$src-ana2.tmp" >"$work_path/dic-$src-ana3.tmp"
	$prg_path/sort  -f "$work_path/dic-$src-ana3.tmp" | uniq   -c | $prg_path/sort  -gr  >"$work_path/dic-$src-ana-fl.stat"

	grep "^..:" "$work_path/dic-$src-ana1.tmp" > "$work_path/dic-$src-ana10.tmp"
	$perldir/perl -pe "s/^(..):[^\n]+/\1/" "$work_path/dic-$src-ana10.tmp" > "$work_path/dic-$src-ana11.tmp"
	$prg_path/sort  -f "$work_path/dic-$src-ana11.tmp" | uniq   -c | $prg_path/sort -gr  >"$work_path/dic-$src-ana-mcat.stat"
	:skipstats4
fi

fi #:skipdictgen

# --------------------------------------------------------------------

if [ $ADDFORMSTEP ]; then 
#echo Add morphological forms to source text terms 

#if EXIST "$work_path/found-srcnum-$src.txt" GOTO skip_addform

$perldir/perl $script_path/header.pl  <"$iate_dic_path/$dicin" >  "$work_path/header0.tsv"
$perldir/perl -pe "s/\t(TL_INST)/\t\1\tTL_FORM/" "$work_path/header0.tsv" > "$work_path/header1.tsv"
$perldir/perl -pe "s/^(([^\t]*\t){17}[^\t\n]*)\n/\1\t\n/i" "$work_path/header1.tsv" > "$work_path/header.tsv"


# FILTER IATE SOURCE TERMS with DOCUMENT SOURCE TERMS  [and generate list of morphological variants]
#                                          in:file1=stemmed_text           in:file2=src_keys_of_stemmed_dict  in:file2_dic              out=FOUND_IN_DICT                      out=NOT_FOUND_IN_DIC                out=FOUND_DIC_TERMS               [in=morph variant dict               in=id    in=lang   in=primary   in=term    out=FOUND_DIC_MORPHVAR]                     LOG



$perldir/perl $script_path/compare_sar.pl  "$work_path/text.stm-$src.ng" "$hfst_dic_path/dict-$src.stm"  "$iate_dic_path/$dicin"  "$work_path/found_in_text-$src.txt"  "$work_path/not_in_text-$src.txt" "$work_path/found-src-$src.txt" "$work_path/textsrftab.stm-$src.ng" $idpos $langpos $primarypos $termpos  "$work_path/found-src-$src-morfvar.txt" > "$work_path/compare_sar.log"


$perldir/perl -pe "s/^([^\t]+)\t([^\t]+)\t+([^\t]+)\t+\3\n//" "$work_path/found-src-$src-morfvar.txt" > "$work_path/found-src-$src-morfvarq.txt"

# add frequency info to found_dict:
$perldir/perl $script_path/linehash.pl  "$work_path/found_in_text-$src.txt"  "$work_path/text.stm-$src.ng"  "$work_path/text.num-$src.ng" "$work_path/found_num.txt" "$work_path/found_notused.txt" "$work_path/notfound_notused.txt"

# add vertical: dictionary and morphvar form info
$perldir/perl $script_path/adlinesbytab.pl "$work_path/found-src-$src.txt"  "$work_path/found_num.txt"  "$work_path/found-srcnum-$src.txt"


# Get lookupforms from lookupform dictionary: 
if [ "$lookupform" == "YES" ]; then 
	$perldir/perl $script_path/getidlangline.pl  $termpos "$work_path/found-srcnum-$src.txt"  $termpos $langpos notused $src   "$hfst_dic_path/lookup-$src$lus.tsv" "$work_path/lookup-dic-$src-rev.tsv" 0	
	$perldir/perl $script_path/ordertermcol.pl  "$work_path/lookup-dic-$src-rev.tsv" 1-2-3-4-5-6-7-8-9-20-11-12-13-14-15-16-17-18-19-10 "$work_path/lookup-dic-$src.tsv"
	#skip_lookupform
else
	touch "$work_path/lookup-dic-$src.tsv"
fi

fi #:skip_addform

# ---------------------------
if [ "$FILTERSTEP" ]; then
#echo Filter document terms with IATE terms

# Get target terms based upon source term id-s
$perldir/perl $script_path/getidlangline.pl  $idpos  "$work_path/found-srcnum-$src.txt"  $idpos $langpos $src $trg "$iate_dic_path/$dicin" "$work_path/dic0-$src-$trg.tsv" 1


# copy together:  header       +src                                     + lookupform                         +trg                              + morf variants
cat "$work_path/header.tsv"  "$work_path/found-srcnum-$src.txt"   "$work_path/lookup-dic-$src.tsv" "$work_path/dic0-$src-$trg.tsv"  "$work_path/found-src-$src-morfvarq.txt" > "$work_path/$in-$src-$trg.tsv" 

fi #:skipfilter

# ---------------------------
if [ "$STATSTEP" ]; then
#echo Create statistics

# STATISICS:
# OLD grep "^[^\n]" "$work_path/found_in_text-$src.txt" > "$work_path/foundintext-$src.txt"
$perldir/perl -pe "s/^\n//"  "$work_path/found_in_text-$src.txt" > "$work_path/foundintext-$src.txt"
$perldir/perl $script_path/cntdelim.pl  "$work_path/foundin{text-$src.txt" "$work_path/foundintext-$src.tmp" " "
$prg_path/sort  -f "$work_path/foundintext-$src.tmp" | uniq   -c | $prg_path/sort  -gr  >"$work_path/foundintext-$src.len.stat"
rm "$work_path/foundintext-$src.tmp"

echo +++DGT recognition+++ > "$work_path/$in-$src-$trg.stat"
$perldir/perl $script_path/getlangline.pl $src $langpos "$work_path/$in-$src-$trg.tsv" "$work_path/$in-$src.tsv" -nohead
$perldir/perl $script_path/getlangline.pl $trg $langpos "$work_path/$in-$src-$trg.tsv" "$work_path/$in-$trg.tsv" -nohead
$perldir/perl $script_path/ordertermcol.pl  "$work_path/$in-$src.tsv" $termpos "$work_path/$in-$src-term.txt"
$perldir/perl $script_path/ordertermcol.pl  "$work_path/$in-$trg.tsv" $termpos "$work_path/$in-$trg-term.txt"

echo source terms: >> "$work_path/$in-$src-$trg.stat"
wc -l  "$work_path/$in-$src-term.txt" >> "$work_path/$in-$src-$trg.stat"
echo uniq source terms: >> "$work_path/$in-$src-$trg.stat"
sort  -u   "$work_path/$in-$src-term.txt" > "$work_path/$in-$src-term.unq"
wc -l  "$work_path/$in-$src-term.unq" >> "$work_path/$in-$src-$trg.stat"

echo target terms: >> "$work_path/$in-$src-$trg.stat"
wc -l  "$work_path/$in-$trg-term.txt" >> "$work_path/$in-$src-$trg.stat"
echo uniq target terms: >> "$work_path/$in-$src-$trg.stat"
$prg_path/sort -u   "$work_path/$in-$trg-term.txt" > "$work_path/$in-$trg-term.unq"
wc -l  "$work_path/$in-$trg-term.unq" >> "$work_path/$in-$src-$trg.stat"

echo source terms lenghts: >> "$work_path/$in-$src-$trg.stat"
cat   "$work_path/$in-$src-$trg.stat"  "$work_path/foundintext-$src.len.stat" > "$work_path/$in-$src-$trg.stat"

#:skipstats5
fi #STATSTEP

# --------------------------------------------------------------------

if [ "$USEDICTSTEP" ]; then 
echo Generate SDLTB dictionary

if [ "$out_format_gloss" == "SDLTB_OUT" ]; then
	source $iateconf_path/iate_convert.sh $src $trg TXT SDLTB list $iateconf_path/$iate_cfg "$work_path/$in-$src-$trg.tsv" "$work_path" "$in-$src-$trg"
fi 

if [ "$out_format_gloss" == "SMT_OUT" ]; then
	# :perl filters.pl input output filtered [-f_prim=N] [-f_pifs=N] [-f_evaluation=N] [-f_reliability=N] [-f_reliabilitylang=N] [-f_language=ID]\n";

	$perldir/perl $script_path/filters.pl  "$work_path/$in-$src-$trg.tsv"  "$work_path/$in-$src-$trg-fil.tsv" "$work_path/$in-$src-$trg-filtered.log"  -f_evaluation=$evalpos -f_reliability=$relypos
	$perldir/perl $script_path/ordertermcol.pl "$work_path/$in-$src-$trg.tsv" 1-2-11  "$work_path/$in-$src-$trg-ordered-1to1.tsv"
	$perldir/perl $script_path/filter_1trg.pl $src $trg "$work_path/$in-$src-$trg-ordered-1to1.tsv" "$work_path/$in-$src-$trg-1to1.tsv" "$work_path/$in-$src-$trg-1to1_skip.log" 

fi

fi #:usedictstep

# --------------------------------------------------------------------




if [ "$CLEANUPSTEP" ]; then
rm "$work_path/$in-$src-$trg.tsv" 
rm "$work_path/text.ana" 
rm "$work_path/text.tok" 
rm "$work_path/textl1.dtok" 
rm "$work_path/text.stm" 
rm "$work_path/text.txt" 
rm "$work_path/compare_sar.log" 


rm "$work_path/found-src-$src-$trg.txt" 
rm "$work_path/found_in_text-$src.txt" 
rm "$work_path/not_in_text-$src-$trg.txt" 
rm "$work_path/$in-$src-$trg-list.tsv.log" 
rm "$work_path/$in-$src-$trg-list.tsv.stat" 
rm "$work_path/$in-preproc-$src-$trg.tsv" 
rm "$work_path/$in-$src-$trg.ordered.tsv" 
rm "$work_path/$in-$src-$trg.ordered.filt.tsv" 

rm "$work_path/text.stm-$src.ng" 
rm "$work_path/text.stm.ngf-$src.txt" 
rm "$work_path/textsrf.stm.ng" 
rm "$work_path/textsrf.stm.ngf-$src.txt" 

rm "$work_path/found-srcnum-en-hu.txt" 
rm "$work_path/found_notused.txt" 
rm "$work_path/found_num.txt" 

rm "$work_path/notfound_notused.txt" 
rm "$work_path/text.num-$src.ng" 
rm "$work_path/dic0-$src-$trg.tsv" 

rm "$work_path/found-src-$src-morfvar.txt" 
rm "$work_path/found-src-$src-morfvarq.txt" 
rm "$work_path/header.tsv" 
rm "$work_path/header0.tsv" 
rm "$work_path/header1.tsv" 
rm "$work_path/lookup-dic-$src-rev.tsv" 
rm "$work_path/lookup-dic-$src.tsv" 
rm "$work_path/text.dtok" 
rm "$work_path/textsrf.stm" 
rm "$work_path/textsrftab.stm.ng" 

rm "$work_path/found-src-$src.txt" 
rm "$work_path/found-srcnum-$src.txt" 
rm "$work_path/foundintext-$src.txt" 
rm "$work_path/not_in_text-$src.txt" 
rm "$work_path/text-hfst.ana" 
rm "$work_path/text-hfst.stm" 
rm "$work_path/text-hsp.ana" 
rm "$work_path/text-hsp.stm" 
rm "$work_path/text.dtok1" 
rm "$work_path/textnotag.txt" 
rm "$work_path/textsrf-4hsp.stm" 
rm "$work_path/textsrf.stm-$src.ng" 
rm "$work_path/textsrftab.stm-$src.ng" 
rm "$work_path/unknown-$src.stm" 

# dependent removes:
rm "$work_path/found-srcnum-en-de.txt" 
rm "$work_path/text-p.ana" 
rm "$work_path/text-p.err" 
rm "$work_path/text.ana.freq.stat" 
rm "$work_path/text.ana.stat" 
rm "$work_path/textl1.dtok" 


if [ $DICTGENRUN ]; then
rm "$work_path/dict-$src-$trg.ana" 
rm "$work_path/dict-$src-$trg.dtok" 
rm "$work_path/dict-$src-$trg.tok" 
rm "$work_path/dict-$src-$trg.tok1" 
rm "$work_path/dict.ord-$src-$trg.tsv" 
rm "$work_path/dict-$src-$trg.stm" 

rm "$work_path/dict-$src-4hsp.stm"
rm "$work_path/dict-$src-hfst.ana"
rm "$work_path/dict-$src-hfst.stm"  
rm "$work_path/dict-$src-hsp-srf.stm"
rm "$work_path/dict-$src-hsp.ana"
rm "$work_path/dict-$src.dtok"
rm "$work_path/dict-$src.dtok1"
rm "$work_path/dict-$src.stm"
rm "$work_path/dict-$src.tok"
rm "$work_path/dict.ord-$src.tsv"
rm "$work_path/found_in_text-$trg.txt"
#:skipdictgenrun
fi               #DICGENRUN	

if [ "$STATSTEP" ]; then
# delete temporary files created during statistics generation
# not to generate stats and logs files, STATSTEP=0
rm "$work_path/foundintext-$src-$trg-$src.txt" 
rm "$work_path/$in-$src-term.txt" 
rm "$work_path/$in-$src-term.unq" 
rm "$work_path/$in-$src.tsv" 
rm "$work_path/$in-$trg-term.txt" 
rm "$work_path/$in-$trg-term.unq" 
rm "$work_path/$in-$trg.tsv" 
rm "$work_path/foundintext-$src-$trg-$src.len.stat" 
rm "$work_path/found-srcnum-$src-$trg.txt"
# rm "$work_path/$in-$src-$trg.stat"
#:skipstatdel
#:skipcleanup
fi #STATSTEP

fi   #CLEANUPSTEP

