rem Conversion of the .TXT extraction. This format is exported from IATE with database management software.
rem call C:\Pgm\DGTapps\IATEconvert\iate_convert.bat en hu TXT SDLTB list C:\Pgm\DGTapps\IATEconvert\dgt-txt_base_cfg.bat C:\Pgm\DGTapps\IATEconvert\work\sample100.txt  C:\Pgm\DGTapps\IATEconvert\work sample100

set convert_zip=YES
set convert_stat=YES
set convert_log=YES
set convert_tmpfiles=NO
set convert_tbxid=YES

:headword grouped entries 
:call iate_convert_headkey.bat en hu TXT SDLTB h .\dcfg-base.bat  .\cfg-dgtnew.bat .\work\sample1000.txt .\work sample1000

:id grouped entries 
call iate_convert.bat en hu TXT SDLTB l .\dcfg-base.bat  .\cfg-dgtnew.bat .\work\sample1000.txt .\work sample1000



