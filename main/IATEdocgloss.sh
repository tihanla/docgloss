#set -x 

if [ ! "$#" == 3  ]; then
	echo Usage: IATEdocgloss.sh src trg input
	echo where src and trg are the two letter language ID-s
	echo input is the input document with full path name 
	exit
fi

TEXTGENSTEP=YES
DICTGENSTEP=YES
ADDFORMSTEP=YES
FILTERSTEP=YES
IATECONVERTSTEP=YES
                                               


#values: Python, C, Java
STEMMING=C
#set STEMMING=Python
#set STEMMING=Java

lookupform=no

mypath=$(dirname "$0")
#echo mypath=$mypath

source "$mypath/iatedocgloss_sys_cfg.sh"

                                                            
if [ -z "$perl_path" ]; then                                 
	echo perl_path undifined                                                                        
	exit
fi
if [ -z "$docgloss_path" ]; then 
	echo docgloss_path undifined 
	exit
fi
if [ -z "$work_path" ]; then 
	echo work_path undifined 
	exit
fi
if [ -z "$out_path" ]; then 
	echo out_path undifined 
	exit
fi
if [ -z "$iate_dict" ]; then 
	echo iate_dict undifined 
	exit
fi

#set iate_cfg=dgt-txt_form_2015-04-30_oldlng_cfg.bat
#obsolete call $iateconf_path/$iate_cfg

src=$1
trg=$2
in_fullname=$3




#echo in_fullname=$in_fullname

#path without trailing / :
in_path=$(dirname "$3")
#echo in_path=$in_path

inname=$(basename "$3")
#echo inname=$inname

in=`echo $inname| cut -f1 -d'.'`
#echo in=$in

in_ext=`rev <<< "$inname" | cut -d"." -f1 | rev`
#echo in_ext=$in_ext

                                                        
if [ "$trg" == "da" ] || [ "$trg" == "de" ] || [ "$trg" == "el" ] || [ "$trg" == "es" ] || [ "$trg" == "fi" ] ||
   [ "$trg" == "fr" ] || [ "$trg" == "it" ] || [ "$trg" == "nl" ] || [ "$trg" == "pt" ] || [ "$trg" == "sv" ] ; then
	cfg_dgt=$iateconf_path/cfg-dgtold.sh
#echo  cfg_dgt=$cfg_dgt

else
	cfg_dgt=$iateconf_path/cfg-dgtnew.sh
fi


source $cfg_dgt

source $iateconf_path/dcfg-form.sh



#if NOT $src==en goto setlookend
#if $langroup==old goto setlookold
#set lus=-eu13
#goto setlookend
#:setlookold
#rem look-up  specifyer:
#set lus=-prim
#:setlookend

#if no surface print only the first ana:
#if NOT DEFINED srf (
#firstana=-first
#)
                                                                      


#delete zero size txt files from target folder
#filemask="$work_path/*.txt"
#for %$A in (filemask$) do if $~zA==0 del "%A"

#filemask="$work_path/*.tsv"
#for %$A in (filemask$) do if $~zA==0 del "%A"

#if $use_prefix==YES set p=$in             
if [ "$use_prefix" == "YES" ]; then
	NOW=$(date +"%Y-%m-%d-%H-%M-%S")
	p=$in-$NOW
	
fi



#goto jump

if [ ! -d  $work_path ]; then
	mkdir  $work_path  
fi

wp=$work_path/tmp-$inname-$src-$trg-$NOW
mkdir  $wp


#--------------------------------------------------------------------


#if [ "$DEBUG" ]; then
#if NOT DEFINED TEXTGENSTEP goto skiptextgen
if [ $TEXTGENSTEP == YES ]; then 

if [ ! -f  "$wp/$p-textsrf.stm-$src.ng" ]; then
#if NOT DEFINED TEXTGENSTEP goto skiptextgen
echo Get stemmed phrases from source document


#if skip if we alread parsed the text

if [ -f  "$wp/$p-text.txt" ]; then
	rm "$wp/$p-text.txt"
fi  
#extension is doc or docx:

if [ ! "$in_ext" == "TXT" ]; then
	echo The script expects .TXT files 
fi

cp --preserve=mode $in_path/$in.$in_ext "$wp/$p-text.txt" 

if [ ! -f  "$wp/$p-text.txt" ]; then
	exit
fi



#deletion of simple XML tags (without attributes)
#  "$wp/$p-text.txt"  ->  "`cygpath -w $wp/$p-text.txt`"
$perl_path/perl -pe "s/<[^ >\/n]+>//g"  "$wp/$p-text.txt" >   "$wp/$p-textnotag.txt"


orig_path="$(pwd)"
cd $iateconf_path
$perl_path/perl $iateconf_path/tokenizer.perl -l $src -q -a < "$wp/$p-textnotag.txt" >  "$wp/$p-text.tok"
cd $orig_path
#echo orig_path=$orig_path


#C HFST analyser:
if [ "$STEMMING" == "C" ]; then
	$perl_path/perl $script_path/destxt.pl  -lang=$src "$wp/$p-text.tok"  "$wp/$p-text.dtok1"
	$perl_path/perl $script_path/dos2unix.pl   "$wp/$p-text.dtok1" >"$wp/$p-text.dtok"
	$hfst_prg_path/hfst-proc --apertium --dictionary-case  "$hfst_dic_path/$src.ana.hfstol" "$wp/$p-text.dtok" > "$wp/$p-text-hfst.ana"

fi #:skip_c

#Python HFST  analyser:
if [ "$STEMMING" == "Python" ]; then
	$perl_path/perl -pe "s//n/ _eol/n/;s/^[ ]+/n//n/;s/[ ]+//n/g;s/^/n//"  "$wp/$p-text.tok" > "$wp/$p-textl1.dtok"
	#$perl_path/perl -pe "s/^[ ]+/n//n/;s/[ ]+//n/g;s/^/n//"  $wp/$p-text.tok > $wp/$p-textl1.dtok
	$python_path/python $script_path/hfst_lookup.py $hfst_dic_path/$src.ana.hfstol   <"$wp/$p-textl1.dtok"   > "$wp/$p-text-p.ana" 2>"$wp/$p-text-p.err"
	$perl_path/perl $script_path/hfstp2txt.pl  "$wp/$p-text-p.ana" "$wp/$p-text-hfst.ana" "$wp/$p-text.hfst-ana.stat"

	if [ "$STATSTEP" ]; then
		$bin_path/sortx  -f "$wp/$p-text.hfst-ana.stat" | uniq   -c | $bin_path/sortx  -gr  > "$wp/$p-text.hfst-ana.freq.stat"

		
	fi #:skipstats2

fi #:skip_python                                                         

#Java HFST analyser:
if [ "$STEMMING" == "Java" ]; then
	$perl_path/perl -pe "s/[ ]+//n/g"  "$wp/$p-text.tok" > "$wp/$p-textl1.dtok"
	$java_path/java -jar $script_path/hfst-ol.jar "$hfst_dic_path/$src.ana.hfstol"   <"$wp/$p-textl1.dtok"   > "$wp/$p-text-hfst-j.ana"
	#tbdo: hfstj2txt.pl text-hfst-j.ana text-hfst.ana
fi #:skip_java


#OLD: HFST Stemming+ Hunspell:
#conversion of ana to stm form + recognition user dictionary words and non-alfanumeric char words + protection against hunspell crash in case of some hu/sv words
#$perl_path/perl $script_path/hfst-stem.pl  -lang=$src -srf -udic=$hfst_dic_path/hfst-$src.udic $pos $part $srf -first < "$wp/$p-text-hfst.ana"  >"$wp/$p-text-hfst.stm"
#break to lines with the unknown words:
#:$perl_path/perl $script_path/prep4hunspell.pl   "$wp/$p-text-hfst.stm"  "$wp/$p-textsrf-4hsp.stm"
#Hunspell analyser:  analyse words with a version of hunspell which expects 1 word per line and returns surface:
#:$hfst_prg_path/hunspell_m2w.exe -i utf-8 -m -d $hunspell_dict_path/$src  < "$wp/$p-textsrf-4hsp.stm"  >"$wp/$p-text-hsp.ana"



#delete the last space in line
$perl_path/perl $script_path/fixhfstana.pl "$wp/$p-text-hfst.ana"  > "$wp/$p-text.nosp.ana"
#check:            
#grep " "  $wp/$p-$src.nosp.ana > $wp/$p-$src.sp.err
#stem:
$perl_path/perl $script_path/hfst-stem.pl  -lang=$src -udic=$hfst_dic_path/hfst-$src.udic -srf  -first < "$wp/$p-text.nosp.ana"  > "$wp/$p-text-hfst.stm"
#replace per (/) -> tab
$perl_path/perl $script_path/prep4hunsp.pl   "$wp/$p-text-hfst.stm"  "$wp/$p-text-4hsp.stm"  

#Hunspell***
#analyse:



$hunspell_prg_path/analyze_dgt a $hunspell_dict_path/$src.aff $hunspell_dict_path/$src.dic  "$wp/$p-text-4hsp.stm"  >"$wp/$p-text-hsp.ana"


#Hunspell stem:  
$perl_path/perl $script_path/hunspell-stem.pl  $src  "$wp/$p-text-hsp.ana"  "$wp/$p-text-hsp.line.stm"

#restore lines:
$perl_path/perl $script_path/restoreline.pl "$wp/$p-text-hsp.line.stm"  > "$wp/$p-text-hsp.stm"


#statistics on unknown words:
#if NOT $convert_stat==YES goto skipstats3
# $perl_path/perl $script_path/prep4hunspell.pl  "$wp/$p-text-hsp.stm"  "$wp/$p-unknown-$src.stm"
# $perl_path/perl -pe "s/^/n//"  "$wp/$p-unknown-$src.stm" > "$wp/$p-unknown-$src.err"
# $hfst_prg_path/sortx  -f "$wp/$p-unknown-$src.err" | uniq   -c | $hfst_prg_path/sortx  -gr  > "$wp/$p-unknown-$src.err.stat"
#:skipstats3



#connect with old system:
cp --preserve=mode "$wp/$p-text-hsp.stm"  "$wp/$p-textsrf.stm" 
$perl_path/perl $script_path/cleanstemmed.pl   "$wp/$p-text-hsp.stm"   > "$wp/$p-text.stm"

#fi #DEBUG

cd  $iateconf_path

#stem suffix array to creat phrases to be looked up in the dictionary, also its frequency infois added to the dictionary:
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 1 --token ./scripts/satoken.txt "$wp/$p-text.stm-01.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 2 --token $script_path/satoken.txt "$wp/$p-text.stm-02.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 3 --token $script_path/satoken.txt "$wp/$p-text.stm-03.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 4 --token $script_path/satoken.txt "$wp/$p-text.stm-04.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 5 --token $script_path/satoken.txt "$wp/$p-text.stm-05.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 6 --token $script_path/satoken.txt "$wp/$p-text.stm-06.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 7 --token $script_path/satoken.txt "$wp/$p-text.stm-07.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 8 --token $script_path/satoken.txt "$wp/$p-text.stm-08.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 9 --token $script_path/satoken.txt "$wp/$p-text.stm-09.txt" "$wp/$p-text.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 10 --token $script_path/satoken.txt "$wp/$p-text.stm-10.txt" "$wp/$p-text.stm"

cat $wp/$p-text.stm-*.txt  > $wp/$p-text.stm.ngf-$src.txt

#normalize hyphenated compounds:
$perl_path/perl $script_path/cleanhypcomplex.pl  "$wp/$p-text.stm.ngf-$src.txt" > "$wp/$p-text.stm.ngf-$src.hc.txt"

$perl_path/perl -pe "s/ [0-9]+ +\n/\n/"            "$wp/$p-text.stm.ngf-$src.hc.txt" > "$wp/$p-text.stm-$src.ng"
$perl_path/perl -pe "s/^[^\n]+ ([0-9]+) +\n/\1\n/" "$wp/$p-text.stm.ngf-$src.hc.txt" > "$wp/$p-text.num-$src.ng"



#morphological variants: create surface=stem pairs by suffix array using the stemmed suffix array
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 1 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-01.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 2 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-02.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 3 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-03.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 4 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-04.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 5 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-05.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 6 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-06.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 7 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-07.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 8 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-08.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 9 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-09.txt" "$wp/$p-textsrf.stm"
$perl_path/perl $iateconf_path/array-suffix-driver.pl --newLine --ngram 10 --token $script_path/satoken.txt "$wp/$p-textsrf.stm-10.txt" "$wp/$p-textsrf.stm"
cat $wp/$p-textsrf.stm-*.txt > $wp/$p-textsrf.stm.ngf-$src.txt

#rem: delete lines with numbers, delete * unknown word markers


$perl_path/perl -pe "s/ [0-9]+ +\n/\n/"   "$wp/$p-textsrf.stm.ngf-$src.txt" > "$wp/$p-textsrf.stm-$src.ng"
#convert / to tab
$perl_path/perl $script_path/textsrf2tab.pl  "$wp/$p-textsrf.stm-$src.ng" > "$wp/$p-textsrftab.stm-$src.caps.ng"
$perl_path/perl $script_path/filter_upperterm.pl   "$wp/$p-textsrftab.stm-$src.caps.ng"  "$wp/$p-textsrftab.stm-$src.caps.4hc.ng"
$perl_path/perl $script_path/cleanhypcompsurf.pl             "$wp/$p-textsrftab.stm-$src.caps.4hc.ng" > "$wp/$p-textsrftab.stm-$src.ng"


#debug:
#$perl_path/perl $script_path/textsrf2tab.pl  "$wp/$p-textsrf.stm-$src.ng" > "$wp/$p-textsrftab.stm-$src.ng"
#$bin_path/sortx "$wp/$p-textsrftab.stm-$src.caps.ng" > "$wp/$p-textsrftab.stm-$src.caps.ng.srt"
#$bin_path/sortx "$wp/$p-textsrftab.stm-$src.ng" > "$wp/$p-textsrftab.stm-$src.ng.srt"

#:not needed: $perl_path/perl -pe "s/^[^/n]+ ([0-9]+) +/n//1/n/" $wp/$p-textsrf.stm.ngf-$src.txt > $wp/$p-textsrf.num.ng

cd "$orig_path"


#not used creates only noise:
#set orig_path=$CD
#cd  $script_path
#call sar.bat  $wp/$p-text.tok
#cd $orig_path

#returns $wp/$p-text.tok.ng

fi #if result exists

#:skiptextgen
fi  #skiptestgen

#--------------------------------------------------------------------

if [ $DICTGENSTEP == "YES" ]; then 
#if NOT DEFINED DICTGENSTEP goto skipdictgen


echo Create a full stemmed IATE list for the source language 

#we need to generate dic file 1. if the server source file has been updated 2. the dic file does not exist 3. or this script is changed (if so delete dic)
#if EXIST $iate_dict goto chkdicindate
#copy  $dicin_path_remote/$dicin $iate_dict  >nul
#echo file copied...
#goto endupdatedicin
#:chkdicindate
#echo Check if server dictionary is updated
#copy  $dicin_path_remote/$dicin $iate_dict-2chk  >nul
#FOR /F %$i IN ('DIR iate_dict$* /B /O:D') DO SET NEWEST=%i
#rem ECHO $NEWEST is newer
#if $NEWEST==$dicin goto endupdatedicin1
#del $iate_dict
#mv  $iate_dict-2chk $iate_dict
#rem echo Dictionary updated...
#goto dictgen
#:endupdatedicin1
#del $iate_dict-2chk   
#rem echo Test copy file deleted
#:endupdatedicin
#rem Do not generate dict stem list if already exists: 
#rem if EXIST $iate_dic_path/dict-$src.stm echo dict phrase list already exist, generation skipped

# tbd filemask=$hfst_dic_path/dict-$src.stm
# tbd for %$A in (filemask$) do if $~zA==0 del "%A"


#delete if zero size 
# tbd for /F %$A in ("hfst_dic_path$/dict-src$.stm") do If $~zA equ 0 del "hfst_dic_path$/dict-src%.stm"  

#if EXIST $hfst_dic_path/dict-$src.stm GOTO skipdictgen
if [ ! -f  "$hfst_dic_path/dict-$src.stm" ]; then
#:dictgen

                                   
DICTGENRUN=1

$perl_path/perl $script_path/gettermcol.pl  $src $langpos $termpos "$iate_dict"   "$wp/dict.ord-$src.tsv" 

#Tokenization
#orig_path=$CD
#cd $script_path
# NOT USED for IATE dicts: $perl_path/perl $script_path/normalise.perl --lang $src < "$wp/dict.ord.tsv" >  "$wp/dict.norm"
$perl_path/perl ./tokenizer.perl -l $src -q -ad < "$wp/dict.ord-$src.tsv"  >  "$wp/dict-$src.tok" 
#cd $orig_path
#ONLY for debugging purpoces:
#dos2unix  $wp/dict-$src.tok


#if NOT $src==en  goto skipentok
if [ $src == "en" ]; then
	$perl_path/perl -pe "s/^to //" "$wp/dict-$src.tok" > "$wp/dict-$src.tok1"
	cp --preserve=mode "$wp/dict-$src.tok1" "$wp/dict-$src.tok" 
fi #:skipentok


#C HFST analyser:
#if NOT $STEMMING==C goto skip_c2
if [ $STEMMING == "C" ]; then
	$perl_path/perl $script_path/destxt.pl  -lang=$src  "$wp/dict-$src.tok"  "$wp/dict-$src.dtok1"
	$perl_path/perl $script_path/dos2unix.pl "$wp/dict-$src.dtok1" > "$wp/dict-$src.dtok"
	$hfst_prg_path/hfst-proc --apertium --dictionary-case  "$hfst_dic_path/$src.ana.hfstol" "$wp/dict-$src.dtok" > "$wp/dict-$src-hfst.ana"
fi #:skip_c2

#delete the last space in line
$perl_path/perl $script_path/fixhfstana.pl "$wp/dict-$src-hfst.ana"  > "$wp/dict-$src.nosp.ana"
#HFST stem:
$perl_path/perl $script_path/hfst-stem.pl  -lang=$src -udic=$hfst_dic_path/hfst-$src.udic -srf  -first < "$wp/dict-$src.nosp.ana"  > "$wp/dict-$src-hfst.stm"
#replace per (/) -> tab
$perl_path/perl $script_path/prep4hunsp.pl   "$wp/dict-$src-hfst.stm"  "$wp/dict-$src-4hsp.stm"  
#Hunspell
$hunspell_prg_path/analyze_dgt a $hunspell_dict_path/$src.aff $hunspell_dict_path/$src.dic  "$wp/dict-$src-4hsp.stm"  >"$wp/dict-$src-hsp.ana"
#Hunspell stem:  
$perl_path/perl $script_path/hunspell-stem.pl  $src  "$wp/dict-$src-hsp.ana"  "$wp/dict-$src-hsp.line.stm"

#restore lines:
$perl_path/perl $script_path/restoreline.pl "$wp/dict-$src-hsp.line.stm"  > "$wp/dict-$src-hsp.stm"


#:for development purposes first we create it in a local folder:
$perl_path/perl $script_path/cleanstemmed.pl  "$wp/dict-$src-hsp.stm" >  "$wp/dict-$src.stm"


cp --preserve=mode  "$wp/dict-$src.stm"  "$hfst_dic_path/dict-$src.stm" 

rm $wp/dict-$src*


#statistics:
#if NOT $convert_stat==YES goto skipstats4
if [ $convert_stat == "YES" ]; then
$perl_path/perl -pe "s/^\n//"  "$wp/dict-$src-hsp.ana" >"$wp/dic-$src-ana.txt"
$perl_path/perl -pe "s/[\t ]+/\n/g" "$wp/dic-$src-ana.txt" > "$wp/dic-$src-ana1.tmp"
grep "^fl:" "$wp/dic-$src-ana1.tmp" > "$wp/dic-$src-ana2.tmp"
$perl_path/perl -pe "s/fl://" "$wp/dic-$src-ana2.tmp" >"$wp/dic-$src-ana3.tmp"
$bin_path/sortx  -f "$wp/dic-$src-ana3.tmp" | uniq   -c | $bin_path/sortx  -gr  >"$wp/dic-$src-ana-fl.stat"

grep "^..:" "$wp/dic-$src-ana1.tmp" > "$wp/dic-$src-ana10.tmp"
$perl_path/perl -pe "s/^(..):[^\n]+/\1/" "$wp/dic-$src-ana10.tmp" > "$wp/dic-$src-ana11.tmp"
$bin_path/sortx  -f "$wp/dic-$src-ana11.tmp" | uniq   -c | $bin_path/sortx  -gr  >"$wp/dic-$src-ana-mcat.stat"

rm $wp/dic-$src*.tmp
rm $wp/dic-$src-ana.txt

fi #:skipstats4

fi #if exists
fi #:skipdictgen



#--------------------------------------------------------------------


#if NOT DEFINED ADDFORMSTEP goto skip_addform 
if [ $ADDFORMSTEP == "YES" ]; then 

echo Add morphological forms to source text terms 

#if EXIST "$wp/found-srcnum-$src.txt" GOTO skip_addform
if [ ! -f "$wp/found-srcnum-$src.txt" ]; then 

$perl_path/perl $script_path/header.pl  <"$iate_dict" >  "$wp/$p-header.tsv"
#old $perl_path/perl -pe "s/(.)\n/\1\tTL_FORM\n/" "$wp/$p-header0.tsv" > "$wp/$p-header.tsv"
#$perl_path/perl  $scripts/tlform.pl "$wp/$p-header0.tsv" > "$wp/$p-header.tsv"


#FILTER IATE SOURCE TERMS with DOCUMENT SOURCE TERMS  [and generate list of morphological variants]
#                                         in:file1=stemmed_text           in:file2=src_keys_of_stemmed_dict  in:file2_dic              out=FOUND_IN_DICT                      out=NOT_FOUND_IN_DIC                out=FOUND_DIC_TERMS               [in=morph variant dict               in=id    in=lang   in=primary   in=term    out=FOUND_DIC_MORPHVAR]                     LOG

$perl_path/perl $script_path/compare_sar.pl  "$wp/$p-text.stm-$src.ng" "$hfst_dic_path/dict-$src.stm"  "$iate_dict"  "$wp/$p-found_in_text-$src.txt"  "$wp/$p-not_in_text-$src.txt" "$wp/$p-found-src-$src.txt" "$wp/$p-textsrftab.stm-$src.ng" $idpos $langpos $primarypos $termpos  "$wp/$p-found-src-$src-morfvar.txt" > "$wp/$p-compare_sar.log"


#$perl_path/perl -pe "s/^([^\t]+)\t([^\t]+)\t+([^\t]+)\t+\3\n//" "$wp/$p-found-src-$src-morfvar.txt" > "$wp/$p-found-src-$src-morfvarq.txt"
$perl_path/perl $script_path/morfvarq.pl "$wp/$p-found-src-$src-morfvar.txt" > "$wp/$p-found-src-$src-morfvarq.txt"


#add frequency info to found_dict:
$perl_path/perl $script_path/linehash.pl  "$wp/$p-found_in_text-$src.txt"  "$wp/$p-text.stm-$src.ng"  "$wp/$p-text.num-$src.ng" "$wp/$p-found_num.txt" "$wp/$p-found_notused.txt" "$wp/$p-notfound_notused.txt"

#add vertical: dictionary and morphvar form info
$perl_path/perl $script_path/adlinesbytab.pl "$wp/$p-found-src-$src.txt"  "$wp/$p-found_num.txt"  "$wp/$p-found-srcnum-$src.txt"

#Get lookupforms from lookupform dictionary: 
#if NOT $lookupform==yes goto skip_lookupform
if [ $lookupform == "yes" ]; then 
	$perl_path/perl $script_path/getidlangline.pl  $termpos "$wp/$p-found-srcnum-$src.txt"  $termpos $langpos notused $src   "$hfst_dic_path/lookup-$src$lus.tsv" "$wp/$p-lookup-dic-$src-rev.tsv" 0	
	$perl_path/perl $script_path/ordertermcol.pl  "$wp/$p-lookup-dic-$src-rev.tsv" 1-2-3-4-5-6-7-8-9-20-11-12-13-14-15-16-17-18-19-10 "$wp/$p-lookup-dic-$src.tsv"
else
	touch $wp/$p-lookup-dic-$src.tsv
fi #:skip_lookupform

fi #:skip_addform
fi #:ADDFRORM

#---------------------------


if [ $FILTERSTEP == "YES" ]; then 
#if NOT DEFINED FILTERSTEP goto skipfilter 
echo Filter document terms with IATE terms

#Get target terms based upon source term id-s
$perl_path/perl $script_path/getidlangline.pl  $idpos  "$wp/$p-found-srcnum-$src.txt"  $idpos $langpos $src $trg "$iate_dict" "$wp/$p-dic0-$src-$trg.tsv" 1

#copy together:  header       +src                                     + lookupform                         +trg                              + morf variants
cat "$wp/$p-header.tsv" "$wp/$p-found-srcnum-$src.txt" "$wp/$p-lookup-dic-$src.tsv" "$wp/$p-dic0-$src-$trg.tsv"  "$wp/$p-found-src-$src-morfvarq.txt"  > "$wp/$in-$src-$trg.tsv" 


fi #:skipfilter


#---------------------------

if [ $convert_stat == "YES" ]; then 
#if NOT $convert_stat==YES goto skipstats5
echo Create statistics



#STATISICS:
#OLD grep "^[^/n]" "$wp/$p-found_in_text-$src.txt" > "$wp/$p-foundintext-$src.txt"
$perl_path/perl -pe "s/^\n//"  "$wp/$p-found_in_text-$src.txt" > "$wp/$p-foundintext-$src.txt"
$perl_path/perl $script_path/cntdelim.pl  "$wp/$p-foundintext-$src.txt" "$wp/$p-foundintext-$src.tmp" " "
$bin_path/sortx  -f "$wp/$p-foundintext-$src.tmp" | uniq   -c | $bin_path/sortx  -gr  >"$wp/$p-foundintext-$src.len.stat"
rm "$wp/$p-foundintext-$src.tmp"

echo +++DGT recognition+++ > "$wp/$in-$src-$trg.stat"
$perl_path/perl $script_path/getlangline.pl $src $langpos "$wp/$in-$src-$trg.tsv" "$wp/$in-$src.tsv" -nohead
$perl_path/perl $script_path/getlangline.pl $trg $langpos "$wp/$in-$src-$trg.tsv" "$wp/$in-$trg.tsv" -nohead
$perl_path/perl $script_path/ordertermcol.pl  "$wp/$in-$src.tsv" $termpos "$wp/$in-$src-term.txt"
$perl_path/perl $script_path/ordertermcol.pl  "$wp/$in-$trg.tsv" $termpos "$wp/$in-$trg-term.txt"

echo source terms: >> "$wp/$in-$src-$trg.stat"
wc -l  "$wp/$in-$src-term.txt" >> "$wp/$in-$src-$trg.stat"
echo uniq source terms: >> "$wp/$in-$src-$trg.stat"
$bin_path/sortx -u   "$wp/$in-$src-term.txt" > "$wp/$in-$src-term.unq"
wc -l  "$wp/$in-$src-term.unq" >> "$wp/$in-$src-$trg.stat"

echo target terms: >> "$wp/$in-$src-$trg.stat"
wc -l  "$wp/$in-$trg-term.txt" >> "$wp/$in-$src-$trg.stat"
echo uniq target terms: >> "$wp/$in-$src-$trg.stat"
$bin_path/sortx -u   "$wp/$in-$trg-term.txt" > "$wp/$in-$trg-term.unq"
wc -l  "$wp/$in-$trg-term.unq" >> "$wp/$in-$src-$trg.stat"

echo source terms lenghts: >> "$wp/$in-$src-$trg.stat"
fi #:skipstats5

#--------------------------------------------------------------------

#if NOT DEFINED IATECONVERTSTEP goto skipiateconvert
if [ $IATECONVERTSTEP == "YES" ]; then 

echo Generate TBX/SDLTB termbase

#:jump


#if NOT $out_format_gloss==TB_OUT  goto skip_sdltbout
if [ $out_format_gloss == "TB_OUT" ]; then 
	source $iateconf_path/iate_convert.sh $src $trg TXT $out_format h $iateconf_path/dcfg-form.sh $cfg_dgt "$wp/$in-$src-$trg.tsv" "$out_path" "$in"
fi #:skip_sdltbout

#--------------------------------------------------------------------

#if NOT $out_format_gloss==SMT_OUT  goto skip_smtout
if [ $out_format_gloss == "SMT_OUT" ]; then 
#simple filter on Reliability and Evaluation:

$perl_path/perl $progdir/filters.pl  "$wp/$in-$src-$trg.tsv"  "$wp/$in-$src-$trg-fil.tsv" "$wp/$in-$src-$trg-filtered.log"  -f_evaluation=$evalpos -f_reliability=$relypos 
#:perl filters.pl input output filtered [-f_prim=N] [-f_pifs=N] [-f_evaluation=N] [-f_reliability=N] [-f_reliabilitylang=N] [-f_language=ID]/n";

$perl_path/perl $progdir/ordertermcol.pl "$wp/$in-$src-$trg.tsv" 1-2-11  "$wp/$in-$src-$trg-ordered-1to1.tsv"
$perl_path/perl $script_path/filter_1trg.pl $src $trg "$wp/$in-$src-$trg-ordered-1to1.tsv" "$wp/$in-$src-$trg-1to1.tsv" "$wp/$in-$src-$trg-1to1_skip.log" 
#dos2unix  "$wp/$in-$src-$trg-1to1.tsv"

#create xml-markups for Moses with terms from IATE and lowercased morphological forms with probabilities of the target phrase:
#these last steps can be done only on DGT linux server as it calls Moses pipeline prerocessor that inserts place-holders and lowercase it:
#copy these 3 files to linux server:
#cp --preserve=mode "$wp/text.tok"
#cp --preserve=mode "$wp/$in-$src-$trg-1to1.tsv"
#cp --preserve=mode "$work2_path/$trg-stm-num-lc.tsv"
#where "$trg-stm-num-lc.tsv"  was genereted in two steps: 
#1  Euramis corpora to $src.frq on linux, 
#2  $src.frq -> ec/dgt/local/exodus/user/tihanla/IATEdocgloss/test/mttest10k/test10k-en-hu.markup_frq.txt by C:/DGT/shared/nlp_installations/nlp_tools-2014-10-30/run/stmfrqdic.bat
#swith to linux and issue these commands:
#:preproc   "$wp/$p-text.tok"  "$wp/$p-text.tok.prec"
#:then create markup for moses test input :
#for inclusive/exclusive without probabilites:
#$perl_path/perl $script_path/iatexmarkup.pl "$wp/$in-$src-$trg-1to1.tsv" "$wp/$p-text.tok"  "$wp/$in-$src-$trg.markup.txt"
#for exclusive with probabiilities:
#$perl_path/perl $script_path/iatexmarkupfrq.pl "$wp/$in-$src-$trg-1to1.tsv" "$work2_path/$trg-stm-num-lc.tsv" "$wp/$p-text.tok"  "$wp/$in-$src-$trg.markup_frq.txt"

fi #:skip_smtout

fi #:skipiateconvert


#rm -r -f $wp

echo Done


