Installation of IATEdocgloss

1. Deploy package from the repository to the target location

2. Set environment variable for libraries, e.g.:
LD_LIBRARY_PATH=/ec/dev/app/euramis/home/iatetrid/IATEgloss/lib:${LD_LIBRARY_PATH}
export $LD_LIBRARY_PATH

3. Specify the root folder of the package in iatedocgloss_sys_cfg.sh, e.g.:
docgloss_path=/ec/dev/app/euramis/home/iatetrid/IATEgloss

4. Set perl location in iatedocgloss_sys_cfg.sh, e.g.:
perl_path=/ec/dev/app/euramis/home/iatetrid/perl/ActivePerl-5.18/bin
The program requires perl version 14 or later.

Optional settings:

output (pub) and temporary (tmp) folders can be changed from curent defaults in iatedocgloss_sys_cfg.sh:
work_path=$docgloss_path/tmp
out_path=$docgloss_path/pub

The scripts does not delete temporary files by default. Might be changed to NO in production.
convert_tmpfiles=YES
The script can apply time stamps on temporary files for paralel processing in production. By default is turned off. Set to YES in production.
use_prefix=NO


The package uses a regularly extracted IATE database. The file name contains the extraction data (20150601=01.06.2015). 
The file name and its location can be changed in iatedocgloss_sys_cfg.sh:
iate_dict=$docgloss_path/data/20150601_en.txt

5. Set executable permissions for *.sh in the root folder and all files in the ./bin and ./bin/lib folders.  

*****

Running the script

The script requires three input parameters: source_language, target_language and the input_file, e.g.:
./IATEdocgloss.sh en de ./work/SANCO-2014-80581-00-00-EN-ORC-00.TXT

the resulting TBX glossary containing the IATE terms that occurred in the input document will be place into the pub folder, e.g:
./pub/SANCO-2014-80581-00-00-EN-ORC-00-en-de-prim.tbx
or 
./pub/SANCO-2014-80581-00-00-EN-ORC-00-en-hu.tbx
The output file name is derived from the input file name and is extended with the language pair. The script also applies several filters. For pre-2004 languages it applies the Primary filter. Since this filters leaves out 90% (!) of the original content, we indicate its application even in the output file name.
 

*****

Explanation of IATEdocgloss files and folders 


IATEdocgloss.sh:  the glossary generator tool

IATEdocgloss_DEMO.sh:  example calls of the tool 

iatedocgloss_sys_cfg.sh   config file of tool

cfg-dgtold.sh:     config file for 'old' (pre-2004) language filters (included IATEdocgloss.sh)
cfg-dgtnew.sh:	   config file for 'new' (post-2004) language filters (included by IATEdocgloss.sh)
dcfg-form.sh:	   config file describing for IATE field structure

tokenizer.perl:	   tokenizer, called by IATEdocgloss, taken from in MT@EC project

array-suffix-driver.pl:   a perl suffix array package module  called by IATEdocgloss 
Suffix.pm:	          package module of the array-suffix-driver.pl

iate_convert.sh:          this script converts a tab separated table into TBX (and SDLTB), called by IATEdocgloss.sh which creates such a table from the input and IATE
iateconvert_sys_cfg.sh:   config file of iate_convert.sh

README_IATEdocgloss.pdf: readme for windows users
README_IATEconvert.pdf   readme for windows users

Subfolders:
bin:	   two morphological analysers: hfst (hfst-proc) and hunspell (analyze_dgt) and a version independent sort (sortx)
data:      databases for hfst (*.ana.hfstol, hfst-*.udic) and hunspell morph analysers (*.dic, *.aff), blacklists (exclude.*) and the current IATE extract (20150601_en.txt)
           the folder may contain artifacts created by the IATEdocgloss.sh (dict-*.stm) dependent on the IATE extract
lib:	   libs of hfst (libhfst.so.37) and hunspell (linhunspell-1.3.so.0)
scripts:   scripts called by IATEdocgloss.sh
work       input file folder
pub:       output file folder
tmp:       temporary file folder
nonbreaking_prefixes:  data files of tokenizer.perl (Moses tokenizer, modified at DGT)
Normalise:             scripts called by tokenizer.perl

*****
