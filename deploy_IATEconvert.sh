set -x
if [ ! "$#" == 1  ]; then
	echo deploy_IATEconvert.bat target_path
	echo more info: tihanyi@ext.ec.europa.eu
	exit
fi

repo=.
pack=$1

mkdir $pack
cp --preserve=mode $repo/main/cfg-dgtnew.sh $pack
cp --preserve=mode $repo/main/cfg-dgtold.sh $pack
cp --preserve=mode $repo/main/dcfg-form.sh $pack
cp --preserve=mode $repo/main/dcfg-base.sh $pack

cp --preserve=mode $repo/main/iateconvert_sys_cfg.sh $pack
cp --preserve=mode $repo/main/iate_convert.sh $pack
cp --preserve=mode $repo/main/README_IATEconvert.pdf $pack
cp --preserve=mode $repo/main/iate_convert_txt-tbx_DEMO.sh $pack
cp --preserve=mode $repo/main/run_call_iate_convert.sh $pack
cp --preserve=mode $repo/main/call_iate_convert.sh $pack


mkdir $pack/scripts
cp --preserve=mode $repo/scripts/filters.pl $pack/scripts
#IATEconvert:
cp --preserve=mode $repo/scripts/cvsquote.pl $pack/scripts
cp --preserve=mode $repo/scripts/filtersrclang.pl $pack/scripts
cp --preserve=mode $repo/scripts/iatepair.pl $pack/scripts
cp --preserve=mode $repo/scripts/iate_converter.jar $pack/scripts
cp --preserve=mode $repo/scripts/iate_trgcnt.pl $pack/scripts
cp --preserve=mode $repo/scripts/iate_xml2sql.pl $pack/scripts
cp --preserve=mode $repo/scripts/mergetermextra-u8.pl $pack/scripts
cp --preserve=mode $repo/scripts/myprettyxml.pl $pack/scripts
cp --preserve=mode $repo/scripts/TBXcoreStructV02.dtd $pack/scripts
cp --preserve=mode $repo/scripts/tbxfilter.pl $pack/scripts
cp --preserve=mode $repo/scripts/tbx_filter.xslt $pack/scripts
cp --preserve=mode $repo/scripts/txt2tbx.pl $pack/scripts
cp --preserve=mode $repo/scripts/txtclean.pl $pack/scripts
cp --preserve=mode $repo/scripts/xls2txt.vbs $pack/scripts
cp --preserve=mode $repo/scripts/xml2lines.pl $pack/scripts
cp --preserve=mode $repo/scripts/xmlclean.pl $pack/scripts
cp --preserve=mode $repo/scripts/ordertermcol.pl $pack/scripts
cp --preserve=mode $repo/scripts/utf8to16.pl $pack/scripts
cp --preserve=mode $repo/scripts/filterlookup.pl $pack/scripts
cp --preserve=mode $repo/scripts/lookupform_add.pl $pack/scripts
cp --preserve=mode $repo/scripts/cleanhypcomplex.pl $pack/scripts
cp --preserve=mode $repo/scripts/cleanhypcompsurf.pl $pack/scripts
cp --preserve=mode $repo/scripts/mdblink.accdb $pack/scripts
cp --preserve=mode $repo/scripts/uniq.pl $pack/scripts
cp --preserve=mode $repo/scripts/txt2tbx_no_trg_merge.pl $pack/scripts


mkdir $pack/data
cp --preserve=mode $repo/data/20150611_en.txt $pack/data
cp --preserve=mode $repo/data/exclude.cs.txt $pack/data
cp --preserve=mode $repo/data/exclude.hu.txt $pack/data
cp --preserve=mode $repo/data/exclude.pl.txt $pack/data
cp --preserve=mode $repo/data/exclude.sk.txt $pack/data
cp --preserve=mode $repo/data/exclude.sl.txt $pack/data


mkdir $pack/work
cp --preserve=mode $repo/work/sample1000.txt $pack/work
#cp --preserve=mode $repo/work/SANCO-2014-80581-00-00-EN-ORC-00.txt $pack/work

