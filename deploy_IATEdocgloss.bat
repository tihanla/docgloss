:@echo off
if "%1"=="" goto terminate

set repo=.
set pack=%1


mkdir %pack%
copy %repo%\main\iatedocgloss_sys_cfg.bat %pack%
copy %repo%\main\IATEdocgloss.bat %pack%
copy %repo%\main\IATEdocgloss_DEMO.bat %pack%
copy %repo%\main\README_IATEdocgloss.pdf %pack%
copy %repo%\main\cfg-dgtnew.bat %pack%
copy %repo%\main\cfg-dgtoldallprim.bat %pack%\cfg-dgtold.bat
copy %repo%\main\dcfg-form.bat %pack%
copy %repo%\main\iateconvert_sys_cfg.bat %pack%
copy %repo%\main\iate_convert.bat %pack%
copy %repo%\main\README_IATEconvert.pdf %pack%

copy %repo%\main\array-suffix-driver.pl %pack%
copy %repo%\main\Suffix.pm %pack%
copy %repo%\main\tokenizer.perl %pack%


mkdir %pack%\bin
copy %repo%\bin\analyze_dgt.exe %pack%\bin
copy %repo%\bin\cyggcc_s-seh-1.dll %pack%\bin
copy %repo%\bin\cyghunspell-1.3-0.dll %pack%\bin
copy %repo%\bin\cygiconv-2.dll %pack%\bin
copy %repo%\bin\cygintl-8.dll %pack%\bin
copy "%repo%\bin\cygstdc++-6.dll" %pack%\bin
copy %repo%\bin\cygwin1.dll %pack%\bin
copy %repo%\bin\dos2unix.exe %pack%\bin
copy %repo%\bin\hfst-proc.exe %pack%\bin
copy %repo%\bin\libgcc_s_dw2-1.dll %pack%\bin
copy %repo%\bin\libhfst-35.dll %pack%\bin
copy "%repo%\bin\libstdc++-6.dll" %pack%\bin
copy %repo%\bin\sortx.exe %pack%\bin


mkdir %pack%\data
copy %repo%\data\20150611_en.txt %pack%\data
copy %repo%\data\bg.aff %pack%\data
copy %repo%\data\bg.ana.hfstol %pack%\data
copy %repo%\data\bg.dic %pack%\data
copy %repo%\data\cs.aff %pack%\data
copy %repo%\data\cs.ana.hfstol %pack%\data
copy %repo%\data\cs.dic %pack%\data
copy %repo%\data\da.aff %pack%\data
copy %repo%\data\da.ana.hfstol %pack%\data
copy %repo%\data\da.dic %pack%\data
copy %repo%\data\de.aff %pack%\data
copy %repo%\data\de.ana.hfstol %pack%\data
copy %repo%\data\de.dic %pack%\data
copy %repo%\data\el.aff %pack%\data
copy %repo%\data\el.ana.hfstol %pack%\data
copy %repo%\data\el.dic %pack%\data
copy %repo%\data\en.aff %pack%\data
copy %repo%\data\en.ana.hfstol %pack%\data
copy %repo%\data\en.dic %pack%\data
copy %repo%\data\es.aff %pack%\data
copy %repo%\data\es.ana.hfstol %pack%\data
copy %repo%\data\es.dic %pack%\data
copy %repo%\data\et.aff %pack%\data
copy %repo%\data\et.ana.hfstol %pack%\data
copy %repo%\data\et.dic %pack%\data
copy %repo%\data\fi.aff %pack%\data
copy %repo%\data\fi.ana.hfstol %pack%\data
copy %repo%\data\fi.dic %pack%\data
copy %repo%\data\fr.aff %pack%\data
copy %repo%\data\fr.ana.hfstol %pack%\data
copy %repo%\data\fr.dic %pack%\data
copy %repo%\data\ga.aff %pack%\data
copy %repo%\data\ga.ana.hfstol %pack%\data
copy %repo%\data\ga.dic %pack%\data
copy %repo%\data\head.tsv %pack%\data
copy %repo%\data\hfst-bg.udic %pack%\data
copy %repo%\data\hfst-cs.udic %pack%\data
copy %repo%\data\hfst-da.udic %pack%\data
copy %repo%\data\hfst-de.udic %pack%\data
copy %repo%\data\hfst-el.udic %pack%\data
copy %repo%\data\hfst-en.udic %pack%\data
copy %repo%\data\hfst-es.udic %pack%\data
copy %repo%\data\hfst-et.udic %pack%\data
copy %repo%\data\hfst-fi.udic %pack%\data
copy %repo%\data\hfst-fr.udic %pack%\data
copy %repo%\data\hfst-ga.udic %pack%\data
copy %repo%\data\hfst-hr.udic %pack%\data
copy %repo%\data\hfst-hu.udic %pack%\data
copy %repo%\data\hfst-it.udic %pack%\data
copy %repo%\data\hfst-lt.udic %pack%\data
copy %repo%\data\hfst-lv.udic %pack%\data
copy %repo%\data\hfst-mt.udic %pack%\data
copy %repo%\data\hfst-nl.udic %pack%\data
copy %repo%\data\hfst-pl.udic %pack%\data
copy %repo%\data\hfst-pt.udic %pack%\data
copy %repo%\data\hfst-ro.udic %pack%\data
copy %repo%\data\hfst-sk.udic %pack%\data
copy %repo%\data\hfst-sl.udic %pack%\data
copy %repo%\data\hfst-sv.udic %pack%\data
copy %repo%\data\hr.aff %pack%\data
copy %repo%\data\hr.ana.hfstol %pack%\data
copy %repo%\data\hr.dic %pack%\data
copy %repo%\data\hu.aff %pack%\data
copy %repo%\data\hu.ana.hfstol %pack%\data
copy %repo%\data\hu.dic %pack%\data
copy %repo%\data\it.aff %pack%\data
copy %repo%\data\it.ana.hfstol %pack%\data
copy %repo%\data\it.dic %pack%\data
copy %repo%\data\lt.aff %pack%\data
copy %repo%\data\lt.ana.hfstol %pack%\data
copy %repo%\data\lt.dic %pack%\data
copy %repo%\data\lv.aff %pack%\data
copy %repo%\data\lv.ana.hfstol %pack%\data
copy %repo%\data\lv.dic %pack%\data
copy %repo%\data\mt.aff %pack%\data
copy %repo%\data\mt.ana.hfstol %pack%\data
copy %repo%\data\mt.dic %pack%\data
copy %repo%\data\nl.aff %pack%\data
copy %repo%\data\nl.ana.hfstol %pack%\data
copy %repo%\data\nl.dic %pack%\data
copy %repo%\data\pl.aff %pack%\data
copy %repo%\data\pl.ana.hfstol %pack%\data
copy %repo%\data\pl.dic %pack%\data
copy %repo%\data\pt.aff %pack%\data
copy %repo%\data\pt.ana.hfstol %pack%\data
copy %repo%\data\pt.dic %pack%\data
copy %repo%\data\ro.aff %pack%\data
copy %repo%\data\ro.ana.hfstol %pack%\data
copy %repo%\data\ro.dic %pack%\data
copy %repo%\data\sk.aff %pack%\data
copy %repo%\data\sk.ana.hfstol %pack%\data
copy %repo%\data\sk.dic %pack%\data
copy %repo%\data\sl.aff %pack%\data
copy %repo%\data\sl.ana.hfstol %pack%\data
copy %repo%\data\sl.dic %pack%\data
copy %repo%\data\sv.aff %pack%\data
copy %repo%\data\sv.ana.hfstol %pack%\data
copy %repo%\data\sv.dic %pack%\data
copy %repo%\data\exclude.??.txt %pack%\data
copy %repo%\data\lookup.??.tsv %pack%\data



mkdir %pack%\scripts
copy %repo%\scripts\adlinesbytab.pl %pack%\scripts
copy %repo%\scripts\ansi2to8.pl %pack%\scripts
copy %repo%\scripts\cleanstemmed.pl %pack%\scripts
copy %repo%\scripts\cntdelim.pl %pack%\scripts
copy %repo%\scripts\compare_sar.pl %pack%\scripts
copy %repo%\scripts\concatlines.pl %pack%\scripts
copy %repo%\scripts\constants.py %pack%\scripts
copy %repo%\scripts\constants.pyc %pack%\scripts
copy %repo%\scripts\delpunct.pl %pack%\scripts
copy %repo%\scripts\deltag.pl %pack%\scripts
copy %repo%\scripts\destxt.pl %pack%\scripts
copy %repo%\scripts\dic2lookup.pl %pack%\scripts
copy %repo%\scripts\dos2unix.pl %pack%\scripts
copy %repo%\scripts\filters.pl %pack%\scripts
copy %repo%\scripts\filter_1trg.pl %pack%\scripts
copy %repo%\scripts\filter_upperterm.pl %pack%\scripts
copy %repo%\scripts\fixhfstana.pl %pack%\scripts
copy %repo%\scripts\freq2lower.pl %pack%\scripts
copy %repo%\scripts\freq2lower_test.pl %pack%\scripts
copy %repo%\scripts\freqmaven.pl %pack%\scripts
copy %repo%\scripts\freq_cozza.sh %pack%\scripts
copy %repo%\scripts\freq_robur.sh %pack%\scripts
copy %repo%\scripts\freq_tsih.sh %pack%\scripts
copy %repo%\scripts\freq_unuk.sh %pack%\scripts
copy %repo%\scripts\frq2normalized.pl %pack%\scripts
copy %repo%\scripts\getidlangline.pl %pack%\scripts
copy %repo%\scripts\getlangline.pl %pack%\scripts
copy %repo%\scripts\gettbxseg.pl %pack%\scripts
copy %repo%\scripts\gettermcol.pl %pack%\scripts
copy %repo%\scripts\header.pl %pack%\scripts
copy %repo%\scripts\hfst-stem.pl %pack%\scripts
copy %repo%\scripts\hfst-stem_old.pl %pack%\scripts
copy %repo%\scripts\hfstp2txt.pl %pack%\scripts
copy %repo%\scripts\hfst_lookup.py %pack%\scripts
copy %repo%\scripts\hunspell-stem.pl %pack%\scripts
copy %repo%\scripts\hunspell-stem_old.pl %pack%\scripts
copy %repo%\scripts\iatexmarkup-ali.pl %pack%\scripts
copy %repo%\scripts\iatexmarkup.pl %pack%\scripts
copy %repo%\scripts\iatexmarkupfrq.pl %pack%\scripts
copy %repo%\scripts\iatexmarkupxxx.pl %pack%\scripts
copy %repo%\scripts\linehash.pl %pack%\scripts
copy %repo%\scripts\lookupiate.pl %pack%\scripts
copy %repo%\scripts\normtokenized.pl %pack%\scripts
copy %repo%\scripts\ordertermcol.pl %pack%\scripts
copy %repo%\scripts\part.pl %pack%\scripts
copy %repo%\scripts\prep4hsp.pl %pack%\scripts
copy %repo%\scripts\prep4hunsp.pl %pack%\scripts
copy %repo%\scripts\prep4hunspell.pl %pack%\scripts
copy %repo%\scripts\restoreline.pl %pack%\scripts
copy %repo%\scripts\restorestem.pl %pack%\scripts
copy %repo%\scripts\restoretsv-bad.pl %pack%\scripts
copy %repo%\scripts\satoken.txt %pack%\scripts
copy %repo%\scripts\shared.py %pack%\scripts
copy %repo%\scripts\shared.pyc %pack%\scripts
copy %repo%\scripts\textsrf2tab.pl %pack%\scripts
copy %repo%\scripts\transducer.py %pack%\scripts
copy %repo%\scripts\transducer.pyc %pack%\scripts
copy %repo%\scripts\tsv2id.pl %pack%\scripts
copy %repo%\scripts\utf16to8.pl %pack%\scripts
copy %repo%\scripts\utf8to16.pl %pack%\scripts
copy %repo%\scripts\word2txt.vbs %pack%\scripts
copy %repo%\scripts\filterlookup.pl %pack%\scripts
copy %repo%\scripts\lookupform_add.pl %pack%\scripts
copy %repo%\scripts\cleanhypcomplex.pl %pack%\scripts
copy %repo%\scripts\cleanhypcompsurf.pl %pack%\scripts
copy %repo%\scripts\mdblink.accdb %pack%\scripts
copy %repo%\scripts\uniq.pl %pack%\scripts
copy %repo%\scripts\morfvarq.pl %pack%\scripts



rem IATEconvert:
copy %repo%\scripts\cvsquote.pl %pack%\scripts
copy %repo%\scripts\filtersrclang.pl %pack%\scripts
copy %repo%\scripts\iatepair.pl %pack%\scripts
copy %repo%\scripts\iate_converter.jar %pack%\scripts
copy %repo%\scripts\iate_trgcnt.pl %pack%\scripts
copy %repo%\scripts\iate_xml2sql.pl %pack%\scripts
copy %repo%\scripts\mergetermextra-u8.pl %pack%\scripts
copy %repo%\scripts\myprettyxml.pl %pack%\scripts
copy %repo%\scripts\TBXcoreStructV02.dtd %pack%\scripts
copy %repo%\scripts\tbxfilter.pl %pack%\scripts
copy %repo%\scripts\tbx_filter.xslt %pack%\scripts
copy %repo%\scripts\txt2tbx.pl %pack%\scripts
copy %repo%\scripts\txt2tbx_headword.pl %pack%\scripts
copy %repo%\scripts\txt2xls.vbs %pack%\scripts
copy %repo%\scripts\txtclean.pl %pack%\scripts
copy %repo%\scripts\xls2txt.vbs %pack%\scripts
copy %repo%\scripts\xml2lines.pl %pack%\scripts
copy %repo%\scripts\xmlclean.pl %pack%\scripts



mkdir %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.bg %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.ca %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.cs %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.da %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.de %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.el %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.en %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.es %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.et %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.fi %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.fr %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.ga %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.hu %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.is %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.it %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.lt %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.lv %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.mt %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.nl %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.pl %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.pt %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.ro %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.ru %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.sk %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.sl %pack%\nonbreaking_prefixes
copy %repo%\scripts\nonbreaking_prefixes\nonbreaking_prefix.sv %pack%\nonbreaking_prefixes

mkdir %pack%\Normalise
copy %repo%\scripts\Normalise\Hyphens.pm %pack%\Normalise
copy %repo%\scripts\Normalise\Noise.pm %pack%\Normalise
copy %repo%\scripts\Normalise\Numbers.pm %pack%\Normalise
copy %repo%\scripts\Normalise\OfficialJournal.pm %pack%\Normalise
copy %repo%\scripts\Normalise\Predefregexp.pm %pack%\Normalise
copy %repo%\scripts\Normalise\Quotes.pm %pack%\Normalise
copy %repo%\scripts\Normalise\Spaces.pm %pack%\Normalise
copy %repo%\scripts\Normalise\Spelling.pm %pack%\Normalise
copy %repo%\scripts\Normalise\Units.pm %pack%\Normalise

mkdir %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\bg.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\cs.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\da.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\de.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\el.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\en.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\es.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\et.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\fi.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\fr.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\ga.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\hu.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\it.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\lt.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\lv.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\mt.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\nl.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\pl.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\pt.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\ro.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\sk.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\sl.pm %pack%\Normalise\Quotes
copy %repo%\scripts\Normalise\Quotes\sv.pm %pack%\Normalise\Quotes

mkdir %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\bg.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\de.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\el.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\en.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\es.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\et.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\ga.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\it.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\lt.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\lv.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\mt.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\nl.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\pl.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\ro.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\sl.pm %pack%\Normalise\Spelling
copy %repo%\scripts\Normalise\Spelling\sv.pm %pack%\Normalise\Spelling

mkdir %pack%\work
copy %repo%\work\EMPL-2015-80008-00.doc %pack%\work
copy %repo%\work\MARKT-2014-80060-02-14-HU-TRA-00.DOC %pack%\work
copy %repo%\work\SANCO-2014-80581-00-00-EN-ORC-00.DOCX %pack%\work

rem mkdir %repo%\GlossaryConverter %pack%\GlossaryConverter
xcopy /s /i %repo%\GlossaryConverter %pack%\GlossaryConverter

rem mkdir %repo%\7-Zip %pack%\7-Zip
xcopy /s /i %repo%\7-Zip %pack%\7-Zip

if exist %pack%\..\Perl516 goto skip_perl
xcopy /i /s Perl516_Win  %pack%\..\Perl516
:skip_perl

        
goto end

:terminate
echo usage:
echo deploy_IATEdocgloss.bat target_path
echo more info: tihanyi@ext.ec.europa.eu
:end

