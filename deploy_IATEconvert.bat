:@echo off
if "%1"=="" goto terminate

set repo=.
set pack=%1

                            
mkdir %pack%
copy %repo%\main\cfg-dgtnew.bat %pack%
copy %repo%\main\cfg-dgtold.bat %pack%
copy %repo%\main\dcfg-form.bat %pack%
copy %repo%\main\dcfg-base.bat %pack%
copy %repo%\main\cfg-dgtnew-sk.bat %pack%

copy %repo%\main\iateconvert_sys_cfg.bat %pack%
copy %repo%\main\iate_convert.bat %pack%
copy %repo%\main\README_IATEconvert.pdf %pack%
copy %repo%\main\iate_convert_txt-sdltb_DEMO.bat %pack%
copy %repo%\main\run_call_iate_convert.bat %pack%
copy %repo%\main\call_iate_convert.bat %pack%
copy %repo%\main\stat.bat %pack%
copy %repo%\main\cfg-dgtnew-mtcom.bat %pack%
copy %repo%\main\iate_tsv2lst.bat %pack%
copy %repo%\main\call_iate_tsv2lst.bat %pack%

mkdir %pack%\scripts
copy %repo%\scripts\filters.pl %pack%\scripts
rem IATEconvert:
copy %repo%\scripts\cvsquote.pl %pack%\scripts
copy %repo%\scripts\filtersrclang.pl %pack%\scripts
copy %repo%\scripts\iatepair.pl %pack%\scripts
copy %repo%\scripts\iate_converter.jar %pack%\scripts
copy %repo%\scripts\iate_trgcnt.pl %pack%\scripts
copy %repo%\scripts\iate_xml2sql.pl %pack%\scripts
copy %repo%\scripts\mergetermextra-u8.pl %pack%\scripts
copy %repo%\scripts\myprettyxml.pl %pack%\scripts
copy %repo%\scripts\TBXcoreStructV02.dtd %pack%\scripts
copy %repo%\scripts\tbxfilter.pl %pack%\scripts
copy %repo%\scripts\tbx_filter.xslt %pack%\scripts
copy %repo%\scripts\txt2tbx.pl %pack%\scripts
copy %repo%\scripts\txt2tbx_headword.pl %pack%\scripts
copy %repo%\scripts\txt2xls.vbs %pack%\scripts
copy %repo%\scripts\txtclean.pl %pack%\scripts
copy %repo%\scripts\xls2txt.vbs %pack%\scripts
copy %repo%\scripts\xml2lines.pl %pack%\scripts
copy %repo%\scripts\xmlclean.pl %pack%\scripts
copy %repo%\scripts\ordertermcol.pl %pack%\scripts
copy %repo%\scripts\filterlookup.pl %pack%\scripts
copy %repo%\scripts\lookupform_add.pl %pack%\scripts
copy %repo%\scripts\utf8to16.pl %pack%\scripts
copy %repo%\scripts\utf16to8.pl %pack%\scripts
copy %repo%\scripts\mdblink.accdb %pack%\scripts
copy %repo%\scripts\iate_tsv2lst.pl %pack%\scripts
copy %repo%\scripts\filtpl.pl %pack%\scripts

mkdir %pack%\data
copy %repo%\data\20150611_en.txt %pack%\data
copy %repo%\data\exclude.??.txt %pack%\data
copy %repo%\data\lookup.??.tsv %pack%\data

mkdir %pack%\work
copy %repo%\work\sample1000.txt %pack%\work
rem copy %repo%\work\EMPL-2015-80008-00.doc %pack%\work
rem copy %repo%\work\MARKT-2014-80060-02-14-HU-TRA-00.DOC %pack%\work
rem copy %repo%\work\SANCO-2014-80581-00-00-EN-ORC-00.DOCX %pack%\work

rem mkdir %repo%\GlossaryConverter %pack%\GlossaryConverter
xcopy /s /i %repo%\GlossaryConverter %pack%\GlossaryConverter

rem mkdir %repo%\7-Zip %pack%\7-Zip
xcopy /s /i %repo%\7-Zip %pack%\7-Zip

if exist %pack%\..\Perl516 goto skip_perl
xcopy /i /s Perl516_Win  %pack%\..\Perl516
:skip_perl

goto end

:terminate
echo usage:
echo deploy_IATEconvert.bat target_path
echo more info: tihanyi@ext.ec.europa.eu
:end
