### Document Specific Glossary Generator for IATE 

* Using an IATE extract creates a TBX and/or SDLTB format glossary that contains only those terms which occured in the document.
* The generator uses HFST and Hunspell morphologies for stemming the 24 official languages of the EU.
* Morphological variants of terms added to the termbase to support exact match lookup.
* Glossary generation applies filters referring to Reliability and Evaluation values defined by IATE terminologists.
* The generator use language specific user defined blacklists to reduce and lookupform lists to extend the terms that can be found in the glossary.
* The glossary generator recognise hyphenated terms both with hyphens or spaces. 
* The glossary merges the source and target duplicates which reduces the lenght of Term Recognition list in SDL Trados Studio.
* Contents of the glossaries entry: source term, entry level elements, target side language and term level information.
* If the target side language level info is missing it displays the source side language level (e.g.: Definition).
* The IATE-ID-s in the SDLTB glossaries are links to the IATE web interface and open the page of the terms in the default browser (needs login).


### Supported Platforms

*  Windows, Cygwin and Linux ***

### Settings and Dependencies

* Clone repository: git clone git@bitbucket.org/tihanla/docgloss

* run deploy_IATEdocgloss.bat / deploy_IATEdocgloss.sh to install

* Copy latest IATE extraction into data/iate_extract.txt

* Set the path of location of the project and the perl:
  Linux and cygwin: iatedocgloss_sys_cfg.sh, Windows: iatedocgloss_sys_cfg.bat


### Running

Linux and Cygwin:
IATEdocgloss_DEMO.sh
This script runs IATEdocgloss.sh  wich takes three input parameters:
the name of the document: SANCO-2014-80581-00-00-EN-ORC-00.txt from the ./work folder, the two letter language ID-s of the source and 
target language and generates a standard termbase SANCO-2014-80581-00-00-EN-ORC-00.tbx  in the ./pub folder
Temporary files are generated in ./tm folder


Windows:
IATEdocgloss_DEMO.bat
This script runs IATEdocgloss.bat  wich takes three input parameters:
the name of the document: SANCO-2014-80581-00-00-EN-ORC-00.docx from the ./work folder, the two letter language ID-s of the source
and target language and generates an SDLTB termbase SANCO-2014-80581-00-00-EN-ORC-00.sdltb  in the ./pub folder
it also generated a standard termbase SANCO-2014-80581-00-00-EN-ORC-00.tbx
Temporary files are generated in ./tm folder
The converter contains three additional Windows dependent steps to the Linux/Cygwin versions:
-a Visual Basic script vb2txt.vbs that converts the MS Word .doc and .docx files into plain text in UTF-8 encoding
-Glossary Converter.exe a freeware that converts TBX file to SDLTB
-MSACCESS.exe that adds htpp links to IATE-ID-s


### Repo admin 

Laszlo Tihanyi: tihanyi1123@mail.com
EC DGT, Luxembourg

