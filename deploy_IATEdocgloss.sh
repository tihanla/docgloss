set -x

if [ ! "$#" == 1  ]; then
	echo deploy_IATEdocgloss.bat target_path
	echo more info: tihanyi@ext.ec.europa.eu
	exit
fi

repo=.
pack=$1

mkdir $pack
cp --preserve=mode $repo/main/iatedocgloss_sys_cfg.sh $pack
cp --preserve=mode $repo/main/IATEdocgloss.sh $pack
cp --preserve=mode $repo/main/IATEdocgloss_DEMO.sh $pack
cp --preserve=mode $repo/main/README_IATEdocgloss-unx.txt $pack
cp --preserve=mode $repo/main/cfg-dgtnew.sh $pack
cp --preserve=mode $repo/main/cfg-dgtold.sh $pack
cp --preserve=mode $repo/main/dcfg-form.sh $pack
cp --preserve=mode $repo/main/iateconvert_sys_cfg.sh $pack
cp --preserve=mode $repo/main/iate_convert.sh $pack

cp --preserve=mode $repo/main/tokenizer.perl $pack
cp --preserve=mode $repo/main/array-suffix-driver.pl $pack
cp --preserve=mode $repo/main/Suffix.pm $pack


mkdir $pack/bin
cp --preserve=mode $repo/bin/analyze_dgt $pack/bin
cp --preserve=mode $repo/bin/hfst-proc $pack/bin
cp --preserve=mode $repo/bin/sortx  $pack/bin

mkdir $pack/bin/.libs
cp --preserve=mode $repo/bin/.libs/lt-analyze $pack/bin/.libs

mkdir $pack/lib
cp --preserve=mode $repo/lib/libhunspell-1.3.so.0 $pack/lib
cp --preserve=mode $repo/lib/libhfst.so.37  $pack/lib

mkdir $pack/data
cp --preserve=mode $repo/data/bg.aff $pack/data
cp --preserve=mode $repo/data/bg.ana.hfstol $pack/data
cp --preserve=mode $repo/data/bg.dic $pack/data
cp --preserve=mode $repo/data/cs.aff $pack/data
cp --preserve=mode $repo/data/cs.ana.hfstol $pack/data
cp --preserve=mode $repo/data/cs.dic $pack/data
cp --preserve=mode $repo/data/da.aff $pack/data
cp --preserve=mode $repo/data/da.ana.hfstol $pack/data
cp --preserve=mode $repo/data/da.dic $pack/data
cp --preserve=mode $repo/data/de.aff $pack/data
cp --preserve=mode $repo/data/de.ana.hfstol $pack/data
cp --preserve=mode $repo/data/de.dic $pack/data
cp --preserve=mode $repo/data/el.aff $pack/data
cp --preserve=mode $repo/data/el.ana.hfstol $pack/data
cp --preserve=mode $repo/data/el.dic $pack/data
cp --preserve=mode $repo/data/en.aff $pack/data
cp --preserve=mode $repo/data/en.ana.hfstol $pack/data
cp --preserve=mode $repo/data/en.dic $pack/data
cp --preserve=mode $repo/data/es.aff $pack/data
cp --preserve=mode $repo/data/es.ana.hfstol $pack/data
cp --preserve=mode $repo/data/es.dic $pack/data
cp --preserve=mode $repo/data/et.aff $pack/data
cp --preserve=mode $repo/data/et.ana.hfstol $pack/data
cp --preserve=mode $repo/data/et.dic $pack/data
cp --preserve=mode $repo/data/fi.aff $pack/data
cp --preserve=mode $repo/data/fi.ana.hfstol $pack/data
cp --preserve=mode $repo/data/fi.dic $pack/data
cp --preserve=mode $repo/data/fr.aff $pack/data
cp --preserve=mode $repo/data/fr.ana.hfstol $pack/data
cp --preserve=mode $repo/data/fr.dic $pack/data
cp --preserve=mode $repo/data/ga.aff $pack/data
cp --preserve=mode $repo/data/ga.ana.hfstol $pack/data
cp --preserve=mode $repo/data/ga.dic $pack/data
cp --preserve=mode $repo/data/head.tsv $pack/data
cp --preserve=mode $repo/data/hfst-bg.udic $pack/data
cp --preserve=mode $repo/data/hfst-cs.udic $pack/data
cp --preserve=mode $repo/data/hfst-da.udic $pack/data
cp --preserve=mode $repo/data/hfst-de.udic $pack/data
cp --preserve=mode $repo/data/hfst-el.udic $pack/data
cp --preserve=mode $repo/data/hfst-en.udic $pack/data
cp --preserve=mode $repo/data/hfst-es.udic $pack/data
cp --preserve=mode $repo/data/hfst-et.udic $pack/data
cp --preserve=mode $repo/data/hfst-fi.udic $pack/data
cp --preserve=mode $repo/data/hfst-fr.udic $pack/data
cp --preserve=mode $repo/data/hfst-ga.udic $pack/data
cp --preserve=mode $repo/data/hfst-hr.udic $pack/data
cp --preserve=mode $repo/data/hfst-hu.udic $pack/data
cp --preserve=mode $repo/data/hfst-it.udic $pack/data
cp --preserve=mode $repo/data/hfst-lt.udic $pack/data
cp --preserve=mode $repo/data/hfst-lv.udic $pack/data
cp --preserve=mode $repo/data/hfst-mt.udic $pack/data
cp --preserve=mode $repo/data/hfst-nl.udic $pack/data
cp --preserve=mode $repo/data/hfst-pl.udic $pack/data
cp --preserve=mode $repo/data/hfst-pt.udic $pack/data
cp --preserve=mode $repo/data/hfst-ro.udic $pack/data
cp --preserve=mode $repo/data/hfst-sk.udic $pack/data
cp --preserve=mode $repo/data/hfst-sl.udic $pack/data
cp --preserve=mode $repo/data/hfst-sv.udic $pack/data
cp --preserve=mode $repo/data/hr.aff $pack/data
cp --preserve=mode $repo/data/hr.ana.hfstol $pack/data
cp --preserve=mode $repo/data/hr.dic $pack/data
cp --preserve=mode $repo/data/hu.aff $pack/data
cp --preserve=mode $repo/data/hu.ana.hfstol $pack/data
cp --preserve=mode $repo/data/hu.dic $pack/data
cp --preserve=mode $repo/data/it.aff $pack/data
cp --preserve=mode $repo/data/it.ana.hfstol $pack/data
cp --preserve=mode $repo/data/it.dic $pack/data
cp --preserve=mode $repo/data/lt.aff $pack/data
cp --preserve=mode $repo/data/lt.ana.hfstol $pack/data
cp --preserve=mode $repo/data/lt.dic $pack/data
cp --preserve=mode $repo/data/lv.aff $pack/data
cp --preserve=mode $repo/data/lv.ana.hfstol $pack/data
cp --preserve=mode $repo/data/lv.dic $pack/data
cp --preserve=mode $repo/data/mt.aff $pack/data
cp --preserve=mode $repo/data/mt.ana.hfstol $pack/data
cp --preserve=mode $repo/data/mt.dic $pack/data
cp --preserve=mode $repo/data/nl.aff $pack/data
cp --preserve=mode $repo/data/nl.ana.hfstol $pack/data
cp --preserve=mode $repo/data/nl.dic $pack/data
cp --preserve=mode $repo/data/pl.aff $pack/data
cp --preserve=mode $repo/data/pl.ana.hfstol $pack/data
cp --preserve=mode $repo/data/pl.dic $pack/data
cp --preserve=mode $repo/data/pt.aff $pack/data
cp --preserve=mode $repo/data/pt.ana.hfstol $pack/data
cp --preserve=mode $repo/data/pt.dic $pack/data
cp --preserve=mode $repo/data/ro.aff $pack/data
cp --preserve=mode $repo/data/ro.ana.hfstol $pack/data
cp --preserve=mode $repo/data/ro.dic $pack/data
cp --preserve=mode $repo/data/sk.aff $pack/data
cp --preserve=mode $repo/data/sk.ana.hfstol $pack/data
cp --preserve=mode $repo/data/sk.dic $pack/data
cp --preserve=mode $repo/data/sl.aff $pack/data
cp --preserve=mode $repo/data/sl.ana.hfstol $pack/data
cp --preserve=mode $repo/data/sl.dic $pack/data
cp --preserve=mode $repo/data/sv.aff $pack/data
cp --preserve=mode $repo/data/sv.ana.hfstol $pack/data
cp --preserve=mode $repo/data/sv.dic $pack/data
cp --preserve=mode $repo/data/exclude.??.txt $pack/data
cp --preserve=mode $repo/data/lookup.??.tsv $pack/data

mkdir $pack/scripts
cp --preserve=mode $repo/scripts/adlinesbytab.pl $pack/scripts
cp --preserve=mode $repo/scripts/ansi2to8.pl $pack/scripts
cp --preserve=mode $repo/scripts/cleanstemmed.pl $pack/scripts
cp --preserve=mode $repo/scripts/cntdelim.pl $pack/scripts
cp --preserve=mode $repo/scripts/compare_sar.pl $pack/scripts
cp --preserve=mode $repo/scripts/concatlines.pl $pack/scripts
cp --preserve=mode $repo/scripts/constants.py $pack/scripts
cp --preserve=mode $repo/scripts/constants.pyc $pack/scripts
cp --preserve=mode $repo/scripts/delpunct.pl $pack/scripts
cp --preserve=mode $repo/scripts/deltag.pl $pack/scripts
cp --preserve=mode $repo/scripts/destxt.pl $pack/scripts
cp --preserve=mode $repo/scripts/dic2lookup.pl $pack/scripts
cp --preserve=mode $repo/scripts/dos2unix.pl $pack/scripts
cp --preserve=mode $repo/scripts/filters.pl $pack/scripts
cp --preserve=mode $repo/scripts/filter_1trg.pl $pack/scripts
cp --preserve=mode $repo/scripts/filter_upperterm.pl $pack/scripts
cp --preserve=mode $repo/scripts/fixhfstana.pl $pack/scripts
cp --preserve=mode $repo/scripts/freq2lower.pl $pack/scripts
cp --preserve=mode $repo/scripts/freq2lower_test.pl $pack/scripts
cp --preserve=mode $repo/scripts/freqmaven.pl $pack/scripts
cp --preserve=mode $repo/scripts/freq_cozza.sh $pack/scripts
cp --preserve=mode $repo/scripts/freq_robur.sh $pack/scripts
cp --preserve=mode $repo/scripts/freq_tsih.sh $pack/scripts
cp --preserve=mode $repo/scripts/freq_unuk.sh $pack/scripts
cp --preserve=mode $repo/scripts/frq2normalized.pl $pack/scripts
cp --preserve=mode $repo/scripts/getidlangline.pl $pack/scripts
cp --preserve=mode $repo/scripts/getlangline.pl $pack/scripts
cp --preserve=mode $repo/scripts/gettbxseg.pl $pack/scripts
cp --preserve=mode $repo/scripts/gettermcol.pl $pack/scripts
cp --preserve=mode $repo/scripts/header.pl $pack/scripts
cp --preserve=mode $repo/scripts/hfst-stem.pl $pack/scripts
cp --preserve=mode $repo/scripts/hfst-stem_old.pl $pack/scripts
cp --preserve=mode $repo/scripts/hfstp2txt.pl $pack/scripts
cp --preserve=mode $repo/scripts/hfst_lookup.py $pack/scripts
cp --preserve=mode $repo/scripts/hunspell-stem.pl $pack/scripts
cp --preserve=mode $repo/scripts/hunspell-stem_old.pl $pack/scripts
cp --preserve=mode $repo/scripts/iatexmarkup-ali.pl $pack/scripts
cp --preserve=mode $repo/scripts/iatexmarkup.pl $pack/scripts
cp --preserve=mode $repo/scripts/iatexmarkupfrq.pl $pack/scripts
cp --preserve=mode $repo/scripts/iatexmarkupxxx.pl $pack/scripts
cp --preserve=mode $repo/scripts/linehash.pl $pack/scripts
cp --preserve=mode $repo/scripts/lookupiate.pl $pack/scripts
cp --preserve=mode $repo/scripts/normtokenized.pl $pack/scripts
cp --preserve=mode $repo/scripts/ordertermcol.pl $pack/scripts
cp --preserve=mode $repo/scripts/part.pl $pack/scripts
cp --preserve=mode $repo/scripts/prep4hsp.pl $pack/scripts
cp --preserve=mode $repo/scripts/prep4hunsp.pl $pack/scripts
cp --preserve=mode $repo/scripts/prep4hunspell.pl $pack/scripts
cp --preserve=mode $repo/scripts/restoreline.pl $pack/scripts
cp --preserve=mode $repo/scripts/restorestem.pl $pack/scripts
cp --preserve=mode $repo/scripts/restoretsv-bad.pl $pack/scripts
cp --preserve=mode $repo/scripts/satoken.txt $pack/scripts
cp --preserve=mode $repo/scripts/shared.py $pack/scripts
cp --preserve=mode $repo/scripts/shared.pyc $pack/scripts
cp --preserve=mode $repo/scripts/textsrf2tab.pl $pack/scripts
cp --preserve=mode $repo/scripts/transducer.py $pack/scripts
cp --preserve=mode $repo/scripts/transducer.pyc $pack/scripts
cp --preserve=mode $repo/scripts/tsv2id.pl $pack/scripts
cp --preserve=mode $repo/scripts/utf16to8.pl $pack/scripts
cp --preserve=mode $repo/scripts/utf8to16.pl $pack/scripts
cp --preserve=mode $repo/scripts/word2txt.vbs $pack/scripts
cp --preserve=mode $repo/scripts/filterlookup.pl $pack/scripts
cp --preserve=mode $repo/scripts/lookupform_add.pl $pack/scripts
cp --preserve=mode $repo/scripts/cleanhypcomplex.pl $pack/scripts
cp --preserve=mode $repo/scripts/cleanhypcompsurf.pl $pack/scripts
cp --preserve=mode $repo/scripts/mdblink.accdb $pack/scripts
cp --preserve=mode $repo/scripts/uniq.pl $pack/scripts
cp --preserve=mode $repo/scripts/morfvarq.pl $pack/scripts



#IATEconvert:
cp --preserve=mode $repo/scripts/cvsquote.pl $pack/scripts
cp --preserve=mode $repo/scripts/filtersrclang.pl $pack/scripts
cp --preserve=mode $repo/scripts/iatepair.pl $pack/scripts
cp --preserve=mode $repo/scripts/iate_converter.jar $pack/scripts
cp --preserve=mode $repo/scripts/iate_trgcnt.pl $pack/scripts
cp --preserve=mode $repo/scripts/iate_xml2sql.pl $pack/scripts
cp --preserve=mode $repo/scripts/mergetermextra-u8.pl $pack/scripts
cp --preserve=mode $repo/scripts/myprettyxml.pl $pack/scripts
cp --preserve=mode $repo/scripts/TBXcoreStructV02.dtd $pack/scripts
cp --preserve=mode $repo/scripts/tbxfilter.pl $pack/scripts
cp --preserve=mode $repo/scripts/tbx_filter.xslt $pack/scripts
cp --preserve=mode $repo/scripts/txt2tbx.pl $pack/scripts
cp --preserve=mode $repo/scripts/txt2tbx_headword.pl $pack/scripts
cp --preserve=mode $repo/scripts/txt2xls.vbs $pack/scripts
cp --preserve=mode $repo/scripts/txtclean.pl $pack/scripts
cp --preserve=mode $repo/scripts/xls2txt.vbs $pack/scripts
cp --preserve=mode $repo/scripts/xml2lines.pl $pack/scripts

cp --preserve=mode $repo/scripts/xmlclean.pl $pack/scripts



mkdir $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.bg $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.ca $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.cs $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.da $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.de $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.el $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.en $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.es $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.et $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.fi $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.fr $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.ga $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.hu $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.is $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.it $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.lt $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.lv $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.mt $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.nl $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.pl $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.pt $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.ro $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.ru $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.sk $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.sl $pack/nonbreaking_prefixes
cp --preserve=mode $repo/scripts/nonbreaking_prefixes/nonbreaking_prefix.sv $pack/nonbreaking_prefixes

mkdir $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/Hyphens.pm $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/Noise.pm $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/Numbers.pm $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/OfficialJournal.pm $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/Predefregexp.pm $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/Quotes.pm $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/Spaces.pm $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/Spelling.pm $pack/Normalise
cp --preserve=mode $repo/scripts/Normalise/Units.pm $pack/Normalise

mkdir $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/bg.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/cs.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/da.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/de.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/el.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/en.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/es.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/et.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/fi.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/fr.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/ga.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/hu.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/it.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/lt.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/lv.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/mt.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/nl.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/pl.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/pt.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/ro.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/sk.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/sl.pm $pack/Normalise/Quotes
cp --preserve=mode $repo/scripts/Normalise/Quotes/sv.pm $pack/Normalise/Quotes

mkdir $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/bg.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/de.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/el.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/en.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/es.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/et.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/ga.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/it.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/lt.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/lv.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/mt.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/nl.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/pl.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/ro.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/sl.pm $pack/Normalise/Spelling
cp --preserve=mode $repo/scripts/Normalise/Spelling/sv.pm $pack/Normalise/Spelling

mkdir $pack/work
cp --preserve=mode $repo/work/SANCO-2014-80581-00-00-EN-ORC-00.TXT $pack/work
cp --preserve=mode $repo/work/MARKT-2014-80060-02-14-HU-TRA-00.TXT $pack/work

